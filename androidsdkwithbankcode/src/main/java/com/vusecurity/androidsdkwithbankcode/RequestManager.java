package com.vusecurity.androidsdkwithbankcode;

import android.os.AsyncTask;
import android.text.TextUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;

/**
 * @author <a href="mailto:martiniano@vusecurity.com">Martiniano Aizaga</a>
 */
enum RequestManager {
  INSTANCE;

  // Length of the response, it should be more than enough to read response from server
  private static final int RESPONSE_LENGTH = 500;
  private static final int READ_TIMEOUT = 10000;
  private static final int CONN_TIMEOUT = 15000;

  private SeedCallback seedCallback;
  private TimeDeltaCallback deltaCallback;

  private RequestManager() { }

  public void requestSeed(SeedCallback callback, String serverUrl, String coupon, String bankCode) {
    this.seedCallback = callback;
    new SeedRequestAsyncTask().execute(serverUrl, coupon, bankCode);
  }

  public void requestSeed(SeedCallback callback, String serverUrl, String coupon, String password, String bankCode) {
    this.seedCallback = callback;
    try {
      new SeedRequestAsyncTask().execute(serverUrl, coupon, SecretManager.SHA512Bytes(coupon + password), bankCode);
    } catch (NoSuchAlgorithmException e) {
      seedErrorResponse(VUMobileTokenSDK.ERROR_HASH_INVALIDO, "Error creando Hash de la clave");
    }
  }

  void seedSuccessResponse(String encryptedSeed) {
    seedCallback.onResultsAvailable(encryptedSeed);
  }

  void seedErrorResponse(int code, String error) {
    seedCallback.onError(new ServerException(code, error));
  }

  public void requestTimeDelta(TimeDeltaCallback callback, String serverUrl, String bankCode) {
    this.deltaCallback = callback;
    new TimeDeltaAsyncTask().execute(serverUrl, bankCode);
  }

  void timeDeltaSuccessResponse(long serverMillis, int syncEvery) {
    Long delta = ((serverMillis / 1000)) - ((System.currentTimeMillis() / 1000));
    deltaCallback.onResultsAvailable(delta, syncEvery);
  }

  void timeDeltaErrorResponse(int code, String error) {
    deltaCallback.onError(new ServerException(code, error));
  }

  private class SeedRequestAsyncTask extends AsyncTask<String, Void, String> {

    private int responseCode;

    @Override
    protected String doInBackground(String... strings) {
      String serverUrl;
      String passwordHash = "";
      String coupon;
      String bankCode;
      String urlString;
      if (strings.length == 4) {
        serverUrl = strings[0];
        coupon = strings[1];
        passwordHash = strings[2];
        bankCode = strings[3];
      } else {
        serverUrl = strings[0];
        coupon = strings[1];
        bankCode = strings[2];
      }
      urlString = serverUrl + "/vuserver/a.php?cupon=" + coupon;
      urlString += TextUtils.isEmpty(passwordHash) ? "" : "&check=" + passwordHash;
      urlString += TextUtils.isEmpty(bankCode) ? "" : "&bank=" + bankCode;
      urlString += "&callback=call";
      try {
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(READ_TIMEOUT);
        conn.setConnectTimeout(CONN_TIMEOUT);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // executes request
        conn.connect();
        responseCode = conn.getResponseCode();
        if (responseCode >= 200 && responseCode < 300) {
          return readIt(conn.getInputStream(), RESPONSE_LENGTH);
        } else {
          return "Error getting response";
        }
      } catch (MalformedURLException e) {
        return e.getMessage();
      } catch (IOException e) {
        return e.getMessage();
      }
    }

    @Override
    protected void onPostExecute(String response) {
      super.onPostExecute(response);
      // Parse seed and return response
      // 22345456(["TPfmT6SkpKQaAj53gW4ZiheBUklcKIGc+6TN04\/Qyw6UHDu47K6XJXysu1dRldV47w=="])
      int start = response.indexOf("([\"") + 3;
      int end = response.indexOf("\"])");
      if (start > 0 && start < end) {
        String result = response.substring(start, end);
        seedSuccessResponse(result);
      } else {
        int code;
        String error;
        if (response.contains("URL Incorrecta - contacte Soporte Security")) {
          code = VUMobileTokenSDK.ERROR_CODIGO_ASOCIACION_INCORRECTO;
          error = "Código de Asociación incorrecto";
        } else {
          code = VUMobileTokenSDK.ERROR_CUPON_EXPIRADO;
          error = response;
        }
        seedErrorResponse(code, error);
      }
    }

    private String readIt(InputStream stream, int len) throws IOException {
      Reader reader;
      reader = new InputStreamReader(stream, "UTF-8");
      char[] buffer = new char[len];
      reader.read(buffer);
      return new String(buffer);
    }
  }

  private class TimeDeltaAsyncTask extends AsyncTask<String, Void, String> {

    private int responseCode;

    @Override
    protected String doInBackground(String... strings) {
      String serverUrl = strings[0];
      String bankCode = strings[1];
      String urlString = serverUrl + "/vuserver/a.php?timesyncmanual=do";
      urlString += TextUtils.isEmpty(bankCode) ? "" : "&bank=" + bankCode;
      urlString += "&callback=call";
      try {
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(READ_TIMEOUT);
        conn.setConnectTimeout(CONN_TIMEOUT);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // executes request
        conn.connect();
        responseCode = conn.getResponseCode();
        if (responseCode >= 200 && responseCode < 300) {
          return readIt(conn.getInputStream(), RESPONSE_LENGTH);
        } else {
          return "Error getting response";
        }
      } catch (MalformedURLException e) {
        return e.getMessage();
      } catch (IOException e) {
        return e.getMessage();
      }
    }

    @Override
    protected void onPostExecute(String response) {
      super.onPostExecute(response);
      //call([1412649485989,5])
      int start = response.indexOf("([") + 2;
      int end = response.indexOf("])");
      if (start > 0 && start < end) {
        String[] result = response.substring(start, end).split(",");
        try {
          Long delta = Long.parseLong(result[0]);
          Integer syncEvery = Integer.parseInt(result[1]);
          timeDeltaSuccessResponse(delta, syncEvery);
        } catch (NumberFormatException e) {
          timeDeltaErrorResponse(VUMobileTokenSDK.ERROR_TIME_SYNC, response);
        }
      } else {
        timeDeltaErrorResponse(VUMobileTokenSDK.ERROR_TIME_SYNC, response);
      }
    }

    private String readIt(InputStream stream, int len) throws IOException {
      Reader reader;
      reader = new InputStreamReader(stream, "UTF-8");
      char[] buffer = new char[len];
      reader.read(buffer);
      return new String(buffer);
    }
  }
}
