package com.vusecurity.androidsdkwithbankcode;

import android.text.TextUtils;
import android.util.Log;

import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.util.encoders.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author <a href="mailto:martiniano@vusecurity.com">Martiniano Aizaga</a>
 */
class SecretManager {

    private static final int[] DIGITS_POWER = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000};
    public static final long VU_DURATION = 40;
    private static final int MIN_DIGITS = 6;
    private static final int MAX_DIGITS = 10;

    public static String decryptSeed(String encryptedSeed, String passcode) {
        if (TextUtils.isEmpty(passcode)) {
            return null;
        }
        String decryptedText = decrypt(encryptedSeed, passcode);
        if (!decryptedText.matches("[A-Za-z0-9\\s]+")) {
            return null;
        }
        // TOTP
        String crcText = decryptedText.split(" ")[0];
        String decryptedSeed = decryptedText.split(" ")[1];
        if (crc32(crcText, decryptedSeed)) {
            return decryptedSeed;
        }

        return null;
    }

    public static String encryptSeed(String decryptedSeed, String newPasscode) {
        String value;
        if (TextUtils.isEmpty(decryptedSeed) || TextUtils.isEmpty(newPasscode)) {
            return null;
        }
        String newCrc32 = createCrc32(decryptedSeed);
        String valueToEncrypt = newCrc32 + " " + decryptedSeed;
        value = encrypt(valueToEncrypt, newPasscode);
        return value;
    }

    public static String generateOTP(byte[] secret, long delta) {
        long now = getTimeNow(delta);
        long counter = getValueAtTime(now);
        return generate(secret, counter, 6);
    }

    public static long getValueAtTime(double time) {
        return (long)(time / VU_DURATION);
    }

    public static long getTimeNow(long delta) {
        return System.currentTimeMillis() / 1000 + delta;
    }

    public static long getValueStartTime(long value) {
        return value * VU_DURATION;
    }

    public static long getTimeTillNextCounterValue(long delta) {
        long now = getTimeNow(delta);
        long current = getValueAtTime(now);
        long next = current + 1;
        return getValueStartTime(next) - now;
    }

    private static String generate( byte[] secret, long counter, int digits ) {
        // StringBuffer result = new StringBuffer();
        if ( MIN_DIGITS < digits || digits > MAX_DIGITS ) {
            throw new IllegalArgumentException( "Number of digits not within range: "
                    + MIN_DIGITS + " < digits > " + MAX_DIGITS );
        }

        if ( secret == null || secret.length == 0 ) {
            throw new IllegalArgumentException( "Shared secret shouldn't be null or empty" );
        }

        byte[] hash = stepOne(secret, counter);

        // put selected bytes into result int
        int offset = hash[hash.length - 1] & 0xf;
        int binary =	((hash[offset] & 0x7f) << 24)
                | 	((hash[offset + 1] & 0xff) << 16)
                | 	((hash[offset + 2] & 0xff) << 8)
                | 	(hash[offset + 3] & 0xff);

        int otp = binary % DIGITS_POWER[digits];

        String result = Integer.toString(otp);
        while (result.length() < digits) {
            result = "0" + result;
        }
        return result;
    }

    public static byte[] stepOne( byte[] secretKey, long counter ) {
        HMac mac = new HMac(new SHA1Digest());
        byte[] value = new byte[mac.getMacSize()];
        CipherParameters params = new KeyParameter( secretKey );
        mac.init(params);
        mac.update(getCounterBytes( counter ), 0, 8);
        mac.doFinal(value, 0);
        return value;
    }

    public static byte[] getCounterBytes( long counter ) {
        byte[] counterBytes = new byte[8];
        counterBytes[7] = ( byte ) counter;
        counterBytes[6] = ( byte ) ( counter >> 8 );
        counterBytes[5] = ( byte ) ( counter >> 16 );
        counterBytes[4] = ( byte ) ( counter >> 24 );
        counterBytes[3] = ( byte ) ( counter >> 32 );
        counterBytes[2] = ( byte ) ( counter >> 40 );
        counterBytes[1] = ( byte ) ( counter >> 48 );
        counterBytes[0] = ( byte ) ( counter >> 56 );
        return counterBytes;
    }

    public static boolean crc32(String crcText, String decryptedSeed) {
        // get bytes from string
        byte bytes[] = decryptedSeed.getBytes();
        Checksum checksum = new CRC32();
        // update the current checksum with the specified array of bytes
        checksum.update(bytes, 0, bytes.length);
        // get the current checksum value
        long checksumValue = checksum.getValue();
        long crcCheckValue = Long.parseLong(crcText);
        return checksumValue == crcCheckValue;
    }

    public static String createCrc32(String newDecryptedSeed) {
        byte bytes[] = newDecryptedSeed.getBytes();
        Checksum checksum = new CRC32();
        checksum.update(bytes, 0, bytes.length);
        Long value = checksum.getValue();
        return value.toString();
    }

    public static String decrypt(String cipherText, String passcode) {
        String decryptedString = "";
        try {
            decryptedString = new BouncyCastleAESCounterMode().decrypt(cipherText, passcode, 256);
        } catch (Throwable e) {
            Log.e(SecretManager.class.getName(), "Failed to decrypt seed");
        }

        return decryptedString;
    }

    public static String encrypt(String textToEncrypt, String passcode) {
        String encryptedString = "";
        try {
            encryptedString = new BouncyCastleAESCounterMode().encrypt(textToEncrypt, passcode, 256);
        } catch (Throwable e) {
            Log.e(SecretManager.class.getName(), "Failed to encrypt seed");
        }
        return encryptedString;
    }

    /*
     * This decrypt method, is used when the seed is encrypted by the encryption method of the server. It was chosen
     * after several tests and found to be the one that works. It uses SICBlockCipher which is supposed to be
     * the real AES/CTR/NoPadding server side method encryption.
     */
    private static class BouncyCastleAESCounterMode {
        private String decrypt(String cipherText, String key, int nBits) throws Exception {
            if (!(nBits == 128 || nBits == 192 || nBits == 256)) return "Error: Must be a key mode of either 128, 192, 256 bits";
            if (cipherText == null || key == null) return "Error: cipher and/or key equals null";
            byte[] decrypted;
            nBits = nBits / 8;
            byte[] data = android.util.Base64.decode(cipherText, android.util.Base64.DEFAULT);
            /* CHECK: we should always use getBytes("UTF-8") or with wanted charset, never system charset! */
            byte[] k = Arrays.copyOf(key.getBytes(), nBits);
            /* AES/CTR/NoPadding (SIC == CTR) */
            BufferedBlockCipher cipher = new BufferedBlockCipher(new org.bouncycastle.crypto.modes.SICBlockCipher(new org.bouncycastle.crypto.engines.AESEngine()));
            cipher.reset();
            SecretKey secretKey = generateSecretKey(k, nBits);
            byte[] nonceBytes = Arrays.copyOf(Arrays.copyOf(data, 8), nBits / 2);
            IvParameterSpec nonce = new IvParameterSpec(nonceBytes);
            /* true == encrypt; false == decrypt */
            cipher.init(true, new org.bouncycastle.crypto.params.ParametersWithIV(new org.bouncycastle.crypto.params.KeyParameter(secretKey.getEncoded()), nonce.getIV()));
            decrypted = new byte[cipher.getOutputSize(data.length - 8)];
            int decLength = cipher.processBytes(data, 8, data.length - 8, decrypted, 0);
            cipher.doFinal(decrypted, decLength);
            /* CHECK: we should always use new String (bytes,charset) to avoid issues with system charset and utf-8 */
            return new String(decrypted);
        }

        private String encrypt(String text, String key, int nBits) throws Exception {
            if (!(nBits == 128 || nBits == 192 || nBits == 256)) return "Error: Must be a key mode of either 128, 192, 256 bits";
            if (text == null || key == null) return "Error: cipher and/or key equals null";
            byte[] encrypted;
            nBits = nBits / 8;
            byte[] data = text.getBytes();
            byte[] k = Arrays.copyOf(key.getBytes(), nBits);
            BufferedBlockCipher cipher = new BufferedBlockCipher(new org.bouncycastle.crypto.modes.SICBlockCipher(new org.bouncycastle.crypto.engines.AESEngine()));
            cipher.reset();
            SecretKey secretKey = generateSecretKey(k, nBits);
            IvParameterSpec ivSpec = generateIv(nBits);
            cipher.init(true, new org.bouncycastle.crypto.params.ParametersWithIV(new org.bouncycastle.crypto.params.KeyParameter(secretKey.getEncoded()), ivSpec.getIV()));
            encrypted = new byte[cipher.getOutputSize(data.length)];
            int decLength = cipher.processBytes(data, 0, data.length, encrypted, 0);
            cipher.doFinal(encrypted, decLength);
            byte[] ivToEncrypt = Arrays.copyOf(ivSpec.getIV(), 8);
            byte[] encryptedWithIv = new byte[ivToEncrypt.length + encrypted.length];
            System.arraycopy(ivToEncrypt, 0, encryptedWithIv, 0, ivToEncrypt.length);
            System.arraycopy(encrypted, 0, encryptedWithIv, ivToEncrypt.length, encrypted.length);

            return android.util.Base64.encodeToString(encryptedWithIv, android.util.Base64.DEFAULT);
        }

        private SecretKey generateSecretKey(byte[] keyBytes, int nBits) throws Exception {
            try {
                SecretKey secretKey = new SecretKeySpec(keyBytes, "AES");
                /* AES/ECB/NoPadding */
                BufferedBlockCipher cipher = new BufferedBlockCipher(new org.bouncycastle.crypto.engines.AESEngine());
                cipher.init(true, new org.bouncycastle.crypto.params.KeyParameter(secretKey.getEncoded()));
                keyBytes = new byte[cipher.getOutputSize(secretKey.getEncoded().length)];
                int decLength = cipher.processBytes(secretKey.getEncoded(), 0, secretKey.getEncoded().length, keyBytes, 0);
                cipher.doFinal(keyBytes, decLength);
            } catch (Throwable e) {
                return null;
            }
            System.arraycopy(keyBytes, 0, keyBytes, nBits / 2, nBits / 2);
            return new SecretKeySpec(keyBytes, "AES");
        }

        private IvParameterSpec generateIv(int nBits) {
            byte [] randomBytes = new byte[nBits / 2];
            SecureRandom random = new SecureRandom();
            random.nextBytes(randomBytes);
            byte[] ivBytes = Arrays.copyOf(Arrays.copyOf(randomBytes, 8), nBits / 2);
            return new IvParameterSpec(ivBytes);
        }
    }

    public static String SHA512Bytes(String text) throws NoSuchAlgorithmException {
        MessageDigest digester = MessageDigest.getInstance("SHA-512");
        digester.reset();
        digester.update(text.getBytes());

        return Hex.toHexString(digester.digest());
    }
}
