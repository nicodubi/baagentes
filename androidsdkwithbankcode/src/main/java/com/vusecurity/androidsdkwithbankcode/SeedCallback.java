package com.vusecurity.androidsdkwithbankcode;


/**
 * Result callback usado para comunicarse con el server y el SDK, para obtener la semilla
 *
 * @author <a href="mailto:martiniano@vusecurity.com">Martiniano Aizaga</a>
 */
public interface SeedCallback {
    /**
     * Respuesta satisfactoria del servidor
     * @param result si el request es exitoso se obtiene la semilla
     */
    public void onResultsAvailable(String result);

    /**
     * Respuesta con error del servidor
     * @param e la excepcion producida
     */
    public void onError(Throwable e);
}