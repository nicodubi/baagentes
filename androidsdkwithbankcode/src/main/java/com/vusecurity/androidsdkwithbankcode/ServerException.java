package com.vusecurity.androidsdkwithbankcode;

/**
 * Created by maizaga on 11/3/15.
 */
public class ServerException extends Exception {

    int code;

    public ServerException(int code, String detailMessage) {
        super(detailMessage);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
