package com.vusecurity.androidsdkwithbankcode;


/**
 * Result callback usado para comunicarse con el server y el SDK, para obtener el delta y syncEvery
 *
 * @author <a href="mailto:martiniano@vusecurity.com">Martiniano Aizaga</a>
 */
public interface TimeDeltaCallback {
    /**
     * Respuesta satisfactoria del servidor
     * @param delta diferencia entre la hora del servidor y el device en segundos (delta)
     * @param syncEvery Numero de inicios de la app a partir de los cuales la aplicacion realiza automaticamente una sincronizacion (syncEvery).
     */
    public void onResultsAvailable(long delta, int syncEvery);

    /**
     * Respuesta con error del servidor
     * @param e la excepcion producida
     */
    public void onError(Throwable e);
}