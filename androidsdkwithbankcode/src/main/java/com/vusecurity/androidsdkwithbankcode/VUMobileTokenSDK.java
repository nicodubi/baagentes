package com.vusecurity.androidsdkwithbankcode;

/**
 * Clase principal de acceso al SDK
 * @author <a href="mailto:martiniano@vusecurity.com">Martiniano Aizaga</a>
 */
public class VUMobileTokenSDK {

  public static int ERROR_CODIGO_ASOCIACION_INCORRECTO = 101;
  public static int ERROR_CUPON_EXPIRADO = 102;
  public static int ERROR_TIME_SYNC = 103;
  public static int ERROR_HASH_INVALIDO = 104;

  private VUMobileTokenSDK() { }

  /**
   Obtiene el secreto compartido entre el Mobile Token y el servidor cifrado en AES de 256
   @param serverUrl la URL del servidor donde se obtiene la semilla
   @param coupon codigo que facilita el usuario para asociar su cuenta
   @param bankCode codigo de banco. Si es vacío ("" o null) será Comafi Individuos
   @param callback para obtener asincronicamente el resultado del request al server de la semilla encryptada
   */
  public static void getEncryptedSeed(String serverUrl, String coupon, String bankCode, SeedCallback callback) {
    RequestManager.INSTANCE.requestSeed(callback, serverUrl, coupon, bankCode);
  }

  /**
   Obtiene el secreto compartido entre el Mobile Token y el servidor cifrado en AES de 256
   @param serverUrl la URL del servidor donde se obtiene la semilla
   @param coupon codigo que facilita el usuario para asociar su cuenta
   @param password password a validar antes de obtener la semilla
   @param bankCode codigo de banco. Si es vacío ("" o null) será Comafi Individuos
   @param callback para obtener asincronicamente el resultado del request al server de la semilla encryptada, si la password es correcta
   */
  public static void getEncryptedSeed(String serverUrl, String coupon, String password, String bankCode, SeedCallback callback) {
    RequestManager.INSTANCE.requestSeed(callback, serverUrl, coupon, password, bankCode);
  }

  /**
   Esta funcion obtiene la hora del servidor, y entrega la diferencia entre la hora del dispositivo y el servidor (delta), tambien entrega cada cuantos inicios de la aplicacion se debe realizar una sincronizacion en background (syncEvery).
   @param serverUrl la URL del servidor donde se obtiene la semilla
   @param manual true si la sincronizacion es manual, false si automatica
   @param bankCode codigo de banco. Si es vacío ("" o null) será Comafi Individuos
   @param callback para obtener asincronicamente la diferencia entre la hora del servidor y el device en minutos (delta)
   */
  public static void getTimeDelta(String serverUrl, boolean manual, String bankCode, TimeDeltaCallback callback) {
    RequestManager.INSTANCE.requestTimeDelta(callback, serverUrl, bankCode);
  }

  /**
   Descifra el secreto compartido entre el Mobile Token y el Servidor
   @param encryptedSeed semilla encriptada obtenida previamente desde el servidor.
   @param passcode clave generada por el usuario
   @return semilla desencriptada
   */
  public static String decryptedSeed(String encryptedSeed, String passcode) {
    return SecretManager.decryptSeed(encryptedSeed, passcode);
  }

  /**
   Genera el codigo de 6 digitos (OTP) para mostrar al usuario, a partir de la semilla y el delta de diferencia de tiempo entre el servidor y el device.
   @param decryptedSeed semilla desencriptada previamente
   @param delta diferencia entre la hora del servidor y el device en minutos.
   @return OTP de 6 digitos. Nil si no se pudo generar el codigo
   */
  public static String genTotp(String decryptedSeed, long delta) {
    return SecretManager.generateOTP(decryptedSeed.getBytes(), delta);
  }

  /**
   Se usa principalmente para mostrar al usuario de alguna forma (progress bar por ejemplo) cuanto tiempo le queda de vida util al OTP actual.
   @param delta diferencia entre la hora del servidor y el device en minutos.
   @return segundos restantes para que el OTP actual termine su vida util
   */
  public static long getTotpTtl(long delta) {
    return SecretManager.getTimeTillNextCounterValue(delta);
  }

  /**
   Cifra la semilla compartido entre el Mobile Token y el Servidor con una nueva password.
   @param decryptedSeed semilla descifrada previamente con la password original.
   @param newPasscode nueva password para encriptar la semilla.
   @return Entrega el secreto listo para almacenarse (ecryptedSeed).
   */
  public static String encryptSeed(String decryptedSeed, String newPasscode) {
    return SecretManager.encryptSeed(decryptedSeed, newPasscode);
  }
}
