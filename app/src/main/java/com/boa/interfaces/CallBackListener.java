package com.boa.interfaces;

/**
 * Callback para salir de task
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 10/12/2016.
 */
public interface CallBackListener{
	void invoke();
}
