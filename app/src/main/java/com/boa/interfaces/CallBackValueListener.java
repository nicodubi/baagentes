package com.boa.interfaces;

/**
 * Callback para salir de task
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 10/03/2018.
 */
public interface CallBackValueListener{
	void invoke(String result);
}