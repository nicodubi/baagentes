package com.boa.multilistfilter;

public interface MultiSpinnerListener{
	void onItemsSelected(boolean[] selected);
}