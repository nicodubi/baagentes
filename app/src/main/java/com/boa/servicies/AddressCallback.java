package com.boa.servicies;

import android.location.Address;

/**
 * Calback para utilización de geocoder en la pantalla del mapa
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 22/12/2016.
 */
public interface AddressCallback{
	public void onSuccess(Address addr);
	public void onError();
}