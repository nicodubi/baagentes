package com.boa.servicies;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;

import com.boa.utils.Utils;
import com.kwan.baagentes.App;

/**
 * Servicio para re-localizar la app según movimiento y tiempo
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 15/04/2017.
 */
public class AppLocationService extends Service implements LocationListener{
	protected LocationManager locationManager;
	public Location location;
	public static final long MIN_DISTANCE_FOR_UPDATE = 10000;
	private static final long MIN_TIME_FOR_UPDATE = 10000 * 60 * 2;
	
	public AppLocationService(Context context){
		locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
	}
	
	public Location getLocation(String provider){
		try{
			if(locationManager.isProviderEnabled(provider)){
				if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
						ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
					locationManager.requestLocationUpdates(provider, MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, this);
					
					if(locationManager != null){
						location = locationManager.getLastKnownLocation(provider);
						return location;
					}
				}
			}
		}catch(Exception e){
			Utils.logError(App.getContext(), "AppLocationService - Exception", e);
		}
		
		return null;
	}
	
	@Override
	public void onLocationChanged(Location location){
	}
	
	@Override
	public void onStatusChanged(final String provider, final int status, final Bundle extras){
	}
	
	@Override
	public void onProviderDisabled(String provider){
	}
	
	@Override
	public void onProviderEnabled(String provider){
	}
	
	@Nullable
	@Override
	public IBinder onBind(final Intent intent){
		return null;
	}
}