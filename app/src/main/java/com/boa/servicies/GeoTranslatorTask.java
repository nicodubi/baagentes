package com.boa.servicies;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import com.boa.utils.Common;
import com.boa.utils.Utils;
import com.google.android.gms.maps.model.LatLng;
import com.kwan.baagentes.App;
import java.util.List;

/**
 * Traductor de direcciones a coordenadas y viceversa
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 22/12/2016.
 */
public class GeoTranslatorTask extends AsyncTask<Void, Void, Address>{
	private final AddressCallback	callback;
	private LatLng					latLng;
	private Activity				activity;
	private String					street;
	
	public GeoTranslatorTask(final LatLng latLng, final Activity activity, final String street, final AddressCallback callback){
		this.callback	= callback;
		this.latLng		= latLng;
		this.activity	= activity;
		this.street		= street;
	}
	
	@Override
	protected Address doInBackground(Void... params){
		try{
			if(NetworkStateReceiver.isConnected(App.getContext())){
				Geocoder geoCoder	= new Geocoder(activity);
				List<Address> matches;
				Address bestMatch;
				
				if(!Utils.isEmpty(street)){
					matches		= geoCoder.getFromLocationName(street+", Buenos Aires", 1);
					bestMatch	= (matches.isEmpty() ? null : matches.get(0));
				}else{
					matches		= geoCoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
					bestMatch	= (matches.isEmpty() ? null : matches.get(0));
				}
				
				if(Common.DEBUG){
					System.out.println("Street: "+street);
					System.out.println("Matches: "+matches.toString());
				}
				
				if(bestMatch != null){
					if(Common.DEBUG){
						System.out.println("Best Match: "+bestMatch.toString());
						System.out.println("Lat: "+bestMatch.getLatitude()+" Lon: "+bestMatch.getLongitude());
					}
					
					return bestMatch;
				}
			}
		}catch(Exception e){
			Utils.logError(activity, "GeoTranslatorTask:doInBackground - Exception: ", e);
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(final Address result){
		super.onPostExecute(result);
		
		if(result == null){
			activity.runOnUiThread(new Runnable(){
				@Override
				public void run(){
					callback.onSuccess(result);
				}
			});
			return;
		}
		activity.runOnUiThread(new Runnable(){
			@Override
			public void run(){
				callback.onSuccess(result);
			}
		});
	}
}