package com.boa.servicies;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import com.boa.utils.Common;
import com.boa.utils.SendFormTaskLocker;
import com.kwan.baagentes.services.SendFormTask;

import io.realm.Realm;

/**
 * Receiver de cambio en la conexión para intentar reportar a api
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 23/12/2016.
 */
public class NetworkStateReceiver extends BroadcastReceiver{
	private static boolean proccess			= false;
	private static boolean firstConnection	= false;
	
	@Override
	@SuppressWarnings("deprecation")
	public void onReceive(final Context context, final Intent intent){
		if(Common.DEBUG){
			System.out.println("onReceive: -proccess: "+proccess+" -firstConnection:"+firstConnection);
		}
		
		intent.getAction();
		
		if(proccess || firstConnection && !isConnected(context)){
			return;
		}

		if(isConnected(context) && !thereIsASendingFormTaskRunning()){
			proccess		= true;
			firstConnection	= true;
			Realm.init(context);
			new SendFormTask(context, 0, "", null).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			proccess		= false;
			firstConnection	= false;
		}
	}

	private boolean thereIsASendingFormTaskRunning() {
		return SendFormTaskLocker.getInstance().isSendFormTaskRunning();
	}

	public static boolean isConnected(Context context){
		ConnectivityManager cm		= (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		if(cm != null){
			NetworkInfo activeNetwork	= cm.getActiveNetworkInfo();
			
			if(activeNetwork != null){
				if(Common.DEBUG){
					System.out.println(	"isConnected: "+activeNetwork.isConnected()+ " isFailover: "+activeNetwork.isFailover()+
										" isConnectedOrConnecting: "+activeNetwork.isConnectedOrConnecting()+ " isRoaming: "+activeNetwork.isRoaming());
				}
				
				return activeNetwork.isConnected();
			}else{
				if(Common.DEBUG){
					System.out.println("No hay red activa!");
				}
				
				return false;
			}
		}
		
		return false;
	}
}