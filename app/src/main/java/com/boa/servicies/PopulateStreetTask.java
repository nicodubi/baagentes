package com.boa.servicies;

import android.app.Activity;
import android.os.AsyncTask;
import com.boa.utils.Utils;
import com.kwan.baagentes.data.Street;
import com.kwan.baagentes.services.PopulateFormTask;
import java.io.InputStream;
import io.realm.Realm;

/**
 * Importador de calles mediante archivo json ubicado en assets
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 25/03/2017.
 */
public class PopulateStreetTask extends AsyncTask<Void, Void, String>
{
	private Activity activity;
	
	public PopulateStreetTask(final Activity activity)
	{
		this.activity = activity;
	}
	
	@Override
	protected String doInBackground(final Void... voids)
	{
		try
		{
			Realm.init(activity);
			Realm realm = Realm.getDefaultInstance();
			
			if(realm.where(Street.class).count() == 0)
			{
				realm.executeTransaction(new Realm.Transaction()
				{
					@Override
					public void execute(Realm realm)
					{
						try
						{
							InputStream is = activity.getAssets().open("usig.json");
							realm.createAllFromJson(Street.class, is);
						}
						catch(Exception e)
						{
							Utils.logError(activity, "PopulateStreetTask:doInBackground:execute - Exception: ", e);
						}
					}
				});
			}
			
			realm.close();
		}
		catch(Exception e)
		{
			Utils.logError(activity, "PopulateStreetTask:doInBackground - Exception: ", e);
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(final String result)
	{
		try
		{
			new PopulateFormTask(activity).executeOnExecutor(SERIAL_EXECUTOR);
		}
		catch(Exception e)
		{
			Utils.logError(activity, "PopulateStreetTask:onPostExecute - Exception: ", e);
		}
	}
}