package com.boa.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import com.boa.servicies.NetworkStateReceiver;
import com.kwan.baagentes.App;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.AlgorithmParameters;
import java.security.MessageDigest;
import java.security.spec.KeySpec;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import io.fabric.sdk.android.services.network.HttpRequest;

/**
 * Utilitario para conexión con APIs REST
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 22/03/2017.
 */
public abstract class Api {
    private static final String SERVERNAME = "aHR0cDovL3d3dy5pbmZyYWNjaW9uZXMuY29tLmFy";
    private static final String SERVERNAMETEST = "aHR0cDovL3NreW5ldC5oYWw5MDAwLmNvbS5hcjo4MDgw";
    //SERVERNAMEGCBA ADDRESS IS FOR POINTING TO TEST ENVIRONMENT!
    private static final String SERVERNAMEGCBA = "aHR0cHM6Ly90cmFuc2l0by5idWVub3NhaXJlcy5nb2IuYXI=";
    private static final String SERVERIP = "aHR0cDovLzM0LjIwNS4xNjUuMTc=";//Backoffice : server/infraccionesWeb/web/login
    //SERVERPOLICE ADDRESS IS FOR POINTING TO PRODUCTION ENVIRONMENT!
    private static final String SERVERPOLICE = "aHR0cDovLzEwLjcwLjIxMC4xNjU6ODA4MA==";
    private static final String SERVERPOLICE2 = "https://www.infracciones.com.ar";
    //TEST
    //private static final String SERVER = decrypt(SERVERNAMEGCBA) + getFolder() + "/api/";
    //PROD VIEJA
    private static final String SERVER =  decrypt(SERVERPOLICE) + getFolder() + "/api/";
    //PROD NUEVA
    //private static final String SERVER =  SERVERPOLICE2 + getFolder() + "/api/";
    public static final String SERVERVU = decrypt(SERVERNAMETEST) + "/vu";//Prod
    public static final String BRANDS = SERVER + "marcas";
    public static final String COMPANIES = SERVER + "lineasEmpresaColectivo";
    public static final String COUNTRIES = SERVER + "paises";
    public static final String DEPARTMENTS = SERVER + "playas";
    public static final String INFRINGEMENT_REPORT = SERVER + "reportarInfraccion2";
    public static final String LOGIN = SERVER + "login";
    public static final String LOGOUT = SERVER + "logout";
    public static final String MODELS = SERVER + "modelos";
    public static final String MUNICIPALITIES = SERVER + "centrosEmision";
    public static final String PROVINCE = SERVER + "provincias";
    public static final String TICKETS = SERVER + "actasDisponibles";
    public static final String TYPE_DOCUMENT = SERVER + "tiposDocumento";
    public static final String TYPE_INFRINGEMENT = SERVER + "tipoInfracciones";
    public static final String TYPE_VEHICLE = SERVER + "tiposVehiculo";
    public static final String INFRINGEMENT_GCBA = decrypt(SERVERNAMEGCBA) + getFolder() + "/api/reportarInfraccion2";
    public static final String METHOD_GET = "GET";
    public static final String METHOD_POST = "POST";

    private static String getFolder() {
        return "/infraccionesApi";
    }

    private static String decrypt(String pass) {
        String result = "";

        try {
            //byte[] dataEn = pass.getBytes("UTF-8");
            String base64 = "";//Base64.encodeToString(dataEn, Base64.DEFAULT);
            //Para probar usar lo de comentado
            byte[] data = Base64.decode(pass, Base64.DEFAULT);
            result = new String(data, "UTF-8");

            if (Common.DEBUG) {
                System.out.println("Original: " + pass);
                System.out.println("Encrypt: " + base64);
                System.out.println("Decrypt: " + result);
            }
        } catch (Exception e) {
            Utils.logError(App.getContext(), "Api:decrypt - Exception: ", e);
        }

        return result;
    }

    /**
     * Realiza una conexión con una API REST bajo JSON
     *
     * @param link    url de recurso al que se conectará
     * @param method  Método de comunicación
     * @param params  Valores a enviar
     * @param token   Token de autorización en caso de ser necesario
     * @param context Contexto de la app
     * @return String
     */
    public static String request(String link, final String method, final String params, final String token, final Context context, final String authType) {
        String json = "{}";
        String message = "Sin internet";
        int code = 0;

        try {
            link = link.replace("https://https://", "https://");
            link = link.replace("http://http://", "http://");
            link = link.replace("https://http://", "https://");
            link = link.replace("http://https://", "http://");
            String lang = "", version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName, imei = Utils.getImei(context);

            if (Locale.getDefault() != null) {
                if (Locale.getDefault().getLanguage() != null) {
                    if (Locale.getDefault().getCountry() != null) {
                        lang = Locale.getDefault().getLanguage() + "-" + Locale.getDefault()
                                .getCountry();
                    } else {
                        lang = Locale.getDefault().getLanguage();
                    }
                }
            }

            if (!Utils.isEmpty(lang)) {
                lang = "es-AR";
            }

            if (Common.DEBUG) {
                Log.d("API request:",method + " " + link + " Authorization: " + authType + token + " Lang: " + lang + " APP_VERSION: " + version + " IMEI: " + imei + " Params: " + params);
            }

            if (!Utils.isLink(link)) {
                return json;
            }

            URL url = new URL(link);
            URLConnection connection = url.openConnection();
            InputStream stream = null;
            Map<String, List<String>> headers = null;
            HttpURLConnection httpConnection = (HttpURLConnection) connection;
            httpConnection.setRequestMethod(method);
            httpConnection.setRequestProperty("Accept", "application/json");
            httpConnection.setRequestProperty("Content-type", "application/json");
            httpConnection.setRequestProperty("Accept-Language", lang);
            //Cabeceras personalizadas para api
            httpConnection.setRequestProperty("APP_VERSION", version);
            httpConnection.setRequestProperty("IMEI", imei);
            httpConnection.setUseCaches(false);

            //Si es POST agregamos la data
            if (method.equals("POST")) {
                if (!Utils.isEmpty(token)) {
                    httpConnection.setRequestProperty("Authorization", authType.replace("\n", "").replace("0x0a", "") +
                            token.replace("\n", "").replace("0x0a", ""));
                }

                httpConnection.setDoOutput(true);

                try {
                    if (!Utils.isEmpty(params) && NetworkStateReceiver.isConnected(context)) {
                        byte[] outputInBytes = params.getBytes("UTF-8");
                        OutputStream os = httpConnection.getOutputStream();
                        os.write(outputInBytes);
                        os.close();
                    }
                } catch (Exception e) {
                    Utils.logError(context, "Api:request:getOutputStream - Exception: ", e);
                }
            }

            if (NetworkStateReceiver.isConnected(context)) {
                try {
                    httpConnection.connect();
                    code = httpConnection.getResponseCode();
                    headers = httpConnection.getHeaderFields();
                    message = httpConnection.getResponseMessage();
                } catch (Exception e) {
                    Utils.logError(context, "Api:request:getResponseCode - Exception: ", e);
                }
            }

            if (Common.DEBUG) {
                System.out.println("Headers: " + headers);
                System.out.println("Response Code: " + code);
                System.out.println("Response Message: " + message);
            }

            if (code == HttpURLConnection.HTTP_OK || code == HttpURLConnection.HTTP_CREATED || code == HttpURLConnection.HTTP_ACCEPTED) {
                stream = httpConnection.getInputStream();
            } else {
                if (code < HttpURLConnection.HTTP_NO_CONTENT) {
                    stream = httpConnection.getErrorStream();
                }
            }

            if (stream != null) {
                json = convertInputStreamToString(stream, context);
            }

            if (Common.DEBUG && !link.equals(MUNICIPALITIES) && !link.equals(BRANDS)) {
                System.out.println("Response Indicted: " + json);
            }

            json = json.trim().replace("desripcion", Common.KEY_DESCRIPTION).replace("  ", " ").replace("\n", "").replace("\t", " ");

            if (json.startsWith("\"") && json.endsWith("\"") && json.length() > 1) {
                json = json.substring(1, json.length()-1);
            }

            if (!(json.startsWith("{") && json.endsWith("}")) && !(json.startsWith("[") && json.endsWith("]")) || Utils.isEmpty(json)) {
                json = "{}";
            }

            json = json.replaceAll("\\\\", "");

            //TODO verificar si hay que hacer tratamiento especial al response como en colectivos
        } catch (Exception e) {
            Utils.logError(context, "Api:request - Exception: ", e);
        }

        if (Common.DEBUG) {
            if (!link.equals(MUNICIPALITIES) && !link.equals(TYPE_INFRINGEMENT) && !link.equals(TICKETS) && !link.equals(BRANDS)) {
                System.out.println("Response Ready: " + message + " code: " + code + " response: " + json);
            } else {
                System.out.println("Response Ready: " + message + " code: " + code + " response: ");
            }
        }

        if (message.equals("Sin internet")) {
            json = message;
        }

        if (Common.DEBUG && !link.equals(MUNICIPALITIES) && !link.equals(TYPE_INFRINGEMENT) && !link.equals(TICKETS) && !link.equals(BRANDS)) {
            System.out.println("Response Final: " + message + " code: " + code + " response: " + json);
        }

        if (link.equals(Api.INFRINGEMENT_REPORT) && Common.DEBUG) {
            Utils.writeStringInFile("Servidor dice: " + code + " " + message + " " + json, "", context);
        }

        //Manejo especial para error por versión vieja
        SharedPreferences preferences = context.getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);

        if (code == 403) {
            json = "xxx";
            preferences.edit().putBoolean(Common.PREF_VERSION, true).apply();

            if (link.contains("login")) {
                Utils.checkVersion(context, true);
            } else {
                Utils.checkVersion(context, false);
            }
        } else {
            preferences.edit().putBoolean(Common.PREF_VERSION, false).apply();
        }

        return json;
    }

    /**
     * Convierte en String la respuesta cruda proveniente del servidor
     *
     * @param inputStream Respuesta cruda del servidor
     * @param context     Contexto de app
     * @return String
     */
    private static String convertInputStreamToString(final InputStream inputStream, final Context context) {
        String result = "";

        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                result += line;
            }

            inputStream.close();
        } catch (Exception e) {
            Utils.logError(context, "Api:convertInputStreamToString - Exception: ", e);
        }

        return result;
    }

    public static String encryptPasswordSHA1(final String password, final Context context) {
        String sha1 = "";

        try {
            if (!Utils.isEmpty(password)) {
                MessageDigest crypt = MessageDigest.getInstance("SHA-1");
                crypt.reset();
                crypt.update(password.getBytes("UTF-8"));
                sha1 = byte2Hex(crypt.digest(), context);
            }
        } catch (Exception e) {
            Utils.logError(context, "Api:encryptPasswordSHA1 - Exception: ", e);
        }

        return sha1;
    }

    public static String encryptPasswordMD5(final String password, final Context context) {
        String md5 = "";

        try {
            if (!Utils.isEmpty(password)) {
                MessageDigest crypt = MessageDigest.getInstance("MD5");
                crypt.reset();
                crypt.update(password.getBytes("UTF-8"));
                md5 = byte2Hex(crypt.digest(), context);
            }
        } catch (Exception e) {
            Utils.logError(context, "Api:encryptPasswordMD5 - Exception: ", e);
        }

        return md5;
    }

    private static String byte2Hex(final byte[] hash, Context context) {
        String result = "";

        try {
            Formatter formatter = new Formatter();
            for (byte b : hash) {
                formatter.format("%02x", b);
            }

            result = formatter.toString();
            formatter.close();
        } catch (Exception e) {
            Utils.logError(context, "Api:byteToHex - Exception: ", e);
        }

        return result;
    }

    public static String encryptBySauloOld(Context context, String pass) {
        try {
            byte[] salt = new String("KwaNA$eN7es").getBytes();
            int iterationCount = 1024;
            int keyStrength = 256;
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            KeySpec spec = new PBEKeySpec("kwan".toCharArray(), salt, iterationCount, keyStrength);
            SecretKey tmp = factory.generateSecret(spec);
            SecretKey key = new SecretKeySpec(tmp.getEncoded(), "AES");
            Cipher dcipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            dcipher.init(Cipher.ENCRYPT_MODE, key);
            AlgorithmParameters params = dcipher.getParameters();
            byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
            byte[] utf8EncryptedData = dcipher.doFinal(pass.getBytes());
            String base64EncryptedData = HttpRequest.Base64.encodeBytes(utf8EncryptedData);

            if (Common.DEBUG) {
                System.out.println("Original: " + pass);
                System.out.println("Encrypt: " + base64EncryptedData);
                dcipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
                byte[] decryptedData = Base64.decode(base64EncryptedData, Base64.DEFAULT);
                byte[] utf8 = dcipher.doFinal(decryptedData);
                System.out.println("Decrypt: " + new String(utf8, "UTF8"));
            }

            return base64EncryptedData;
        } catch (Exception e) {
            Utils.logError(context, "Api:encryptBySaulo - Exception: ", e);
        }

        return "";
    }

    public static String encryptBySaulo(Context context, String pass) {
        try {
            byte[] salt = new String("Kw4nAG3Nt3sCEr1G").getBytes();
            String iv = "0123456789ZXYRFG";
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec skeySpec = new SecretKeySpec(salt, "AES");
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec);
            byte[] encrypted = cipher.doFinal(pass.getBytes());
            String base64EncryptedData = HttpRequest.Base64.encodeBytes(encrypted);

            if (Common.DEBUG) {
                System.out.println("Encrypt: " + base64EncryptedData);
                System.out.println("Igual encrypt: " + (base64EncryptedData.equals("Evm8Mxy3YtT2oCCcbZeAMg==")));
            }

            return base64EncryptedData;
        } catch (Exception e) {
            Utils.logError(context, "Api:encryptBySaulo - Exception: ", e);
        }

        return "";
    }
}