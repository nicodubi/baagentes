package com.boa.utils;

import android.Manifest;

/**
 * Constantes comunes de uso dentro de la app
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 22/03/2017.
 */
public abstract class Common{
	//En true desbloquea la impresión de trackers y variables para debug
	public static final boolean		DEBUG					= false;
	public static final boolean		DEBUG_PDF				= false;
	public static final double		DEFAULT_LAT				= -34.603857;
	public static final double		DEFAULT_LON				= -58.381863;
	//Indica la api en la que se está ejecutando la app
	public static final int			API_LEVEL				= android.os.Build.VERSION.SDK_INT;
	//Equivalente a Exception para diferenciar boolean
	public static final int			BOOL_EXCEPTION			= -2;
	//Equivalente a error para diferenciar boolean
	public static final int			BOOL_ERROR				= -1;
	//Equivalente false de boolean en int para funciones especiales
	public static final int			BOOL_NO					= 0;
	//Equivalente true de boolean en int para funciones especiales
	public static final int			BOOL_YES				= 1;
	//Clave por defecto para usuario
	public static final String		DEFAULT_PASS			= "agente";
	//Por defecto para usuario
	public static final String		DEFAULT_USER			= "30034980";//Agente: 32143440 Policia: 21072081
	//Clave por defecto para abrir keystore
	public static final String		DEFAULT_PASSKEY			= "Diego456!";//Agente: Diego456! Policia: Asd4567
	//Mientras más cerca del 0 más pequeño es el tamaño y mientras más cerca del 100 mejor calidad
	public static final int			RATIO_COMPRESS_BITMAP	= 80;
	//Referencia a método Basic de autorización seguido de token
	public static final String		AUTH_BASIC				= "Basic ";
	public static final String		KEY_ID					= "id"; //Campo principal en algunas entidades
	public static final String		KEY_JSONID				= "Id"; //Campo principal en algunas entidades
	public static final String		KEY_JSONSTATUS			= "Estado";
	public static final String		KEY_CODE				= "codigo";
	public static final String		KEY_DESCRIPTION			= "descripcion";
	public static final String		KEY_EMAIL				= "email";
	public static final String		KEY_NAME				= "name";
	public static final String		KEY_OTHER				= "other";
	public static final String		KEY_STATUS				= "status";
	//Referencia al diccionario de preferencias para la app actual
	public static final String		KEY_PREF				= "AgentesPref";
	public static final String[]	PERMISSIONS				= {	Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_NETWORK_STATE,
		Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.INTERNET, Manifest.permission.MANAGE_DOCUMENTS, Manifest.permission.READ_EXTERNAL_STORAGE,
		Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
	public static final String		PREF_COUPON				= "prefCoupon";
	public static final String		PREF_CURRENT_ALIAS		= "prefCurrentAlias";
	public static final String		PREF_CURRENT_CERT		= "prefCurrentCert";
	public static final String		PREF_CURRENT_LAT		= "prefCurrentLat";
	public static final String		PREF_CURRENT_LON		= "prefCurrentLon";
	public static final String		PREF_CURRENT_KEY		= "prefCurrentPublicKey";
	public static final String		PREF_CURRENT_PASS		= "prefCurrentPass";
	public static final String		PREF_CURRENT_PASSKEY	= "prefCurrentPassKeystore";
	public static final String		PREF_CURRENT_USER		= "prefCurrentUser";
	public static final String		PREF_IS_POLICE			= "prefIsPolice";
	public static final String		PREF_IMEI				= "prefImei";
	public static final String		PREF_SELECTED_LAT		= "prefSelectedLat";
	public static final String		PREF_SELECTED_LON		= "prefSelectedLon";
	//Referencia para saber si no hay una nueva versión
	public static final String		PREF_VERSION			= "prefVersion";
	//Referencia para saber si no hay una nueva versión
	public static final String		PREF_APPVERSION			= "prefAppVersion";
	//Referencia para saber si hubo o no sesión iniciada
	public static final String		PREF_SESSION_STARTED	= "prefSessionStarted";
	//Referencia para saber desde cuando hay sesión iniciada
	public static final String		PREF_SESSION_TS			= "prefSessionTs";
	//Referencia para saber si se poputan o no datos
	public static final String		PREF_SPLASHED			= "prefSplashed";
	//Referencia para mantener el token de autorización en caso de ser necesario
	public static final String		PREF_TOKEN				= "prefToken";
	public static final String		PREF_SEED				= "encryptedSeed";
	//Referencia para a la ip o nombre del servidor
	public static final String		PREF_SERVER				= "prefServer";
	//Clave de preferencia que almacena si el usuario cumplió con el proceso procesado necesario luego de un update
	public static final String		PREF_UPGRADE			= "prefUpgrade";
}