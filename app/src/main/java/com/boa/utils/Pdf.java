package com.boa.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.kwan.baagentes.App;
import com.kwan.baagentes.R;
import com.kwan.baagentes.data.Brand;
import com.kwan.baagentes.data.Infringement;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Enumeration;

import io.realm.Realm;

/**
 * Created by dgfig on 28/01/2019.
 */
public abstract class Pdf{
	private static final String TAG = Pdf.class.getSimpleName();
	/**
	 * Genera un pdf en base a una vista
	 * @param activity
	 * @param ts
	 */
	public static String generateSignedPdf(Activity activity, long ts, long id, boolean compress, boolean includePdf, String pass){
		try{
			//Generar carpeta y paths
			if(ts == 0){
				ts = System.currentTimeMillis();
			}
			
			pass					= Utils.encryptPass(activity, pass);
			File root				= new File(Environment.getExternalStorageDirectory(), activity.getString(R.string.app_name));
			root.mkdirs();
			File gpxfile			= new File(root, activity.getString(R.string.app_name)+"_"+ts+".pdf");
			ArrayList<String> files	= new ArrayList<>();
			
			//Primero keystore para firmar
			//Adjuntar keystore al pdf dentro de zip
			File rootKey = null;
			
			if(activity.getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE).getBoolean(Common.PREF_IS_POLICE, false)){
				rootKey = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "POLICIA-RA");
			}else{
				rootKey = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "GCBA-RA");
			}
			
			File keystoreFile = new File(rootKey, "ks.gcba");
			//No se envia más porque la firma se hace en la app
			
			//Segundo el pdf si hace falta
			if(includePdf){
				DisplayMetrics displaymetrics	= new DisplayMetrics();
				activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
				boolean showObs = false;
				
				//Nueva forma http://www.vogella.com/tutorials/JavaPDF/article.html
				Document document = new Document();
				PdfWriter.getInstance(document, new FileOutputStream(gpxfile));
				document.open();
				Realm realm = Realm.getDefaultInstance();
				Infringement infringement = realm.where(Infringement.class).equalTo(Common.KEY_ID, id).findFirst();
				
				if(infringement != null){
					if(infringement.getTypesInfringements().contains("6172") || infringement.getTypesInfringements().contains("6878") ||
						infringement.getTypesInfringements().contains("6879") || infringement.getTypesInfringements().contains("6882") ||
						infringement.getTypesInfringements().contains("6883") || infringement.getTypesInfringements().contains("8942") ||
						infringement.getTypesInfringements().contains("8943") || infringement.getTypesInfringements().contains("8944")){
						showObs = true;
					}
				}
				
				addContent(document, infringement, showObs, realm);
				realm.close();
				document.close();
				
				//Preparar firma
				KeyStore keyStore = KeyStore.getInstance("PKCS12");
				keyStore.load(new FileInputStream(keystoreFile), pass.toCharArray());
				String alias = getValidKeyStoreAlias(keyStore,pass);
				if(TextUtils.isEmpty(alias)){
					//there aren't any alias with a valid key associated, so alias returned is empty or null
					//TODO do you want to show any special Toast/Snack Bar??
				}
				PrivateKey key = (PrivateKey) keyStore.getKey(alias, pass.toCharArray());
				Certificate[] chain = keyStore.getCertificateChain(alias);
				//Empieza a firmar
				try{
					PdfReader reader = new PdfReader(gpxfile.getAbsolutePath());
					FileOutputStream os = new FileOutputStream(gpxfile.getAbsolutePath().replace(".pdf", "signed.pdf"));
					PdfStamper stamper = PdfStamper.createSignature(reader, os, '\0', new File(root.getAbsolutePath()));
					PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
					appearance.setLocation("Ciudad Autónoma de Buenos Aires");

					appearance.setVisibleSignature(new Rectangle(165, 0, 500, 120), 1, null);

					ExternalSignature pks = new PrivateKeySignature(key, DigestAlgorithms.SHA256, null);
					ExternalDigest digest = new BouncyCastleDigest();
					MakeSignature.signDetached(appearance, digest, pks, chain, null, null, null, 0, MakeSignature.CryptoStandard.CMS);
					files.add(gpxfile.getAbsolutePath().replace(".pdf", "signed.pdf"));
				}catch(Exception e){
					Log.w("exc",e.toString());
				}
			}
			
			//Comprimir pdf
			if(Common.DEBUG){
				System.out.println("Root: "+root.getAbsolutePath());
				System.out.println("File: "+gpxfile.getAbsolutePath());
				System.out.println("File signed: "+gpxfile.getAbsolutePath().replace(".pdf", "signed.pdf"));
				System.out.println("Zip: "+gpxfile.getAbsolutePath().replace(".pdf", ".zip"));
			}
			
			if(compress){
				Utils.compressFiles(activity, files, gpxfile.getAbsolutePath());
				
				if(includePdf){
					new File(gpxfile.getPath()).delete();
					new File(gpxfile.getPath().replace(".pdf", "signed.pdf")).delete();
				}
				
				return gpxfile.getAbsolutePath().replace(".pdf", ".zip");
			}else{
				return gpxfile.getAbsolutePath();
			}
		}catch(Exception e){
			Utils.logError(activity, "Utils:generatePdf - Exception: ", e);
		}
		
		return "";
	}

	private static String getValidKeyStoreAlias(KeyStore keyStore, String pass) {
		Enumeration<String> aliases = null;
		PrivateKey key = null;
		try {
			aliases = keyStore.aliases();
			while (aliases.hasMoreElements()){
				String aliasToBeChecked = aliases.nextElement();
				try {
					key = (PrivateKey) keyStore.getKey(aliasToBeChecked, pass.toCharArray());
					if(key != null){
						return aliasToBeChecked;
					}
				} catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException e) {
					e.printStackTrace();
				}
			}
		} catch (KeyStoreException e) {
			e.printStackTrace();
			Log.d(TAG,"It is not possible to get keyStore aliasses");
			//there aren't any alias with a valid key associated, so returns empty alias
			return "";
		}
		//there aren't any alias with a valid key associated, so returns empty alias
		return "";
	}

	private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
	private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
	
	private static PdfPCell createCell(String text){
		PdfPCell c1 = new PdfPCell(new Phrase(text, subFont));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		c1.setBorder(0);
		return c1;
	}
	
	private static Paragraph createTitle(String text){
		Paragraph preface = new Paragraph();
		preface.add(new Paragraph(text, catFont));
		return preface;
	}
	
	private static void addContent(Document document, Infringement infringement, boolean showObs, Realm realm){
		try{
			SharedPreferences preferences = App.getContext().getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			
			if(infringement != null){
				Bitmap bmp  = BitmapFactory.decodeResource(App.getContext().getResources(), R.drawable.rsz_sasa);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
				Image image = Image.getInstance(stream.toByteArray());
				image.setAlignment(Element.ALIGN_RIGHT);
				document.add(image);
				//Contenido
				document.add(createTitle("Contenido"));
				PdfPTable table = new PdfPTable(new float[] { 1, 3 });
				table.setHorizontalAlignment(Element.ALIGN_LEFT);
				table.setWidthPercentage(100);
				table.addCell(createCell("Nro de acta:"));
				table.addCell(createCell("I"+infringement.getTicketNumber()));
				
				table.addCell(createCell("Fecha Hora:"));
				table.addCell(createCell(infringement.getDate()+" "+infringement.getHour()));
				
				table.addCell(createCell("Agente:"));
				table.addCell(createCell(infringement.getUser().getApellido()+" "+infringement.getUser().getNombre()));
				
				table.addCell(createCell("Cargo:"));
				
				if(!preferences.getBoolean(Common.PREF_IS_POLICE, false)){
					table.addCell(createCell("AGENTE DE CONTROL DE TRANSITO"));
				}else{
					table.addCell(createCell("AGENTE DE LA POLICIA DE LA CIUDAD AUTONOMA DE BUENOS AIRES"));
				}
				
				table.addCell(createCell("Modalidad:"));
				
				if(infringement.getFormInfringement().equals("P")){
					table.addCell(createCell("Con presencia del infractor"));
				}else{
					table.addCell(createCell("Sin presencia del infractor"));
				}
				
				document.add(table);
				//Datos del Vehículo:
				document.add(createTitle("Datos del Vehículo:"));
				table = new PdfPTable(new float[] { 1, 3 });
				table.setHorizontalAlignment(Element.ALIGN_LEFT);
				table.setWidthPercentage(100);
				table.addCell(createCell("Nro de Dominio y versión:"));
				
				if(!Utils.isEmpty(infringement.getDomain())){
					table.addCell(createCell(infringement.getDomain()+" "+infringement.getCopyDomain()));
				}else{
					table.addCell(createCell(""));
				}
				
				table.addCell(createCell("Marca:"));
				
				if(infringement.getBrandId() != 0){
					Brand brand = realm.where(Brand.class).equalTo(Common.KEY_ID, infringement.getBrandId()).findFirst();
					
					if(brand != null){
						table.addCell(createCell(brand.getMarca()));
					}else{
						table.addCell(createCell(""));
					}
				}else{
					table.addCell(createCell(""));
				}
				
				table.addCell(createCell("Modelo:"));
				
				if(!Utils.isEmpty(infringement.getModel())){
					table.addCell(createCell(infringement.getModel()));
				}else{
					table.addCell(createCell(""));
				}
				
				table.addCell(createCell("Tipo de Infracción:"));
				table.addCell(createCell(infringement.getTypesInfringements().replace(";", ";"+System.lineSeparator())));
				
				table.addCell(createCell("Lugar de la Infracción:"));
				table.addCell(createCell(infringement.getStreet().toUpperCase()));
				
				table.addCell(createCell("Altura:"));
				table.addCell(createCell(infringement.getHeight()));
				
				table.addCell(createCell("Referencia del lugar (KM):"));
				
				if(!Utils.isEmpty(infringement.getReferencePlace())){
					table.addCell(createCell(infringement.getReferencePlace()));
				}else{
					table.addCell(createCell(""));
				}
				
				table.addCell(createCell("Entre Calles:"));
				
				if(!Utils.isEmpty(infringement.getBetween1()) && !Utils.isEmpty(infringement.getBetween2())){
					table.addCell(createCell(infringement.getBetween1()+" y "+infringement.getBetween2()));
				}else{
					if(!Utils.isEmpty(infringement.getBetween1())){
						table.addCell(createCell(infringement.getBetween1()));
					}else{
						if(!Utils.isEmpty(infringement.getBetween2())){
							table.addCell(createCell(infringement.getBetween2()));
						}else{
							table.addCell(createCell(""));
						}
					}
				}
				
				document.add(table);
				//Datos del Vehículo:
				document.add(createTitle("Datos del Imputado:"));
				table = new PdfPTable(new float[] { 1, 3 });
				table.setHorizontalAlignment(Element.ALIGN_LEFT);
				table.setWidthPercentage(100);
				table.addCell(createCell("Nombre:"));
				table.addCell(createCell(infringement.getNameImputed()));
				
				table.addCell(createCell("Apellido:"));
				table.addCell(createCell(infringement.getLastNameImputed()));

				//table.addCell(createCell("Sexo:"));
				//table.addCell(createCell(infringement.getGenderImputed()));
				
				table.addCell(createCell("Provincia:"));
				table.addCell(createCell(infringement.getApartmentImputed()));
				
				table.addCell(createCell("Calle:"));
				table.addCell(createCell(infringement.getStreetImputed()));
				
				table.addCell(createCell("Altura:"));
				table.addCell(createCell(infringement.getHeightImputed()));
				
				table.addCell(createCell("Piso:"));
				table.addCell(createCell(infringement.getFloor()));
				
				table.addCell(createCell("Depto / Oficina:"));
				table.addCell(createCell(infringement.getOffice()));
				
				table.addCell(createCell("Localidad:"));
				
				if(infringement.getLocalityImputed() != null){
					table.addCell(createCell(infringement.getLocalityImputed().getDescripcion()));
				}else{
					table.addCell(createCell(""));
				}
				
				table.addCell(createCell("Tipo de Documento:"));
				
				if(infringement.getTypeDocumentImputed() != null){
					table.addCell(createCell(infringement.getTypeDocumentImputed().getDocumento()));
				}else{
					table.addCell(createCell(""));
				}
				
				table.addCell(createCell("Nro de Documento:"));
				
				if(!Utils.isEmpty(infringement.getDocumentImputed())){
					table.addCell(createCell(infringement.getDocumentImputed()));
				}else{
					table.addCell(createCell(""));
				}
				
				table.addCell(createCell("Licencia de Conductor:"));
				
				if(!Utils.isEmpty(infringement.getLicense())){
					table.addCell(createCell(infringement.getLicense()));
				}else{
					table.addCell(createCell(""));
				}
				
				table.addCell(createCell("Municipio de la Licencia:"));
				
				if(!Utils.isEmpty(infringement.getIssuedBy())){
					table.addCell(createCell(infringement.getIssuedBy()));
				}else{
					table.addCell(createCell(""));
				}
				
				table.addCell(createCell("Categoría de la Licencia:"));
				
				if(infringement.getCategories() != null){
					table.addCell(createCell(infringement.getCategories()));
				}else{
					table.addCell(createCell(""));
				}
				
				table.addCell(createCell("Retención de Licencia:"));
				table.addCell(createCell(infringement.getDocumentationHeld().replace("S", "Si").replace("N", "No")));
				
				table.addCell(createCell("Vehículo Retenido:"));
				table.addCell(createCell(infringement.getVehicleRemission().replace("S", "Si").replace("N", "No")));

				table.addCell(createCell("Dependencia:"));
				String dependence = "";
				if(!TextUtils.isEmpty(infringement.getDependence())){
					dependence = infringement.getDependence();
				}
				table.addCell(createCell(dependence));


				table.addCell(createCell("Nro de Acta z Vinculada:"));
				
				if(!Utils.isEmpty(infringement.getTicketZNumber())){
					table.addCell(createCell(infringement.getTicketZNumber()));
				}else{
					table.addCell(createCell(""));
				}
				
				table.addCell(createCell("Nro de Acta Contravención:"));
				
				if(!Utils.isEmpty(infringement.getNumberTicketContravention())){
					table.addCell(createCell(infringement.getNumberTicketContravention()));
				}else{
					table.addCell(createCell(""));
				}
				
				//Si el código está en los mencionados mostrar observaciones
				if(showObs){
					table.addCell(createCell("Observaciones:"));
					table.addCell(createCell(infringement.getObservations()));
				}
				
				table.addCell(createCell("Descripción de conducta:"));
				table.addCell(createCell(infringement.getZipCodeImputed()));
				/*for(int i = 0 ; i < 20 ;++i){
					table.addCell(createCell(""));
				}*/

				document.add(table);

				//Firma
				document.add(createTitle(""));
				document.add(createTitle("Firma"));


			}
		}catch(Exception e){
			Utils.logError(App.getContext(), "Utils:addContent - Exception: ", e);
		}
	}
}