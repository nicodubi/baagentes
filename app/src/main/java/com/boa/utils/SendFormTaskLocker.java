package com.boa.utils;

/**
 * Created by Nicolas Dubiansky on 25/04/19.
 */
public class SendFormTaskLocker {
    private static SendFormTaskLocker INSTANCE;
    private boolean sendFormTaskRunning;

    private SendFormTaskLocker() {
        sendFormTaskRunning = false;
    }

    public static SendFormTaskLocker getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SendFormTaskLocker();
        }
        return INSTANCE;
    }

    public void lockTask() {
        sendFormTaskRunning = true;
    }

    public void unlockTask() {
        sendFormTaskRunning = false;
    }

    public boolean isSendFormTaskRunning() {
        return sendFormTaskRunning;
    }
}
