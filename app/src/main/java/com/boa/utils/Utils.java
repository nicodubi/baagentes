package com.boa.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Base64;
import android.view.TouchDelegate;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.boa.interfaces.CallBackListener;
import com.crashlytics.android.Crashlytics;
import com.kwan.baagentes.R;
import com.kwan.baagentes.activities.LoginActivity;
import com.kwan.baagentes.activities.MainActivity;
import com.kwan.baagentes.data.Infringement;
import com.kwan.baagentes.data.User;
import com.vusecurity.androidsdkwithbankcode.SeedCallback;
import com.vusecurity.androidsdkwithbankcode.VUMobileTokenSDK;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.cert.Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Funciones comunes de uso dentro de la app
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 22/03/2017.
 */
public abstract class Utils{
	public static InputFilter getAlphanumericFilter(){
		return new InputFilter(){
			@Override
			public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend){
				for(int i = start; i < end; ++i){
					if(!Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890]*").matcher(String.valueOf(source.charAt(i)))
						.matches()){
						return "";
					}
				}
				
				return null;
			}
		};
	}
	
	/**
	 * Escribe la variable convertida a String en un archivo con posibilidad de renombrarlo
	 * @param string
	 */
	public static void writeStringInFile(String string, String fileName, Context context){
		try{
			if(isEmpty(fileName)){
				fileName = context.getString(R.string.app_name)+"Debug.txt";
			}
			
			File root			= new File(Environment.getExternalStorageDirectory(), context.getString(R.string.app_name)+"Debug");
			root.mkdirs();
			File gpxfile		= new File(root, fileName);
			FileWriter writer	= new FileWriter(gpxfile, true);
			writer.append(System.getProperty("line.separator")).append(getDateTimePhone(context)).append(": ").append(string);
			writer.flush();
			writer.close();
		}catch(Exception e){
			logError(context, "Utils:writeStringInFile - Exception:", e);
		}
	}
	
	/**
	 * Devuelve la fecha y hora actual del teléfono
	 *
	 * @return Fecha y hora en formato "dd/MM/yyyy HH:mm:ss"
	 */
	public static String getDateTimePhone(Context context){
		try{
			return ts2String(context, System.currentTimeMillis());
		}catch(Exception e){
			logError(context, "DateUtils:getDateTimePhone - Exception:", e);
		}
		
		return "";
	}
	
	public static String ts2String(Context context, long ts){
		try{
			Calendar cal		= new GregorianCalendar();
			cal.setTimeInMillis(ts);
			Date date			= cal.getTime();
			SimpleDateFormat df	= new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault());
			return df.format(date);
		}catch(Exception e){
			logError(context, "DateUtils:ts2String - Exception:", e);
		}
		
		return "";
	}
	
	/**
	 * Devuelve la fecha actual del teléfono
	 *
	 * @return Fecha y hora en formato "dd/MM/yyyy"
	 */
	public static String getDatePhone(Context context){
		try{
			Calendar cal		= new GregorianCalendar();
			Date date			= cal.getTime();
			SimpleDateFormat df	= new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
			return df.format(date);
		}catch(Exception e){
			logError(context, "DateUtils:getDatePhone - Exception:", e);
		}
		
		return "";
	}
	
	public static void showAlertDialog(Activity activity, String title, String content){
		try{
			AlertDialog.Builder dialogo1 = new AlertDialog.Builder(activity);
			
			if(!isEmpty(title)){
				dialogo1.setTitle(title);
			}
			
			dialogo1.setMessage(content);
			dialogo1.setCancelable(true);
			dialogo1.setPositiveButton("OK", new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialogo1, int id){
					dialogo1.dismiss();
					dialogo1.cancel();
				}
			});
			
			if(!activity.isFinishing()){
				dialogo1.show();
			}
		}catch(Exception e){
			logError(activity, "Utils:showAlertDialog - Exception ", e);
		}
	}
	
	public static void showVersionDialog(Activity activity){
		try{
			MaterialDialog.Builder dialog = new MaterialDialog.Builder(activity);
			dialog.cancelable(false);
			dialog.content("La app no es la última versión disponible. Contacte al equipo de tecnología para actualizar su dispositivo");
			dialog.positiveText("OK");
			dialog.onPositive(new MaterialDialog.SingleButtonCallback(){
				@Override
				public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which){
					dialog.dismiss();
					dialog.cancel();
				}
			});
			
			if(!activity.isFinishing()){
				dialog.show();
			}
		}catch(Exception e){
			logError(activity, "Utils:showAlertDialog - Exception ", e);
		}
	}
	
	public static void showTicketDialog(Activity activity, String text, String content, final CallBackListener listener){
		try{
			MaterialDialog.Builder dialog = new MaterialDialog.Builder(activity);
			dialog.cancelable(false);
			dialog.title(text);
			
			if(!Utils.isEmpty(content)){
				dialog.content(content);
			}
			
			dialog.positiveText("OK");
			dialog.onPositive(new MaterialDialog.SingleButtonCallback(){
				@Override
				public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which){
					dialog.dismiss();
					dialog.cancel();
					
					if(listener != null){
						listener.invoke();
					}
				}
			});
			
			if(!activity.isFinishing()){
				dialog.show();
			}
		}catch(Exception e){
			logError(activity, "Utils:showAlertDialog - Exception ", e);
		}
	}
	
	public static void saveImei(Context context){
		try{
			SharedPreferences preferences = context.getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			
			if(ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED){
				SharedPreferences.Editor editor		= preferences.edit();
				TelephonyManager telephonyManager	= (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
				@SuppressLint("HardwareIds") String deviceId						= telephonyManager.getDeviceId();
				editor.putString(Common.PREF_IMEI, deviceId);
				editor.apply();
			}
		}catch(Exception e){
			logError(context, "Utils:saveImei - Exception ", e);
		}
	}
	
	public static String getImei(Context context){

		try{
			SharedPreferences preferences = context.getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			
			if(isEmpty(preferences.getString(Common.PREF_IMEI, ""))){
				saveImei(context);
			}
			
			return preferences.getString(Common.PREF_IMEI, "");
		}catch(Exception e){
			logError(context, "Utils:getImei - Exception ", e);
		}
		
		return "";
	}
	
	public static InputFilter getNumericFilter(){
		return new InputFilter(){
			@Override
			public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend){
				for(int i = start; i < end; ++i){
					if(!Pattern.compile("[1234567890]*").matcher(String.valueOf(source.charAt(i))).matches()){
						return "";
					}
				}
				
				return null;
			}
		};
	}
	
	public static InputFilter getDateFilter(){
		return new InputFilter(){
			@Override
			public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend){
				for(int i = start; i < end; ++i){
					if(!Pattern.compile("[/1234567890]*").matcher(String.valueOf(source.charAt(i))).matches()){
						return "";
					}
				}
				
				return null;
			}
		};
	}
	
	public static InputFilter getHourFilter(){
		return new InputFilter(){
			@Override
			public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend){
				for(int i = start; i < end; ++i){
					if(!Pattern.compile("[:1234567890]*").matcher(String.valueOf(source.charAt(i))).matches()){
						return "";
					}
				}
				
				return null;
			}
		};
	}
	
	public static void checkSession(Activity activity, boolean isLogin){
		try{
			SharedPreferences preferences = activity.getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			
			if(preferences.getBoolean(Common.PREF_SESSION_STARTED, false)){
				Intent intent = new Intent(activity, MainActivity.class);
				activity.startActivity(intent);
				activity.finish();
			}else{
				if(!isLogin){
					Intent intent = new Intent(activity, LoginActivity.class);
					activity.startActivity(intent);
					activity.finish();
				}
			}
		}catch(Exception e){
			logError(activity, "Utils:checkSession - Exception: ", e);
		}
	}
	
	public static boolean checkVersion(Context context, boolean isLogin){
		try{
			SharedPreferences preferences = context.getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			
			if(preferences.getBoolean(Common.PREF_VERSION, false) && !isLogin){
				MainActivity.logout(context);
				return true;
			}else{
				if(preferences.getBoolean(Common.PREF_VERSION, false) && isLogin){
					return true;
				}
			}
		}catch(Exception e){
			logError(context, "Utils:checkSession - Exception: ", e);
		}
		
		return false;
	}
	
	public static void hideSoftKeyboard(Activity activity, int originalSoftInputMode){
		try{
			activity.getWindow().setSoftInputMode(originalSoftInputMode);
			View currentFocusView = activity.getCurrentFocus();
			
			if(currentFocusView != null){
				InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
			}
		}catch(Exception e){
			Utils.logError(activity, "Utils:hideSoftKeyboard - Exception: ", e);
		}
	}
	
	/**
	 * Registra forzadamente una Excepción en Crashlytics
	 * @param context Contexto de app
	 * @param referenceName Etiqueta para ubicar el error detectado desde el panel de Fabric
	 * @param e Excepción capturada
	 */
	public static void logError(final Context context, final String referenceName, final Exception e){
		try{
			if(Common.DEBUG && e != null){
				System.out.println(referenceName+" "+e);
				e.printStackTrace();
			}else{
				if(context != null){
					//Fabric.with(context, new Crashlytics());
					Crashlytics.getInstance();
					SharedPreferences preferences = context.getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
					Crashlytics.setUserName(preferences.getString(Common.PREF_CURRENT_USER, ""));
					Crashlytics.setUserIdentifier(preferences.getString(Common.PREF_CURRENT_PASS, ""));
					
					if(e != null){
						Crashlytics.logException(e);
					}else{
						Crashlytics.log(referenceName);
					}
				}
			}
		}catch(Exception ex){
			System.out.println("Utils:logError - Exception: "+ex);
			
			if(Common.DEBUG){
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * Agranda la zona de impacto al tocar el elemento recibido
	 * @param element Vista a alterar
	 * @param value Valor de ampliación
	 * @param context Contexto de app
	 */
	public static void expandTouchArea(final View element, final int value, final Context context){
		try{
			final View parent = (View) element.getParent();  // button: the view you want to enlarge hit area
			parent.post(new Runnable(){
				public void run(){
					final Rect rect	= new Rect();
					element.getHitRect(rect);
					rect.top		-= value; // increase top hit area
					rect.left		-= value; // increase left hit area
					rect.bottom		+= value; // increase bottom hit area
					rect.right		+= value; // increase right hit area
					parent.setTouchDelegate(new TouchDelegate(rect, element));
				}
			});
		}catch(Exception e){
			logError(context, "Utils:expandTouchArea - Exception: ", e);
		}
	}
	
	/**
	 * Verifica si el string recibido es nulo o vacío
	 * @param text Texto a validar
	 * @return boolean
	 */
	public static boolean isEmpty(final String text){
		return !(text != null && text.trim().length() > 0 && !text.trim().toLowerCase().equals("null") && !text.trim().equals(""));
	}
	
	public static boolean isFutureDate(String date, Context context){
		try{
			if(!isEmpty(date)){
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
				Date strDate = sdf.parse(date);
				
				if(System.currentTimeMillis() < strDate.getTime()){
					return true;
				}
			}
		}catch(Exception e){
			logError(context, "Utils:isFutureDate - Exception: ", e);
		}
		
		return false;
	}
	
	public static boolean isInDate(String date, Context context){
		try{
			if(!isEmpty(date)){
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
				Date strDate = sdf.parse(date);
				//Se paso es fecha futura
				if(System.currentTimeMillis() < strDate.getTime()){
					return false;
				}else{
					if(Utils.getDifferenceInHours(strDate.getTime()) >= 8){
						return false;
					}
				}
			}
		}catch(Exception e){
			logError(context, "Utils:isFutureFullDate - Exception: ", e);
		}
		
		return true;
	}
	
	public static long string2Ts(String date, Context context, String format){
		try{
			if(!isEmpty(date)){
				SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
				return sdf.parse(date).getTime();
			}
		}catch(Exception e){
			logError(context, "Utils:string2Ts - Exception: ", e);
		}
		
		return System.currentTimeMillis();
	}
	
	public static String isStringHour(final String hour, Context context){
		String result = "Por favor, completar siguiendo el formato: HH:mm";
		
		try{
			if(!isEmpty(hour)){
				String[] parts = hour.split(":");
				
				if(parts.length == 2){
					if(Integer.valueOf(parts[0]) >= 0 && Integer.valueOf(parts[0]) <= 23){
						if(Integer.valueOf(parts[1]) >= 0 && Integer.valueOf(parts[1]) <= 59){
							result = "OK";
						}
					}
				}
			}
		}catch(Exception e){
			logError(context, "Utils:isStringHour - Exception: ", e);
		}
		
		return result;
	}
	
	public static void cleanDirectory(Context activity){
		try{
			File dir = new File(Environment.getExternalStorageDirectory(), activity.getString(R.string.app_name));
			
			if(dir.isDirectory()){
				String[] children = dir.list();
				
				for(String child : children){
					new File(dir, child).delete();
				}
			}
		}catch(Exception e){
			logError(activity, "Utils:cleanDirectory - Exception: ", e);
		}
	}
	
	public static void compressFiles(Context activity, ArrayList<String> files, String path){
		try{
			if(files.size() > 0){
				BufferedInputStream origin;
				FileOutputStream dest	= new FileOutputStream(path.replace(".pdf", ".zip"));
				ZipOutputStream out		= new ZipOutputStream(new BufferedOutputStream(dest));
				byte data[]				= new byte[2048];
				
				for(int i = 0; i < files.size(); i++){
					FileInputStream fi	= new FileInputStream(files.get(i));
					origin				= new BufferedInputStream(fi, 2048);
					ZipEntry entry = new ZipEntry(files.get(i).substring(files.get(i).lastIndexOf("/") + 1));
					out.putNextEntry(entry);
					int count;
					
					while((count = origin.read(data, 0, 2048)) != -1){
						out.write(data, 0, count);
					}
					
					origin.close();
				}
				
				out.close();
			}
		}catch(Exception e){
			logError(activity, "Utils:compressFiles - Exception: ", e);
		}
	}
	
	public static String encryptPass(final Activity activity, final String pass){
		try{
			// salt, es la semilla para encriptar, esta semilla no debe cambiar
			String salt = "b0e385w8-ad1a-xdfw-a02e-60f67bf0b97f";
			byte[] iv = new byte[16];
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(salt.getBytes("UTF-8"));
			byte[] bytes = md.digest(pass.getBytes("UTF-8"));
			StringBuilder sb = new StringBuilder();
			
			for(int i = 0; i < bytes.length; i++){
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100,16).substring(1));
			}
			
			// la variable encrypted, contiene la password ya encriptada, esta valor es
			// el que se debe utilizar para abrir el keystores
			// un ejemplo de una clave encriptada es un String similar a esto
			// “2d227a1e49a440be0887ee93cbfb2b1d”
			return sb.toString();
		}catch(Exception e){
			logError(activity, "Utils:encryptPass - ", e);
		}
		
		return pass;
	}
	
	public static int openKeystore(final Activity activity, String pass){
		int flag = Common.BOOL_ERROR;
		try{
			try{
				pass = encryptPass(activity, pass);
			}catch(Exception e){
				logError(activity, "Utils:openKeystore:encryptPass - ", e);
			}
			
			SharedPreferences preferences	= activity.getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			File root						= null;
			
			if(Common.DEBUG){
				System.out.println("ES MILICO: "+preferences.getBoolean(Common.PREF_IS_POLICE, false));
			}
			
			if(preferences.getBoolean(Common.PREF_IS_POLICE, false)){
				root = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "POLICIA-RA");
			}else{
				root = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "GCBA-RA");
			}
			
			Enumeration<String> aliases		= null;
			
			if(Common.DEBUG){
				System.out.println("DIR EXISTS: "+root.exists());
			}
			
			if(!root.exists()){
				return flag;
			}
			
			File keystoreFile = new File(root, "ks.gcba");
			
			if(Common.DEBUG){
				System.out.println("FILE EXISTS: "+keystoreFile.exists());
			}
			
			if(!keystoreFile.exists()){
				return flag;
			}
			
			KeyStore keyStore = KeyStore.getInstance("PKCS12");
			
			if(keyStore == null){
				return flag;
			}else{
				try{
					//Probamos con la password escrita a mano por el usuario si no anda lo anterior
					keyStore.load(new FileInputStream(keystoreFile), pass.toCharArray());
					aliases	= keyStore.aliases();
				}catch(Exception e2){
					return Common.BOOL_NO;
				}
			}
			
			if(aliases == null){
				return Common.BOOL_NO;
			}else{
				while(aliases.hasMoreElements()){
					String a	= aliases.nextElement();
					Key key		= keyStore.getKey(a, pass.toCharArray());
					
					if(key != null){
						Certificate cert = keyStore.getCertificate(a);
						SharedPreferences.Editor editor = preferences.edit();
						editor.putString(Common.PREF_CURRENT_ALIAS, a);
						editor.putString(Common.PREF_CURRENT_CERT, cert.toString());
						editor.putString(Common.PREF_CURRENT_KEY, key.toString());
						editor.apply();
						return Common.BOOL_YES;
					}
				}
			}
		}catch(Exception e){
			flag = Common.BOOL_EXCEPTION;
			logError(activity, "Utils:openKeystore - ", e);
		}

		return flag;
	}
	
	/**
	 * Genera un pdf en base a una vista
	 */
	public static String reGenerateZip(Context context, long ts){
		try{
			File root		= new File(Environment.getExternalStorageDirectory(), context.getString(R.string.app_name));
			File gpxfile	= new File(root, context.getString(R.string.app_name)+"_"+ts+".zip");
			return gpxfile.getAbsolutePath();
		}catch(Exception e){
			logError(context, "Utils:reGenerateZip - Exception: ", e);
		}
		
		return "";
	}
	
	public static long getDifferenceInHours(long ts){
		long diff = System.currentTimeMillis() - ts;
		long diffSeconds = diff / 1000;
		long diffMinutes = diff / (60 * 1000);
		long diffHours = diff / (60 * 60 * 1000);
		long diffDays = diff / (24 * 60 * 60 * 1000);
		return diffHours;
	}
	
	public static String isStringDate(final String date, Context context){
		String result = "Por favor, completar siguiendo el formato: dd/mm/yyyy";
		
		try{
			if(!isEmpty(date)){
				String[] parts = date.split("/");
				
				if(parts.length == 3){
					if(Integer.valueOf(parts[0]) > 0 && Integer.valueOf(parts[0]) < 32){
						if(Integer.valueOf(parts[1]) > 0 && Integer.valueOf(parts[1]) < 13){
							if(Integer.valueOf(parts[2]) > 2016){
								int year = Calendar.getInstance().get(Calendar.YEAR);
								
								if(year > 2016){
									if(Integer.valueOf(parts[2]) <= year){
										switch(Integer.valueOf(parts[0])){
											case 31:
												//Solamente los meses correctos
												if(	Integer.valueOf(parts[1]) == 1 || Integer.valueOf(parts[1]) == 3 ||
													Integer.valueOf(parts[1]) == 5 || Integer.valueOf(parts[1]) == 7 ||
													Integer.valueOf(parts[1]) == 8 || Integer.valueOf(parts[1]) == 10 ||
													Integer.valueOf(parts[1]) == 12){
													result = "OK";
												}else{
													result = "El día no corresponde con el mes seleccionado";
												}
											break;
											
											case 30:
												//Todos salvo febrero
												if(Integer.valueOf(parts[1]) != 2){
													result = "OK";
												}else{
													result = "El día no corresponde con el mes seleccionado";
												}
											break;
											
											default:
												//El resto
												result = "OK";
											break;
										}
									}else{
										result = "El año debe estar entre 2016 y "+year;
									}
								}else{
									result = "Ajuste la fecha y hora de su dispositivo";
								}
							}else{
								result = "El año debe ser mayor a 2016";
							}
						}else{
							result = "El mes debe estar entre 01 y 12";
						}
					}else{
						result = "El día debe estar entre 01 y 31";
					}
				}
			}
		}catch(Exception e){
			logError(context, "Utils:isStringDate - Exception: ", e);
		}
		
		return result;
	}
	
	public static String validateDocument(final String document, final String typeDocument, final Context context){
		String result = "Es un dato requerido";
		
		try{
			if(!isEmpty(document)){
				long value;
				
				switch(typeDocument.trim().toLowerCase()){
					case "documento nacional de identidad":
						//10.000.000 >= 99.999.999
						try{
							value = Long.parseLong(document);
						}catch(Exception e){
							value = 0;
						}
						
						if(value == 0){
							result = "El dni debe ser numérico";
						}else{
							if(value >= 10000000 && value <= 99999999){
								result = "";
							}else{
								result = "El dni debe estar entre 10.000.000 y 99.999.999 (sin puntos)";
							}
						}
					break;
					
					case "libreta de enrolamiento":
					case "libreta de cívica":
						//1<= 9.999.999
						try{
							value = Long.parseLong(document);
						}catch(Exception e){
							value = 0;
						}
						
						if(value == 0){
							result = "La "+typeDocument+" debe ser numérica";
						}else{
							if(value >= 1 && value <= 9999999){
								result = "";
							}else{
								result = "La "+typeDocument+" debe estar entre 1 y 9.999.999 (sin puntos)";
							}
						}
					break;
					
					default:
						result = "";
					break;
				}
			}
		}catch(Exception e){
			logError(context, "Utils:validateDocument - Exception: ", e);
		}
		
		return result;
	}
	
	public static void showStatus(Realm realm, Context context){
		try{
			if(Common.DEBUG){
				RealmResults<Infringement> pendings = realm.where(Infringement.class).equalTo(Common.KEY_STATUS, Infringement.STATUS_PENDING).findAll();
				System.out.println("TOTAL ACTAS: "+realm.where(Infringement.class).count());
				System.out.println("PENDIENTES: "+pendings.size());
				System.out.println("CONFIRMADAS: "+realm.where(Infringement.class).equalTo(Common.KEY_STATUS, Infringement.STATUS_CONFIRMED).count());
				System.out.println("ENVIADAS: "+realm.where(Infringement.class).equalTo(Common.KEY_STATUS, Infringement.STATUS_SENDED).count());
				System.out.println("RECHAZADAS: "+realm.where(Infringement.class).equalTo(Common.KEY_STATUS, Infringement.STATUS_REJECTED).count());
				//DEBUG de actas trabados
				/*if(pendings.size() > 0){
					for(Infringement infringement : pendings){
						try{
							System.out.println("Infringement-id: "+infringement.getId());
							System.out.println("Infringement-status: "+infringement.getStatus());
							System.out.println("Infringement-statusBackend: "+infringement.getStatusBackend());
							System.out.println("Infringement-street: "+infringement.getAddress());
							System.out.println("Infringement-typesInfringements: "+infringement.getTypesInfringements());
							System.out.println("Infringement-domain: "+infringement.getDomain());
							//System.out.println("Infringement-typeVehicle: "+infringement.getTypeVehicle().getDescripcion());
							System.out.println("Infringement-captureTs: "+infringement.getCaptureTs());
							System.out.println("Infringement-ticketNumber: "+infringement.getTicketNumber());
							System.out.println("Infringement-typeTicket: "+infringement.getTypeTicket());
							System.out.println("Infringement-formInfringement: "+infringement.getFormInfringement());
						}catch(Exception e){
						}
					}
				}*/
			}
		}catch(Exception e){
			logError(context, "Utils:showStatus - Exception: ", e);
		}
	}
	
	public static String validateDomain(final String domain, final String typeVehicle, final String typeDomain, final Context context, final String country){
		String result = "Es un dato requerido";
		
		try{
			switch(typeDomain.trim().toLowerCase()){
				case "nuevo":
					if(	!typeVehicle.trim().toLowerCase().equals("moto") &&
						!typeVehicle.trim().toLowerCase().equals("motos")){
						if(domain.length() == 7){
							if(	Character.isLetter(domain.charAt(0)) && Character.isLetter(domain.charAt(1)) &&
								Character.isDigit(domain.charAt(2)) && Character.isDigit(domain.charAt(3)) &&
								Character.isDigit(domain.charAt(4)) && Character.isLetter(domain.charAt(5)) &&
								Character.isLetter(domain.charAt(6))){
								result = "";
							}else{
								result = "Un dominio nuevo sigue el formato AA111AA";
							}
						}else{
							result = "Un dominio nuevo sigue el formato AA111AA";
						}
					}else{
						if(domain.length() == 7){
							if(	Character.isLetter(domain.charAt(0)) && Character.isDigit(domain.charAt(1)) &&
								Character.isDigit(domain.charAt(2)) && Character.isDigit(domain.charAt(3)) &&
								Character.isLetter(domain.charAt(4)) && Character.isLetter(domain.charAt(5)) &&
								Character.isLetter(domain.charAt(6))){
								result = "";
							}else{
								result = "Un dominio nuevo de moto sigue el formato A111AAA";
							}
						}else{
							result = "Un dominio nuevo de moto sigue el formato A111AAA";
						}
					}
				break;
				
				case "viejo":
					if(	!typeVehicle.trim().toLowerCase().equals("moto") &&
						!typeVehicle.trim().toLowerCase().equals("motos")){
						if(domain.length() == 6){
							if(	Character.isLetter(domain.charAt(0)) && Character.isLetter(domain.charAt(1)) &&
								Character.isLetter(domain.charAt(2)) && Character.isDigit(domain.charAt(3)) &&
								Character.isDigit(domain.charAt(4)) && Character.isDigit(domain.charAt(5))){
								result = "";
							}else{
								result = "Un dominio viejo sigue el formato AAA111";
							}
						}else{
							result = "Un dominio viejo sigue el formato AAA111";
						}
					}else{
						if(domain.length() == 6){
							if(	Character.isDigit(domain.charAt(0)) && Character.isDigit(domain.charAt(1)) &&
								Character.isDigit(domain.charAt(2)) && Character.isLetter(domain.charAt(3)) &&
								Character.isLetter(domain.charAt(4)) && Character.isLetter(domain.charAt(5))){
								result = "";
							}else{
								result = "Un dominio viejo de moto sigue el formato 111AAA";
							}
						}else{
							result = "Un dominio viejo de moto sigue el formato 111AAA";
						}
					}
				break;
				
				case "internacional":
					//Hasta 10 digitos
					if(domain.length() > 0 && domain.length() <= 9){
						result = "";
					}else{
						result = "Un dominio "+typeDomain+" debe conter de 1 a 9 caracteres";
					}
				break;
				
				case "diplomático":
					//Hasta 10 digitos
					if(country.toLowerCase().equals("argentina")){
						if(domain.length() > 0 && domain.length() <= 10){
							result = "";
						}else{
							result = "Un dominio "+typeDomain+" debe conter de 1 a 10 caracteres";
						}
					}else{
						if(domain.length() > 0 && domain.length() <= 9){
							result = "";
						}else{
							result = "Un dominio "+typeDomain+" debe conter de 1 a 9 caracteres";
						}
					}
				break;
				
				case "sin patente":
					result = "block";
				break;
				
				default:
					result = "";
				break;
			}
		}catch(Exception e){
			logError(context, "Utils:validateDomain - Exception: ", e);
		}
		
		if(Common.DEBUG){
			System.out.println("Tipo: "+typeDomain+" Vehículo: "+typeVehicle+" Dominio: "+domain+" length: "+domain.length());
		}
		
		return result;
	}
	
	public static void generateVUOtp(final Context activity, final CallBackListener listener){
		try{
			//Ya no hace falta sincronizar el reloj
			SharedPreferences preferences = activity.getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			String result = VUMobileTokenSDK.getOriginCounter(preferences.getString(Common.PREF_SEED, ""), preferences.getString(Common.PREF_CURRENT_USER, Common.DEFAULT_USER));
			long originCounter;
			
			try{
				originCounter = Long.valueOf(result);
			}catch(Exception e){
				originCounter = 1;
			}
			
			long counter = preferences.getLong(User.KEY_OTPCONSUMED, 1)+1;
			
			if(Common.DEBUG){
				System.out.println("ORIGIN_COUNTER: "+originCounter+" COUNTER_PREF: "+preferences.getLong("originCounter", 1)+" COUNTER: "+counter);
			}
			
			//Si estos putos de VU no aumentan su propio contador lo voy a hacer yo
			if(preferences.getLong("originCounter", 1) == originCounter && originCounter != 1){
				originCounter = originCounter+1;
			}else{
				if(preferences.getLong("originCounter", 1) > originCounter){
					originCounter = preferences.getLong("originCounter", 1)+1;
				}
			}
			
			//Camino hacia el otp maldito
			String decryptedSeed = VUMobileTokenSDK.decryptedSeed(preferences.getString(Common.PREF_SEED, ""), preferences.getString(Common.PREF_CURRENT_USER, Common.DEFAULT_USER));
			
			if(Common.DEBUG){
				System.out.println("DECREYPTED_SEED: "+decryptedSeed);
			}
			
			if(!Utils.isEmpty(decryptedSeed)){
				String otp = VUMobileTokenSDK.genHotp(decryptedSeed, originCounter+counter);
				if(Common.DEBUG){
					System.out.println("VUOTP: "+otp);
				}
				
				if(!Utils.isEmpty(otp) && originCounter >= 1 && counter >= 1){
					preferences.edit().putString(User.KEY_TOTP, otp).putLong(User.KEY_OTPCONSUMED, counter).putLong("originCounter", originCounter).apply();
				}else{
					if(!Utils.isEmpty(otp) && counter >= 1){
						preferences.edit().putString(User.KEY_TOTP, otp).putLong(User.KEY_OTPCONSUMED, counter).apply();
					}
				}
			}
		}catch(Exception e){
			logError(activity, "Utils:generateVUOtp - ", e);
		}finally{
			if(listener != null){
				listener.invoke();
			}
		}
	}
	
	public static SeedCallback VUCallback(final Activity activity){
		return new SeedCallback(){
			@Override
			public void onResultsAvailable(String encryptedSeed){
				try{
					if(Common.DEBUG){
						Toast.makeText(activity, "Autenticación exitosa OK: "+encryptedSeed, Toast.LENGTH_SHORT).show();
						System.out.println("Token: "+encryptedSeed);
					}
					
					SharedPreferences preferences = activity.getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
					SharedPreferences.Editor editor = preferences.edit();
					editor.putString(Common.PREF_SEED, encryptedSeed);
					editor.apply();
				}catch(Exception e){
					logError(activity, "Utils:VUCallback:onResultsAvailable - ", e);
				}
			}
			
			@Override
			public void onError(Throwable throwable){
				try{
					if(Common.DEBUG){
						Toast.makeText(activity, "ERROR: "+throwable.getMessage(), Toast.LENGTH_SHORT).show();
					}
					
					if(throwable instanceof Exception){
						Utils.logError(activity, "MainActivity:onErrorVU - ", (Exception) throwable);
					}
				}catch(Exception e){
					logError(activity, "Utils:VUCallback:onError - ", e);
				}
			}
		};
	}
	
	/**
	 * Verifica si el string recibido es un link a un recurso web
	 * @param link
	 * @return
	 */
	public static boolean isLink(final String link){
		return (!isEmpty(link) && link.trim().toLowerCase().startsWith("http"));
	}
	
	public static String getBase64FromFile(String path, Context context){
		try{
			File file					= new File(path);
			InputStream inputStream		= new FileInputStream(file);
			ByteArrayOutputStream bos	= new ByteArrayOutputStream();
			byte[] b					= new byte[1024 * 11];
			int bytesRead				= 0;
			
			while((bytesRead = inputStream.read(b)) != -1){
				bos.write(b, 0, bytesRead);
			}
			
			return Base64.encodeToString(bos.toByteArray(), Base64.NO_WRAP);
		}catch(Exception e){
			logError(context, "Utils:getBase64FromFile - Exception:", e);
		}
		
		return "";
	}
	
	public static String getBase64FromBitmap(Bitmap bitmap){
		ByteArrayOutputStream byteArrayOutputStream	= new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, Common.RATIO_COMPRESS_BITMAP, byteArrayOutputStream);
		byte[] byteArray							= byteArrayOutputStream.toByteArray();
		return Base64.encodeToString(byteArray, Base64.DEFAULT);
	}
}