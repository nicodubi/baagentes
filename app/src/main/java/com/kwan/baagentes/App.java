package com.kwan.baagentes;

import android.app.Activity;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.boa.utils.Common;
import com.boa.utils.Utils;
import com.google.firebase.FirebaseApp;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by davo on 01/07/2018.
 */
public class App extends MultiDexApplication{
	private static App instance;
	private static Activity currentActivity;

	public static Context getContext(){
		return instance;
	}

	public static App getInstance(){
		return instance;
	}

	@Override
	public void onCreate(){
		FirebaseApp.initializeApp(this);

		try{
			if(!Common.DEBUG){
				//Fabric.with(this, new Crashlytics());
			}

			instance = this;
			super.onCreate();
			Realm.init(this);
			RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
			Realm.setDefaultConfiguration(realmConfiguration);
			Realm realm = Realm.getDefaultInstance();
			realm.setAutoRefresh(true);
			realm.close();
		}catch(Exception e){
			Utils.logError(this, "App - Exception: ", e);
		}
	}
}