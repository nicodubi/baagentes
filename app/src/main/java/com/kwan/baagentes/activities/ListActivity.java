package com.kwan.baagentes.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.boa.utils.Utils;
import com.kwan.baagentes.R;
import com.kwan.baagentes.adapters.ListAdapter;
import io.realm.Realm;

/**
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 4 nov 2017.
 */
public class ListActivity extends AppCompatActivity{
	private RecyclerView rvList;
	private ListAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		try{
			setContentView(R.layout.activity_list);
			ImageView ibBack	= (ImageView) findViewById(R.id.ibBack);
			ImageView ivProfile	= (ImageView) findViewById(R.id.ivProfile);
			ImageView ivSList	= (ImageView) findViewById(R.id.ivSecurity);
			TextView txtTitle	= (TextView) findViewById(R.id.txtTitle);
			rvList				= (RecyclerView) findViewById(R.id.rvList);
			ivProfile.setVisibility(ImageView.GONE);
			ivSList.setVisibility(ImageView.GONE);
			txtTitle.setText("Listado de actas");
			Realm.init(this);
			Realm realm = Realm.getDefaultInstance();
			realm.close();
			ibBack.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View view){
					startActivity(new Intent(ListActivity.this, MainActivity.class));
				}
			});
			LinearLayoutManager layoutManager	= new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
			rvList.setLayoutManager(layoutManager);
			rvList.setHasFixedSize(true);
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onCreate - ", e);
		}
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		try{
			adapter = new ListAdapter(this);
			rvList.setAdapter(adapter);
			Utils.checkVersion(this, false);
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onResume - ", e);
		}
	}
}