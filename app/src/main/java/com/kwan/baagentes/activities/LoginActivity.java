package com.kwan.baagentes.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.boa.servicies.NetworkStateReceiver;
import com.boa.utils.Common;
import com.boa.utils.Utils;
import com.kwan.baagentes.R;
import com.kwan.baagentes.services.LoginTask;

import io.realm.Realm;

/**
 * Manejador de pantalla para iniciar sesión
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 25/03/2017.
 */
public class LoginActivity extends AppCompatActivity{
	private EditText		editUser, editPass;
	private int				originalSoftInputMode;
	private RelativeLayout	rlUser;

	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		try{
			setContentView(R.layout.activity_login);
			Utils.checkSession(this, true);
			SharedPreferences preferences	= getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			editUser						= (EditText) findViewById(R.id.editUser);
			editPass						= (EditText) findViewById(R.id.editPass);
			rlUser							= (RelativeLayout) findViewById(R.id.rlUser);
			TextView txtTitle				= (TextView) findViewById(R.id.txtTitle);
			ImageView ibBack				= (ImageView) findViewById(R.id.ibBack);
			ibBack.setVisibility(ImageView.GONE);
			//Agregado para ver versión de app
			String version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			preferences.edit().putString(Common.PREF_APPVERSION, version).apply();
			txtTitle.setText(getString(R.string.app_name)+" "+version);
			editPass.setOnEditorActionListener(new TextView.OnEditorActionListener(){
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
					if(actionId == EditorInfo.IME_ACTION_SEND){
						login(v);
						return true;
					}

					return false;
				}
			});
			editPass.setTypeface(Typeface.DEFAULT);
			editPass.setTransformationMethod(new PasswordTransformationMethod());
			Realm.init(this);
			Realm realm = Realm.getDefaultInstance();

			//Por si se deslogueo sugerimos las credenciales usadas
			if(Common.DEBUG){
				editUser.setText(Common.DEFAULT_USER);
				editPass.setText(Common.DEFAULT_PASS);
			}else{
				editUser.setText(preferences.getString(Common.PREF_CURRENT_USER, ""));
			}

			realm.close();
			//Si ya está acá es porque no hace falta iniciar session de nuevo
			SharedPreferences.Editor editor = preferences.edit();
			editor.putBoolean(Common.PREF_UPGRADE + version, true);
			editor.apply();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onCreate - Exception: ", e);
		}
	}

	public void login(View view){
		try{
			Utils.hideSoftKeyboard(this, originalSoftInputMode);

			if(Utils.checkVersion(this, true)){
				//Mostrar advertencia y no dejar usar
				Utils.showVersionDialog(this);
			}else{
				if(Utils.isEmpty(editUser.getText().toString()) || Utils.isEmpty(editPass.getText().toString())){
					Utils.showAlertDialog(this, "", getString(R.string.bad_login));
				}else{
					//La primer parte de VU se hace desde api al hacer login
					if(NetworkStateReceiver.isConnected(this)){
						new LoginTask(this, editUser.getText().toString().trim(), editPass.getText().toString().trim()).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
					}else{
						Utils.showAlertDialog(this, "", "No tienes conexión con el servidor");
					}
				}
			}
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":login - Exception: ", e);
		}
	}

	public void register(View view){
	}

	@Override
	public void onResume(){
		super.onResume();

		try{
			Realm.init(this);
			Window window			= getWindow();
			originalSoftInputMode	= window.getAttributes().softInputMode;
			window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

			//Probar con una api así sabemos si se actualizó o no para dejar pasar
			new Thread(new Runnable(){
				@Override
				public void run(){
					try{
						if(Utils.checkVersion(LoginActivity.this, true)){
							//Mostrar advertencia y no dejar usar
							LoginActivity.this.runOnUiThread(new Runnable(){
								@Override
								public void run(){
									Utils.showVersionDialog(LoginActivity.this);
								}
							});
						}
					}catch(Exception e){
						Utils.logError(LoginActivity.this, getLocalClassName()+":onResume:start - Exception: ", e);
					}
				}
			}).start();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onResume - Exception: ", e);
		}
	}
}