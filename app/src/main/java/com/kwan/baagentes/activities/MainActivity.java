package com.kwan.baagentes.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.boa.interfaces.CallBackValueListener;
import com.boa.multilistfilter.KeyPairBoolData;
import com.boa.multilistfilter.MultiSpinnerSearch;
import com.boa.multilistfilter.SpinnerListener;
import com.boa.servicies.AppLocationService;
import com.boa.utils.Api;
import com.boa.utils.Common;
import com.boa.utils.Utils;
import com.kwan.baagentes.R;
import com.kwan.baagentes.data.Brand;
import com.kwan.baagentes.data.Category;
import com.kwan.baagentes.data.Color;
import com.kwan.baagentes.data.Company;
import com.kwan.baagentes.data.Country;
import com.kwan.baagentes.data.Department;
import com.kwan.baagentes.data.Infringement;
import com.kwan.baagentes.data.Line;
import com.kwan.baagentes.data.Locality;
import com.kwan.baagentes.data.Model;
import com.kwan.baagentes.data.Municipality;
import com.kwan.baagentes.data.Province;
import com.kwan.baagentes.data.Street;
import com.kwan.baagentes.data.TicketB;
import com.kwan.baagentes.data.TypeDocument;
import com.kwan.baagentes.data.TypeInfringement;
import com.kwan.baagentes.data.TypeVehicle;
import com.kwan.baagentes.data.User;
import com.kwan.baagentes.services.PopulateTicketsTask;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Manejador de pantalla para cargar infracciones (pantalla principal).
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 25/03/2017.
 */
public class MainActivity extends AppCompatActivity implements LocationListener{
	private SharedPreferences		preferences;
	private int						originalSoftInputMode;
	private String					strTypesInfringement = "", strMunicipality = "", otherMunicipality = "";
	private long					ticketB = 0, idInfringement = 0, ticketBOriginal = 0;
	private double					lat = Common.DEFAULT_LAT, lng = Common.DEFAULT_LON;
	private boolean					hasSignature = false, isErasing = false;
	private ArrayList<String>		data = new ArrayList<>();
	private List<String>			listBrands = new ArrayList<>(), listCategories = new ArrayList<>(), listColors = new ArrayList<>(), listCopies = new ArrayList<>(), listCountries = new ArrayList<>(),
		listDependences = new ArrayList<>(), listLines = new ArrayList<>(), listLocalities = new ArrayList<>(), listModels = new ArrayList<>(), listMunicipalities = new ArrayList<>(),
		listTypesDocument = new ArrayList<>(), listTypesDomain = new ArrayList<>(), listTypesInfringement = new ArrayList<>(), listTypesVehicle = new ArrayList<>(), listProvinces = new ArrayList<>(),
		listCompanies = new ArrayList<>();
	private RelativeLayout			listLine, listDependence, listTypeDocumentOwner, rlVehicleZ, rlImputedZ, rlCinemometer, listCompany, rlImputed, rlAll, rlRemissionDistance, listDependenceDistance;
	private TextView				txtTypeDomain, txtTypeVehicle, txtLine, txtTypeDocument, txtCategory, txtDependence, txtTypeDocumentOwner, txtColor, txtVehicleZ, txtImputedZ, txtCinemometer,
		txtTitle, txtCompany, txtImputed, txtCopy, txtDependenceDistance;
	private EditText				editDomain, editInternal, editName, editDocument, editOwner, editDocumentOwner, editZip, editLicenseExpiration, editCinemometerNumber, editCinemometerValue,
		editCinemometerBrand, editCinemometerModel, editLicense, editTicketNumber, editFloor, editOffice, editHeight, editTicketNumberIncluded, editDate, editHeightPlace, editObservations,
		editTicketNumberContravention, editHour, editReference, editLastName, editEmail;
	private AutoCompleteTextView	editModel, editPlace, editAddress, editProvince, editLocality, editStreet1, editStreet2, editBrand, editCountry;
	private RadioButton				rbF, rbM, rbFace, rbDistance, rbHold, rbNoHold, rbRemission, rbNoRemission, rbOwner, rbNoOwner, rbCondition, rbNoCondition, rbRemissionDistance,
		rbNoRemissionDistance;
	private MultiSpinnerSearch		multiTypeInfringement, multiMunicipality;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		try{
			setContentView(R.layout.activity_main);
			preferences						= getApplicationContext().getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			editModel						= (AutoCompleteTextView) findViewById(R.id.editModel);
			editPlace						= (AutoCompleteTextView) findViewById(R.id.editPlace);
			editAddress						= (AutoCompleteTextView) findViewById(R.id.editAddress);
			editProvince					= (AutoCompleteTextView) findViewById(R.id.editProvince);
			editLocality					= (AutoCompleteTextView) findViewById(R.id.editLocality);
			editStreet1						= (AutoCompleteTextView) findViewById(R.id.editStreet1);
			editStreet2						= (AutoCompleteTextView) findViewById(R.id.editStreet2);
			editBrand						= (AutoCompleteTextView) findViewById(R.id.editBrand);
			editCountry						= (AutoCompleteTextView) findViewById(R.id.editCountry);
			editDomain						= (EditText) findViewById(R.id.editDomain);
			editInternal					= (EditText) findViewById(R.id.editInternal);
			editName						= (EditText) findViewById(R.id.editName);
			editLastName					= (EditText) findViewById(R.id.editLastName);
			editDocument					= (EditText) findViewById(R.id.editDocument);
			editOwner						= (EditText) findViewById(R.id.editOwner);
			editDocumentOwner				= (EditText) findViewById(R.id.editDocumentOwner);
			editZip							= (EditText) findViewById(R.id.editZip);
			editLicenseExpiration			= (EditText) findViewById(R.id.editLicenseExpiration);
			editCinemometerNumber			= (EditText) findViewById(R.id.editCinemometerNumber);
			editCinemometerValue			= (EditText) findViewById(R.id.editCinemometerValue);
			editCinemometerBrand			= (EditText) findViewById(R.id.editCinemometerBrand);
			editCinemometerModel			= (EditText) findViewById(R.id.editCinemometerModel);
			editLicense						= (EditText) findViewById(R.id.editLicense);
			editTicketNumber				= (EditText) findViewById(R.id.editTicketNumber);
			editFloor						= (EditText) findViewById(R.id.editFloor);
			editHeight						= (EditText) findViewById(R.id.editHeight);
			editOffice						= (EditText) findViewById(R.id.editOffice);
			editTicketNumberIncluded		= (EditText) findViewById(R.id.editTicketNumberIncluded);
			editDate						= (EditText) findViewById(R.id.editDate);
			editHeightPlace					= (EditText) findViewById(R.id.editHeightPlace);
			editObservations				= (EditText) findViewById(R.id.editObservations);
			editTicketNumberContravention	= (EditText) findViewById(R.id.editTicketNumberContravention);
			editHour						= (EditText) findViewById(R.id.editHour);
			editReference					= (EditText) findViewById(R.id.editReference);
			editEmail						= (EditText) findViewById(R.id.editEmail);
			ImageView ibBack				= (ImageView) findViewById(R.id.ibBack);
			ImageView ivProfile				= (ImageView) findViewById(R.id.ivProfile);
			ImageView ivSecurity			= (ImageView) findViewById(R.id.ivSecurity);
			rbF								= (RadioButton) findViewById(R.id.rbF);
			rbM								= (RadioButton) findViewById(R.id.rbM);
			rbFace							= (RadioButton) findViewById(R.id.rbFace);
			rbDistance						= (RadioButton) findViewById(R.id.rbDistance);
			rbHold							= (RadioButton) findViewById(R.id.rbHold);
			rbNoHold						= (RadioButton) findViewById(R.id.rbNoHold);
			rbRemission						= (RadioButton) findViewById(R.id.rbRemission);
			rbNoRemission					= (RadioButton) findViewById(R.id.rbNoRemission);
			rbOwner							= (RadioButton) findViewById(R.id.rbOwner);
			rbNoOwner						= (RadioButton) findViewById(R.id.rbNoOwner);
			rbCondition						= (RadioButton) findViewById(R.id.rbCondition);
			rbNoCondition					= (RadioButton) findViewById(R.id.rbNoCondition);
			rbRemissionDistance				= (RadioButton) findViewById(R.id.rbRemissionDistance);
			rbNoRemissionDistance			= (RadioButton) findViewById(R.id.rbNoRemissionDistance);
			rlAll							= (RelativeLayout) findViewById(R.id.rlAll);
			listLine						= (RelativeLayout) findViewById(R.id.listLine);
			listDependence					= (RelativeLayout) findViewById(R.id.listDependence);
			listDependenceDistance			= (RelativeLayout) findViewById(R.id.listDependenceDistance);
			listTypeDocumentOwner			= (RelativeLayout) findViewById(R.id.listTypeDocumentOwner);
			rlVehicleZ						= (RelativeLayout) findViewById(R.id.rlVehicleZ);
			rlImputedZ						= (RelativeLayout) findViewById(R.id.rlImputedZ);
			rlCinemometer					= (RelativeLayout) findViewById(R.id.rlCinemometer);
			listCompany						= (RelativeLayout) findViewById(R.id.listCompany);
			rlImputed						= (RelativeLayout) findViewById(R.id.rlImputed);
			rlRemissionDistance				= (RelativeLayout) findViewById(R.id.rlRemissionDistance);
			txtTitle						= (TextView) findViewById(R.id.txtTitle);
			txtCinemometer					= (TextView) findViewById(R.id.txtCinemometer);
			txtImputedZ						= (TextView) findViewById(R.id.txtImputedZ);
			txtVehicleZ						= (TextView) findViewById(R.id.txtVehicleZ);
			txtColor						= (TextView) findViewById(R.id.txtColor);
			txtCopy							= (TextView) findViewById(R.id.txtCopy);
			txtTypeDocumentOwner			= (TextView) findViewById(R.id.txtTypeDocumentOwner);
			txtDependence					= (TextView) findViewById(R.id.txtDependence);
			txtDependenceDistance			= (TextView) findViewById(R.id.txtDependenceDistance);
			txtCategory						= (TextView) findViewById(R.id.txtCategory);
			txtTypeDocument					= (TextView) findViewById(R.id.txtTypeDocument);
			txtLine							= (TextView) findViewById(R.id.txtLine);
			txtTypeVehicle					= (TextView) findViewById(R.id.txtTypeVehicle);
			txtTypeDomain					= (TextView) findViewById(R.id.txtTypeDomain);
			txtCompany						= (TextView) findViewById(R.id.txtCompany);
			txtImputed						= (TextView) findViewById(R.id.txtImputed);
			multiTypeInfringement			= (MultiSpinnerSearch) findViewById(R.id.multiTypeInfringement);
			multiMunicipality				= (MultiSpinnerSearch) findViewById(R.id.multiMunicipality);
			final Activity activity			= this;
			txtTitle.setText("ACTA COMPROBACIÓN B");
			ivProfile.setVisibility(ImageView.VISIBLE);
			ivSecurity.setVisibility(ImageView.VISIBLE);
			Utils.saveImei(this);
			String version = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
			//Init autocomplete de calles
			Realm.init(this);
			final Realm realm = Realm.getDefaultInstance();
			String logoutStr = "Se cerró la sesión de la App. Por favor, ingresá nuevamente para realizar el acta";
			
			if(!preferences.getBoolean(Common.PREF_UPGRADE + version, false)){
				SharedPreferences.Editor editor = preferences.edit();
				editor.putBoolean(Common.PREF_UPGRADE + version, true);
				editor.apply();
				
				if(realm.where(Infringement.class).count() > 0){
					Toast.makeText(this, logoutStr, Toast.LENGTH_SHORT).show();
					logout(activity);
				}
			}else{
				if(preferences.getLong(Common.PREF_SESSION_TS, 0) == 0){
					Toast.makeText(this, logoutStr, Toast.LENGTH_SHORT).show();
					logout(activity);
				}else{
					//Cerrar sesión luego de 8 horas
					if(Utils.getDifferenceInHours(preferences.getLong(Common.PREF_SESSION_TS, 0)) >= 8){
						Toast.makeText(this, logoutStr, Toast.LENGTH_SHORT).show();
						logout(activity);
					}
				}
			}
			
			if(getIntent() != null){
				idInfringement = getIntent().getLongExtra(Common.KEY_ID, 0);
			}
			
			new PopulateTicketsTask(this, null, false, "", new CallBackValueListener(){
				@Override
				public void invoke(final String result){
					activity.runOnUiThread(new Runnable(){
						@Override
						public void run(){
							Utils.checkVersion(MainActivity.this, false);
							Realm realm = Realm.getDefaultInstance();
							listTypesInfringement.clear();
							RealmResults<TypeInfringement> typeInfringements = realm.where(TypeInfringement.class).findAllSorted(Common.KEY_CODE);
							
							if(typeInfringements.size() > 0){
								for(TypeInfringement typeInfringement : typeInfringements){
									listTypesInfringement.add(typeInfringement.getCodigo()+" "+typeInfringement.getDescripcion());
								}
							}
							
							realm.close();
							editTicketNumber			= (EditText) activity.findViewById(R.id.editTicketNumber);
							editTicketNumberIncluded	= (EditText) activity.findViewById(R.id.editTicketNumberIncluded);
							selectTicketNumber();
							refreshTickets();
							rbFace.setEnabled(true);
							rbDistance.setEnabled(true);
							//Verificamos que si no hay actas disponibles no se pueda elegir
							if(idInfringement == 0){
								if(result.equals("ERROR")){
									Utils.showAlertDialog(MainActivity.this, "No hay actas disponibles", "Contacte al equipo de tecnología para actualizar sus datos");
									rbFace.setEnabled(false);
									rbDistance.setEnabled(false);
								}else{
									askModality();
								}
							}
						}
					});
				}
			}).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
			ibBack.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View view){
					onLogout();
				}
			});
			ivProfile.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View view){
					if(Common.DEBUG){
						Realm realm1 = Realm.getDefaultInstance();
						RealmResults<Infringement> infringements = realm1.where(Infringement.class).findAllSorted(Common.KEY_ID, Sort.ASCENDING);
						Intent intent = new Intent(activity, SummaryActivity.class);
						intent.putExtra(Common.KEY_ID, infringements.get(0).getId());
						activity.startActivity(intent);
						activity.finish();
					}else{
						Intent intent = new Intent(activity, ProfileActivity.class);
						intent.putExtra(Common.KEY_ID, idInfringement);
						activity.startActivity(intent);
						activity.finish();
					}
				}
			});
			ivSecurity.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					startActivity(new Intent(MainActivity.this, ListActivity.class));
				}
			});
			Utils.expandTouchArea(ibBack, 200, this);
			//Limpiar listas de cada spinner
			listBrands.clear();
			listCategories.clear();
			listColors.clear();
			listCopies.clear();
			listCountries.clear();
			listDependences.clear();
			listLines.clear();
			listLocalities.clear();
			listModels.clear();
			listMunicipalities.clear();
			listTypesDocument.clear();
			listTypesDomain.clear();
			listTypesInfringement.clear();
			listTypesVehicle.clear();
			listCompanies.clear();
			listProvinces.clear();
			//Sin placeholders
			hideSoftKeyboard();
			//Copy domain
			listCopies.add("O");
			listCopies.add("D");
			listCopies.add("T");
			listCopies.add("C");
			txtCopy.setText("O");
			txtCopy.setTextColor(android.graphics.Color.BLACK);
			//Init spinners
			RealmResults<Brand> brands = realm.where(Brand.class).findAllSorted(Brand.KEY_MARCA);
			
			if(brands.size() > 0) {
				for(Brand brand : brands) {
					listBrands.add(brand.getMarca());
				}
			}
			
			RealmResults<Category> categories = realm.where(Category.class).findAllSorted(Common.KEY_DESCRIPTION);
			
			if(categories.size() > 0){
				for(Category category : categories){
					listCategories.add(category.getDescripcion());
				}
			}
			
			final RealmResults<Color> colors = realm.where(Color.class).findAllSorted(Color.KEY_VALOR);
			
			if(colors.size() > 0){
				for(Color color : colors){
					listColors.add(color.getValor());
				}
			}
			
			RealmResults<Country> countries = realm.where(Country.class).findAllSorted(Common.KEY_DESCRIPTION);
			
			if(countries.size() > 0){
				for(Country country : countries){
					listCountries.add(country.getDescripcion());//Ahora también se puede elegir diplomaticos de argentina
				}
			}
			
			RealmResults<Department> dependences = realm.where(Department.class).findAllSorted(Common.KEY_DESCRIPTION);
			
			if(dependences.size() > 0){
				for(Department department : dependences){
					listDependences.add(department.getDescripcion());
				}
			}
			
			//Agregar valor OTRO a Dependencia
			listDependences.add("Otro");
			RealmResults<Company> lines	= realm.where(Company.class).findAllSorted(Line.KEY_LINE).distinct(Line.KEY_LINE);
			String[] sortLine			= {Line.KEY_LINE};
			Sort[] sorts				= {Sort.ASCENDING};
			
			if(lines.size() > 0){
				lines.sort(sortLine, sorts);
				
				for(Company line : lines){
					if(!Utils.isEmpty(line.getLinea())){
						int linea = 0;
						try{
							linea = Integer.parseInt(line.getLinea());
						}catch(Exception e){
						}
						
						if(linea > 0){
							listLines.add(line.getLinea());
						}
					}
				}
			}
			
			RealmResults<Locality> localities = realm.where(Locality.class).findAllSorted(Common.KEY_DESCRIPTION);
			
			if(localities.size() > 0){
				for(Locality locality : localities){
					listLocalities.add(locality.getDescripcion());
				}
			}
			
			RealmResults<Model> models = realm.where(Model.class).findAllSorted(Model.KEY_MODELO);//TODO filter by brand
			
			if(models.size() > 0){
				for(Model model : models){
					if(!Utils.isEmpty(model.getModelo())){
						listModels.add(model.getModelo());
					}
				}
			}
			
			RealmResults<Municipality> municipalities = realm.where(Municipality.class).findAllSorted(Common.KEY_DESCRIPTION).distinct(Common.KEY_DESCRIPTION);
			String[] sortMuni	= {Common.KEY_DESCRIPTION};
			Sort[] sortsMuni	= {Sort.ASCENDING};
			
			if(municipalities.size() > 0){
				municipalities.sort(sortMuni, sortsMuni);
				
				for(Municipality municipality : municipalities){
					if(!Utils.isEmpty(municipality.getDescripcion())){
						listMunicipalities.add(municipality.getDescripcion());
					}
				}
			}
			
			RealmResults<TypeDocument> typeDocuments = realm.where(TypeDocument.class).findAllSorted(TypeDocument.KEY_DOCUMENTO);
			
			if(typeDocuments.size() > 0){
				for(TypeDocument typeDocument : typeDocuments){
					listTypesDocument.add(typeDocument.getDocumento());
				}
			}
			
			//Pantentes
			listTypesDomain.add("Nuevo");
			listTypesDomain.add("Viejo");
			listTypesDomain.add("Internacional");
			listTypesDomain.add("Diplomático");
			listTypesDomain.add("Sin patente");
			final RealmResults<TypeInfringement> typeInfringements = realm.where(TypeInfringement.class).findAll();
			
			if(typeInfringements.size() > 0){
				for(TypeInfringement typeInfringement : typeInfringements){
					listTypesInfringement.add(typeInfringement.getCodigo()+" "+typeInfringement.getDescripcion());
				}
			}
			
			RealmResults<TypeVehicle> typeVehicles = realm.where(TypeVehicle.class).notEqualTo("other", Common.BOOL_YES).findAllSorted(Common.KEY_DESCRIPTION);
			
			if(typeVehicles.size() > 0){
				for(TypeVehicle typeVehicle : typeVehicles){
					listTypesVehicle.add(typeVehicle.getDescripcion());
				}
			}
			
			RealmResults<Province> provinces = realm.where(Province.class).findAllSorted(Province.KEY_PROVINCIA);
			
			if(provinces.size() > 0){
				for(Province province : provinces){
					listProvinces.add(province.getProvincia());
				}
			}
			
			RealmResults<Company> companies = realm.where(Company.class).findAllSorted(Company.KEY_EMPRESA).distinct(Company.KEY_EMPRESA);
			
			if(companies.size() > 0){
				for(Company company : companies){
					if(!Utils.isEmpty(company.getEmpresa())){
						listCompanies.add(company.getEmpresa());
					}
				}
			}
			
			integrateStreet(editPlace, 1);
			integrateStreet(editStreet1, 2);
			integrateStreet(editStreet2, 3);
			integrateStreet(editAddress, 4);
			
			//Países
			final ArrayAdapter<String> adapterCountry = new ArrayAdapter<>(this, R.layout.item_location, listCountries.toArray(new String[listCountries.size()]));
			editCountry.setAdapter(adapterCountry);
			editCountry.setOnEditorActionListener(new TextView.OnEditorActionListener(){
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
					if(actionId == EditorInfo.IME_ACTION_NEXT){
						hideSoftKeyboard();
						return true;
					}
					
					return false;
				}
			});
			editCountry.setOnItemClickListener(new AdapterView.OnItemClickListener(){
				@Override
				public void onItemClick(final AdapterView<?> adapterView, final View view, final int i, final long l){
					editCountry.setSelection(editCountry.getText().toString().length());
					editCountry.setAdapter(null);
					hideSoftKeyboard();
				}
			});
			editCountry.addTextChangedListener(new TextWatcher(){
				@Override
				public void beforeTextChanged(final CharSequence charSequence, final int start, final int count, final int after){
				}
				
				@Override
				public void onTextChanged(final CharSequence charSequence, final int start, final int before, final int count){
				}
				
				@Override
				public void afterTextChanged(final Editable editable){
					if(Utils.isEmpty(editable.toString())){
						editCountry.setAdapter(adapterCountry);
					}
				}
			});
			
			//Marcas
			final ArrayAdapter<String> adapterBrand = new ArrayAdapter<>(this, R.layout.item_location, listBrands.toArray(new String[listBrands.size()]));
			editBrand.setAdapter(adapterBrand);
			editBrand.setOnEditorActionListener(new TextView.OnEditorActionListener(){
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
					if(actionId == EditorInfo.IME_ACTION_NEXT){
						return true;
					}
					
					return false;
				}
			});
			editBrand.setOnItemClickListener(new AdapterView.OnItemClickListener(){
				@Override
				public void onItemClick(final AdapterView<?> adapterView, final View view, final int i, final long l){
					editBrand.setSelection(editBrand.getText().toString().length());
					editBrand.setAdapter(null);
					//Si en algún momento viene la relación modelos de una marca, filtrar
				}
			});
			editBrand.addTextChangedListener(new TextWatcher(){
				@Override
				public void beforeTextChanged(final CharSequence charSequence, final int start, final int count, final int after){
				}
				
				@Override
				public void onTextChanged(final CharSequence charSequence, final int start, final int before, final int count){
				}
				
				@Override
				public void afterTextChanged(final Editable editable){
					if(Utils.isEmpty(editable.toString())){
						editBrand.setAdapter(adapterBrand);
						//Soltar filtro si se pudo realizar
					}
				}
			});
			
			//Modelos
			final ArrayAdapter<String> adapterModel = new ArrayAdapter<>(this, R.layout.item_location, listModels.toArray(new String[listModels.size()]));
			editModel.setAdapter(adapterModel);
			editModel.setOnEditorActionListener(new TextView.OnEditorActionListener(){
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
					if(actionId == EditorInfo.IME_ACTION_NEXT){
						hideSoftKeyboard();
						return true;
					}
					
					return false;
				}
			});
			editModel.setOnItemClickListener(new AdapterView.OnItemClickListener(){
				@Override
				public void onItemClick(final AdapterView<?> adapterView, final View view, final int i, final long l){
					editModel.setSelection(editModel.getText().toString().length());
					editModel.setAdapter(null);
					hideSoftKeyboard();
				}
			});
			editModel.addTextChangedListener(new TextWatcher(){
				@Override
				public void beforeTextChanged(final CharSequence charSequence, final int start, final int count, final int after){
				}
				
				@Override
				public void onTextChanged(final CharSequence charSequence, final int start, final int before, final int count){
				}
				
				@Override
				public void afterTextChanged(final Editable editable){
					if(Utils.isEmpty(editable.toString())){
						editModel.setAdapter(adapterModel);
					}
				}
			});
			
			//Provincias
			final ArrayAdapter<String> adapterProvince = new ArrayAdapter<>(this, R.layout.item_location, listProvinces.toArray(new String[listProvinces.size()]));
			editProvince.setAdapter(adapterProvince);
			editProvince.setOnEditorActionListener(new TextView.OnEditorActionListener(){
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
					if(actionId == EditorInfo.IME_ACTION_SEARCH){
						hideSoftKeyboard();
						return true;
					}
					
					return false;
				}
			});
			editProvince.setOnItemClickListener(new AdapterView.OnItemClickListener(){
				@Override
				public void onItemClick(final AdapterView<?> adapterView, final View view, final int i, final long l){
					editProvince.setSelection(editProvince.getText().toString().length());
					editProvince.setAdapter(null);
					hideSoftKeyboard();
				}
			});
			editProvince.addTextChangedListener(new TextWatcher(){
				@Override
				public void beforeTextChanged(final CharSequence charSequence, final int start, final int count, final int after){
				}
				
				@Override
				public void onTextChanged(final CharSequence charSequence, final int start, final int before, final int count){
				}
				
				@Override
				public void afterTextChanged(final Editable editable){
					if(Utils.isEmpty(editable.toString())){
						editProvince.setAdapter(adapterProvince);
					}
				}
			});
			
			//Locality
			final ArrayAdapter<String> adapterLocality = new ArrayAdapter<>(this, R.layout.item_location, listLocalities.toArray(new String[listLocalities.size()]));
			editLocality.setAdapter(adapterLocality);
			editLocality.setOnEditorActionListener(new TextView.OnEditorActionListener(){
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
					if(actionId == EditorInfo.IME_ACTION_NEXT){
						hideSoftKeyboard();
						return true;
					}
					
					return false;
				}
			});
			editLocality.setOnItemClickListener(new AdapterView.OnItemClickListener(){
				@Override
				public void onItemClick(final AdapterView<?> adapterView, final View view, final int i, final long l){
					editLocality.setSelection(editLocality.getText().toString().length());
					editLocality.setAdapter(null);
					hideSoftKeyboard();
				}
			});
			editLocality.addTextChangedListener(new TextWatcher(){
				@Override
				public void beforeTextChanged(final CharSequence charSequence, final int start, final int count, final int after){
				}
				
				@Override
				public void onTextChanged(final CharSequence charSequence, final int start, final int before, final int count){
				}
				
				@Override
				public void afterTextChanged(final Editable editable){
					if(Utils.isEmpty(editable.toString())){
						editLocality.setAdapter(adapterLocality);
					}
				}
			});
			
			//Agregar barras a la fecha al escribir
			editLicenseExpiration.addTextChangedListener(new TextWatcher(){
				@Override
				public void beforeTextChanged(final CharSequence charSequence, final int start, final int count, final int after){
					if(count != 0){
						isErasing = true;
					}else{
						isErasing = false;
					}
				}
				
				@Override
				public void onTextChanged(final CharSequence charSequence, final int start, final int before, final int count){
				}
				
				@Override
				public void afterTextChanged(final Editable editable){
					if(isErasing){
						editable.clear();
						isErasing = false;
					}else{
						if(editable.toString().length() == 2 || editable.toString().length() == 5){
							editable.append("/");
						}
					}
				}
			});
			//Radios
			rbRemission.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(final CompoundButton compoundButton, final boolean b){
					if(b){
						listDependence.setVisibility(RelativeLayout.VISIBLE);
						rbNoRemission.setChecked(false);
					}
				}
			});
			rbNoRemission.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(final CompoundButton compoundButton, final boolean b){
					if(b){
						listDependence.setVisibility(RelativeLayout.GONE);
						txtDependence.setText("Dependencia");
						rbRemission.setChecked(false);
					}
				}
			});
			rbRemissionDistance.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(final CompoundButton compoundButton, final boolean b){
					if(b){
						listDependenceDistance.setVisibility(RelativeLayout.VISIBLE);
						rbNoRemissionDistance.setChecked(false);
					}
				}
			});
			rbNoRemissionDistance.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(final CompoundButton compoundButton, final boolean b){
					if(b){
						listDependenceDistance.setVisibility(RelativeLayout.GONE);
						txtDependenceDistance.setText("Dependencia");
						rbRemissionDistance.setChecked(false);
					}
				}
			});
			rbOwner.setChecked(true);
			rbOwner.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(final CompoundButton compoundButton, final boolean b){
					if(b){
						editOwner.setVisibility(EditText.GONE);
						listTypeDocumentOwner.setVisibility(RelativeLayout.GONE);
						editDocumentOwner.setVisibility(EditText.GONE);
						rbNoOwner.setChecked(false);
					}
				}
			});
			rbNoOwner.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(final CompoundButton compoundButton, final boolean b){
					if(b){
						editOwner.setVisibility(EditText.VISIBLE);
						listTypeDocumentOwner.setVisibility(RelativeLayout.VISIBLE);
						editDocumentOwner.setVisibility(EditText.VISIBLE);
						rbOwner.setChecked(false);
					}
				}
			});
			//Campo nuevo raro
			rbNoCondition.setChecked(true);
			rbCondition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(final CompoundButton compoundButton, final boolean b){
					if(b){
						rbNoCondition.setChecked(false);
					}
				}
			});
			rbNoCondition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(final CompoundButton compoundButton, final boolean b){
					if(b){
						rbCondition.setChecked(false);
					}
				}
			});
			rbHold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(final CompoundButton compoundButton, final boolean b){
					if(b){
						rbNoHold.setChecked(false);
					}
				}
			});
			rbNoHold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(final CompoundButton compoundButton, final boolean b){
					if(b){
						rbHold.setChecked(false);
					}
				}
			});
			rbF.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(final CompoundButton compoundButton, final boolean b){
					if(b){
						rbM.setChecked(false);
					}
				}
			});
			rbM.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(final CompoundButton compoundButton, final boolean b){
					if(b){
						rbF.setChecked(false);
					}
				}
			});
			rbFace.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(final CompoundButton compoundButton, final boolean b){
					if(b){
						txtImputed.setVisibility(TextView.VISIBLE);
						rlImputed.setVisibility(RelativeLayout.VISIBLE);
						rbDistance.setChecked(false);
						refreshTickets();
						clearInputs();
						rlRemissionDistance.setVisibility(RelativeLayout.GONE);
					}
				}
			});
			rbDistance.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
				@Override
				public void onCheckedChanged(final CompoundButton compoundButton, final boolean b){
					if(b){
						txtImputed.setVisibility(TextView.GONE);
						rlImputed.setVisibility(RelativeLayout.GONE);
						rbFace.setChecked(false);
						refreshTickets();
						clearInputs();
						rlRemissionDistance.setVisibility(RelativeLayout.VISIBLE);
					}
				}
			});
			//Filtros para asegurar formato de datos
			editInternal.setFilters(new InputFilter[]{Utils.getNumericFilter(), new InputFilter.LengthFilter(5)});
			editLicenseExpiration.setFilters(new InputFilter[]{Utils.getDateFilter(), new InputFilter.LengthFilter(10)});
			
			if(Common.DEBUG){
				Utils.showStatus(realm, this);
				System.out.println("OTP_CONSUMIDO: "+preferences.getLong(User.KEY_OTPCONSUMED, 1));
				System.out.println("ORIGIN_COUNTER: "+preferences.getLong("originCounter", 1));
				System.out.println("TOTP: "+preferences.getString(User.KEY_TOTP, ""));
			}
			
			realm.close();
			preValidateInputs();
			reload();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onCreate - Exception: ", e);
		}
	}
	
	public void clearInputs(){
		try{
			//Limpiar los campos ocultos por agente borracho
			editName.setText("");
			editLastName.setText("");
			editProvince.setText("");
			editAddress.setText("");
			editHeight.setText("");
			editFloor.setText("");
			editOffice.setText("");
			editLocality.setText("");
			txtTypeDocument.setText("Tipo de documento");
			editDocument.setText("");
			editLicense.setText("");
			txtCategory.setText(getString(R.string.prompt_category));
			editEmail.setText("");
			rbCondition.setChecked(false);
			rbNoCondition.setChecked(true);
			rbHold.setChecked(false);
			rbNoHold.setChecked(false);
			rbRemission.setChecked(false);
			rbNoRemission.setChecked(false);
			txtDependence.setText("Dependencia");
			//Se agrega remisión en actas a distancia
			rbRemissionDistance.setChecked(false);
			rbNoRemissionDistance.setChecked(false);
			txtDependenceDistance.setText("Dependencia");
			editTicketNumberIncluded.setText("");
			editTicketNumberContravention.setText("");
			editObservations.setText("");
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":clearInputs - Exception: ", e);
		}
	}
	
	public void reloadTicket(){
		try{
			//Ahora siempre va a viajar el número de acta
			selectTicketNumber();
			refreshTickets();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":reloadTicket - Exception: ", e);
		}
	}
	
	public void selectTicketNumber(){
		try{
			Realm realm						= Realm.getDefaultInstance();
			RealmResults<TicketB> ticketBs	= realm.where(TicketB.class).equalTo(TicketB.KEY_USED, Common.BOOL_NO).findAllSorted(Common.KEY_ID);
			
			if(ticketBs.size() > 0){
				ticketB = ticketBs.get(0).getId();
				
				if(ticketB == 0 && ticketBs.size() > 1){
					ticketB = ticketBs.get(1).getId();
				}
				
				//Evitar que un acta pueda tener un número ya usado
				if(realm.where(Infringement.class).equalTo(Infringement.KEY_TICKETNUMBER, ticketB).count() > 0){
					for(TicketB tb : ticketBs){
						if(tb.getUsed() == Common.BOOL_NO && realm.where(Infringement.class).equalTo(Infringement.KEY_TICKETNUMBER, tb.getId()).count() == 0){
							ticketB = tb.getId();
							break;
						}
					}
				}
			}
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":selectTicketNumber - Exception: ", e);
		}
	}
	
	public void filterDate(){
		try{
			//Si quedan vacios intentar llenarlos
			if(Utils.isEmpty(editDate.getText().toString().trim())){
				editDate.setText(Utils.getDatePhone(this));
			}
			
			if(Utils.isEmpty(editHour.getText().toString().trim())){
				String[] partTime = Utils.getDateTimePhone(this).split(" ");
				editHour.setText(partTime[1].substring(0, partTime[1].length()-3));
			}
			
			editDate.addTextChangedListener(new TextWatcher(){
				@Override
				public void beforeTextChanged(final CharSequence charSequence, final int start, final int count, final int after){
					if(count != 0){
						isErasing = true;
					}else{
						isErasing = false;
					}
				}
				
				@Override
				public void onTextChanged(final CharSequence charSequence, final int start, final int before, final int count){
				}
				
				@Override
				public void afterTextChanged(final Editable editable){
					if(isErasing){
						editable.clear();
						isErasing = false;
					}else{
						if(editable.toString().length() == 2 || editable.toString().length() == 5){
							editable.append("/");
						}
					}
				}
			});
			editHour.addTextChangedListener(new TextWatcher(){
				@Override
				public void beforeTextChanged(final CharSequence charSequence, final int start, final int count, final int after){
					if(count != 0){
						isErasing = true;
					}else{
						isErasing = false;
					}
				}
				
				@Override
				public void onTextChanged(final CharSequence charSequence, final int start, final int before, final int count){
				}
				
				@Override
				public void afterTextChanged(final Editable editable){
					if(isErasing){
						editable.clear();
						isErasing = false;
					}else{
						if(editable.toString().length() == 2){
							editable.append(":");
						}
					}
				}
			});
			editDate.setFilters(new InputFilter[]{Utils.getDateFilter(), new InputFilter.LengthFilter(10)});
			editHour.setFilters(new InputFilter[]{Utils.getHourFilter(), new InputFilter.LengthFilter(5)});
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":filterDate - Exception: ", e);
		}
	}
	
	public void refreshTickets(){
		try{
			if(rbFace.isChecked()){
				editTicketNumberContravention.setVisibility(EditText.VISIBLE);
				editTicketNumberIncluded.setVisibility(EditText.VISIBLE);
			}else{
				editTicketNumberContravention.setVisibility(EditText.GONE);
				editTicketNumberIncluded.setVisibility(EditText.GONE);
			}
			
			if(idInfringement == 0){
				//Ahora siempre tiene que tomar número de acta
				editTicketNumber.setText(String.valueOf(ticketB));
			}else{
				editTicketNumber.setText(String.valueOf(ticketBOriginal));
			}
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":refreshTickets - Exception: ", e);
		}
	}
	
	public void preValidateInputs(){
		try{
			final Activity activity	= this;
			
			//Validar campos previos a Fecha de acta
			editDate.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View view){
					if(!rbFace.isChecked() && !rbDistance.isChecked()){
						Utils.showAlertDialog(activity, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}
				}
			});
			editHour.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View view){
					if(!rbFace.isChecked() && !rbDistance.isChecked()){
						Utils.showAlertDialog(activity, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}
				}
			});
			//Validar campos previos a nro de dominio
			editDomain.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View v){
					if(!rbFace.isChecked() && !rbDistance.isChecked()){
						Utils.showAlertDialog(activity, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim())){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(activity, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), activity).equals("OK")){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(activity, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), activity));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), activity)){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(activity, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio")){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(activity, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(activity, "País", "Es un dato requerido");
										}
									}
								}
							}
						}
					}
				}
			});
			//Validar campos previos a Interno
			editInternal.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View v){
					if(!rbFace.isChecked() && !rbDistance.isChecked()){
						Utils.showAlertDialog(activity, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim())){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(activity, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), activity).equals("OK")){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(activity, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), activity));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), activity)){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(activity, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio")){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(activity, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(activity, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo")){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(activity, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE){
													txtLine.requestFocus();
													Utils.showAlertDialog(activity, "Línea", "Es un dato requerido");
												}
											}
										}
									}
								}
							}
						}
					}
				}
			});
			//Validar campos previos a Marca
			editBrand.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					if(!rbFace.isChecked() && !rbDistance.isChecked()){
						Utils.showAlertDialog(activity, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim())){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(activity, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), activity).equals("OK")){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(activity, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), activity));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), activity)){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(activity, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio")){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(activity, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(activity, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo")){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(activity, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE){
													txtLine.requestFocus();
													Utils.showAlertDialog(activity, "Línea", "Es un dato requerido");
												}else{
													if(txtCompany.getText().toString().trim().equals("Empresa") && listCompany.getVisibility() == RelativeLayout.VISIBLE){
														txtCompany.requestFocus();
														Utils.showAlertDialog(activity, "Empresa", "Es un dato requerido");
													}else{
														if(!Utils.validateDomain(editDomain.getText().toString(), txtTypeVehicle.getText().toString(), txtTypeDomain.getText().toString(), activity,
															editCountry.getText().toString().trim()).equals("")){
															if(Utils.validateDomain(editDomain.getText().toString(), txtTypeVehicle.getText().toString(), txtTypeDomain.getText().toString(), activity,
																editCountry.getText().toString().trim()).equals("block")){
																editDomain.setText("");
																editDomain.setEnabled(false);
															}else{
																editDomain.requestFocus();
																showSoftKeyboard();
																Utils.showAlertDialog(activity, "Número de dominio", Utils.validateDomain(editDomain.getText().toString(), txtTypeVehicle.getText().toString(),
																	txtTypeDomain.getText().toString(), activity, editCountry.getText().toString().trim()));
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			});
			//Validar campos previos a Calle del lugar
			editPlace.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View v){
					if(!rbFace.isChecked() && !rbDistance.isChecked()){
						Utils.showAlertDialog(activity, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim())){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(activity, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), activity).equals("OK")){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(activity, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), activity));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), activity)){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(activity, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio")){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(activity, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(activity, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo")){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(activity, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE){
													txtLine.requestFocus();
													Utils.showAlertDialog(activity, "Línea", "Es un dato requerido");
												}else{
													if(txtCompany.getText().toString().trim().equals("Empresa") && listCompany.getVisibility() == RelativeLayout.VISIBLE){
														txtCompany.requestFocus();
														Utils.showAlertDialog(activity, "Empresa", "Es un dato requerido");
													}else{
														if(Utils.isEmpty(strTypesInfringement)){
															Utils.showAlertDialog(activity, "Tipo de infracción", "Es un dato requerido");
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			});
			//Validar campos previos a Nombre imputado
			editName.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View v){
					if(!rbFace.isChecked() && !rbDistance.isChecked()){
						Utils.showAlertDialog(activity, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim())){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(activity, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), activity).equals("OK")){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(activity, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), activity));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), activity)){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(activity, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio")){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(activity, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(activity, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo")){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(activity, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE){
													txtLine.requestFocus();
													Utils.showAlertDialog(activity, "Línea", "Es un dato requerido");
												}else{
													if(txtCompany.getText().toString().trim().equals("Empresa") && listCompany.getVisibility() == RelativeLayout.VISIBLE){
														txtCompany.requestFocus();
														Utils.showAlertDialog(activity, "Empresa", "Es un dato requerido");
													}else{
														if(Utils.isEmpty(strTypesInfringement)){
															Utils.showAlertDialog(activity, "Tipo de infracción", "Es un dato requerido");
														}else{
															if(!validateAddress(1)){
																showSoftKeyboard();
															}else{
																if(!validateAddress(2)){
																	showSoftKeyboard();
																}else{
																	if(!validateAddress(3)){
																		showSoftKeyboard();
																	}else{
																		if(editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editReference.getText().toString().trim())){
																			//Es autopista y tiene que tener referencia
																			editReference.requestFocus();
																			showSoftKeyboard();
																			Utils.showAlertDialog(activity, "Referencia de lugar", "Es un dato requerido");
																		}else{
																			if(	!editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editHeightPlace.getText().toString()) &&
																				Utils.isEmpty(editStreet1.getText().toString().trim()) && Utils.isEmpty(editStreet2.getText().toString().trim())){
																				editHeightPlace.requestFocus();
																				showSoftKeyboard();
																				Utils.showAlertDialog(activity, "Lugar", "Debe cargar la altura o intersección");
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			});
			//Validar campos previos a Provincia
			editProvince.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View v){
					if(!rbFace.isChecked() && !rbDistance.isChecked()){
						Utils.showAlertDialog(activity, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim())){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(activity, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), activity).equals("OK")){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(activity, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), activity));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), activity)){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(activity, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio")){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(activity, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(activity, "País", "Es un dato requerido");
										}
										else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo")){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(activity, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE){
													txtLine.requestFocus();
													Utils.showAlertDialog(activity, "Línea", "Es un dato requerido");
												}else{
													if(txtCompany.getText().toString().trim().equals("Empresa") && listCompany.getVisibility() == RelativeLayout.VISIBLE){
														txtCompany.requestFocus();
														Utils.showAlertDialog(activity, "Empresa", "Es un dato requerido");
													}else{
														if(Utils.isEmpty(strTypesInfringement)){
															Utils.showAlertDialog(activity, "Tipo de infracción", "Es un dato requerido");
														}else{
															if(!validateAddress(1)){
																showSoftKeyboard();
															}else{
																if(!validateAddress(2)){
																	showSoftKeyboard();
																}else{
																	if(!validateAddress(3)){
																		showSoftKeyboard();
																	}else{
																		if(editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editReference.getText().toString().trim())){
																			//Es autopista y tiene que tener referencia
																			editReference.requestFocus();
																			showSoftKeyboard();
																			Utils.showAlertDialog(activity, "Referencia de lugar", "Es un dato requerido");
																		}else{
																			if(	!editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editHeightPlace.getText().toString()) &&
																				Utils.isEmpty(editStreet1.getText().toString().trim()) && Utils.isEmpty(editStreet2.getText().toString().trim())){
																				editHeightPlace.requestFocus();
																				showSoftKeyboard();
																				Utils.showAlertDialog(activity, "Lugar", "Debe cargar la altura o intersección");
																			}else{
																				if(Utils.isEmpty(editName.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																					editName.requestFocus();
																					showSoftKeyboard();
																					Utils.showAlertDialog(activity, "Nombre y apellido", "Es un dato requerido");
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			});
			//Validar campos previos a Calle de domicilio
			editAddress.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View v){
					if(!rbFace.isChecked() && !rbDistance.isChecked()){
						Utils.showAlertDialog(activity, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim())){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(activity, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), activity).equals("OK")){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(activity, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), activity));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), activity)){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(activity, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio")){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(activity, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(activity, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo")){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(activity, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE){
													txtLine.requestFocus();
													Utils.showAlertDialog(activity, "Línea", "Es un dato requerido");
												}else{
													if(txtCompany.getText().toString().trim().equals("Empresa") && listCompany.getVisibility() == RelativeLayout.VISIBLE){
														txtCompany.requestFocus();
														Utils.showAlertDialog(activity, "Empresa", "Es un dato requerido");
													}else{
														if(Utils.isEmpty(strTypesInfringement)){
															Utils.showAlertDialog(activity, "Tipo de infracción", "Es un dato requerido");
														}else{
															if(!validateAddress(1)){
																showSoftKeyboard();
															}else{
																if(!validateAddress(2)){
																	showSoftKeyboard();
																}else{
																	if(!validateAddress(3)){
																		showSoftKeyboard();
																	}else{
																		if(editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editReference.getText().toString().trim())){
																			//Es autopista y tiene que tener referencia
																			editReference.requestFocus();
																			showSoftKeyboard();
																			Utils.showAlertDialog(activity, "Referencia de lugar", "Es un dato requerido");
																		}else{
																			if(	!editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editHeightPlace.getText().toString()) &&
																				Utils.isEmpty(editStreet1.getText().toString().trim()) && Utils.isEmpty(editStreet2.getText().toString().trim())){
																				editHeightPlace.requestFocus();
																				showSoftKeyboard();
																				Utils.showAlertDialog(activity, "Lugar", "Debe cargar la altura o intersección");
																			}else{
																				if(Utils.isEmpty(editName.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																					editName.requestFocus();
																					showSoftKeyboard();
																					Utils.showAlertDialog(activity, "Nombre y apellido", "Es un dato requerido");
																				}else{
																					if(Utils.isEmpty(editProvince.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																						editProvince.requestFocus();
																						showSoftKeyboard();
																						Utils.showAlertDialog(activity, "Provincia", "Es un dato requerido");
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			});
			//Validar campos previos a Altura de domicilio
			editHeight.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View v){
					if(!rbFace.isChecked() && !rbDistance.isChecked()){
						Utils.showAlertDialog(activity, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim())){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(activity, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), activity).equals("OK")){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(activity, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), activity));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), activity)){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(activity, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio")){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(activity, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(activity, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo")){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(activity, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE){
													txtLine.requestFocus();
													Utils.showAlertDialog(activity, "Línea", "Es un dato requerido");
												}else{
													if(txtCompany.getText().toString().trim().equals("Empresa") && listCompany.getVisibility() == RelativeLayout.VISIBLE){
														txtCompany.requestFocus();
														Utils.showAlertDialog(activity, "Empresa", "Es un dato requerido");
													}else{
														if(Utils.isEmpty(strTypesInfringement)){
															Utils.showAlertDialog(activity, "Tipo de infracción", "Es un dato requerido");
														}else{
															if(!validateAddress(1)){
																showSoftKeyboard();
															}else{
																if(!validateAddress(2)){
																	showSoftKeyboard();
																}else{
																	if(!validateAddress(3)){
																		showSoftKeyboard();
																	}else{
																		if(editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editReference.getText().toString().trim())){
																			//Es autopista y tiene que tener referencia
																			editReference.requestFocus();
																			showSoftKeyboard();
																			Utils.showAlertDialog(activity, "Referencia de lugar", "Es un dato requerido");
																		}else{
																			if(	!editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editHeightPlace.getText().toString()) &&
																				Utils.isEmpty(editStreet1.getText().toString().trim()) && Utils.isEmpty(editStreet2.getText().toString().trim())){
																				editHeightPlace.requestFocus();
																				showSoftKeyboard();
																				Utils.showAlertDialog(activity, "Lugar", "Debe cargar la altura o intersección");
																			}else{
																				if(Utils.isEmpty(editName.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																					editName.requestFocus();
																					showSoftKeyboard();
																					Utils.showAlertDialog(activity, "Nombre y apellido", "Es un dato requerido");
																				}else{
																					if(!validateAddress(4) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																						showSoftKeyboard();
																					}else{
																						if(Utils.isEmpty(editProvince.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																							editProvince.requestFocus();
																							showSoftKeyboard();
																							Utils.showAlertDialog(activity, "Provincia", "Es un dato requerido");
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			});
			//Validar campos previos a Piso
			editFloor.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View v){
					if(!rbFace.isChecked() && !rbDistance.isChecked()){
						Utils.showAlertDialog(activity, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim())){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(activity, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), activity).equals("OK")){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(activity, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), activity));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), activity)){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(activity, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio")){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(activity, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(activity, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo")){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(activity, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE){
													txtLine.requestFocus();
													Utils.showAlertDialog(activity, "Línea", "Es un dato requerido");
												}else{
													if(txtCompany.getText().toString().trim().equals("Empresa") && listCompany.getVisibility() == RelativeLayout.VISIBLE){
														txtCompany.requestFocus();
														Utils.showAlertDialog(activity, "Empresa", "Es un dato requerido");
													}else{
														if(Utils.isEmpty(strTypesInfringement)){
															Utils.showAlertDialog(activity, "Tipo de infracción", "Es un dato requerido");
														}else{
															if(!validateAddress(1)){
																showSoftKeyboard();
															}else{
																if(!validateAddress(2)){
																	showSoftKeyboard();
																}else{
																	if(!validateAddress(3)){
																		showSoftKeyboard();
																	}else{
																		if(editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editReference.getText().toString().trim())){
																			//Es autopista y tiene que tener referencia
																			editReference.requestFocus();
																			showSoftKeyboard();
																			Utils.showAlertDialog(activity, "Referencia de lugar", "Es un dato requerido");
																		}else{
																			if(	!editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editHeightPlace.getText().toString()) &&
																				Utils.isEmpty(editStreet1.getText().toString().trim()) && Utils.isEmpty(editStreet2.getText().toString().trim())){
																				editHeightPlace.requestFocus();
																				showSoftKeyboard();
																				Utils.showAlertDialog(activity, "Lugar", "Debe cargar la altura o intersección");
																			}else{
																				if(Utils.isEmpty(editName.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																					editName.requestFocus();
																					showSoftKeyboard();
																					Utils.showAlertDialog(activity, "Nombre y apellido", "Es un dato requerido");
																				}else{
																					if(!validateAddress(4) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																						showSoftKeyboard();
																					}else{
																						if(	Utils.isEmpty(editHeight.getText().toString().trim()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																							editHeight.requestFocus();
																							showSoftKeyboard();
																							Utils.showAlertDialog(activity, "Altura del domicilio", "Es un dato requerido");
																						}else{
																							if(Utils.isEmpty(editProvince.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																								editProvince.requestFocus();
																								showSoftKeyboard();
																								Utils.showAlertDialog(activity, "Provincia", "Es un dato requerido");
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			});
			//Validar campos previos a Oficina
			editOffice.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View v){
					if(!rbFace.isChecked() && !rbDistance.isChecked()){
						Utils.showAlertDialog(activity, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim())){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(activity, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), activity).equals("OK")){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(activity, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), activity));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), activity)){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(activity, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio")){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(activity, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(activity, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo")){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(activity, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE){
													txtLine.requestFocus();
													Utils.showAlertDialog(activity, "Línea", "Es un dato requerido");
												}else{
													if(txtCompany.getText().toString().trim().equals("Empresa") && listCompany.getVisibility() == RelativeLayout.VISIBLE){
														txtCompany.requestFocus();
														Utils.showAlertDialog(activity, "Empresa", "Es un dato requerido");
													}else{
														if(Utils.isEmpty(strTypesInfringement)){
															Utils.showAlertDialog(activity, "Tipo de infracción", "Es un dato requerido");
														}else{
															if(!validateAddress(1)){
																showSoftKeyboard();
															}else{
																if(!validateAddress(2)){
																	showSoftKeyboard();
																}else{
																	if(!validateAddress(3)){
																		showSoftKeyboard();
																	}else{
																		if(editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editReference.getText().toString().trim())){
																			//Es autopista y tiene que tener referencia
																			editReference.requestFocus();
																			showSoftKeyboard();
																			Utils.showAlertDialog(activity, "Referencia de lugar", "Es un dato requerido");
																		}else{
																			if(	!editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editHeightPlace.getText().toString()) &&
																				Utils.isEmpty(editStreet1.getText().toString().trim()) && Utils.isEmpty(editStreet2.getText().toString().trim())){
																				editHeightPlace.requestFocus();
																				showSoftKeyboard();
																				Utils.showAlertDialog(activity, "Lugar", "Debe cargar la altura o intersección");
																			}else{
																				if(Utils.isEmpty(editName.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																					editName.requestFocus();
																					showSoftKeyboard();
																					Utils.showAlertDialog(activity, "Nombre y apellido", "Es un dato requerido");
																				}else{
																					if(!validateAddress(4) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																						showSoftKeyboard();
																					}else{
																						if(	Utils.isEmpty(editHeight.getText().toString().trim()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																							editHeight.requestFocus();
																							showSoftKeyboard();
																							Utils.showAlertDialog(activity, "Altura del domicilio", "Es un dato requerido");
																						}else{
																							if(Utils.isEmpty(editProvince.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																								editProvince.requestFocus();
																								showSoftKeyboard();
																								Utils.showAlertDialog(activity, "Provincia", "Es un dato requerido");
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			});
			//Validar campos previos a Localidad
			editLocality.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View v){
					if(!rbFace.isChecked() && !rbDistance.isChecked()){
						Utils.showAlertDialog(activity, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim())){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(activity, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), activity).equals("OK")){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(activity, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), activity));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), activity)){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(activity, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio")){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(activity, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(activity, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo")){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(activity, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE){
													txtLine.requestFocus();
													Utils.showAlertDialog(activity, "Línea", "Es un dato requerido");
												}else{
													if(txtCompany.getText().toString().trim().equals("Empresa") && listCompany.getVisibility() == RelativeLayout.VISIBLE){
														txtCompany.requestFocus();
														Utils.showAlertDialog(activity, "Empresa", "Es un dato requerido");
													}else{
														if(Utils.isEmpty(strTypesInfringement)){
															Utils.showAlertDialog(activity, "Tipo de infracción", "Es un dato requerido");
														}else{
															if(!validateAddress(1)){
																showSoftKeyboard();
															}else{
																if(!validateAddress(2)){
																	showSoftKeyboard();
																}else{
																	if(!validateAddress(3)){
																		showSoftKeyboard();
																	}else{
																		if(editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editReference.getText().toString().trim())){
																			//Es autopista y tiene que tener referencia
																			editReference.requestFocus();
																			showSoftKeyboard();
																			Utils.showAlertDialog(activity, "Referencia de lugar", "Es un dato requerido");
																		}else{
																			if(	!editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editHeightPlace.getText().toString()) &&
																				Utils.isEmpty(editStreet1.getText().toString().trim()) && Utils.isEmpty(editStreet2.getText().toString().trim())){
																				editHeightPlace.requestFocus();
																				showSoftKeyboard();
																				Utils.showAlertDialog(activity, "Lugar", "Debe cargar la altura o intersección");
																			}else{
																				if(Utils.isEmpty(editName.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																					editName.requestFocus();
																					showSoftKeyboard();
																					Utils.showAlertDialog(activity, "Nombre y apellido", "Es un dato requerido");
																				}else{
																					if(!validateAddress(4) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																						showSoftKeyboard();
																					}else{
																						if(	Utils.isEmpty(editHeight.getText().toString().trim()) &&rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																							editHeight.requestFocus();
																							showSoftKeyboard();
																							Utils.showAlertDialog(activity, "Altura del domicilio", "Es un dato requerido");
																						}else{
																							if(Utils.isEmpty(editProvince.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																								editProvince.requestFocus();
																								showSoftKeyboard();
																								Utils.showAlertDialog(activity, "Provincia", "Es un dato requerido");
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			});
			//Validar campos previos a Nro. documento
			editDocument.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View v){
					if(!rbFace.isChecked() && !rbDistance.isChecked()){
						Utils.showAlertDialog(activity, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim())){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(activity, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), activity).equals("OK")){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(activity, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), activity));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), activity)){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(activity, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio")){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(activity, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(activity, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo")){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(activity, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE){
													txtLine.requestFocus();
													Utils.showAlertDialog(activity, "Línea", "Es un dato requerido");
												}else{
													if(txtCompany.getText().toString().trim().equals("Empresa") && listCompany.getVisibility() == RelativeLayout.VISIBLE){
														txtCompany.requestFocus();
														Utils.showAlertDialog(activity, "Empresa", "Es un dato requerido");
													}else{
														if(Utils.isEmpty(strTypesInfringement)){
															Utils.showAlertDialog(activity, "Tipo de infracción", "Es un dato requerido");
														}else{
															if(!validateAddress(1)){
																showSoftKeyboard();
															}else{
																if(!validateAddress(2)){
																	showSoftKeyboard();
																}else{
																	if(!validateAddress(3)){
																		showSoftKeyboard();
																	}else{
																		if(editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editReference.getText().toString().trim())){
																			//Es autopista y tiene que tener referencia
																			editReference.requestFocus();
																			showSoftKeyboard();
																			Utils.showAlertDialog(activity, "Referencia de lugar", "Es un dato requerido");
																		}else{
																			if(	!editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editHeightPlace.getText().toString()) &&
																				Utils.isEmpty(editStreet1.getText().toString().trim()) && Utils.isEmpty(editStreet2.getText().toString().trim())){
																				editHeightPlace.requestFocus();
																				showSoftKeyboard();
																				Utils.showAlertDialog(activity, "Lugar", "Debe cargar la altura o intersección");
																			}else{
																				if(Utils.isEmpty(editName.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																					editName.requestFocus();
																					showSoftKeyboard();
																					Utils.showAlertDialog(activity, "Nombre y apellido", "Es un dato requerido");
																				}else{
																					if(!validateAddress(4) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																						showSoftKeyboard();
																					}else{
																						if(	Utils.isEmpty(editHeight.getText().toString().trim()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																							editHeight.requestFocus();
																							showSoftKeyboard();
																							Utils.showAlertDialog(activity, "Altura del domicilio", "Es un dato requerido");
																						}else{
																							if(Utils.isEmpty(editProvince.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																								editProvince.requestFocus();
																								showSoftKeyboard();
																								Utils.showAlertDialog(activity, "Provincia", "Es un dato requerido");
																							}else{
																								if(	txtTypeDocument.getText().toString().trim().equals("Tipo de documento")&&
																									rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																									txtTypeDocument.requestFocus();
																									Utils.showAlertDialog(activity, "Tipo de documento", "Es un dato requerido");
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			});
			//Validar campos previos a Nro. de licencia
			editLicense.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View v){
					if(!rbFace.isChecked() && !rbDistance.isChecked()){
						Utils.showAlertDialog(activity, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim())){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(activity, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), activity).equals("OK")){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(activity, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), activity));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), activity)){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(activity, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio")){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(activity, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(activity, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo")){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(activity, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE){
													txtLine.requestFocus();
													Utils.showAlertDialog(activity, "Línea", "Es un dato requerido");
												}else{
													if(txtCompany.getText().toString().trim().equals("Empresa") && listCompany.getVisibility() == RelativeLayout.VISIBLE){
														txtCompany.requestFocus();
														Utils.showAlertDialog(activity, "Empresa", "Es un dato requerido");
													}else{
														if(Utils.isEmpty(strTypesInfringement)){
															Utils.showAlertDialog(activity, "Tipo de infracción", "Es un dato requerido");
														}else{
															if(!validateAddress(1)){
																showSoftKeyboard();
															}else{
																if(!validateAddress(2)){
																	showSoftKeyboard();
																}else{
																	if(!validateAddress(3)){
																		showSoftKeyboard();
																	}else{
																		if(editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editReference.getText().toString().trim())){
																			//Es autopista y tiene que tener referencia
																			editReference.requestFocus();
																			showSoftKeyboard();
																			Utils.showAlertDialog(activity, "Referencia de lugar", "Es un dato requerido");
																		}else{
																			if(	!editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editHeightPlace.getText().toString()) &&
																				Utils.isEmpty(editStreet1.getText().toString().trim()) && Utils.isEmpty(editStreet2.getText().toString().trim())){
																				editHeightPlace.requestFocus();
																				showSoftKeyboard();
																				Utils.showAlertDialog(activity, "Lugar", "Debe cargar la altura o intersección");
																			}else{
																				if(Utils.isEmpty(editName.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																					editName.requestFocus();
																					showSoftKeyboard();
																					Utils.showAlertDialog(activity, "Nombre y apellido", "Es un dato requerido");
																				}else{
																					if(!validateAddress(4) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																						showSoftKeyboard();
																					}else{
																						if(	Utils.isEmpty(editHeight.getText().toString().trim()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																							editHeight.requestFocus();
																							showSoftKeyboard();
																							Utils.showAlertDialog(activity, "Altura del domicilio", "Es un dato requerido");
																						}else{
																							if(Utils.isEmpty(editProvince.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																								editProvince.requestFocus();
																								showSoftKeyboard();
																								Utils.showAlertDialog(activity, "Provincia", "Es un dato requerido");
																							}else{
																								if(	txtTypeDocument.getText().toString().trim().equals("Tipo de documento") &&
																									rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																									txtTypeDocument.requestFocus();
																									Utils.showAlertDialog(activity, "Tipo de documento", "Es un dato requerido");
																								}else{
																									if(Utils.isEmpty(editDocument.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
																										editDocument.requestFocus();
																										showSoftKeyboard();
																										Utils.showAlertDialog(activity, "Nro. de documento", "Es un dato requerido");
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			});
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":preValidateInputs - Exception: ", e);
		}
	}
	
	public void integrateStreet(final AutoCompleteTextView autoCompleteTextView, final int input){
		try
		{
			if(data.size() == 0)
			{
				Realm realm					= Realm.getDefaultInstance();
				RealmResults<Street> calles	= realm.where(Street.class).findAll().sort(Street.KEY_NAME);
				
				if(calles.size() > 0)
				{
					for(Street calle: calles)
					{
						data.add(calle.getNombreCalle());
					}
				}
				
				realm.close();
			}
			
			final ArrayAdapter<String> placeAdapter = new ArrayAdapter<>(this, R.layout.item_location, data.toArray(new String[data.size()]));
			autoCompleteTextView.setAdapter(placeAdapter);
			autoCompleteTextView.setOnEditorActionListener(new TextView.OnEditorActionListener()
			{
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
				{
					if(actionId == EditorInfo.IME_ACTION_NEXT)
					{
						validateAddress(input);
						//Ver si según reglas nos movemos a altura o a referencia
						return true;
					}
					
					return false;
				}
			});
			autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener()
			{
				@Override
				public void onItemClick(final AdapterView<?> adapterView, final View view, final int i, final long l)
				{
					autoCompleteTextView.setSelection(autoCompleteTextView.getText().toString().length());
					autoCompleteTextView.setAdapter(null);
					
					//Vaciar altura, intersección y referencia al elegir calle de lugar
					if(input == 1)
					{
						editHeightPlace.setText("");
						editStreet1.setText("");
						editStreet2.setText("");
						editReference.setText("");
					}
					
					validateAddress(input);
				}
			});
			autoCompleteTextView.addTextChangedListener(new TextWatcher(){
				@Override
				public void beforeTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2){
				}
				
				@Override
				public void onTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2){
				}
				
				@Override
				public void afterTextChanged(final Editable editable){
					if(Utils.isEmpty(editable.toString())){
						autoCompleteTextView.setAdapter(placeAdapter);
					}
					
					if(input == 1){
						editHeightPlace.setEnabled(true);
						editStreet1.setEnabled(true);
						editStreet2.setEnabled(true);
					}
				}
			});
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":integrateStreet - Exception: ", e);
		}
	}
	
	public void reload(){
		try{
			Realm realm = Realm.getDefaultInstance();
			
			if(idInfringement != 0){
				Infringement infringement = realm.where(Infringement.class).equalTo(Common.KEY_ID, idInfringement).findFirst();
				
				if(infringement != null){
					if(!Utils.isEmpty(infringement.getSignatureImputed())){
						hasSignature = true;
					}
					
					//Acta
					if(infringement.getFormInfringement().equals("P")){
						rbFace.setChecked(true);
					}else{
						rbDistance.setChecked(true);
					}
					
					if(!Utils.isEmpty(String.valueOf(infringement.getTicketNumber()))){
						ticketB = infringement.getTicketNumber();
						ticketBOriginal = infringement.getTicketNumber();
						editTicketNumber.setText(String.valueOf(infringement.getTicketNumber()));
					}
					
					if(!Utils.isEmpty(infringement.getTicketZNumber())){
						editTicketNumberIncluded.setText(infringement.getTicketZNumber());
					}
					
					if(!Utils.isEmpty(infringement.getNumberTicketContravention())){
						editTicketNumberContravention.setText(infringement.getNumberTicketContravention());
					}
					
					editDate	= (EditText) findViewById(R.id.editDate);
					editHour	= (EditText) findViewById(R.id.editHour);
					
					if(!Utils.isEmpty(infringement.getDate())){
						editDate.setText(infringement.getDate());
						editDate.setEnabled(false);
					}
					
					if(!Utils.isEmpty(infringement.getHour())){
						editHour.setText(infringement.getHour());
						editHour.setEnabled(false);
					}
					
					if(!Utils.isEmpty(infringement.getReferencePlace())){
						editReference.setText(infringement.getReferencePlace());
					}
					
					//B Vehiculo
					txtTypeDomain.setText(infringement.getTypeDomain());
					txtTypeDomain.setTextColor(android.graphics.Color.BLACK);
					
					if(infringement.getTypeDomain().equals("Internacional")){
						editCountry.setVisibility(AutoCompleteTextView.VISIBLE);
					}
					
					if(infringement.getCountry() != null){
						editCountry.setText(infringement.getCountry().getDescripcion());
					}
					
					editDomain.setText(infringement.getDomain());
					txtCopy.setText(infringement.getCopyDomain());
					
					if(infringement.getTypeVehicle() != null){
						txtTypeVehicle.setText(infringement.getTypeVehicle().getDescripcion());
						txtTypeVehicle.setTextColor(android.graphics.Color.BLACK);
					}else{
						if(!Utils.isEmpty(infringement.getBetween1Imputed())){
							txtTypeVehicle.setText(infringement.getBetween1Imputed());
							txtTypeVehicle.setTextColor(android.graphics.Color.BLACK);
						}
					}
					
					if(txtTypeVehicle.getText().toString().trim().toLowerCase().equals("colectivo") || txtTypeVehicle.getText().toString().trim().toLowerCase().equals("colectivos")){
						listLine.setVisibility(RelativeLayout.VISIBLE);
						editInternal.setVisibility(EditText.VISIBLE);
						listCompany.setVisibility(RelativeLayout.VISIBLE);
						
						if(infringement.getNewLine() != null){
							txtLine.setText(infringement.getNewLine().getLinea());
							txtLine.setTextColor(android.graphics.Color.BLACK);
						}
						
						try{
							if(infringement.getInternal() != 0){
								editInternal = (EditText) findViewById(R.id.editInternal);
								editInternal.setText(infringement.getInternal()+"");
							}
						}catch(Exception e){
							Utils.logError(this, getLocalClassName()+":reload:Internal - Exception: ", e);
						}
						
						if(infringement.getCompany() != null){
							txtCompany.setText(infringement.getCompany().getEmpresa());
							txtCompany.setTextColor(android.graphics.Color.BLACK);
						}else{
							if(!Utils.isEmpty(infringement.getAgentDocument())){
								txtCompany.setText(infringement.getAgentDocument());
								txtCompany.setTextColor(android.graphics.Color.BLACK);
							}
						}
					}else{
						if(txtTypeVehicle.getText().toString().trim().toLowerCase().equals("combi") || txtTypeVehicle.getText().toString().trim().toLowerCase().equals("combis")){
							listLine.setVisibility(RelativeLayout.GONE);
							editInternal.setVisibility(EditText.GONE);
							listCompany.setVisibility(RelativeLayout.VISIBLE);
							
							if(infringement.getCompany() != null){
								txtCompany.setText(infringement.getCompany().getEmpresa());
								txtCompany.setTextColor(android.graphics.Color.BLACK);
							}else{
								if(!Utils.isEmpty(infringement.getAgentDocument())){
									txtCompany.setText(infringement.getAgentDocument());
									txtCompany.setTextColor(android.graphics.Color.BLACK);
								}
							}
						}else{
							listLine.setVisibility(RelativeLayout.GONE);
							editInternal.setVisibility(EditText.GONE);
							listCompany.setVisibility(RelativeLayout.GONE);
						}
					}
					
					if(infringement.getBrandId() != 0){
						editBrand.setText(realm.where(Brand.class).equalTo(Common.KEY_ID, infringement.getBrandId()).findFirst().getMarca());
					}else{
						if(!Utils.isEmpty(infringement.getBrandStr())){
							editBrand.setText(infringement.getBrandStr());
						}
					}
					
					if(infringement.getDocumentAgent().equals("1")){
						rbCondition.setChecked(true);
					}else{
						rbNoCondition.setChecked(true);
					}
					
					editModel.setText(infringement.getModel());
					strTypesInfringement = infringement.getTypesInfringements();
					strMunicipality = infringement.getIssuedBy();
					editPlace.setText(infringement.getStreet());
					editHeightPlace.setText(infringement.getHeight());
					editStreet1.setText(infringement.getBetween1());
					editStreet2.setText(infringement.getBetween2());
					//B Imputado
					editName.setText(infringement.getNameImputed());
					editLastName.setText(infringement.getLastNameImputed());
					editProvince.setText(infringement.getApartmentImputed());
					editAddress.setText(infringement.getStreetImputed());
					editHeight.setText(infringement.getHeightImputed());
					editFloor.setText(infringement.getFloor());
					editOffice.setText(infringement.getOffice());
					
					if(infringement.getTypeDocumentImputed() != null){
						txtTypeDocument.setText(infringement.getTypeDocumentImputed().getDocumento());
						txtTypeDocument.setTextColor(android.graphics.Color.BLACK);
					}
					
					if(!Utils.isEmpty(infringement.getDocumentImputed())){
						editDocument.setText(infringement.getDocumentImputed());
					}
					
					if(infringement.getLocalityImputed() != null){
						editLocality.setText(infringement.getLocalityImputed().getDescripcion());
					}
					
					if(!Utils.isEmpty(infringement.getLicense())){
						editLicense.setText(infringement.getLicense());
					}
					
					if(!Utils.isEmpty(infringement.getCategories())){
						txtCategory.setText(infringement.getCategories());
						txtCategory.setTextColor(android.graphics.Color.BLACK);
					}
					
					//Ahí guardamos el email
					if(!Utils.isEmpty(infringement.getExplanationCategory())){
						editEmail.setText(infringement.getExplanationCategory());
					}
					
					if(infringement.getDocumentationHeld().equals("S")){
						rbHold.setChecked(true);
					}else{
						rbNoHold.setChecked(true);
					}
					
					if(infringement.getFormInfringement().equals("P")){
						if(infringement.getVehicleRemission().equals("S")){
							rbRemission.setChecked(true);
							listDependence.setVisibility(RelativeLayout.VISIBLE);
							txtDependence.setText(infringement.getDependence());
							txtDependence.setTextColor(android.graphics.Color.BLACK);
						}else{
							rbNoRemission.setChecked(true);
						}
					}else{
						if(infringement.getVehicleRemission().equals("S")){
							rbRemissionDistance.setChecked(true);
							listDependenceDistance.setVisibility(RelativeLayout.VISIBLE);
							txtDependenceDistance.setText(infringement.getDependence());
							txtDependenceDistance.setTextColor(android.graphics.Color.BLACK);
						}else{
							rbNoRemissionDistance.setChecked(true);
						}
					}
					
					//Z Vehiculo
					if(infringement.getNameImputed().equals(infringement.getLastNameImputed())){
						rbOwner.setChecked(true);
					}else{
						if(!Utils.isEmpty(infringement.getLastNameImputed())){
							rbNoOwner.setChecked(true);
						}
					}
					
					editOwner.setText(infringement.getLastNameImputed());
					
					if(infringement.getTypeDocumentOwner() != null){
						txtTypeDocumentOwner.setText(infringement.getTypeDocumentOwner().getDocumento());
						txtTypeDocumentOwner.setTextColor(android.graphics.Color.BLACK);
					}
					
					editDocumentOwner.setEnabled(true);
					editDocumentOwner.setText(infringement.getDocumentOwner());
					txtColor.setText(infringement.getColor().getValor());
					txtColor.setTextColor(android.graphics.Color.BLACK);
					
					//Z Imputado
					if(infringement.getGenderImputed().equals("F")){
						rbF.setChecked(true);
					}else{
						rbM.setChecked(true);
					}
					
					editZip.setText(infringement.getZipCodeImputed());
					editLicenseExpiration.setText(infringement.getLicenseExpiration());
					
					//Z cinemometro
					if(infringement.getCinemometer() != null){
						editCinemometerBrand.setText(infringement.getCinemometer().getBrand());
						editCinemometerModel.setText(infringement.getCinemometer().getModel());
						editCinemometerNumber.setText(infringement.getCinemometer().getSerie());
						editCinemometerValue.setText(infringement.getCinemometer().getValue());
					}
					
					editObservations = (EditText) findViewById(R.id.editObservations);
					editObservations.setText(infringement.getObservations());
					
					if(Common.DEBUG){
						System.out.println("RELOAD DATE: "+infringement.getDate()+" HOUR: "+infringement.getHour());
					}
					
					if(!Utils.isEmpty(infringement.getDate())){
						editDate.setText(infringement.getDate());
					}
					
					if(!Utils.isEmpty(infringement.getHour())){
						editHour.setText(infringement.getHour());
					}
					
					if(infringement.getTypeVehicle() != null){
						txtTypeVehicle.setText(infringement.getTypeVehicle().getDescripcion());
						txtTypeVehicle.setTextColor(android.graphics.Color.BLACK);
					}else{
						if(!Utils.isEmpty(infringement.getBetween1())){
							TypeVehicle typeVehicle = realm.where(TypeVehicle.class).equalTo(TypeVehicle.KEY_ID, infringement.getBetween1()).findFirst();
							
							if(typeVehicle != null){
								txtTypeVehicle.setText(typeVehicle.getDescripcion());
								txtTypeVehicle.setTextColor(android.graphics.Color.BLACK);
							}
						}
					}
					
					if(infringement.getTypeDocumentImputed() != null){
						txtTypeDocument.setText(infringement.getTypeDocumentImputed().getDocumento());
						txtTypeDocument.setTextColor(android.graphics.Color.BLACK);
					}else{
						if(infringement.getTypeDocumentOwner() != null){
							txtTypeDocument.setText(infringement.getTypeDocumentOwner().getDocumento());
							txtTypeDocument.setTextColor(android.graphics.Color.BLACK);
						}else{
							if(!Utils.isEmpty(infringement.getBetween2Imputed())){
								txtTypeDocument.setText(infringement.getBetween2Imputed());
								txtTypeDocument.setTextColor(android.graphics.Color.BLACK);
							}
						}
					}
				}
			}else{
				editDate.setText(Utils.getDatePhone(this));
				String[] partTime = Utils.getDateTimePhone(this).split(" ");
				editHour.setText(partTime[1].substring(0, partTime[1].length()-3));
			}
			
			//Nuevo combo con filtro
			final RealmResults<TypeInfringement> typeInfringements = realm.where(TypeInfringement.class).findAll();
			listTypesInfringement.clear();
			
			if(typeInfringements.size() > 0){
				for(TypeInfringement typeInfringement : typeInfringements){
					listTypesInfringement.add(typeInfringement.getCodigo()+" "+typeInfringement.getDescripcion());
				}
			}
			
			if(listTypesInfringement.size() > 0){
				final List<KeyPairBoolData> listArray0 = new ArrayList<>();
				
				for(int i = 0; i < listTypesInfringement.size(); i++){
					KeyPairBoolData h = new KeyPairBoolData();
					h.setId(i + 1);
					h.setName(listTypesInfringement.get(i));
					
					if(strTypesInfringement.contains(listTypesInfringement.get(i))){
						h.setSelected(true);
					}else{
						h.setSelected(false);
					}
					
					listArray0.add(h);
				}
				
				multiTypeInfringement.setItems(listArray0, -1, "", new SpinnerListener(){
					@Override
					public void onItemsSelected(List<KeyPairBoolData> items){
						strTypesInfringement = "";
						
						if(items.size() > 0){
							for(int i = 0; i < items.size(); i++){
								if(items.get(i).isSelected()){
									strTypesInfringement += items.get(i).getName()+";";
									
									if(Common.DEBUG){
										System.out.println("Multi: "+i+":"+ items.get(i).getName() + " : "+ items.get(i).isSelected());
									}
								}
							}
							
							if(!Utils.isEmpty(strTypesInfringement)){
								strTypesInfringement = strTypesInfringement.substring(0, strTypesInfringement.length()-1);
							}
						}
					}
				});
				final int limit = 5;
				multiTypeInfringement.setLimit(limit, new MultiSpinnerSearch.LimitExceedListener(){
					@Override
					public void onLimitListener(KeyPairBoolData data){
						Toast.makeText(MainActivity.this, "El máximo de infracciones es "+limit, Toast.LENGTH_SHORT).show();
					}
				});
			}
			
			refreshMunicipalities();
			realm.close();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":reload - Exception: ", e);
		}
	}
	
	public void refreshMunicipalities(){
		try{
			if(listMunicipalities.size() > 0){
				final List<KeyPairBoolData> listArray0 = new ArrayList<>();
				
				for(int i = 0; i < listMunicipalities.size(); i++){
					KeyPairBoolData h = new KeyPairBoolData();
					h.setId(i + 1);
					h.setName(listMunicipalities.get(i));
					
					if(!Utils.isEmpty(strMunicipality)){
						if(strMunicipality.equals(listMunicipalities.get(i))){
							h.setSelected(true);
						}else{
							h.setSelected(false);
						}
					}else{
						h.setSelected(false);
					}
					
					listArray0.add(h);
				}
				
				final Activity activity = this;
				multiMunicipality.setItems(listArray0, -1, strMunicipality, new SpinnerListener(){
					@Override
					public void onItemsSelected(List<KeyPairBoolData> items){
						String old = "";
						boolean other = false;
						
						if(!Utils.isEmpty(strMunicipality)){
							old = strMunicipality;
						}
						
						strMunicipality = "";
						
						if(items.size() > 0){
							boolean selected = false;
							for(int i = 0; i < items.size(); i++){
								if(items.get(i).isSelected() && old.equals(items.get(i).getName())){
									items.get(i).setSelected(false);
									listArray0.get(i).setSelected(false);
								}
								
								if(items.get(i).isSelected() && !selected){
									strMunicipality = items.get(i).getName();
									
									if(Common.DEBUG){
										System.out.println("Multi2: "+i+":"+ items.get(i).getName() + " : "+ items.get(i).isSelected());
									}
									
									selected = true;
									
									//Si es otro que lo tipee
									if(strMunicipality.toLowerCase().equals("otro")){
										other = true;
									}
								}
							}
						}
						
						if(other){
							setOtherMunicipality();
						}else{
							otherMunicipality = "";
						}
					}
				});
				final int limit = 1;
				multiMunicipality.setLimit(limit, new MultiSpinnerSearch.LimitExceedListener(){
					@Override
					public void onLimitListener(KeyPairBoolData data){
						Toast.makeText(MainActivity.this, "El máximo de municipios de licencia es "+limit, Toast.LENGTH_SHORT).show();
					}
				});
			}
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":refreshMunicipalities - Exception: ", e);
		}
	}
	
	@Override
	public void onBackPressed(){
		try{
			onLogout();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onBackPressed - Exception: ", e);
		}
	}
	
	public void verify(View view){
		try{
			if(onVerify()){
				save();
			}
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":verify - Exception: ", e);
		}
	}
	
	public boolean onVerify(){
		try{
			View view = new View(this);
			
			if(!rbFace.isChecked() && !rbDistance.isChecked()){
				Utils.showAlertDialog(this, "Modalidad del acta", "Es un dato requerido");
				askModality();
				return false;
			}
			
			//Validar que tenga si o si número de acta siempre
			if(Utils.isEmpty(editTicketNumber.getText().toString().trim()) || editTicketNumber.getText().toString().trim().equals("0")){
				reloadTicket();
			}
			
			if(Utils.isEmpty(editDate.getText().toString().trim())){
				editDate.requestFocus();
				showSoftKeyboard();
				Utils.showAlertDialog(this, "Fecha del acta", "Es un dato requerido");
				return false;
			}else{
				if(!Utils.isStringDate(editDate.getText().toString().trim(), this).equals("OK")){
					editDate.requestFocus();
					showSoftKeyboard();
					Utils.showAlertDialog(this, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), this));
					return false;
				}else{
					if(Utils.isFutureDate(editDate.getText().toString().trim(), this)){
						editDate.requestFocus();
						showSoftKeyboard();
						Utils.showAlertDialog(this, "Fecha del acta", "No se permite una fecha mayor a la actual");
						return false;
					}
				}
			}
			
			if(Utils.isEmpty(editHour.getText().toString().trim())){
				editHour.requestFocus();
				showSoftKeyboard();
				Utils.showAlertDialog(this, "Hora del acta", "Es un dato requerido");
				return false;
			}else{
				if(!Utils.isStringHour(editHour.getText().toString().trim(), this).equals("OK")){
					editHour.requestFocus();
					showSoftKeyboard();
					Utils.showAlertDialog(this, "Hora del acta", Utils.isStringHour(editHour.getText().toString().trim(), this));
					return false;
				}
			}
			
			/*if(!Utils.isInDate(editDate.getText().toString().trim()+" "+editHour.getText().toString().trim(), this)){
				editHour.requestFocus();
				showSoftKeyboard();
				Utils.showAlertDialog(this, "Hora del acta", "La fecha debe estar dentro del turno de trabajo");
				return false;
			}*/
			
			//B Vehiculo
			if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio")){
				view.setTag(91);
				showList(view);
				txtTypeDomain.requestFocus();
				Utils.showAlertDialog(this, "Tipo de dominio", "Es un dato requerido");
				return false;
			}
			
			if(rbDistance.isChecked() && txtTypeDomain.getText().toString().trim().equals("Sin patente") &&
				!txtTypeVehicle.getText().toString().trim().toLowerCase().equals("colectivo")){
				view.setTag(91);
				showList(view);
				txtTypeDomain.requestFocus();
				Utils.showAlertDialog(this, "Tipo de dominio", "Sin patente en actas a distancia es válido para Colectivos únicamente");
				return false;
			}
			
			if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
				editCountry.requestFocus();
				showSoftKeyboard();
				Utils.showAlertDialog(this, "País", "Es un dato requerido");
				return false;
			}else{
				if(!Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
					//Filtrar valores manuales
					Realm realm = Realm.getDefaultInstance();
					Country country = realm.where(Country.class).equalTo(Common.KEY_DESCRIPTION, editCountry.getText().toString().trim()).findFirst();
					
					if(country == null){
						//No existe pedimos que cargue un valor sugerido
						editCountry.requestFocus();
						showSoftKeyboard();
						Utils.showAlertDialog(this, "País", "Debe seleccionar un valor sugerido");
						realm.close();
						return false;
					}
					
					realm.close();
				}
			}
			
			if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo")){
				view.setTag(93);
				showList(view);
				txtTypeVehicle.requestFocus();
				Utils.showAlertDialog(this, "Tipo de vehículo", "Es un dato requerido");
				return false;
			}
			
			if(!Utils.validateDomain(editDomain.getText().toString(), txtTypeVehicle.getText().toString(), txtTypeDomain.getText().toString(), this,
				editCountry.getText().toString().trim()).equals("")){
				if(	Utils.validateDomain(editDomain.getText().toString(), txtTypeVehicle.getText().toString(), txtTypeDomain.getText().toString(), this,
					editCountry.getText().toString().trim()).equals("block")){
					editDomain.setText("");
					editDomain.setEnabled(false);
				}else{
					editDomain.requestFocus();
					showSoftKeyboard();
					Utils.showAlertDialog(this, "Número de dominio", Utils.validateDomain(editDomain.getText().toString(), txtTypeVehicle.getText().toString(),
						txtTypeDomain.getText().toString(), this, editCountry.getText().toString().trim()));
					return false;
				}
			}
			
			if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE){
				view.setTag(94);
				showList(view);
				txtLine.requestFocus();
				Utils.showAlertDialog(this, "Línea", "Es un dato requerido");
				return false;
			}
			
			if(Utils.isEmpty(editInternal.getText().toString().trim()) && editInternal.getVisibility() == EditText.VISIBLE){
				editInternal.requestFocus();
				Utils.showAlertDialog(this, "Interno", "Es un dato requerido");
				return false;
			}else{
				int interno = 0;
				
				try{
					interno = Integer.parseInt(editInternal.getText().toString().trim());
				}catch(Exception e){
				}
				
				if(interno <=0 && editInternal.getVisibility() == EditText.VISIBLE){
					editInternal.requestFocus();
					Utils.showAlertDialog(this, "Interno", "Debe ser mayor a 0");
					return false;
				}
			}
			
			if(txtCompany.getText().toString().trim().equals("Empresa") && listCompany.getVisibility() == RelativeLayout.VISIBLE){
				view.setTag(95);
				showList(view);
				txtCompany.requestFocus();
				Utils.showAlertDialog(this, "Empresa", "Es un dato requerido");
				return false;
			}
			
			if(!txtTypeVehicle.getText().toString().trim().toLowerCase().equals("camion")){
				if(Utils.isEmpty(editBrand.getText().toString().trim())){
					editBrand.requestFocus();
					showSoftKeyboard();
					Utils.showAlertDialog(this, "Marca", "Es un dato requerido");
					return false;
				}else{
					//Filtrar valores manuales
					Realm realm = Realm.getDefaultInstance();
					Brand brand = realm.where(Brand.class).equalTo(Brand.KEY_MARCA, editBrand.getText().toString().trim()).findFirst();
					
					if(brand == null){
						//No existe pedimos que cargue un valor sugerido
						editBrand.requestFocus();
						showSoftKeyboard();
						Utils.showAlertDialog(this, "Marca", "Debe seleccionar un valor sugerido");
						realm.close();
						return false;
					}
					
					realm.close();
				}
			}
			
			if(Utils.isEmpty(strTypesInfringement)){
				Utils.showAlertDialog(this, "Tipo de infracción", "Es un dato requerido");
				return false;
			}
			
			if(!validateAddress(1)){
				showSoftKeyboard();
				return false;
			}
			
			if(editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editReference.getText().toString().trim())){
				//Es autopista y tiene que tener referencia
				editReference.requestFocus();
				showSoftKeyboard();
				Utils.showAlertDialog(this, "Referencia de lugar", "Es un dato requerido");
				return false;
			}else{
				if(	!editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") &&
					Utils.isEmpty(editHeightPlace.getText().toString()) && Utils.isEmpty(editStreet1.getText().toString().trim()) &&
					Utils.isEmpty(editStreet2.getText().toString().trim())){
					editHeightPlace.requestFocus();
					showSoftKeyboard();
					Utils.showAlertDialog(this, "Lugar", "Debe cargar la altura o intersección");
					return false;
				}
			}
			
			if(!validateAddress(2)){
				showSoftKeyboard();
				return false;
			}
			
			if(!validateAddress(3)){
				showSoftKeyboard();
				return false;
			}
			
			//B Imputado
			if(Utils.isEmpty(editName.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
				editName.requestFocus();
				showSoftKeyboard();
				Utils.showAlertDialog(this, "Nombre y apellido", "Es un dato requerido");
				return false;
			}
			
			if(Utils.isEmpty(editProvince.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
				editProvince.requestFocus();
				showSoftKeyboard();
				Utils.showAlertDialog(this, "Provincia", "Es un dato requerido");
				return false;
			}else{
				if(!Utils.isEmpty(editProvince.getText().toString().trim()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
					//Filtrar valores manuales
					Realm realm = Realm.getDefaultInstance();
					Province province = realm.where(Province.class).equalTo(Province.KEY_PROVINCIA, editProvince.getText().toString().trim()).findFirst();

					if(province == null){
						//No existe pedimos que cargue un valor sugerido
						editProvince.requestFocus();
						showSoftKeyboard();
						Utils.showAlertDialog(this, "Provincia", "Debe seleccionar un valor sugerido");
						realm.close();
						return false;
					}

					realm.close();
				}
			}
			
			if(!validateAddress(4) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
				return false;
			}
			
			if(Utils.isEmpty(editHeight.getText().toString().trim()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
				editHeight.requestFocus();
				showSoftKeyboard();
				Utils.showAlertDialog(this, "Altura del domicilio", "Es un dato requerido");
				return false;
			}else{
				int altura = 0;
				
				try{
					altura = Integer.parseInt(editHeight.getText().toString().trim());
				}catch(Exception e){
				}
				
				if(altura <= 0 && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
					editHeight.requestFocus();
					showSoftKeyboard();
					Utils.showAlertDialog(this, "Altura del domicilio", "Debe ser mayor a 0");
					return false;
				}
			}
			
			if(Utils.isEmpty(editLocality.getText().toString().trim()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
				editLocality.requestFocus();
				showSoftKeyboard();
				Utils.showAlertDialog(this, "Localidad", "Es un dato requerido");
				return false;
			}
			
			if(txtTypeDocument.getText().toString().trim().equals("Tipo de documento") && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
				view.setTag(98);
				showList(view);
				txtTypeDocument.requestFocus();
				Utils.showAlertDialog(this, "Tipo de documento", "Es un dato requerido");
				return false;
			}
			
			if(!Utils.validateDocument(editDocument.getText().toString(), txtTypeDocument.getText().toString(), this).equals("") &&
				rlImputed.getVisibility() == RelativeLayout.VISIBLE){
				editDocument.requestFocus();
				showSoftKeyboard();
				Utils.showAlertDialog(this, "Nro. de documento", Utils.validateDocument(editDocument.getText().toString(), txtTypeDocument.getText().toString(), this));
				return false;
			}
			
			//Si es presencial pedimos sexo
			if(!rbF.isChecked() && !rbM.isChecked() && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
				rbF.requestFocus();
				Utils.showAlertDialog(this, "Sexo", "Es un dato requerido");
				return false;
			}
			
			//Validaciones sobre licencia
			if(Utils.isEmpty(strMunicipality) && !Utils.isEmpty(otherMunicipality)){
				strMunicipality = otherMunicipality;
			}
			
			if(!Utils.isEmpty(editLicense.getText().toString().trim()) && Utils.isEmpty(strMunicipality) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
				Utils.showAlertDialog(this, "Municipio Licencia", "Es un dato requerido");
				return false;
			}
			
			if(!Utils.isEmpty(editLicense.getText().toString().trim()) && txtCategory.getText().toString().trim().equals("Categoría de licencia")
				&& rlImputed.getVisibility() == RelativeLayout.VISIBLE){
				view.setTag(910);
				showList(view);
				txtCategory.requestFocus();
				Utils.showAlertDialog(this, "Categoría de licencia", "Es un dato requerido");
				return false;
			}
			
			if(	!Utils.isEmpty(editEmail.getText().toString().trim()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE && !editEmail.getText().toString().trim().contains("@") &&
				!editEmail.getText().toString().trim().contains(".")){
				editEmail.requestFocus();
				showSoftKeyboard();
				Utils.showAlertDialog(this, "Email imputado", "Debe ser un email válido");
				return false;
			}
			
			if(!rbHold.isChecked() && !rbNoHold.isChecked() && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
				rbHold.requestFocus();
				Utils.showAlertDialog(this, "Documentación retenida", "Es un dato requerido");
				return false;
			}
			
			if(!rbRemission.isChecked() && !rbNoRemission.isChecked() && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
				rbRemission.requestFocus();
				Utils.showAlertDialog(this, "Vehículo retenido", "Es un dato requerido");
				return false;
			}
			
			if(!rbRemissionDistance.isChecked() && !rbNoRemissionDistance.isChecked() && rlRemissionDistance.getVisibility() == RelativeLayout.VISIBLE){
				rbRemissionDistance.requestFocus();
				Utils.showAlertDialog(this, "Vehículo retenido", "Es un dato requerido");
				return false;
			}
			
			if(txtDependence.getText().toString().trim().equals("Dependencia") &&
				listDependence.getVisibility() == RelativeLayout.VISIBLE && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
				txtDependence.requestFocus();
				Utils.showAlertDialog(this, "Dependencia", "Es un dato requerido");
				return false;
			}
			
			if(txtDependenceDistance.getText().toString().trim().equals("Dependencia") &&
				listDependenceDistance.getVisibility() == RelativeLayout.VISIBLE && rlRemissionDistance.getVisibility() == RelativeLayout.VISIBLE){
				txtDependenceDistance.requestFocus();
				Utils.showAlertDialog(this, "Dependencia", "Es un dato requerido");
				return false;
			}
			
			if(Utils.isEmpty(editZip.getText().toString().trim())){
				editZip.requestFocus();
				Utils.showAlertDialog(this, "Descripción de conducta", "Es un dato requerido");
				return false;
			}
			
			if(!Utils.isEmpty(editObservations.getText().toString().trim())){
				if(editObservations.getText().toString().trim().length() > 500){
					editObservations.requestFocus();
					Utils.showAlertDialog(this, "Observaciones", "Las observaciones no pueden superar los 500 caracteres");
					return false;
				}
			}
			
			hideSoftKeyboard();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onVerify - Exception: ", e);
		}
		
		return true;
	}
	
	public void save(){
		try{
			final Activity activity	= this;
			
			new Thread(new Runnable(){
				public void run(){
					try{
						Realm.init(activity);
						Realm realm = Realm.getDefaultInstance();
						realm.executeTransaction(new Realm.Transaction(){
							@Override
							public void execute(final Realm bgRealm){
								try{
									Infringement	infringement;
									Color			color;
									TypeVehicle		typeVehicle;
									Locality		locality;
									Brand			brand;
									Country			country = null;
									long ts			= System.currentTimeMillis();
									
									if(Common.DEBUG){
										System.out.println("TicketB: "+ticketB+" input: "+editTicketNumber.getText().toString().trim());
									}
									
									if(idInfringement == 0){
										//Es nuevo lo generamos y guardamos
										idInfringement	= ts;
										infringement	= new Infringement();
										infringement.setId(idInfringement);
										
										if(!Utils.isEmpty(editTicketNumber.getText().toString().trim())){
											infringement.setTicketNumber(Long.valueOf(editTicketNumber.getText().toString().trim()));
										}else{
											//Antes de mandar 0 verificamos para asignar si o si el número
											activity.runOnUiThread(new Runnable(){
												@Override
												public void run(){
													reloadTicket();
												}
											});

											infringement.setTicketNumber(ticketB);
										}
									}else{
										//Lo buscamos porque existe
										infringement = bgRealm.where(Infringement.class).equalTo(Common.KEY_ID, idInfringement).findFirst();
										
										if(Common.DEBUG){
											System.out.println("Error: "+infringement.getMatch());
										}

										//Verificar si el nro de acta asociado esta dentro del rango
										if((rbRemissionDistance.isChecked() || rbFace.isChecked()) && infringement.getTicketNumber() == 0){
											infringement.setTicketNumber(ticketB);
											
											if(Common.DEBUG){
												System.out.println("Le intenta colocar el acta que le tocaría porque antes no tenía y lo rechazara");
											}
										}
									}
									
									//Siempre recalculamos la fecha al editar
									infringement.setCaptureTs(idInfringement);
									
									if(Common.DEBUG){
										System.out.println("Acta asignado: "+infringement.getTicketNumber()+" Es nuevo: "+(idInfringement == 0));
									}
									
									infringement.setLatitude(String.valueOf(lat));
									infringement.setLongitude(String.valueOf(lng));
									infringement.setUser(bgRealm.where(User.class).equalTo(User.KEY_USERNAME, preferences.getString(Common.PREF_CURRENT_USER, ""), Case.INSENSITIVE).findFirst());
									infringement.setIdBackend(0);
									infringement.setStatus(Infringement.STATUS_PENDING);
									infringement.setStatusBackend(Infringement.STATUS_PENDING);
									infringement.setDeviceId(Utils.getImei(activity));
									//Vamos a guardar el nuevo campo para no actualizar la db
									infringement.setCopyDomain(txtCopy.getText().toString().trim());
									
									if(rbFace.isChecked()){
										infringement.setFormInfringement("P");
									}else{
										infringement.setFormInfringement("D");
									}
									
									infringement.setTypeTicket("B");
									infringement.setTicketZNumber(editTicketNumberIncluded.getText().toString().trim());
									infringement.setDate(editDate.getText().toString().trim());
									infringement.setHour(editHour.getText().toString().trim());
									infringement.setTypeDomain(txtTypeDomain.getText().toString().trim());
									
									if(!Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE){
										country = bgRealm.where(Country.class).equalTo(Common.KEY_DESCRIPTION, editCountry.getText().toString().trim(), Case.INSENSITIVE).findFirst();
									}else{
										if(!txtTypeDomain.getText().toString().trim().toLowerCase().equals("sin patente")){
											country = bgRealm.where(Country.class).equalTo(Common.KEY_DESCRIPTION, "Argentina", Case.INSENSITIVE).findFirst();
										}
									}
									
									if(country == null && !txtTypeDomain.getText().toString().trim().toLowerCase().equals("sin patente")){
										country = bgRealm.where(Country.class).equalTo(Common.KEY_DESCRIPTION, "Argentina", Case.INSENSITIVE).findFirst();
									}
									
									infringement.setCountry(country);
									infringement.setDomain(editDomain.getText().toString().trim().toUpperCase());
									typeVehicle = bgRealm.where(TypeVehicle.class).equalTo(Common.KEY_DESCRIPTION, txtTypeVehicle.getText().toString().trim(), Case.INSENSITIVE).findFirst();
									
									if(typeVehicle == null){
										int id = bgRealm.where(TypeVehicle.class).findAllSorted(TypeVehicle.KEY_ID, Sort.DESCENDING).get(0).getIdTipoVehiculo();
										id = id+1;
										//Persistir nuevo tipo de vehículo
										typeVehicle = new TypeVehicle();
										typeVehicle.setIdTipoVehiculo(id);
										typeVehicle.setDescripcion(txtTypeVehicle.getText().toString().trim());
										typeVehicle.setOther(Common.BOOL_YES);
										bgRealm.insert(typeVehicle);
									}
									
									infringement.setBetween1Imputed(typeVehicle.getDescripcion());
									infringement.setTypeVehicle(typeVehicle);
									
									if(listLine.getVisibility() == RelativeLayout.VISIBLE){
										if(txtTypeVehicle.getText().toString().trim().toLowerCase().equals("colectivo")){
											infringement.setNewLine(bgRealm.where(Company.class).equalTo(Line.KEY_LINE, txtLine.getText().toString().trim()).findFirst());
										}else{
											infringement.setNewLine(null);
										}
									}else{
										infringement.setNewLine(null);
									}
									
									if(editInternal.getVisibility() == EditText.VISIBLE && !Utils.isEmpty(editInternal.getText().toString().trim())){
										if(txtTypeVehicle.getText().toString().trim().toLowerCase().equals("colectivo")){
											infringement.setInternal(Integer.valueOf(editInternal.getText().toString().trim()));
										}else{
											infringement.setInternal(0);
										}
									}else{
										infringement.setInternal(0);
									}
									
									if(listCompany.getVisibility() == RelativeLayout.VISIBLE){
										//Se asigna directamente la company encontrada o no, ya no se presupone larga distancia porque solo admite números
										if(txtTypeVehicle.getText().toString().trim().toLowerCase().equals("colectivo") ||
											txtTypeVehicle.getText().toString().trim().toLowerCase().equals("combis")){
											infringement.setAgentDocument(txtCompany.getText().toString().trim());
											
											try{
												Company company = bgRealm.where(Company.class).equalTo(Company.KEY_EMPRESA, txtCompany.getText().toString().trim(), Case.INSENSITIVE).findFirst();
												
												if(company == null){
													company = new Company();
													int id = Integer.parseInt(bgRealm.where(Company.class).findAllSorted(Common.KEY_ID, Sort.DESCENDING).get(0).getId());
													id = id+1;
													company.setId(String.valueOf(id));
													company.setEmpresa(txtCompany.getText().toString().trim());
													company.setLinea("0");
													bgRealm.copyToRealmOrUpdate(company);
												}
												
												if(company.isValid()){
													infringement.setCompany(company);
												}
											}catch(Exception e){
											}
										}else{
											infringement.setCompany(null);
										}
									}else{
										infringement.setCompany(null);
									}
									
									if(!Utils.isEmpty(editBrand.getText().toString().trim())){
										brand = bgRealm.where(Brand.class).equalTo(Brand.KEY_MARCA, editBrand.getText().toString().trim(), Case.INSENSITIVE).findFirst();
										
										if(brand == null){
											brand = new Brand();
											brand.setId(bgRealm.where(Brand.class).findAllSorted(Common.KEY_ID, Sort.DESCENDING).get(0).getId()+1);
											brand.setMarca(editBrand.getText().toString().trim());
											bgRealm.copyToRealmOrUpdate(brand);
											infringement.setBrandId(0);
										}else{
											infringement.setBrandId(brand.getId());
										}
										
										infringement.setBrandStr(editBrand.getText().toString().trim());
									}else{
										if(!txtTypeVehicle.getText().toString().trim().toLowerCase().equals("camion")){
											infringement.setBrandId(120);
											infringement.setBrandStr("Otros");
										}else{
											infringement.setBrandId(0);
											infringement.setBrandStr("");
										}
									}
									
									infringement.setModel(editModel.getText().toString().trim());
									//Permitir elegir más de un tipo de infracción Vamos a guardar los ids de tipos de infracción para no actualizar la db
									infringement.setTypesInfringements(strTypesInfringement);
									infringement.setStreet(editPlace.getText().toString().trim());
									infringement.setHeight(editHeightPlace.getText().toString().trim());
									infringement.setAddress(editPlace.getText().toString().trim()+" "+editHeightPlace.getText().toString().trim());
									infringement.setReferencePlace(editReference.getText().toString().trim());
									infringement.setBetween1(editStreet1.getText().toString().trim());
									infringement.setBetween2(editStreet2.getText().toString().trim());
									//B Imputado
									infringement.setNameImputed(editName.getText().toString().trim());
									infringement.setLastNameImputed(editLastName.getText().toString().trim());
									infringement.setExplanationCategory(editEmail.getText().toString().trim());
									//Acá guardamos la provincia
									infringement.setApartmentImputed(editProvince.getText().toString().trim());
									infringement.setStreetImputed(editAddress.getText().toString().trim());
									infringement.setHeightImputed(editHeight.getText().toString().trim());
									infringement.setFloor(editFloor.getText().toString().trim());
									infringement.setOffice(editOffice.getText().toString().trim());
									
									if(!Utils.isEmpty(editLocality.getText().toString().trim())){
										locality = bgRealm.where(Locality.class).equalTo(Common.KEY_DESCRIPTION, editLocality.getText().toString().trim(), Case.INSENSITIVE).findFirst();
										
										if(locality == null){
											locality = new Locality();
											locality.setCodigo(System.currentTimeMillis()+"");
											locality.setDescripcion(editLocality.getText().toString().trim());
											locality.setDepartment(null);
											bgRealm.copyToRealmOrUpdate(locality);
										}
										
										infringement.setLocalityImputed(locality);
									}
									
									if(rlImputed.getVisibility() == RelativeLayout.VISIBLE){
										TypeDocument typeDocument = bgRealm.where(TypeDocument.class).equalTo(TypeDocument.KEY_DOCUMENTO, txtTypeDocument.getText().toString().trim()).findFirst();
										infringement.setTypeDocumentImputed(typeDocument);
										infringement.setBetween2Imputed(typeDocument.getDocumento());
										
										if(txtCategory.getText().toString().trim().equals("Categoría de licencia")){
											infringement.setCategories("");
										}else{
											infringement.setCategories(txtCategory.getText().toString().trim());
										}
									}
									
									infringement.setDocumentImputed(editDocument.getText().toString().trim());
									infringement.setLicense(editLicense.getText().toString().trim());
									
									if(!Utils.isEmpty(strMunicipality)){
										infringement.setIssuedBy(strMunicipality);
										//Acá vamos a guardar el Expedido por o Municipio de Licencia o centro de emisión
									}
									
									if(rbCondition.isChecked()){
										infringement.setDocumentAgent("1");
									}else{
										infringement.setDocumentAgent("0");
									}
									
									if(rbHold.isChecked()){
										infringement.setDocumentationHeld("S");
									}else{
										infringement.setDocumentationHeld("N");
									}
									
									if(rlRemissionDistance.getVisibility() == RelativeLayout.VISIBLE){
										if(rbRemissionDistance.isChecked()){
											infringement.setVehicleRemission("S");
											infringement.setDependence(txtDependenceDistance.getText().toString().trim());
										}else{
											infringement.setVehicleRemission("N");
										}
									}else{
										if(rbRemission.isChecked()){
											infringement.setVehicleRemission("S");
											infringement.setDependence(txtDependence.getText().toString().trim());
										}else{
											infringement.setVehicleRemission("N");
										}
									}
									
									//Z
									if(listTypeDocumentOwner.getVisibility() == RelativeLayout.VISIBLE){
										infringement.setTypeDocumentOwner(bgRealm.where(TypeDocument.class).equalTo(TypeDocument.KEY_DOCUMENTO, txtTypeDocumentOwner.getText().toString().trim())
											.findFirst());
									}else{
										if(rlImputed.getVisibility() == RelativeLayout.VISIBLE){
											infringement.setTypeDocumentOwner(bgRealm.where(TypeDocument.class).equalTo(TypeDocument.KEY_DOCUMENTO, txtTypeDocument.getText().toString().trim())
												.findFirst());
										}
									}
									
									if(editDocumentOwner.getVisibility() == EditText.VISIBLE){
										infringement.setDocumentOwner(editDocumentOwner.getText().toString().trim());
									}else{
										infringement.setDocumentOwner(editDocument.getText().toString().trim());
									}
									
									if(!txtColor.getText().toString().trim().equals("Color del vehículo") && rlImputedZ.getVisibility() == RelativeLayout.VISIBLE){
										color = bgRealm.where(Color.class).equalTo(Color.KEY_VALOR, txtColor.getText().toString().trim(), Case.INSENSITIVE).findFirst();
									}else{
										color = bgRealm.where(Color.class).equalTo(Color.KEY_VALOR, "Otro", Case.INSENSITIVE).findFirst();
									}
									
									if(color == null){
										color = bgRealm.where(Color.class).equalTo(Color.KEY_VALOR, "Otro", Case.INSENSITIVE).findFirst();
									}
									
									infringement.setColor(color);
									
									if(rbF.isChecked()){
										infringement.setGenderImputed("F");
									}else{
										infringement.setGenderImputed("M");
									}
									
									infringement.setZipCodeImputed(editZip.getText().toString().trim());
									infringement.setLicenseExpiration(editLicenseExpiration.getText().toString().trim());
									
									if(!Utils.isEmpty(editTicketNumber.getText().toString().trim())){
										int i;
										try{
											i = Integer.parseInt(editTicketNumber.getText().toString().trim());
										}catch(Exception e){
											i = 0;
										}
										
										if(i != 0){
											TicketB ticketB = bgRealm.where(TicketB.class).equalTo(Common.KEY_ID, Integer.parseInt(editTicketNumber.getText().toString().trim())).findFirst();
											
											if(ticketB != null){
												ticketB.setUsed(Common.BOOL_YES);
											}
										}
									}
									
									infringement.setObservations(editObservations.getText().toString().trim());
									infringement.setNumberTicketContravention(editTicketNumberContravention.getText().toString().trim());
									bgRealm.copyToRealmOrUpdate(infringement);
								}catch(Exception e){
									Utils.logError(activity, getLocalClassName()+":save:run:execute - Exception: ", e);
								}
							}
						});
						
						realm.close();
						//Enviar directo a resumen porque no quieren más la firma del imputado
						Intent intent = new Intent(activity, SummaryActivity.class);
						intent.putExtra(Common.KEY_ID, idInfringement);
						activity.startActivity(intent);
						activity.finish();
					}catch(Exception e){
						Utils.logError(activity, getLocalClassName()+":save:run - Exception: ", e);
					}
				}
			}).start();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":save - Exception: ", e);
		}
	}
	
	public boolean validateAddress(int input){
		try{
			//Anular la validación del domicilio del imputado si la provincia excede al callejero de USIG
			if(input == 4 && !editProvince.getText().toString().trim().toLowerCase().equals("capital federal") && !Utils.isEmpty(editAddress.getText().toString().trim()) &&
				!Utils.isEmpty(editHeight.getText().toString().trim())){
				return true;
			}
			
			String street	= "";
			String height	= "";
			
			switch(input){
				case 1:
					//Lugar de la infracción
					street	= editPlace.getText().toString().trim();
					height	= editHeightPlace.getText().toString().trim();
					
					if(Utils.isEmpty(street)){
						editPlace.requestFocus();
						showSoftKeyboard();
						Utils.showAlertDialog(this, "Lugar de la infracción", getString(R.string.popup_map_empty));
						return false;
					}
				break;
				
				case 2:
					//Entre primer calle, opcional
					street	= editStreet1.getText().toString().trim();
					
					if(Utils.isEmpty(street) && !Utils.isEmpty(editHeightPlace.getText().toString().trim())){
						return true;
					}else{
						if(editPlace.getText().toString().trim().equals(street) || editStreet2.getText().toString().trim().equals(street) && !Utils.isEmpty(street)){
							editStreet1.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(this, "Intersección del lugar", "Las calles de la intersección no pueden ser iguales");
							return false;
						}
					}
				break;
				
				case 3:
					//Entre segunda calle, opcional
					street	= editStreet2.getText().toString().trim();
					if(Utils.isEmpty(street) && Utils.isEmpty(editStreet1.getText().toString().trim()) && !Utils.isEmpty(editHeightPlace.getText().toString().trim())){
						return true;
					}else{
						if(Utils.isEmpty(street) && !Utils.isEmpty(editStreet1.getText().toString().trim())){
							editStreet2.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(this, "Intersección del lugar", "Ambas calles son obligatorias");
							return false;
						}else{
							if(editPlace.getText().toString().trim().equals(street) || editStreet1.getText().toString().trim().equals(street) && !Utils.isEmpty(street)){
								editStreet2.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(this, "Intersección del lugar", "Las calles de la intersección no pueden ser iguales");
								return false;
							}
						}
					}
				break;
				
				case 4:
					//Domicilio del imputado
					street	= editAddress.getText().toString().trim();
					height	= editHeight.getText().toString().trim();
					
					if(Utils.isEmpty(street) && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
						editAddress.requestFocus();
						showSoftKeyboard();
						Utils.showAlertDialog(this, "Domicilio del imputado", getString(R.string.popup_map_empty));
						return false;
					}
				break;
			}
			
			if(Common.DEBUG){
				System.out.println("Input: "+input);
				System.out.println("Street: "+street);
				System.out.println("Height: "+height);
			}
			
			Realm.init(this);
			Realm realm		= Realm.getDefaultInstance();
			Street streetDB	= realm.where(Street.class).equalTo(Street.KEY_NAME, street, Case.INSENSITIVE).findFirst();
			
			if(streetDB != null){
				if(input == 2 || input == 3){
					realm.close();
					return true;
				}else{
					if(	(streetDB.getAlturaDesde().trim().equals("0") && streetDB.getAlturaHasta().trim().equals("0")) ||
						(Utils.isEmpty(streetDB.getAlturaDesde().trim()) && Utils.isEmpty(streetDB.getAlturaHasta().trim()))){
						if(streetDB.getNombreCalle().trim().toUpperCase().startsWith("AU ") && input == 1){
							editHeightPlace.setEnabled(false);
							editStreet1.setEnabled(false);
							editStreet2.setEnabled(false);
							
							if(Utils.isEmpty(editReference.getText().toString().trim())){
								editReference.requestFocus();
								showSoftKeyboard();
							}
						}else{
							editHeightPlace.setEnabled(true);
							editStreet1.setEnabled(true);
							editStreet2.setEnabled(true);
							
							if(Utils.isEmpty(editHeightPlace.getText().toString().trim())){
								editHeightPlace.requestFocus();
								showSoftKeyboard();
							}
						}
						
						//No hay rangos definido y correcto por lo que pasa como piña
						realm.close();
						return true;
					}else{
						if(Utils.isEmpty(height)){
							if(input == 1){
								if(Utils.isEmpty(editStreet1.getText().toString().trim()) && Utils.isEmpty(editStreet2.getText().toString().trim())){
									editHeightPlace.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(this, "Lugar de la infracción", getString(R.string.popup_height)+" "+streetDB.getAlturaDesde().trim()+"-"+streetDB.getAlturaHasta().trim());
								}else{
									return true;
								}
							}else{
								if(editProvince.getText().toString().trim().toLowerCase().equals("capital federal") && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
									editHeight.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(this, "Domicilio del imputado", getString(R.string.popup_height)+" "+streetDB.getAlturaDesde().trim()+"-"+streetDB.getAlturaHasta().trim());
								}
							}
						}else{
							String pattern	= "^[0-9]*$";
							Pattern r		= Pattern.compile(pattern);
							
							if(r.matcher(height.trim()).find()){
								int number = 0;
								try{
									number = Integer.parseInt(height);
								}catch(Exception e){
									if(input == 1){
										if(Utils.isEmpty(editStreet1.getText().toString().trim()) && Utils.isEmpty(editStreet2.getText().toString().trim())){
											editHeightPlace.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(this, "Lugar de la infracción", getString(R.string.popup_height)+" "+streetDB.getAlturaDesde().trim()+"-"+streetDB.getAlturaHasta().trim());
										}else{
											return true;
										}
									}else{
										if(editProvince.getText().toString().trim().toLowerCase().equals("capital federal") && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
											editHeight.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(this, "Domicilio del imputado", getString(R.string.popup_height)+" "+streetDB.getAlturaDesde().trim()+"-"+streetDB.getAlturaHasta().trim());
										}
									}
								}
								
								//Comparamos
								if(number >= Integer.parseInt(streetDB.getAlturaDesde().trim()) && number <= Integer.parseInt(streetDB.getAlturaHasta().trim())){
									//Número correcto y dentro del rango
									realm.close();
									return true;
								}else{
									//No está en el rango de la numeración
									if(input == 1){
										if(Utils.isEmpty(editStreet1.getText().toString().trim()) && Utils.isEmpty(editStreet2.getText().toString().trim())){
											editHeightPlace.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(this, "Lugar de la infracción", getString(R.string.popup_height)+" "+streetDB.getAlturaDesde().trim()+"-"+streetDB.getAlturaHasta().trim());
										}else{
											return true;
										}
									}else{
										if(editProvince.getText().toString().trim().toLowerCase().equals("capital federal") && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
											editHeight.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(this, "Domicilio del imputado", getString(R.string.popup_height)+" "+streetDB.getAlturaDesde().trim()+"-"+streetDB.getAlturaHasta().trim());
										}
									}
								}
							}else{
								//No está en el rango de la numeración
								if(input == 1){
									if(Utils.isEmpty(editStreet1.getText().toString().trim()) && Utils.isEmpty(editStreet2.getText().toString().trim())){
										editHeightPlace.requestFocus();
										showSoftKeyboard();
										Utils.showAlertDialog(this, "Lugar de la infracción", getString(R.string.popup_height)+" "+streetDB.getAlturaDesde().trim()+"-"+streetDB.getAlturaHasta().trim());
									}else{
										return true;
									}
								}else{
									if(editProvince.getText().toString().trim().toLowerCase().equals("capital federal") && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
										editHeight.requestFocus();
										showSoftKeyboard();
										Utils.showAlertDialog(this, "Domicilio del imputado", getString(R.string.popup_height)+" "+streetDB.getAlturaDesde().trim()+"-"+streetDB.getAlturaHasta().trim());
									}
								}
							}
						}
					}
				}
			}else{
				//Calle no encontrada
				switch(input){
					case 1:
						//Lugar de la infracción
						editPlace.requestFocus();
						showSoftKeyboard();
						Utils.showAlertDialog(this, "Lugar de la infracción", getString(R.string.popup_notfound));
					break;
					
					case 2:
						if(editStreet1.isEnabled()){
							//Entre primer calle, opcional
							editStreet1.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(this, "Primera calle de intersección", getString(R.string.popup_notfound));
						}else{
							return true;
						}
					break;
					
					case 3:
						if(editStreet2.isEnabled()){
							//Entre segunda calle, opcional
							editStreet2.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(this, "Segunda calle de intersección", getString(R.string.popup_notfound));
						}else{
							return true;
						}
					break;
					
					case 4:
						//Domicilio del imputado
						if(editProvince.getText().toString().trim().toLowerCase().equals("capital federal") && rlImputed.getVisibility() == RelativeLayout.VISIBLE){
							editAddress.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(this, "Domicilio del imputado", getString(R.string.popup_notfound));
						}
					break;
				}
			}
			
			showSoftKeyboard();
			realm.close();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":validateAddress - Exception: ", e);
		}
		
		return false;
	}
	
	public void askModality(){
		try{
			if(idInfringement == 0 && !rbFace.isChecked() && !rbDistance.isChecked()){
				final AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setCancelable(false);
				builder.setMessage("¿Este nuevo acta es presencial o a distancia?");
				builder.setPositiveButton("A distancia", new DialogInterface.OnClickListener(){
					@Override
					public void onClick(final DialogInterface dialogInterface, final int i){
						rbDistance.setChecked(true);
					}
				});
				builder.setNegativeButton("Presencial", new DialogInterface.OnClickListener(){
					@Override
					public void onClick(final DialogInterface dialogInterface, final int i){
						rbFace.setChecked(true);
					}
				});
				
				if(!isFinishing()){
					if(!Utils.checkVersion(this, true)){
						if(!rbFace.isChecked() && !rbDistance.isChecked()){
							hideSoftKeyboard();
							builder.show();
						}
					}
				}
			}
			
			refreshTickets();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":askModality - Exception: ", e);
		}
	}
	
	public static void logout(final Context context){
		try{
			SharedPreferences preferences = context.getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			final String token = preferences.getString(Common.PREF_TOKEN, "");
			//Implementación de logout a último momento como todas las cosas
			new Thread(){
				@Override
				public void run(){
					try{
						Api.request(Api.LOGOUT, Api.METHOD_POST, "", token, context, Common.AUTH_BASIC);
						Utils.cleanDirectory(context);
						//Limpiar actas ya enviados
						Realm realm = Realm.getDefaultInstance();
						realm.executeTransaction(new Realm.Transaction(){
							@Override
							public void execute(Realm realm){
								RealmResults<Infringement> infringements = realm.where(Infringement.class).equalTo(Common.KEY_STATUS, Infringement.STATUS_SENDED).findAll();
								
								if(infringements.size() > 0){
									infringements.deleteAllFromRealm();
								}
							}
						});
						
						realm.close();
					}catch(Exception e){
						Utils.logError(context, "MainActivity:logout:start - Exception: ", e);
					}
				}
			}.start();
			
			SharedPreferences.Editor editor = preferences.edit();
			editor.putBoolean(Common.PREF_SESSION_STARTED, false);
			editor.putString(Common.PREF_CURRENT_LAT, String.valueOf(Common.DEFAULT_LAT));
			editor.putString(Common.PREF_CURRENT_LON, String.valueOf(Common.DEFAULT_LON));
			editor.putString(Common.PREF_SELECTED_LAT, "");
			editor.putString(Common.PREF_SELECTED_LON, "");
			editor.putString(Common.PREF_CURRENT_USER, "");
			editor.putString(Common.PREF_CURRENT_PASS, "");
			editor.putString(Common.PREF_TOKEN, "");
			editor.apply();
			Intent intent = new Intent(context, LoginActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		}catch(Exception e){
			Utils.logError(context, "MainActivity:logout - Exception: ", e);
		}
	}
	
	public void onLogout(){
		try{
			hideSoftKeyboard();
			final AlertDialog.Builder builder	= new AlertDialog.Builder(this);
			final Activity activity				= this;
			preferences							= getApplicationContext().getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			builder.setMessage(getString(R.string.popup_logout));
			builder.setCancelable(false);
			builder.setPositiveButton(R.string.btn_logout, new DialogInterface.OnClickListener(){
				@Override
				public void onClick(final DialogInterface dialogInterface, final int i){
					logout(activity);
				}
			});
			builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(){
				@Override
				public void onClick(final DialogInterface dialogInterface, final int i){
				}
			});
			builder.show();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onLogout - Exception: ", e);
		}
	}
	
	public void setOtherMunicipality(){
		try{
			new MaterialDialog.Builder(this).title("Municipio Licencia").inputType(InputType.TYPE_CLASS_TEXT).positiveText("Confirmar").cancelable(false).inputRange(0, 30)
				.input("Especifica el valor", "", new MaterialDialog.InputCallback(){
					@Override
					public void onInput(@NonNull MaterialDialog dialog, CharSequence input){
						if(input != null){
							if(input != ""){
								otherMunicipality = input.toString().trim();
								
								if(!Utils.isEmpty(otherMunicipality)){
									//Seleccionar nuevo valor y recargar lista
									strMunicipality = otherMunicipality;
									listMunicipalities.add(otherMunicipality);
									refreshMunicipalities();
								}
							}
						}
					}
				}).show();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":setOtherMunicipality - Exception: ", e);
		}
	}
	
	public void inputDialog(final String title, final TextView textView, final String value, int maxLenght){
		try{
			new MaterialDialog.Builder(this).title(title).inputType(InputType.TYPE_CLASS_TEXT).positiveText("Confirmar").cancelable(false).inputRange(0, maxLenght)
				.input("Especifica el valor", value, new MaterialDialog.InputCallback(){
					@Override
					public void onInput(@NonNull MaterialDialog dialog, CharSequence input){
						if(input != null){
							if(input != ""){
								final String name = input.toString().trim();
								
								if(!Utils.isEmpty(name)){
									textView.setText(name);
									textView.setTextColor(android.graphics.Color.BLACK);
								}else{
									textView.setText("");
								}
							}else{
								textView.setText("");
							}
						}else{
							textView.setText("");
						}
					}
				}).show();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":inputDialog - Exception: ", e);
		}
	}
	
	public void showList(View v){
		try{
			final Activity activity	= this;
			boolean flag			= true;
			Integer action			= Integer.valueOf(v.getTag().toString());
			
			switch(action){
				case 91:
					action	= 1;
					flag	= false;
				break;
				
				case 93:
					action	= 3;
					flag	= false;
				break;
				
				case 94:
					action	= 4;
					flag	= false;
				break;
				
				case 95:
					action	= 5;
					flag	= false;
				break;
				
				
				case 98:
					action	= 8;
					flag	= false;
				break;
				
				case 99:
					action	= 9;
					flag	= false;
				break;
				
				case 910:
					action	= 10;
					flag	= false;
				break;
				
				case 911:
					action	= 11;
					flag	= false;
				break;
				
				case 912:
					action	= 12;
					flag	= false;
				break;
			}
			
			final boolean reValidate = flag;
			
			switch(action){
				case 1:
					//Validar campos previos a Tipo Dominio
					if(!rbFace.isChecked() && !rbDistance.isChecked() && reValidate){
						Utils.showAlertDialog(this, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim()) && reValidate){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(this, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), this).equals("OK") && reValidate){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(this, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), this));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), this) && reValidate){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(this, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									//listTypeDomain
									new MaterialDialog.Builder(this)
										.title(R.string.label_typedomain)
										.cancelable(true)
										.items(listTypesDomain)
										.itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice(){
											@Override
											public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text){
												txtTypeDomain.setTextColor(android.graphics.Color.BLACK);
												txtTypeDomain.setText(text);
												editDomain.setEnabled(true);
												editCountry.setVisibility(AutoCompleteTextView.GONE);
												editCountry.setText("");
												editDomain.setText("");
												txtCompany.setText("Empresa");
												txtLine.setText("Línea");
												editInternal.setText("");
												editModel.setText("");
												listLine.setVisibility(RelativeLayout.GONE);
												editInternal.setVisibility(EditText.GONE);
												listCompany.setVisibility(RelativeLayout.GONE);
												txtTypeVehicle.setText("Tipo de vehículo");
												
												switch(text.toString().trim().toLowerCase()){
													case "nuevo":
														//Hasta 7 digitos, alfanumérico
														editDomain.setFilters(new InputFilter[]{Utils.getAlphanumericFilter(), new InputFilter.LengthFilter(7)});
													break;
													
													case "viejo":
														//Hasta 6 digitos, alfanumérico
														editDomain.setFilters(new InputFilter[]{Utils.getAlphanumericFilter(), new InputFilter.LengthFilter(6)});
													break;
													
													case "sin patente":
														//Hasta 10 digitos, alfanumérico pero lo bloqueamos
														if(rbDistance.isChecked()){
															editDomain.setText("");
															editDomain.setEnabled(false);
															//Regla cochina de fijar Colectivo
															txtTypeVehicle.setTextColor(android.graphics.Color.BLACK);
															txtTypeVehicle.setText("COLECTIVO");
															listLine.setVisibility(RelativeLayout.VISIBLE);
															editInternal.setVisibility(EditText.VISIBLE);
															listCompany.setVisibility(RelativeLayout.VISIBLE);
															editModel.setVisibility(AutoCompleteTextView.GONE);
															//hacer foco en línea
															view.setTag(4);
															showList(view);
														}else{
															editDomain.setText("");
															editDomain.setEnabled(false);
														}
														
														editDomain.setFilters(new InputFilter[]{Utils.getAlphanumericFilter(), new InputFilter.LengthFilter(9)});
													break;

													default:
														editCountry.setVisibility(AutoCompleteTextView.VISIBLE);
														editCountry.requestFocus();
														showSoftKeyboard();
														//Hasta 10 digitos, alfanumérico
														editDomain.setFilters(new InputFilter[]{Utils.getAlphanumericFilter(), new InputFilter.LengthFilter(10)});
													break;
												}
												
												//Revalidar dominio si cambio tipo de dominio
												if(!txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo")){
													if(!Utils.validateDomain(editDomain.getText().toString(), txtTypeVehicle.getText().toString(),
														txtTypeDomain.getText().toString(), activity,
														editCountry.getText().toString().trim()).equals("")){
														if(Utils.validateDomain(editDomain.getText().toString(), txtTypeVehicle.getText().toString(),
															txtTypeDomain.getText().toString(), activity,
															editCountry.getText().toString().trim()).equals("block")){
															editDomain.setText("");
															editDomain.setEnabled(false);
														}else{
															Utils.showAlertDialog(activity, "Número de dominio", Utils.validateDomain(editDomain.getText().toString(),
																txtTypeVehicle.getText().toString(), txtTypeDomain.getText().toString(), activity,
																editCountry.getText().toString().trim()));
														}
													}
												}
												
												if(text.toString().trim().toLowerCase().equals("internacional")){
													editCountry.requestFocus();
												}else{
													editDomain.requestFocus();
												}
												
												showSoftKeyboard();
												return true;
											}
										})
										.show();
								}
							}
						}
					}
				break;
				
				case 3:
					//Validar campos previos a Tipo Vehículo
					if(!rbFace.isChecked() && !rbDistance.isChecked() && reValidate){
						Utils.showAlertDialog(this, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim()) && reValidate){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(this, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), this).equals("OK") && reValidate){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(this, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), this));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), this) && reValidate){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(this, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio") && reValidate){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(this, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) &&
											editCountry.getVisibility() == AutoCompleteTextView.VISIBLE && reValidate){
											editCountry.requestFocus();
											Utils.showAlertDialog(this, "País", "Es un dato requerido");
										}else{
											//listTypeVehicle
											new MaterialDialog.Builder(this)
												.title(R.string.label_typevehicle)
												.cancelable(true)
												.items(listTypesVehicle)
												.itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice(){
													@Override
													public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text){
														txtTypeVehicle.setTextColor(android.graphics.Color.BLACK);
														txtTypeVehicle.setText(text);
														editDomain.setEnabled(true);
														txtCompany.setText("Empresa");
														txtLine.setText("Línea");
														editInternal.setText("");
														
														//Regla rebuscada corregida
														if(text.toString().toLowerCase().trim().equals("colectivo")){
															listLine.setVisibility(RelativeLayout.VISIBLE);
															editInternal.setVisibility(EditText.VISIBLE);
															listCompany.setVisibility(RelativeLayout.VISIBLE);
															editModel.setVisibility(AutoCompleteTextView.GONE);
															editModel.setText("");
															//hacer foco en línea
															view.setTag(4);
															showList(view);
														}else{
															listLine.setVisibility(RelativeLayout.GONE);
															editInternal.setVisibility(EditText.GONE);
															editModel.setVisibility(AutoCompleteTextView.VISIBLE);
															
															if(txtTypeDomain.getText().toString().trim().toLowerCase().equals("sin patente")){
																editDomain.setEnabled(false);
																editDomain.setText("");
															}
															
															if(text.toString().toLowerCase().trim().equals("combi") ||
																text.toString().toLowerCase().trim().equals("combis")){
																listCompany.setVisibility(RelativeLayout.VISIBLE);
																
																if(txtCompany.getText().toString().trim().equals("Empresa")){
																	inputDialog("Empresa de Combis", txtCompany, "", 50);
																}else{
																	inputDialog("Empresa de Combis", txtCompany, txtCompany.getText().toString().trim(), 50);
																}
															}else{
																listCompany.setVisibility(RelativeLayout.GONE);
																
																if(	text.toString().toLowerCase().trim().equals("otros") ||
																	text.toString().toLowerCase().trim().equals("otro") ||
																	text.toString().toLowerCase().trim().equals("otra") ||
																	text.toString().toLowerCase().trim().equals("otras")){
																	//inputDialog("Especificar tipo de vehículo", txtTypeVehicle, "", 40);
																}else{
																	//hacer foco en marca
																	showSoftKeyboard();
																	editBrand.requestFocus();
																}
															}
														}
														
														//Validar dominio al terminar de tocar
														if(!Utils.validateDomain(editDomain.getText().toString(), txtTypeVehicle.getText().toString(),
															txtTypeDomain.getText().toString(), activity,
															editCountry.getText().toString().trim()).equals("")){
															if(Utils.validateDomain(editDomain.getText().toString(), txtTypeVehicle.getText().toString(),
																txtTypeDomain.getText().toString(), activity,
																editCountry.getText().toString().trim()).equals("block")){
																editDomain.setText("");
																editDomain.setEnabled(false);
															}else{
																editDomain.requestFocus();
																showSoftKeyboard();
																Utils.showAlertDialog(activity, "Número de dominio",
																	Utils.validateDomain(editDomain.getText().toString(), txtTypeVehicle.getText().toString(),
																	txtTypeDomain.getText().toString(), activity, editCountry.getText().toString().trim()));
															}
														}else{
															//hacer foco en marca
															showSoftKeyboard();
															editBrand.requestFocus();
														}
														
														return true;
													}
												})
												.show();
										}
									}
								}
							}
						}
					}
				break;
				
				case 4:
					//Validar campos previos a Línea
					if(!rbFace.isChecked() && !rbDistance.isChecked() && reValidate){
						Utils.showAlertDialog(this, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim()) && reValidate){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(this, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), this).equals("OK") && reValidate){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(this, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), this));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), this) && reValidate){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(this, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio") && reValidate){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(this, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) &&
											editCountry.getVisibility() == AutoCompleteTextView.VISIBLE && reValidate){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(this, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo") && reValidate){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(this, "Tipo de vehículo", "Es un dato requerido");
											}else{
												//listLines
												new MaterialDialog.Builder(this)
													.title(R.string.label_line)
													.cancelable(true)
													.items(listLines)
													.itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice(){
														@Override
														public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text){
															txtLine.setTextColor(android.graphics.Color.BLACK);
															txtLine.setText(text);
															
															//Contemplar si es Larga distancia
															if(text.toString().trim().toLowerCase().equals("larga distancia") ||
																text.toString().trim().toLowerCase().equals("sin identificar")){
																inputDialog("Empresa de Línea "+text.toString().trim(), txtCompany, "", 50);
															}else{
																//Seleccionamos la empresa en base a la línea.
																Realm realm						= Realm.getDefaultInstance();
																String companyStr				= realm.where(Company.class)
																	.equalTo(Line.KEY_LINE, text.toString()).findFirst().getEmpresa();
																RealmResults<Company> companies	= realm.where(Company.class)
																	.equalTo(Line.KEY_LINE, text.toString()).findAllSorted(Company.KEY_EMPRESA)
																	.distinct(Company.KEY_EMPRESA);
																
																if(companies.size() > 0){
																	listCompanies.clear();
																	
																	for(Company company : companies){
																		listCompanies.add(company.getEmpresa());
																	}
																}
																
																realm.close();
																txtCompany.setTextColor(android.graphics.Color.BLACK);
																txtCompany.setText(companyStr);
															}
															
															return true;
														}
													})
													.show();
											}
										}
									}
								}
							}
						}
					}
				break;
				
				case 5:
					//Validar campos previos a Empresa
					if(!rbFace.isChecked() && !rbDistance.isChecked() && reValidate){
						Utils.showAlertDialog(this, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim()) && reValidate){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(this, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), this).equals("OK") && reValidate){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(this, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), this));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), this) && reValidate){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(this, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio") && reValidate){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(this, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) &&
											editCountry.getVisibility() == AutoCompleteTextView.VISIBLE && reValidate){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(this, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo") && reValidate){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(this, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") &&
													listLine.getVisibility() == RelativeLayout.VISIBLE && reValidate){
													txtLine.requestFocus();
													Utils.showAlertDialog(this, "Línea", "Es un dato requerido");
												}else{
													if(txtTypeVehicle.getText().toString().toLowerCase().trim().equals("combis") ||
														txtTypeVehicle.getText().toString().toLowerCase().trim().equals("combi")){
														if(txtCompany.getText().toString().trim().equals("Empresa")){
															inputDialog("Empresa de Combis", txtCompany, "", 50);
														}else{
															inputDialog("Empresa de Combis", txtCompany, txtCompany.getText().toString().trim(), 50);
														}
													}else{
														//Contemplar si es Larga distancia
														if(txtLine.getText().toString().toLowerCase().trim().equals("larga distancia") ||
															txtLine.getText().toString().toLowerCase().trim().equals("sin identificar")){
															if(txtCompany.getText().toString().trim().equals("Empresa")){
																inputDialog("Empresa de Línea", txtCompany, "", 50);
															}else{
																inputDialog("Empresa de Línea", txtCompany, txtCompany.getText().toString().trim(), 50);
															}
														}else{
															new MaterialDialog.Builder(this)
																.title("Empresa")
																.cancelable(true)
																.items(listCompanies)
																.itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice(){
																	@Override
																	public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text){
																		txtCompany.setTextColor(android.graphics.Color.BLACK);
																		txtCompany.setText(text);
																		
																		if(	text.toString().toLowerCase().trim().equals("otros") ||
																			text.toString().toLowerCase().trim().equals("otro") ||
																			text.toString().toLowerCase().trim().equals("otra") ||
																			text.toString().toLowerCase().trim().equals("otras")){
																			inputDialog("Especificar empresa", txtCompany, "", 50);
																		}
																		
																		return true;
																	}
																})
																.show();
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				break;
				
				case 8:
					//Validar campos previos a Tipo documento
					if(!rbFace.isChecked() && !rbDistance.isChecked() && reValidate){
						Utils.showAlertDialog(this, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim()) && reValidate){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(this, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), this).equals("OK") && reValidate){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(this, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), this));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), this)){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(this, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio") && reValidate){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(this, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) &&
											editCountry.getVisibility() == AutoCompleteTextView.VISIBLE && reValidate){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(this, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo") && reValidate){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(this, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") &&
													listLine.getVisibility() == RelativeLayout.VISIBLE && reValidate){
													txtLine.requestFocus();
													Utils.showAlertDialog(this, "Línea", "Es un dato requerido");
												}else{
													if(txtCompany.getText().toString().trim().equals("Empresa") &&
														listCompany.getVisibility() == RelativeLayout.VISIBLE && reValidate){
														txtCompany.requestFocus();
														Utils.showAlertDialog(this, "Empresa", "Es un dato requerido");
													}else{
														if(Utils.isEmpty(strTypesInfringement) && reValidate){
															Utils.showAlertDialog(this, "Tipo de infracción", "Es un dato requerido");
														}else{
															if(!validateAddress(1)){
																showSoftKeyboard();
															}else{
																if(!validateAddress(2)){
																	showSoftKeyboard();
																}else{
																	if(!validateAddress(3)){
																		showSoftKeyboard();
																	}else{
																		if(editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") &&
																			Utils.isEmpty(editReference.getText().toString().trim()) && reValidate){
																			//Es autopista y tiene que tener referencia
																			editReference.requestFocus();
																			showSoftKeyboard();
																			Utils.showAlertDialog(this, "Referencia de lugar", "Es un dato requerido");
																		}else{
																			if(	!editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") &&
																				Utils.isEmpty(editHeightPlace.getText().toString()) &&
																				Utils.isEmpty(editStreet1.getText().toString().trim()) &&
																				Utils.isEmpty(editStreet2.getText().toString().trim()) && reValidate){
																				editHeightPlace.requestFocus();
																				showSoftKeyboard();
																				Utils.showAlertDialog(this, "Lugar", "Debe cargar la altura o intersección");
																			}else{
																				if(Utils.isEmpty(editName.getText().toString()) &&
																					rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																					editName.requestFocus();
																					showSoftKeyboard();
																					Utils.showAlertDialog(this, "Nombre y apellido", "Es un dato requerido");
																				}else{
																					if(!validateAddress(4) && rlImputed.getVisibility() == RelativeLayout.VISIBLE &&
																						reValidate){
																						showSoftKeyboard();
																					}else{
																						if(	Utils.isEmpty(editHeight.getText().toString().trim()) &&
																							rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																							editHeight.requestFocus();
																							showSoftKeyboard();
																							Utils.showAlertDialog(this, "Altura del domicilio", "Es un dato requerido");
																						}else{
																							if(	Utils.isEmpty(editProvince.getText().toString()) &&
																								rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																								editProvince.requestFocus();
																								showSoftKeyboard();
																								Utils.showAlertDialog(this, "Provincia", "Es un dato requerido");
																							}else{
																								//listTypeDocument
																								new MaterialDialog.Builder(this)
																									.title("Tipo de documento")
																									.cancelable(true)
																									.items(listTypesDocument)
																									.itemsCallbackSingleChoice(-1,
																										new MaterialDialog.ListCallbackSingleChoice(){
																										@Override
																										public boolean onSelection(MaterialDialog dialog, View view,
																																	int which, CharSequence text){
																											txtTypeDocument.setTextColor(android.graphics.Color.BLACK);
																											txtTypeDocument.setText(text);
																											
																											if(	text.toString().trim().toLowerCase()
																												.equals("libreta de enrolamiento") ||
																												text.toString().trim().toLowerCase()
																												.equals("libreta cívica") ||
																												text.toString().trim().toLowerCase()
																												.equals("libreta civica")){
																												editDocument.setFilters(new InputFilter[]{Utils.getNumericFilter(), new InputFilter.LengthFilter(7)});
																											}else{
																												if(	text.toString().trim().toLowerCase().equals("documento nacional de identidad") ||
																													text.toString().trim().toLowerCase().equals("cédula de identidad") ||
																													text.toString().trim().toLowerCase().equals("cedula de identidad") ||
																													text.toString().trim().toLowerCase().equals("dni")){
																													editDocument.setFilters(new InputFilter[]{Utils.getNumericFilter(), new InputFilter.LengthFilter(8)});
																												}else{
																													editDocument.setFilters(new InputFilter[]{Utils.getAlphanumericFilter(),
																														new InputFilter.LengthFilter(10)});
																												}
																											}
																											
																											editDocument.setEnabled(true);
																											return true;
																										}
																									})
																									.show();
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				break;
				
				case 10:
					//Validar campos previos a Categoría Licencia
					if(!rbFace.isChecked() && !rbDistance.isChecked() && reValidate){
						Utils.showAlertDialog(this, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim()) && reValidate){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(this, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), this).equals("OK") && reValidate){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(this, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), this));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), this) && reValidate){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(this, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio") && reValidate){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(this, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) &&
											editCountry.getVisibility() == AutoCompleteTextView.VISIBLE && reValidate){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(this, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo") && reValidate){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(this, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE && reValidate){
													txtLine.requestFocus();
													Utils.showAlertDialog(this, "Línea", "Es un dato requerido");
												}else{
													if(txtCompany.getText().toString().trim().equals("Empresa") &&
														listCompany.getVisibility() == RelativeLayout.VISIBLE && reValidate){
														txtCompany.requestFocus();
														Utils.showAlertDialog(this, "Empresa", "Es un dato requerido");
													}else{
														if(Utils.isEmpty(strTypesInfringement) && reValidate){
															Utils.showAlertDialog(this, "Tipo de infracción", "Es un dato requerido");
														}else{
															if(!validateAddress(1) && reValidate){
																showSoftKeyboard();
															}else{
																if(!validateAddress(2) && reValidate){
																	showSoftKeyboard();
																}else{
																	if(!validateAddress(3) && reValidate){
																		showSoftKeyboard();
																	}else{
																		if(editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editReference.getText().toString().trim()) && reValidate){
																			//Es autopista y tiene que tener referencia
																			editReference.requestFocus();
																			showSoftKeyboard();
																			Utils.showAlertDialog(this, "Referencia de lugar", "Es un dato requerido");
																		}else{
																			if(	!editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editHeightPlace.getText().toString()) &&
																				Utils.isEmpty(editStreet1.getText().toString().trim()) && Utils.isEmpty(editStreet2.getText().toString().trim()) && reValidate){
																				editHeightPlace.requestFocus();
																				showSoftKeyboard();
																				Utils.showAlertDialog(this, "Lugar", "Debe cargar la altura o intersección");
																			}else{
																				if(Utils.isEmpty(editName.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																					editName.requestFocus();
																					showSoftKeyboard();
																					Utils.showAlertDialog(this, "Nombre y apellido", "Es un dato requerido");
																				}else{
																					if(!validateAddress(4) && rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																						showSoftKeyboard();
																					}else{
																						if(	Utils.isEmpty(editHeight.getText().toString().trim()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																							editHeight.requestFocus();
																							showSoftKeyboard();
																							Utils.showAlertDialog(this, "Altura del domicilio", "Es un dato requerido");
																						}else{
																							if(Utils.isEmpty(editProvince.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																								editProvince.requestFocus();
																								showSoftKeyboard();
																								Utils.showAlertDialog(this, "Provincia", "Es un dato requerido");
																							}else{
																								if(	txtTypeDocument.getText().toString().trim().equals("Tipo de documento") &&
																									rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																									txtTypeDocument.requestFocus();
																									Utils.showAlertDialog(this, "Tipo de documento", "Es un dato requerido");
																								}else{
																									if(	Utils.isEmpty(editDocument.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE &&
																										reValidate){
																										editDocument.requestFocus();
																										showSoftKeyboard();
																										Utils.showAlertDialog(this, "Nro. de documento", "Es un dato requerido");
																									}else{
																										if(!Utils.isEmpty(editLicense.getText().toString().trim())){
																											new MaterialDialog.Builder(this)
																												.title("Categoría de licencia")
																												.cancelable(true)
																												.items(listCategories)
																												.itemsCallbackMultiChoice(null, new MaterialDialog.ListCallbackMultiChoice(){
																													@Override
																													public boolean onSelection(	MaterialDialog dialog, Integer[] which, CharSequence[] text){
																														if(text.length > 0){
																															txtCategory.setTextColor(android.graphics.Color.BLACK);
																															String category		= "";
																															boolean hasOther	= false;
																															
																															for(CharSequence part : text){
																																if(	!part.toString().toLowerCase().trim().equals("otros") &&
																																	!part.toString().toLowerCase().trim().equals("otro") &&
																																	!part.toString().toLowerCase().trim().equals("otra") &&
																																	!part.toString().toLowerCase().trim().equals("otras")){
																																	category += part+",";
																																}else{
																																	hasOther = true;
																																}
																															}
																															
																															if(!Utils.isEmpty(category)){
																																txtCategory.setText(category.substring(0, category.length()-1));
																															}else{
																																txtCategory.setText(category);
																															}
																															
																															if(hasOther){
																																category = txtCategory.getText().toString().trim().replace("Otros,", "");
																																
																																if(!Utils.isEmpty(category)){
																																	category += ",";
																																}
																																
																																category += "Otros: ";
																																txtCategory.setText(category);
																																
																																if(txtCategory.getText().toString().trim().equals("Categoría de licencia")){
																																	inputDialog("Especificar Categoría", txtCategory, "", 30);
																																}else{
																																	inputDialog("Especificar Categoría", txtCategory,
																																		txtCategory.getText().toString().trim(), 30);
																																}
																															}
																														}
																														
																														return true;
																													}
																												})
																												.positiveText("Confirmar")
																												.show();
																										}else{
																											editLicense.requestFocus();
																											showSoftKeyboard();
																											Utils.showAlertDialog(this, "Licencia de conductor", "Es necesario antes de cargar la categoría de licencia");
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				break;
				
				case 11:
					//Validar campos previos a Dependencia
					if(!rbFace.isChecked() && !rbDistance.isChecked() && reValidate){
						Utils.showAlertDialog(this, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim()) && reValidate){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(this, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), this).equals("OK") && reValidate){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(this, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), this));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), this) && reValidate){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(this, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio") && reValidate){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(this, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE && reValidate){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(this, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo") && reValidate){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(this, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE && reValidate){
													txtLine.requestFocus();
													Utils.showAlertDialog(this, "Línea", "Es un dato requerido");
												}else{
													if(txtCompany.getText().toString().trim().equals("Empresa") && listCompany.getVisibility() == RelativeLayout.VISIBLE && reValidate){
														txtCompany.requestFocus();
														Utils.showAlertDialog(this, "Empresa", "Es un dato requerido");
													}else{
														if(Utils.isEmpty(strTypesInfringement)){
															Utils.showAlertDialog(this, "Tipo de infracción", "Es un dato requerido");
														}else{
															if(!validateAddress(1) && reValidate){
																showSoftKeyboard();
															}else{
																if(!validateAddress(2) && reValidate){
																	showSoftKeyboard();
																}else{
																	if(!validateAddress(3) && reValidate){
																		showSoftKeyboard();
																	}else{
																		if(	editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editReference.getText().toString().trim()) &&
																			reValidate){
																			//Es autopista y tiene que tener referencia
																			editReference.requestFocus();
																			showSoftKeyboard();
																			Utils.showAlertDialog(this, "Referencia de lugar", "Es un dato requerido");
																		}else{
																			if(	!editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") && Utils.isEmpty(editHeightPlace.getText().toString()) &&
																				Utils.isEmpty(editStreet1.getText().toString().trim()) && Utils.isEmpty(editStreet2.getText().toString().trim()) && reValidate){
																				editHeightPlace.requestFocus();
																				showSoftKeyboard();
																				Utils.showAlertDialog(this, "Lugar", "Debe cargar la altura o intersección");
																			}else{
																				if(Utils.isEmpty(editName.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																					editName.requestFocus();
																					showSoftKeyboard();
																					Utils.showAlertDialog(this, "Nombre y apellido", "Es un dato requerido");
																				}else{
																					if(!validateAddress(4) && rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																						showSoftKeyboard();
																					}else{
																						if(	Utils.isEmpty(editHeight.getText().toString().trim()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																							editHeight.requestFocus();
																							showSoftKeyboard();
																							Utils.showAlertDialog(this, "Altura del domicilio", "Es un dato requerido");
																						}else{
																							if(Utils.isEmpty(editProvince.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																								editProvince.requestFocus();
																								showSoftKeyboard();
																								Utils.showAlertDialog(this, "Provincia", "Es un dato requerido");
																							}else{
																								if(	txtTypeDocument.getText().toString().trim().equals("Tipo de documento") &&
																									rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																									txtTypeDocument.requestFocus();
																									Utils.showAlertDialog(this, "Tipo de documento", "Es un dato requerido");
																								}else{
																									if(	Utils.isEmpty(editDocument.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE &&
																										reValidate){
																										editDocument.requestFocus();
																										showSoftKeyboard();
																										Utils.showAlertDialog(this, "Nro. de documento", "Es un dato requerido");
																									}else{
																										if(	!rbHold.isChecked() && !rbNoHold.isChecked() && rlImputed.getVisibility() == RelativeLayout.VISIBLE &&
																											reValidate){
																											rbHold.requestFocus();
																											Utils.showAlertDialog(this, "Documentación retenida", "Es un dato requerido");
																										}else{
																											if(	!rbRemission.isChecked() && !rbNoRemission.isChecked() &&
																												rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																												rbRemission.requestFocus();
																												Utils.showAlertDialog(this, "Vehículo retenido", "Es un dato requerido");
																											}else{
																												//listDependence
																												new MaterialDialog.Builder(this)
																													.title("Dependencia")
																													.cancelable(true)
																													.items(listDependences)
																													.itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice(){
																														@Override
																														public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text){
																															txtDependence.setTextColor(android.graphics.Color.BLACK);
																															txtDependence.setText(text);
																															
																															if(	text.toString().toLowerCase().trim().equals("otros") ||
																																text.toString().toLowerCase().trim().equals("otro") ||
																																text.toString().toLowerCase().trim().equals("otra") ||
																																text.toString().toLowerCase().trim().equals("otras")){
																																inputDialog("Especificar dependencia", txtDependence, "", 50);
																															}
																															
																															return true;
																														}
																													})
																													.show();
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				break;
				
				case 12:
					//Validar campos previos a Dependencia en A distancia
					if(!rbFace.isChecked() && !rbDistance.isChecked() && reValidate){
						Utils.showAlertDialog(this, "Modalidad del acta", "Es un dato requerido");
						askModality();
					}else{
						if(Utils.isEmpty(editDate.getText().toString().trim()) && reValidate){
							editDate.requestFocus();
							showSoftKeyboard();
							Utils.showAlertDialog(this, "Fecha del acta", "Es un dato requerido");
						}else{
							if(!Utils.isStringDate(editDate.getText().toString().trim(), this).equals("OK") && reValidate){
								editDate.requestFocus();
								showSoftKeyboard();
								Utils.showAlertDialog(this, "Fecha del acta", Utils.isStringDate(editDate.getText().toString().trim(), this));
							}else{
								if(Utils.isFutureDate(editDate.getText().toString().trim(), this) && reValidate){
									editDate.requestFocus();
									showSoftKeyboard();
									Utils.showAlertDialog(this, "Fecha del acta", "No se permite una fecha mayor a la actual");
								}else{
									if(txtTypeDomain.getText().toString().trim().equals("Tipo de dominio") && reValidate){
										txtTypeDomain.requestFocus();
										Utils.showAlertDialog(this, "Tipo de dominio", "Es un dato requerido");
									}else{
										if(Utils.isEmpty(editCountry.getText().toString().trim()) && editCountry.getVisibility() == AutoCompleteTextView.VISIBLE && reValidate){
											editCountry.requestFocus();
											showSoftKeyboard();
											Utils.showAlertDialog(this, "País", "Es un dato requerido");
										}else{
											if(txtTypeVehicle.getText().toString().trim().equals("Tipo de vehículo") && reValidate){
												txtTypeVehicle.requestFocus();
												Utils.showAlertDialog(this, "Tipo de vehículo", "Es un dato requerido");
											}else{
												if(txtLine.getText().toString().trim().equals("Línea") && listLine.getVisibility() == RelativeLayout.VISIBLE && reValidate){
													txtLine.requestFocus();
													Utils.showAlertDialog(this, "Línea", "Es un dato requerido");
												}else{
													if(txtCompany.getText().toString().trim().equals("Empresa") && listCompany.getVisibility() == RelativeLayout.VISIBLE && reValidate){
														txtCompany.requestFocus();
														Utils.showAlertDialog(this, "Empresa", "Es un dato requerido");
													}else{
														if(Utils.isEmpty(strTypesInfringement)){
															Utils.showAlertDialog(this, "Tipo de infracción", "Es un dato requerido");
														}else{
															if(!validateAddress(1) && reValidate){
																showSoftKeyboard();
															}else{
																if(!validateAddress(2) && reValidate){
																	showSoftKeyboard();
																}else{
																	if(!validateAddress(3) && reValidate){
																		showSoftKeyboard();
																	}else{
																		if(	editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") &&
																			Utils.isEmpty(editReference.getText().toString().trim()) &&
																				reValidate){
																			//Es autopista y tiene que tener referencia
																			editReference.requestFocus();
																			showSoftKeyboard();
																			Utils.showAlertDialog(this, "Referencia de lugar", "Es un dato requerido");
																		}else{
																			if(	!editPlace.getText().toString().trim().toUpperCase().startsWith("AU ") &&
																				Utils.isEmpty(editHeightPlace.getText().toString()) && Utils.isEmpty(editStreet1.getText().toString().trim()) &&
																				Utils.isEmpty(editStreet2.getText().toString().trim()) && reValidate){
																				editHeightPlace.requestFocus();
																				showSoftKeyboard();
																				Utils.showAlertDialog(this, "Lugar", "Debe cargar la altura o intersección");
																			}else{
																				if(Utils.isEmpty(editName.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																					editName.requestFocus();
																					showSoftKeyboard();
																					Utils.showAlertDialog(this, "Nombre y apellido", "Es un dato requerido");
																				}else{
																					if(!validateAddress(4) && rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																						showSoftKeyboard();
																					}else{
																						if(	Utils.isEmpty(editHeight.getText().toString().trim()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE &&
																							reValidate){
																							editHeight.requestFocus();
																							showSoftKeyboard();
																							Utils.showAlertDialog(this, "Altura del domicilio", "Es un dato requerido");
																						}else{
																							if(Utils.isEmpty(editProvince.getText().toString()) && rlImputed.getVisibility() == RelativeLayout.VISIBLE &&
																								reValidate){
																								editProvince.requestFocus();
																								showSoftKeyboard();
																								Utils.showAlertDialog(this, "Provincia", "Es un dato requerido");
																							}else{
																								if(	txtTypeDocument.getText().toString().trim().equals("Tipo de documento") &&
																										rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																									txtTypeDocument.requestFocus();
																									Utils.showAlertDialog(this, "Tipo de documento", "Es un dato requerido");
																								}else{
																									if(	Utils.isEmpty(editDocument.getText().toString()) &&
																										rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																										editDocument.requestFocus();
																										showSoftKeyboard();
																										Utils.showAlertDialog(this, "Nro. de documento", "Es un dato requerido");
																									}else{
																										if(	!rbHold.isChecked() && !rbNoHold.isChecked() &&
																											rlImputed.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																											rbHold.requestFocus();
																											Utils.showAlertDialog(this, "Documentación retenida", "Es un dato requerido");
																										}else{
																											if(	!rbRemissionDistance.isChecked() && !rbNoRemissionDistance.isChecked() &&
																												rlRemissionDistance.getVisibility() == RelativeLayout.VISIBLE && reValidate){
																												rbRemissionDistance.requestFocus();
																												Utils.showAlertDialog(this, "Vehículo retenido", "Es un dato requerido");
																											}else{
																												//listDependence
																												new MaterialDialog.Builder(this)
																													.title("Dependencia")
																													.cancelable(true)
																													.items(listDependences)
																													.itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice(){
																														@Override
																														public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text){
																															txtDependenceDistance.setTextColor(android.graphics.Color.BLACK);
																															txtDependenceDistance.setText(text);
																															
																															if(	text.toString().toLowerCase().trim().equals("otros") ||
																																text.toString().toLowerCase().trim().equals("otro") ||
																																text.toString().toLowerCase().trim().equals("otra") ||
																																text.toString().toLowerCase().trim().equals("otras")){
																																inputDialog("Especificar dependencia", txtDependenceDistance, "", 50);
																															}
																															
																															return true;
																														}
																													})
																													.show();
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					break;

				case 14:
					//listCopy
					if(editDomain.isEnabled() && !Utils.isEmpty(editDomain.getText().toString().trim())){
						new MaterialDialog.Builder(this)
							.title("Copia de dominio")
							.cancelable(true)
							.items(listCopies)
							.itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice(){
								@Override
								public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text){
									if(	text.toString().toLowerCase().trim().equals("otros") || text.toString().toLowerCase().trim().equals("otro") ||
										text.toString().toLowerCase().trim().equals("otra") || text.toString().toLowerCase().trim().equals("otras")){
										inputDialog("Especificar copia de dominio", txtCopy, "", 1);
									}else{
										txtCopy.setText(text);
									}
									
									txtCopy.setTextColor(android.graphics.Color.BLACK);
									return true;
								}
							})
							.show();
					}
				break;
			}
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":showList - Exception: ", e);
		}
	}
	
	public void showSoftKeyboard(){
		try{
			InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			
			if(!inputMethodManager.isActive()){
				inputMethodManager.toggleSoftInputFromWindow(rlAll.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
			}
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":showSoftKeyboard - Exception: ", e);
		}
	}
	
	private void hideSoftKeyboard(){
		try{
			getWindow().setSoftInputMode(originalSoftInputMode);
			View currentFocusView = getCurrentFocus();
			
			if(currentFocusView != null){
				InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
			}
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":hideSoftKeyboard - Exception: ", e);
		}
	}
	
	@Override
	public void onResume(){
		super.onResume();
		try{
			Realm.init(this);
			Window window = getWindow();
			originalSoftInputMode = window.getAttributes().softInputMode;
			window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
			getCurrentPosition();
			//Refrescar fecha, hora y número de acta
			if(idInfringement == 0){
				editDate.setText(Utils.getDatePhone(this));
				String[] partTime = Utils.getDateTimePhone(this).split(" ");
				editHour.setText(partTime[1].substring(0, partTime[1].length()-3));
			}
			
			Utils.checkVersion(this, false);
			filterDate();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onResume - Exception: ", e);
		}
	}
	
	private void getCurrentPosition(){
		try{
			Criteria criteria				= new Criteria();
			LocationManager locationManager	= (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			
			if(locationManager != null){
				String provider = locationManager.getBestProvider(criteria, false);
				
				if(provider != null){
					if(	ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
						ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
						Location location = locationManager.getLastKnownLocation(provider);
						
						if(location != null){
							lat = location.getLatitude();
							lng = location.getLongitude();
							SharedPreferences.Editor editor = preferences.edit();
							editor.putString(Common.PREF_CURRENT_LAT, String.valueOf(lat));
							editor.putString(Common.PREF_CURRENT_LON, String.valueOf(lng));
							editor.putString(Common.PREF_SELECTED_LAT, "");
							editor.putString(Common.PREF_SELECTED_LON, "");
							editor.apply();
						}
					}
				}else{
					//Buscamos con otro service
					AppLocationService appLocationService	= new AppLocationService(this);
					Location newLocation					= appLocationService.getLocation(LocationManager.GPS_PROVIDER);
					
					if(newLocation != null)
					{
						lat	= newLocation.getLatitude();
						lng	= newLocation.getLongitude();
						SharedPreferences.Editor editor = preferences.edit();
						editor.putString(Common.PREF_CURRENT_LAT, String.valueOf(lat));
						editor.putString(Common.PREF_CURRENT_LON, String.valueOf(lng));
						editor.putString(Common.PREF_SELECTED_LAT, "");
						editor.putString(Common.PREF_SELECTED_LON, "");
						editor.apply();
						
						if(Common.DEBUG){
							System.out.println("Location (GPS): Latitude: " + lat+ " Longitude: " + lng);
						}
					}else{
						newLocation = appLocationService.getLocation(LocationManager.NETWORK_PROVIDER);
						
						if(newLocation != null){
							lat	= newLocation.getLatitude();
							lng	= newLocation.getLongitude();
							SharedPreferences.Editor editor = preferences.edit();
							editor.putString(Common.PREF_CURRENT_LAT, String.valueOf(lat));
							editor.putString(Common.PREF_CURRENT_LON, String.valueOf(lng));
							editor.putString(Common.PREF_SELECTED_LAT, "");
							editor.putString(Common.PREF_SELECTED_LON, "");
							editor.apply();
							
							if(Common.DEBUG){
								System.out.println("Location (NW): Latitude: " + lat+ " Longitude: " + lng);
							}
						}
					}
				}
			}else{
				//Buscamos con otro service
				AppLocationService appLocationService	= new AppLocationService(this);
				Location newLocation					= appLocationService.getLocation(LocationManager.GPS_PROVIDER);
				
				if(newLocation != null){
					lat	= newLocation.getLatitude();
					lng	= newLocation.getLongitude();
					SharedPreferences.Editor editor = preferences.edit();
					editor.putString(Common.PREF_CURRENT_LAT, String.valueOf(lat));
					editor.putString(Common.PREF_CURRENT_LON, String.valueOf(lng));
					editor.putString(Common.PREF_SELECTED_LAT, "");
					editor.putString(Common.PREF_SELECTED_LON, "");
					editor.apply();
					
					if(Common.DEBUG){
						System.out.println("Location (GPS): Latitude: " + lat+ " Longitude: " + lng);
					}
				}else{
					newLocation = appLocationService.getLocation(LocationManager.NETWORK_PROVIDER);
					
					if(newLocation != null){
						lat	= newLocation.getLatitude();
						lng	= newLocation.getLongitude();
						SharedPreferences.Editor editor = preferences.edit();
						editor.putString(Common.PREF_CURRENT_LAT, String.valueOf(lat));
						editor.putString(Common.PREF_CURRENT_LON, String.valueOf(lng));
						editor.putString(Common.PREF_SELECTED_LAT, "");
						editor.putString(Common.PREF_SELECTED_LON, "");
						editor.apply();
						
						if(Common.DEBUG){
							System.out.println("Location (NW): Latitude: " + lat+ " Longitude: " + lng);
						}
					}
				}
			}
			
			if(lat == Common.DEFAULT_LAT && lng == Common.DEFAULT_LON){
				if(locationManager != null){
					String bestProvider	= locationManager.getBestProvider(criteria, false);
					locationManager.getLastKnownLocation(bestProvider);
					Location location	= locationManager.getLastKnownLocation(bestProvider);
					
					if(location != null){
						lat = location.getLatitude();
						lng = location.getLongitude();
						SharedPreferences.Editor editor = preferences.edit();
						editor.putString(Common.PREF_CURRENT_LAT, String.valueOf(lat));
						editor.putString(Common.PREF_CURRENT_LON, String.valueOf(lng));
						editor.putString(Common.PREF_SELECTED_LAT, "");
						editor.putString(Common.PREF_SELECTED_LON, "");
						editor.apply();
					}else{
						List<String> providers = locationManager.getAllProviders();
						
						if(providers.size() > 0){
							for(String provider : providers){
								location = locationManager.getLastKnownLocation(provider);
								
								if(location != null){
									if(Common.DEBUG){
										System.out.println("Provider: "+provider+" Current Location Latitude: "+location.getLatitude()+" Longitude: "+location.getLongitude());
									}
									
									lat = location.getLatitude();
									lng = location.getLongitude();
									SharedPreferences.Editor editor = preferences.edit();
									editor.putString(Common.PREF_CURRENT_LAT, String.valueOf(lat));
									editor.putString(Common.PREF_CURRENT_LON, String.valueOf(lng));
									editor.putString(Common.PREF_SELECTED_LAT, "");
									editor.putString(Common.PREF_SELECTED_LON, "");
									editor.apply();
								}
							}
						}
					}
				}
			}
			
			if(Common.DEBUG){
				System.out.println("Default Location Latitude: "+Common.DEFAULT_LAT+"  Longitude: "+Common.DEFAULT_LON);
				System.out.println("Current Location Latitude: "+lat+" Longitude: "+lng);
			}
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":getCurrentPosition - Exception: ", e);
		}
	}
	
	@Override
	public void onLocationChanged(final Location location){
		try{
			if(location != null){
				lat								= location.getLatitude();
				lng								= location.getLongitude();
				SharedPreferences.Editor editor	= preferences.edit();
				editor.putString(Common.PREF_CURRENT_LAT, String.valueOf(lat));
				editor.putString(Common.PREF_CURRENT_LON, String.valueOf(lng));
				editor.putString(Common.PREF_SELECTED_LAT, "");
				editor.putString(Common.PREF_SELECTED_LON, "");
				editor.apply();
			}
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onLocationChanged - Exception: ", e);
		}
	}
	
	@Override
	public void onStatusChanged(final String s, final int i, final Bundle bundle){
	}
	
	@Override
	public void onProviderEnabled(final String s){
	}
	
	@Override
	public void onProviderDisabled(final String s){
	}
}