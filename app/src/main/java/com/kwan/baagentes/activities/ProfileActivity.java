package com.kwan.baagentes.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.boa.utils.Common;
import com.boa.utils.Utils;
import com.kwan.baagentes.R;
import com.kwan.baagentes.data.User;
import io.realm.Realm;

/**
 * Manejador de pantalla para mostrar la información del usuario logueado
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 22/04/2017.
 */
public class ProfileActivity extends AppCompatActivity{
	private long	idInfringement	= 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		try{
			setContentView(R.layout.activity_profile);
			SharedPreferences preferences	= getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			ImageView ibBack				= (ImageView) findViewById(R.id.ibBack);
			TextView txtTitle				= (TextView) findViewById(R.id.txtTitle);
			TextView txtName				= (TextView) findViewById(R.id.txtName);
			TextView txtDoc					= (TextView) findViewById(R.id.txtDoc);
			TextView txtUser				= (TextView) findViewById(R.id.txtUser);
			ImageView ivSignature			= (ImageView) findViewById(R.id.ivSignature);
			final Activity activity			= this;
			
			if(getIntent() != null){
				idInfringement = getIntent().getLongExtra(Common.KEY_ID, 0);
			}
			
			ibBack.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View view){
					Intent intent = new Intent(activity, MainActivity.class);
					intent.putExtra(Common.KEY_ID, idInfringement);
					activity.startActivity(intent);
					activity.finish();
				}
			});
			
			txtTitle.setText("MI PERFIL");
			Realm.init(this);
			Realm realm	= Realm.getDefaultInstance();
			User user	= realm.where(User.class).equalTo(User.KEY_USERNAME, preferences.getString(Common.PREF_CURRENT_USER, "")).findFirst();
			
			if(user != null){
				txtName.setText(user.getNombre()+" "+user.getApellido());
				
				if(!Utils.isEmpty(user.getDni())){
					txtDoc.setText(user.getDni());
				}else{
					txtDoc.setText(preferences.getString(Common.PREF_CURRENT_USER, ""));
				}
				
				txtUser.setText(user.getUsername());
				//No existe más la funcionalidad de la firma
			}
			
			realm.close();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onCreate - Exception: ", e);
		}
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		try{
			Utils.checkVersion(this, false);
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onResume - Exception: ", e);
		}
	}
}