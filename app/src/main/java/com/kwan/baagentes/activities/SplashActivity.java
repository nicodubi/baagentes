package com.kwan.baagentes.activities;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.boa.servicies.PopulateStreetTask;
import com.boa.utils.Common;
import com.boa.utils.Utils;
import com.kwan.baagentes.R;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.services.concurrency.AsyncTask;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Pantalla inicial de app usada para cargar trackers, db y verificar permisos en Android 6 o supeior
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 22/03/2017.
 */
public class SplashActivity extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		try{
			setContentView(R.layout.activity_splash);

			if(Common.API_LEVEL >= Build.VERSION_CODES.M){
				List<String> permissionsList = new ArrayList<>();
				
				for(String permission : Common.PERMISSIONS){
					if(ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED){
						if(!ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
							permissionsList.add(permission);
						}
					}
				}
				
				String[] permissions = new String[permissionsList.size()];
				permissionsList.toArray(permissions);
				
				if(permissions.length > 0){
					int callBack = 0;
					ActivityCompat.requestPermissions(this, permissions, callBack);
				}
			}else{
				init();
				new PopulateStreetTask(this).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
			}
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onCreate - Exception: ", e);
		}
	}
	
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults){
		try{
			//Check session
			init();
			new PopulateStreetTask(this).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onRequestPermissionsResult - Exception: ", e);
		}
	}
	
	public void init(){
		try{
			if(!Common.DEBUG){
				//Fabric.with(this, new Crashlytics());
			}
			
			Realm.init(this);
			RealmConfiguration realmConfiguration	= new RealmConfiguration.Builder().build();
			Realm.setDefaultConfiguration(realmConfiguration);
			Realm realm								= Realm.getDefaultInstance();
			realm.setAutoRefresh(true);
			realm.close();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":initDB - Exception: ", e);
		}
	}
}