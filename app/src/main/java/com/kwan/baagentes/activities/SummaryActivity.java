package com.kwan.baagentes.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.boa.interfaces.CallBackListener;
import com.boa.interfaces.CallBackValueListener;
import com.boa.utils.Common;
import com.boa.utils.SendFormTaskLocker;
import com.boa.utils.Utils;
import com.kwan.baagentes.R;
import com.kwan.baagentes.data.Brand;
import com.kwan.baagentes.data.Infringement;
import com.kwan.baagentes.services.PdfTask;
import com.kwan.baagentes.services.SendFormTask;
import io.realm.Realm;

/**
 * Manejador de pantalla para visualizar resumen de la infracción registrada
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 25/03/2017.
 */
public class SummaryActivity extends AppCompatActivity{
	private long			idInfringement	= 0, ts = 0;
	private String			pdf				= "";
	private TextView		txtTicket, lblDependenceHead, lblLaw, txtTypeDomain, txtDomain, txtTypeVehicle, lblCountry, txtCountry,
		lblLine, txtLine, lblCompany,txtCompany, txtBrand, txtModel, txtTypeInfringement, txtPlace, txtImputed, txtName, txtAddress,
		txtProvince, txtLocality, txtTypeDocument,txtDocument, txtMunicipality, txtLicense, txtCategory, txtDocumentationHeld,
		txtVehicleRemission, lblDependence, txtDependence, txtTicketZ,txtVehicleZ, txtOwner, txtNameOwner, txtTypeDocumentOwner,
		txtDocumentOwner, txtColor, txtImputedZ, txtGender, txtZip, txtLicenseExpiration, txtCinemometer, txtBrandC, txtModelC, txtSerie,
		txtValue, txtTitle, lblDate, txtInternal, txtStreet1, txtStreet2, lblForm, txtObservations, lblInternal, txtTicketZNumber,
		txtTicketC, txtReference, lblModel, lblTicketZ, lblTicketC, lblLocality, lblCondition, txtCondition, lblTypeDomain, lblObservations,
		tvAgent, tvType;
	private RelativeLayout	rlImputed, rlVehicleZ, rlImputedZ, rlCinemometer, rlAll, rlPdf, rlScreen;
	private ImageView		ibBack;
	private String			pass = "";
	private Button editBtn, confirmBtn;
	private LinearLayout llBtn;
	private SharedPreferences		preferences;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		try{
			setContentView(R.layout.activity_summary);
			txtDocumentationHeld	= (TextView) findViewById(R.id.txtDocumentationHeld);
			txtLicense				= (TextView) findViewById(R.id.txtLicense);
			lblCompany				= (TextView) findViewById(R.id.lblCompany);
			lblLine					= (TextView) findViewById(R.id.lblLine);
			txtDocument				= (TextView) findViewById(R.id.txtDocument);
			lblCountry				= (TextView) findViewById(R.id.lblCountry);
			txtLocality				= (TextView) findViewById(R.id.txtLocality);
			txtDomain				= (TextView) findViewById(R.id.txtDomain);
			txtProvince				= (TextView) findViewById(R.id.txtProvince);
			txtAddress				= (TextView) findViewById(R.id.txtAddress);
			lblLaw					= (TextView) findViewById(R.id.lblLaw);
			txtName					= (TextView) findViewById(R.id.txtName);
			lblDependenceHead		= (TextView) findViewById(R.id.lblDependenceHead);
			txtPlace				= (TextView) findViewById(R.id.txtPlace);
			txtTicket				= (TextView) findViewById(R.id.txtTicket);
			txtTitle				= (TextView) findViewById(R.id.txtTitle);
			txtCinemometer			= (TextView) findViewById(R.id.txtCinemometer);
			txtImputedZ				= (TextView) findViewById(R.id.txtImputedZ);
			txtVehicleZ				= (TextView) findViewById(R.id.txtVehicleZ);
			txtColor				= (TextView) findViewById(R.id.txtColor);
			txtTypeDocumentOwner	= (TextView) findViewById(R.id.txtTypeDocumentOwner);
			txtDependence			= (TextView) findViewById(R.id.txtDependence);
			txtCategory				= (TextView) findViewById(R.id.txtCategory);
			txtMunicipality			= (TextView) findViewById(R.id.txtMunicipality);
			txtTypeDocument			= (TextView) findViewById(R.id.txtTypeDocument);
			txtLine					= (TextView) findViewById(R.id.txtLine);
			txtTypeVehicle			= (TextView) findViewById(R.id.txtTypeVehicle);
			txtCountry				= (TextView) findViewById(R.id.txtCountry);
			txtTypeDomain			= (TextView) findViewById(R.id.txtTypeDomain);
			txtTypeInfringement		= (TextView) findViewById(R.id.txtTypeInfringement);
			txtCompany				= (TextView) findViewById(R.id.txtCompany);
			txtBrand				= (TextView) findViewById(R.id.txtBrand);
			txtBrandC				= (TextView) findViewById(R.id.txtBrandC);
			txtModel				= (TextView) findViewById(R.id.txtModel);
			txtModelC				= (TextView) findViewById(R.id.txtModelC);
			txtImputed				= (TextView) findViewById(R.id.txtImputed);
			txtSerie				= (TextView) findViewById(R.id.txtSerie);
			txtValue				= (TextView) findViewById(R.id.txtValue);
			txtLicenseExpiration	= (TextView) findViewById(R.id.txtLicenseExpiration);
			txtZip					= (TextView) findViewById(R.id.txtZip);
			txtGender				= (TextView) findViewById(R.id.txtGender);
			txtDocumentOwner		= (TextView) findViewById(R.id.txtDocumentOwner);
			txtNameOwner			= (TextView) findViewById(R.id.txtNameOwner);
			txtOwner				= (TextView) findViewById(R.id.txtOwner);
			txtTicketZ				= (TextView) findViewById(R.id.txtTicketZ);
			lblDependence			= (TextView) findViewById(R.id.lblDependence);
			txtVehicleRemission		= (TextView) findViewById(R.id.txtVehicleRemission);
			lblDate					= (TextView) findViewById(R.id.lblDate);
			lblInternal				= (TextView) findViewById(R.id.lblInternal);
			txtInternal				= (TextView) findViewById(R.id.txtInternal);
			txtStreet1				= (TextView) findViewById(R.id.txtStreet1);
			txtStreet2				= (TextView) findViewById(R.id.txtStreet2);
			lblForm					= (TextView) findViewById(R.id.lblForm);
			txtObservations			= (TextView) findViewById(R.id.txtObservations);
			txtTicketZNumber		= (TextView) findViewById(R.id.txtTicketZNumber);
			txtTicketC				= (TextView) findViewById(R.id.txtTicketC);
			txtReference			= (TextView) findViewById(R.id.txtReference);
			lblModel				= (TextView) findViewById(R.id.lblModel);
			lblTicketZ				= (TextView) findViewById(R.id.lblTicketZ);
			lblTicketC				= (TextView) findViewById(R.id.lblTicketC);
			lblLocality				= (TextView) findViewById(R.id.lblLocality);
			lblCondition			= (TextView) findViewById(R.id.lblCondition);
			txtCondition			= (TextView) findViewById(R.id.txtCondition);
			lblTypeDomain			= (TextView) findViewById(R.id.lblTypeDomain);
			lblObservations			= (TextView) findViewById(R.id.lblObservations);
			tvAgent					= (TextView) findViewById(R.id.tvAgent);
			tvType					= (TextView) findViewById(R.id.tvType);
			rlAll					= (RelativeLayout) findViewById(R.id.rlAll);
			rlImputed				= (RelativeLayout) findViewById(R.id.rlImputed);
			rlVehicleZ				= (RelativeLayout) findViewById(R.id.rlVehicleZ);
			rlImputedZ				= (RelativeLayout) findViewById(R.id.rlImputedZ);
			rlCinemometer			= (RelativeLayout) findViewById(R.id.rlCinemometer);
			rlPdf					= (RelativeLayout) findViewById(R.id.rlPdf);
			rlScreen				= (RelativeLayout) findViewById(R.id.rlScreen);
			ibBack					= (ImageView) findViewById(R.id.ibBack);
			editBtn					= (Button) findViewById(R.id.btnEdit);
			confirmBtn				= (Button) findViewById(R.id.btnSave);
			llBtn					= (LinearLayout) findViewById(R.id.llNewBtns);
			//Textos para pdf
			TextView tvTicketNumber					= (TextView) findViewById(R.id.tvTicketNumber);
			TextView tvHour							= (TextView) findViewById(R.id.tvHour);
			TextView tvNumberDomain					= (TextView) findViewById(R.id.tvNumberDomain);
			TextView tvVehicleType					= (TextView) findViewById(R.id.tvVehicleType);
			TextView tvVehicleBrand					= (TextView) findViewById(R.id.tvVehicleBrand);
			TextView tvVehicleModel					= (TextView) findViewById(R.id.tvVehicleModel);
			TextView tvTypeFault					= (TextView) findViewById(R.id.tvTypeFault);
			TextView tvStreet						= (TextView) findViewById(R.id.tvStreet);
			TextView tvHeight						= (TextView) findViewById(R.id.tvHeight);
			TextView tvKm							= (TextView) findViewById(R.id.tvKm);
			TextView tvIntersection					= (TextView) findViewById(R.id.tvIntersection);
			TextView tvInfractorName				= (TextView) findViewById(R.id.tvInfractorName);
			TextView tvInfractorLastName			= (TextView) findViewById(R.id.tvInfractorLastName);
			TextView tvInfractorProvincie			= (TextView) findViewById(R.id.tvInfractorProvincie);
			TextView tvInfractorStreet				= (TextView) findViewById(R.id.tvInfractorStreet);
			TextView tvInfractorHeight				= (TextView) findViewById(R.id.tvInfractorHeight);
			TextView tvInfractorFloor				= (TextView) findViewById(R.id.tvInfractorFloor);
			TextView tvInfractorOffice				= (TextView) findViewById(R.id.tvInfractorOffice);
			TextView tvInfractorLocality			= (TextView) findViewById(R.id.tvInfractorLocality);
			TextView tvInfractorType				= (TextView) findViewById(R.id.tvInfractorType);
			TextView tvInfractorDocument			= (TextView) findViewById(R.id.tvInfractorDocument);
			TextView tvInfractorLicense				= (TextView) findViewById(R.id.tvInfractorLicense);
			TextView tvInfractorMunicipality		= (TextView) findViewById(R.id.tvInfractorMunicipality);
			TextView tvInfractorCategory			= (TextView) findViewById(R.id.tvInfractorCategory);
			TextView tvInfractorDocumentationHeld	= (TextView) findViewById(R.id.tvInfractorDocumentationHeld);
			TextView tvInfractorVehicleRemission	= (TextView) findViewById(R.id.tvInfractorVehicleRemission);
			TextView tvNumberZ						= (TextView) findViewById(R.id.tvNumberZ);
			TextView tvNumberC						= (TextView) findViewById(R.id.tvNumberC);
			TextView tvInfractorObs					= (TextView) findViewById(R.id.tvInfractorObs);
			TextView lblInfractorObs				= (TextView) findViewById(R.id.lblInfractorObs);
			TextView tvModality						= (TextView) findViewById(R.id.tvModality);
			preferences								= getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			txtTitle.setText("RESUMEN");
			ibBack.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(final View view){
					onBackPressed();
				}
			});
			
			if(getIntent() != null){
				idInfringement = getIntent().getLongExtra(Common.KEY_ID, 0);
			}
			
			if(idInfringement != 0){
				Realm.init(this);
				long ts						= System.currentTimeMillis();
				Realm realm					= Realm.getDefaultInstance();
				Infringement infringement	= realm.where(Infringement.class).equalTo(Common.KEY_ID, idInfringement).findFirst();
				
				if(infringement != null){
					ts = infringement.getCaptureTs();

					if(infringement.getUser() != null){
						if(!Utils.isEmpty(infringement.getUser().getDependencia())){
							lblDependenceHead.setText("CÓDIGO DEPENDENCIA "+ infringement.getUser().getDependencia());
						}else{
							lblDependenceHead.setVisibility(TextView.GONE);
						}
						
						//Ahora quieren ver el apellido y nombre del agente en una línea y en la otra el cargo, debajo de la fecha y hora
						tvAgent.setText(infringement.getUser().getApellido()+" "+infringement.getUser().getNombre());
						
						if(!preferences.getBoolean(Common.PREF_IS_POLICE, false)){
							tvType.setText("AGENTE DE CONTROL DE TRANSITO");
						}else{
							tvType.setText("AGENTE DE LA POLICIA DE LA CIUDAD AUTONOMA DE BUENOS AIRES");
						}
					}else{
						lblDependenceHead.setVisibility(TextView.GONE);
					}

					if(infringement.getTicketNumber() != 0){
						tvTicketNumber.setText("I"+String.valueOf(infringement.getTicketNumber()));
					}
					
					if(!Utils.isEmpty(infringement.getTicketZNumber())){
						txtTicketZNumber.setText(String.valueOf(infringement.getTicketZNumber()));
						tvNumberZ.setText(String.valueOf(infringement.getTicketZNumber()));
					}
					
					if(!Utils.isEmpty(infringement.getNumberTicketContravention())){
						txtTicketC.setText(infringement.getNumberTicketContravention());
						tvNumberC.setText(infringement.getNumberTicketContravention());
					}
					
					if(infringement.getFormInfringement().equals("P")){
						txtImputed.setVisibility(TextView.VISIBLE);
						rlImputed.setVisibility(RelativeLayout.VISIBLE);
						lblForm.setText(lblForm.getText()+" Presencial");
						lblTicketZ.setVisibility(TextView.VISIBLE);
						lblTicketC.setVisibility(TextView.VISIBLE);
						txtTicketZNumber.setVisibility(TextView.VISIBLE);
						txtTicketC.setVisibility(TextView.VISIBLE);
						tvModality.setText("Con presencia del infractor");
					}else{
						txtImputed.setVisibility(TextView.GONE);
						rlImputed.setVisibility(RelativeLayout.GONE);
						lblForm.setText(lblForm.getText()+" A distancia");
						lblTicketZ.setVisibility(TextView.GONE);
						lblTicketC.setVisibility(TextView.GONE);
						txtTicketZNumber.setVisibility(TextView.GONE);
						txtTicketC.setVisibility(TextView.GONE);
						tvModality.setText("Sin presencia del infractor");
					}
					
					lblDate.setText(lblDate.getText()+" "+infringement.getDate()+" "+infringement.getHour());
					tvHour.setText(infringement.getDate()+" "+infringement.getHour());
					
					if(infringement.getTypeTicket().equals("Z")){
						txtTicketZ.setVisibility(TextView.VISIBLE);
						txtVehicleZ.setVisibility(TextView.VISIBLE);
						txtImputedZ.setVisibility(TextView.VISIBLE);
						txtCinemometer.setVisibility(TextView.VISIBLE);
						rlImputedZ.setVisibility(RelativeLayout.VISIBLE);
						rlVehicleZ.setVisibility(RelativeLayout.VISIBLE);
						rlCinemometer.setVisibility(RelativeLayout.VISIBLE);
					}else{
						txtTicketZ.setVisibility(TextView.GONE);
						txtVehicleZ.setVisibility(TextView.GONE);
						txtImputedZ.setVisibility(TextView.GONE);
						txtCinemometer.setVisibility(TextView.GONE);
						rlImputedZ.setVisibility(RelativeLayout.GONE);
						rlVehicleZ.setVisibility(RelativeLayout.GONE);
						rlCinemometer.setVisibility(RelativeLayout.GONE);
					}
					
					//Agregar 2do nro de acta, fecha, tipo y modalidad
					//B Vehiculo
					txtTypeDomain.setText(infringement.getTypeDomain());
					
					if(infringement.getCountry() != null){
						txtCountry.setText(infringement.getCountry().getDescripcion());
					}
					
					if(!Utils.isEmpty(infringement.getDomain())){
						txtDomain.setText(infringement.getDomain()+" ("+infringement.getCopyDomain()+")");
						tvNumberDomain.setText(infringement.getDomain()+" "+infringement.getCopyDomain());
						//Este otro guarda que copia de la patente es
					}else{
						txtDomain.setText("-");
					}
					
					if(infringement.getTypeVehicle() != null){
						txtTypeVehicle.setText(infringement.getTypeVehicle().getDescripcion());
						tvVehicleType.setText(infringement.getTypeVehicle().getDescripcion());
						
						if(infringement.getTypeVehicle().getDescripcion().equals("COLECTIVO")){
							lblModel.setVisibility(TextView.GONE);
							txtModel.setVisibility(TextView.GONE);
						}
					}
					
					if(infringement.getNewLine() != null){
						txtLine.setText(infringement.getNewLine().getLinea());
					}else{
						lblLine.setVisibility(TextView.GONE);
						txtLine.setVisibility(TextView.GONE);
					}
					
					if(infringement.getCompany() != null && (infringement.getTypeVehicle().getDescripcion().toLowerCase().equals("colectivo") ||
						infringement.getTypeVehicle().getDescripcion().toLowerCase().equals("combis"))){
						txtCompany.setText(infringement.getCompany().getEmpresa());
					}else{
						lblCompany.setVisibility(TextView.GONE);
						txtCompany.setVisibility(TextView.GONE);
					}
					
					if(infringement.getBrandId() != 0){
						Brand brand = realm.where(Brand.class).equalTo(Common.KEY_ID, infringement.getBrandId()).findFirst();
						
						if(brand != null){
							txtBrand.setText(brand.getMarca());
							tvVehicleBrand.setText(brand.getMarca());
						}
					}
					
					if(!Utils.isEmpty(infringement.getModel())){
						txtModel.setText(infringement.getModel());
						tvVehicleModel.setText(infringement.getModel());
					}
					
					//Se guarda acá porque pueden venir más de una separada por ;
					txtTypeInfringement.setText(infringement.getTypesInfringements().replace(";", ";"+System.lineSeparator()));
					tvTypeFault.setText(infringement.getTypesInfringements().replace(";", ";"+System.lineSeparator()));
					txtPlace.setText(infringement.getStreet()+" "+infringement.getHeight());
					tvStreet.setText(infringement.getStreet().toUpperCase());
					tvHeight.setText(infringement.getHeight());
					
					if(!Utils.isEmpty(infringement.getObservations())){
						txtObservations.setText(infringement.getObservations());
					}

					//Reutilizamos el lugar de las observaciones para mostrar la descripción de conducta
					lblInfractorObs.setVisibility(TextView.VISIBLE);
					tvInfractorObs.setText(infringement.getZipCodeImputed());
					tvInfractorObs.setVisibility(TextView.VISIBLE);
					txtObservations.setVisibility(TextView.GONE);
					
					if(!Utils.isEmpty(infringement.getReferencePlace())){
						txtReference.setText(infringement.getReferencePlace());
						tvKm.setText(infringement.getReferencePlace());
					}

					if(!Utils.isEmpty(infringement.getBetween1()) && !Utils.isEmpty(infringement.getBetween2())){
						tvIntersection.setText(infringement.getBetween1()+" y "+infringement.getBetween2());
					}else{
						if(!Utils.isEmpty(infringement.getBetween1())){
							txtStreet1.setText(infringement.getBetween1());
							tvIntersection.setText(infringement.getBetween1());
						}

						if(!Utils.isEmpty(infringement.getBetween2())){
							txtStreet2.setText(infringement.getBetween2());
							tvIntersection.setText(infringement.getBetween2());
						}
					}
					
					//B Imputado
					txtName.setText(infringement.getNameImputed()+" "+infringement.getLastNameImputed());
					tvInfractorName.setText(infringement.getNameImputed());
					tvInfractorLastName.setText(infringement.getLastNameImputed());
					txtProvince.setText(infringement.getApartmentImputed());
					tvInfractorProvincie.setText(infringement.getApartmentImputed());
					
					String address = infringement.getStreetImputed();
					
					if(!Utils.isEmpty(infringement.getHeightImputed())){
						address += " "+infringement.getHeightImputed();
					}
					
					if(!Utils.isEmpty(infringement.getFloor())){
						address += " Piso "+infringement.getFloor();
					}
					
					if(!Utils.isEmpty(infringement.getOffice())){
						address += " Dpto. "+infringement.getOffice();
					}
					
					txtAddress.setText(address);
					tvInfractorStreet.setText(infringement.getStreetImputed());
					tvInfractorHeight.setText(infringement.getHeightImputed());
					tvInfractorFloor.setText(infringement.getFloor());
					tvInfractorOffice.setText(infringement.getOffice());
					
					if(infringement.getLocalityImputed() != null){
						lblLocality.setVisibility(TextView.VISIBLE);
						txtLocality.setVisibility(TextView.VISIBLE);
						txtLocality.setText(infringement.getLocalityImputed().getDescripcion());
						tvInfractorLocality.setText(infringement.getLocalityImputed().getDescripcion());
					}else{
						lblLocality.setVisibility(TextView.GONE);
						txtLocality.setVisibility(TextView.GONE);
					}
					
					if(infringement.getTypeDocumentImputed() != null){
						txtTypeDocument.setText(infringement.getTypeDocumentImputed().getDocumento());
						tvInfractorType.setText(infringement.getTypeDocumentImputed().getDocumento());
					}
					
					if(!Utils.isEmpty(infringement.getDocumentImputed())){
						txtDocument.setText(infringement.getDocumentImputed());
						tvInfractorDocument.setText(infringement.getDocumentImputed());
					}
					
					if(!Utils.isEmpty(infringement.getLicense())){
						txtLicense.setText(infringement.getLicense());
						tvInfractorLicense.setText(infringement.getLicense());
					}
					
					if(!Utils.isEmpty(infringement.getIssuedBy())){
						txtMunicipality.setText(infringement.getIssuedBy());
						tvInfractorMunicipality.setText(infringement.getIssuedBy());
					}
					
					if(infringement.getCategories() != null){
						txtCategory.setText(infringement.getCategories());
						tvInfractorCategory.setText(infringement.getCategories());
					}
					
					if(infringement.getDocumentAgent().equals("1")){
						txtCondition.setText("Si");
					}else{
						txtCondition.setText("No");
					}
					
					txtDocumentationHeld.setText(infringement.getDocumentationHeld().replace("S", "Si").replace("N", "No"));
					tvInfractorDocumentationHeld.setText(infringement.getDocumentationHeld().replace("S", "Si").replace("N", "No"));
					txtVehicleRemission.setText(infringement.getVehicleRemission().replace("S", "Si").replace("N", "No"));
					tvInfractorVehicleRemission.setText(infringement.getVehicleRemission().replace("S", "Si").replace("N", "No"));
					
					if(Utils.isEmpty(infringement.getDocumentationHeld())){
						tvInfractorDocumentationHeld.setText("No");
					}
					
					if(infringement.getVehicleRemission().equals("S")){
						lblDependence.setVisibility(TextView.VISIBLE);
						txtDependence.setVisibility(TextView.VISIBLE);
						txtDependence.setText(infringement.getDependence());
					}else{
						lblDependence.setVisibility(TextView.GONE);
						txtDependence.setVisibility(TextView.GONE);
					}
					
					//Z Vehiculo
					if(infringement.getNameImputed().equals(infringement.getLastNameImputed())){
						txtOwner.setText("Si");
						txtNameOwner.setVisibility(TextView.GONE);
						txtTypeDocumentOwner.setVisibility(TextView.GONE);
						txtDocumentOwner.setVisibility(TextView.GONE);
					}else{
						txtOwner.setText("No");
						txtNameOwner.setVisibility(TextView.VISIBLE);
						txtTypeDocumentOwner.setVisibility(TextView.VISIBLE);
						txtDocumentOwner.setVisibility(TextView.VISIBLE);
					}
					
					txtNameOwner.setText(infringement.getLastNameImputed());
					
					if(infringement.getTypeDocumentOwner() != null){
						txtTypeDocumentOwner.setText(infringement.getTypeDocumentOwner().getDocumento());
					}
					
					txtDocumentOwner.setText(infringement.getDocumentOwner());
					
					if(infringement.getColor() != null){
						txtColor.setText(infringement.getColor().getValor());
					}
					
					//Z Imputado
					txtGender.setText(infringement.getGenderImputed());
					txtZip.setText(infringement.getZipCodeImputed());
					txtLicenseExpiration.setText(infringement.getLicenseExpiration());
					
					//Z cinemometro
					if(infringement.getCinemometer() != null){
						txtBrandC.setText(infringement.getCinemometer().getBrand());
						txtModelC.setText(infringement.getCinemometer().getModel());
						txtSerie.setText(infringement.getCinemometer().getSerie());
						txtValue.setText(infringement.getCinemometer().getValue());
						txtBrandC.setVisibility(TextView.VISIBLE);
						txtModelC.setVisibility(TextView.VISIBLE);
						txtSerie.setVisibility(TextView.VISIBLE);
						txtValue.setVisibility(TextView.VISIBLE);
					}else{
						txtBrandC.setVisibility(TextView.GONE);
						txtModelC.setVisibility(TextView.GONE);
						txtSerie.setVisibility(TextView.GONE);
						txtValue.setVisibility(TextView.GONE);
					}
					
					try{
						txtInternal = (TextView) findViewById(R.id.txtInternal);
						
						if(infringement.getInternal() != 0){
							txtInternal.setText(infringement.getInternal()+"");
						}else{
							lblInternal.setVisibility(TextView.GONE);
							txtInternal.setVisibility(TextView.GONE);
						}
					}catch(Exception e){
						Utils.logError(this, getLocalClassName()+":onCreate:Internal - Exception: ", e);
					}
				}
				
				realm.close();
			}else{
				Utils.showAlertDialog(this, "Resumen", "Hubo un error al leer la infracción");
			}

			rlPdf.setVisibility(RelativeLayout.VISIBLE);
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onCreate - Exception: ", e);
		}
	}

	public void authenticate(){
		try{
			llBtn.setVisibility(LinearLayout.GONE);
			editBtn.setVisibility(Button.GONE);
			confirmBtn.setVisibility(Button.GONE);
			String value = "";
			
			if(Common.DEBUG){
				value = Common.DEFAULT_PASSKEY;
			}
			
			new MaterialDialog.Builder(this).title("Firma digital").inputType(InputType.TYPE_TEXT_VARIATION_PASSWORD)
				.positiveText("Autenticar").cancelable(true).inputRange(0, 20)
				.input("Ingrese su contraseña para firma digital", value, new MaterialDialog.InputCallback(){
					@Override
					public void onInput(@NonNull MaterialDialog dialog, CharSequence input){
						try{
							if(input != null){
								if(input != ""){
									pass = input.toString().trim();

									if(!Utils.isEmpty(pass)){
										SharedPreferences preferences	= getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
										SharedPreferences.Editor editor	= preferences.edit();
										editor.putString(Common.PREF_CURRENT_PASSKEY, pass);
										editor.apply();
										final int result				= Utils.openKeystore(SummaryActivity.this, pass);
										
										switch(result){
											case Common.BOOL_NO:
												Toast.makeText(SummaryActivity.this, "Autenticación fallida", Toast.LENGTH_SHORT).show();
												authenticate();
											break;
											
											default:
												if(result == Common.BOOL_YES){
													Toast.makeText(SummaryActivity.this, "Autenticación correcta", Toast.LENGTH_SHORT).show();
													confirm();
												}else{
													Utils.showAlertDialog(SummaryActivity.this, "Firma digital", "No se pudo acceder al certificado");
												}
											break;
										}
									}
								}
							}
						}catch(Exception e){
							Utils.logError(SummaryActivity.this, getLocalClassName()+":authenticate:onInput - Exception: ", e);
						}
					}
				})
				.negativeText("Cancelar").onNegative(new MaterialDialog.SingleButtonCallback(){
					@Override
					public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which){
						Toast.makeText(SummaryActivity.this, "Autenticación cancelada", Toast.LENGTH_SHORT).show();
						llBtn.setVisibility(LinearLayout.VISIBLE);
						editBtn.setVisibility(Button.VISIBLE);
						confirmBtn.setVisibility(Button.VISIBLE);
					}
			}).show();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":authenticate - Exception: ", e);
		}
	}
	
	@Override
	public void onBackPressed(){
		onEdit(null);
	}
	
	public void onEdit(View view){
		try{
			Intent intent = new Intent(this, MainActivity.class);
			intent.putExtra(Common.KEY_ID, idInfringement);
			startActivity(intent);
			finish();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onEdit - Exception: ", e);
		}
	}
	
	public void onConfirm(View view){
		try{
			llBtn.setVisibility(LinearLayout.GONE);
			editBtn.setVisibility(Button.GONE);
			confirmBtn.setVisibility(Button.GONE);
			//Caso contrario exploto, no encontró el archivo o si lo pudo abrir
			final AlertDialog.Builder builder	= new AlertDialog.Builder(this);
			builder.setMessage("¿Confirma los datos ingresados en el formulario?");
			builder.setCancelable(false);
			builder.setPositiveButton(R.string.save_signature, new DialogInterface.OnClickListener(){
				@Override
				public void onClick(final DialogInterface dialogInterface, final int i){
					final int result = Utils.openKeystore(SummaryActivity.this, "");
					
					if(result == Common.BOOL_NO){
						//Pedir nueva clave porque no lo pudo abrir
						authenticate();
					}else{
						Utils.showAlertDialog(SummaryActivity.this, "Firma Digital", "No se pudo acceder al certificado");
					}
				}
			});
			builder.setNegativeButton("Editar", new DialogInterface.OnClickListener(){
				@Override
				public void onClick(final DialogInterface dialogInterface, final int i){
					onEdit(null);
				}
			});
			builder.show();
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onConfirm - Exception: ", e);
		}
	}
	
	public void confirm(){
		try{
			//Ocultar los botones por un rato así le pasamos la vista al pdf
			//Pasamos a background por el peso y la demora pero no sale la firma
			new PdfTask(SummaryActivity.this, ts, idInfringement, pass, rlAll, new CallBackValueListener(){
				@Override
				public void invoke(String result){
					try{
						pdf = result;
						
						if(!Utils.isEmpty(pdf)){
							SummaryActivity.this.runOnUiThread(new Runnable(){
								@Override
								public void run(){
									if(!Common.DEBUG_PDF && !SendFormTaskLocker.getInstance().isSendFormTaskRunning()){
										new SendFormTask(SummaryActivity.this, idInfringement, pdf, ts, new CallBackListener(){
											@Override
											public void invoke(){
												SummaryActivity.this.runOnUiThread(new Runnable(){
													@Override
													public void run(){
														Intent intent = new Intent(SummaryActivity.this, MainActivity.class);
														SummaryActivity.this.startActivity(intent);
														SummaryActivity.this.finish();
													}
												});
											}
										}).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
									}
									
									new android.os.Handler().postDelayed(new Runnable(){
										public void run(){
											llBtn.setVisibility(LinearLayout.VISIBLE);
											editBtn.setVisibility(Button.VISIBLE);
											confirmBtn.setVisibility(Button.VISIBLE);
										}
									}, 6000);
								}
							});
						}else{
							new android.os.Handler().postDelayed(new Runnable(){
								public void run(){
									llBtn.setVisibility(LinearLayout.VISIBLE);
									editBtn.setVisibility(Button.VISIBLE);
									confirmBtn.setVisibility(Button.VISIBLE);
								}
							}, 3000);
						}
					}catch(Exception e){
						Utils.logError(SummaryActivity.this, getLocalClassName()+":onConfirm:invoke - Exception: ", e);
					}
				}
			}).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":confirm - Exception: ", e);
		}
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		try{
			Utils.checkVersion(this, false);
		}catch(Exception e){
			Utils.logError(this, getLocalClassName()+":onResume - Exception: ", e);
		}
	}
}