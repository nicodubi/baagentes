package com.kwan.baagentes.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.boa.utils.Common;
import com.boa.utils.Utils;
import com.kwan.baagentes.R;
import com.kwan.baagentes.activities.MainActivity;
import com.kwan.baagentes.data.Infringement;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 4 nov 2017.
 */
public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder>{
	private Activity activity;
	private RealmList<Infringement> infringements;
	
	public ListAdapter(Activity activity){
		final Realm realm							= Realm.getDefaultInstance();
		this.activity								= activity;
		RealmList<Infringement> results				= new RealmList<>();
		RealmResults<Infringement> infringements	= realm.where(Infringement.class).notEqualTo(Common.KEY_STATUS, Infringement.STATUS_SENDED).findAllSorted(Common.KEY_STATUS, Sort.ASCENDING);
		
		if(infringements.size() > 0){
			results.addAll(infringements);
		}
		
		Utils.showStatus(realm, activity);
		this.infringements = results;
	}
	
	@Override
	public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
		try{
			return new MyViewHolder(LayoutInflater.from(activity).inflate(R.layout.item_ticket, parent, false));
		}catch(Exception e){
			Utils.logError(activity, "ListAdapter:onCreateViewHolder - ", e);
		}
		
		return null;
	}
	
	@Override
	public void onBindViewHolder(final MyViewHolder holder, int position){
		try{
			if(infringements != null){
				if(infringements.size() > 0){
					Infringement ticket = infringements.get(position);
					
					if(ticket != null){
						holder.txtDate.setText(Utils.ts2String(activity, ticket.getCaptureTs()));
						
						if(!Utils.isEmpty(ticket.getDomain())){
							holder.txtDomain.setText(ticket.getDomain()+" ("+ticket.getCopyDomain()+")");
						}else{
							holder.txtDomain.setText("-");
						}
						
						if(ticket.getAddress().startsWith("AU ")){
							holder.txtPlace.setText(ticket.getAddress()+" "+ticket.getReferencePlace());
						}else{
							holder.txtPlace.setText(ticket.getAddress());
						}
						
						String status = "";
						
						switch(ticket.getStatus()){
							case Infringement.STATUS_REJECTED:
								status = "(Rechazada) ";
								
								if(!Utils.isEmpty(ticket.getMatch())){
									status += ticket.getMatch();
								}else{
									status += "Se reenviará cuando haya conexión";
								}
							break;
							
							case Infringement.STATUS_SENDED:
								status = "(Enviada) ";
								holder.ivEdit.setVisibility(ImageView.GONE);
							break;
							
							default:
								status = "(Pendiente) Se reenviará cuando haya conexión";
							break;
						}
						
						holder.txtStatus.setText(status);
						holder.ivEdit.setOnClickListener(new View.OnClickListener(){
							@Override
							public void onClick(View v){
								if(Common.DEBUG){
									System.out.println("Acta: "+infringements.get(holder.getAdapterPosition()).getTicketNumber());
								}
								
								Intent intent = new Intent(activity, MainActivity.class);
								intent.putExtra(Common.KEY_ID, infringements.get(holder.getAdapterPosition()).getId());
								activity.startActivity(intent);
								activity.finish();
							}
						});
					}
				}
			}
		}catch(Exception e){
			Utils.logError(activity, "ListAdapter:onBindViewHolder - ", e);
		}
	}
	
	@Override
	public int getItemCount(){
		if(infringements == null){
			return 0;
		}
		
		return infringements.size();
	}
	
	class MyViewHolder extends RecyclerView.ViewHolder{
		private RelativeLayout rlItem;
		private TextView txtDate, txtDomain, txtPlace, txtStatus;
		private ImageView ivEdit;
		
		MyViewHolder(View view){
			super(view);
			rlItem		= (RelativeLayout) view.findViewById(R.id.rlItem);
			txtDate		= (TextView) view.findViewById(R.id.txtDate);
			txtDomain	= (TextView) view.findViewById(R.id.txtDomain);
			txtPlace	= (TextView) view.findViewById(R.id.txtPlace);
			txtStatus	= (TextView) view.findViewById(R.id.txtStatus);
			ivEdit		= (ImageView) view.findViewById(R.id.ivEdit);
		}
	}
}