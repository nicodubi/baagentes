package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para persistir Actas provistos por Kwan
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 14/04/2017.
 */
public class Act extends RealmObject
{
	@PrimaryKey
	private String	codigo;
	private String	descripcion;
	
	public String getCodigo()
	{
		return codigo;
	}
	
	public void setCodigo(final String codigo)
	{
		this.codigo = codigo;
	}
	
	public String getDescripcion()
	{
		return descripcion;
	}
	
	public void setDescripcion(final String descripcion)
	{
		this.descripcion = descripcion;
	}
}