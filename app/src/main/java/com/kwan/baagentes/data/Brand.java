package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para permitirle al usuario elegir la marca del vehiculo
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 25/03/2017.
 */
public class Brand extends RealmObject
{
	@PrimaryKey
	private int		id;
	private String	marca;
	
	@Ignore
	public static final String	KEY_MARCA		= "marca";
	@Ignore
	public static final String	KEY_IDMARCA		= "idMarca";
	
	public int getId()
	{
		return id;
	}
	
	public void setId(final int id)
	{
		this.id = id;
	}
	
	public String getMarca()
	{
		return marca;
	}
	
	public void setMarca(final String marca)
	{
		this.marca = marca;
	}
}