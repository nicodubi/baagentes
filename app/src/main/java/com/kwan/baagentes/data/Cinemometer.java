package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para persistir Cinemómetros provistos por Kwan
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 14/04/2017.
 */
public class Cinemometer extends RealmObject
{
	@PrimaryKey
	private String	serie;
	private String	model;
	private String	brand;
	private String	value;
	
	@Ignore
	public static final String	KEY_SERIE	= "serie";
	
	public String getSerie()
	{
		return serie;
	}
	
	public void setSerie(final String serie)
	{
		this.serie = serie;
	}
	
	public String getModel()
	{
		return model;
	}
	
	public void setModel(final String model)
	{
		this.model = model;
	}
	
	public String getBrand()
	{
		return brand;
	}
	
	public void setBrand(final String brand)
	{
		this.brand = brand;
	}
	
	public String getValue()
	{
		return value;
	}
	
	public void setValue(final String value)
	{
		this.value = value;
	}
}