package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para permitirle al usuario elegir el color del vehiculo
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 25/03/2017.
 */
public class Color extends RealmObject
{
	@PrimaryKey
	private int		codigo;
	private String	valor;
	
	@Ignore
	public static final String	KEY_VALOR	= "valor";
	
	public int getCodigo()
	{
		return codigo;
	}
	
	public void setCodigo(final int codigo)
	{
		this.codigo = codigo;
	}
	
	public String getValor()
	{
		return valor;
	}
	
	public void setValor(final String valor)
	{
		this.valor = valor;
	}
}