package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para persistir Empresas de colectivos de conducir provistos por Kwan
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 20/04/2017.
 */
public class Company extends RealmObject
{
	@PrimaryKey
	private String	id;
	private String	linea;
	private String	empresa;
	
	@Ignore
	public static final String	KEY_EMPRESA	= "empresa";
	
	public String getId()
	{
		return id;
	}
	
	public void setId(final String id)
	{
		this.id = id;
	}
	
	public String getLinea()
	{
		return linea;
	}
	
	public void setLinea(final String linea)
	{
		this.linea = linea;
	}
	
	public String getEmpresa()
	{
		return empresa;
	}
	
	public void setEmpresa(final String empresa)
	{
		this.empresa = empresa;
	}
}