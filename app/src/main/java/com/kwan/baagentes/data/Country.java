package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para persistir Países provistos por Kwan
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 14/04/2017.
 */
public class Country extends RealmObject
{
	@PrimaryKey
	private String	codPais;
	private String	descripcion;
	
	@Ignore
	public static final String	KEY_CODPAIS	= "codPais";
	
	public String getCodPais()
	{
		return codPais;
	}
	
	public void setCodPais(final String codPais)
	{
		this.codPais = codPais;
	}
	
	public String getDescripcion()
	{
		return descripcion;
	}
	
	public void setDescripcion(final String descripcion)
	{
		this.descripcion = descripcion;
	}
}