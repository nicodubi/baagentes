package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para persistir Departamentos o Partidos provistos por Kwan
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 14/04/2017.
 */
public class Department extends RealmObject
{
	@PrimaryKey
	private String		codigo;
	private String		descripcion;
	private Province	province;
	
	public Department()
	{
	}
	
	public Department(final String codigo, final String descripcion)
	{
		this.codigo			= codigo;
		this.descripcion	= descripcion;
	}
	
	public String getCodigo()
	{
		return codigo;
	}
	
	public void setCodigo(final String codigo)
	{
		this.codigo = codigo;
	}
	
	public String getDescripcion()
	{
		return descripcion;
	}
	
	public void setDescripcion(final String descripcion)
	{
		this.descripcion = descripcion;
	}
	
	public Province getProvince()
	{
		return province;
	}
	
	public void setProvince(final Province province)
	{
		this.province = province;
	}
}