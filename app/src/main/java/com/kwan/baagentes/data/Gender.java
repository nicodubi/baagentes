package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para persistir Géneros de personas provistos por Kwan
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 17/04/2017.
 */
public class Gender extends RealmObject
{
	@PrimaryKey
	private String	id;
	private String	sexo;
	
	public String getId()
	{
		return id;
	}
	
	public void setId(final String id)
	{
		this.id = id;
	}
	
	public String getSexo()
	{
		return sexo;
	}
	
	public void setSexo(final String sexo)
	{
		this.sexo = sexo;
	}
}