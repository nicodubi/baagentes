package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para persistir Infracciones relevadas por esta app
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 25/03/2017.
 */
public class Infringement extends RealmObject{
	@PrimaryKey
	private long				id;
	private String				latitude;
	private String				longitude;
	private TypeInfringement	typeInfringement;
	private int					status;
	private int					statusBackend;
	private String				street;
	private String				height;
	private String				address;
	private User				user;
	private long				idBackend;
	private String				copyDomain;//Acá vamos a guardar la O, D, T del dominio
	private String				typesInfringements;
	private String				typeDomain;
	private String				domain;
	private String				model;
	private Color				color;
	private TypeVehicle			typeVehicle;
	private int					internal;
	private Act					act;
	private Actz				actz;
	private long				captureTs;
	private String				deviceId;//IMEI
	private Country				country;
	private Line				line;
	private String				nameImputed;
	private String				lastNameImputed;
	private String				genderImputed;
	private String				streetImputed;
	private String				heightImputed;
	private String				floorImputed;
	private String				apartmentImputed;//Provincia
	private Locality			localityImputed;
	private String				zipCodeImputed;
	private TypeDocument		typeDocumentImputed;
	private String				documentImputed;
	private String				documentIssuedImputed;//Documento expedido imputado
	private String				license;
	private Municipality		licenseMunicipality;
	private Category			licenseCategory;
	private String				licenseExpiration;
	private String				signatureImputed;
	private String				documentationHeld;//Documentación retenida
	private String				documentAgent;
	private String				vehicleRemission;
	private int					retentionLicense;//Boolean
	private int					retentionVehicle;//Boolean
	private Cinemometer			cinemometer;
	private Article				article;
	private TypeDocument		typeDocumentOwner;
	private String				documentOwner;
	private int					brandId;
	private long				ticketNumber;
	private String				ticketZNumber;
	private String				floor;
	private String				office;
	private String				match;//Guardamos la respuesta del backend para envios en background
	private String				imagen1;
	private String				imagen2;//Guardamos el base64 del pdf dentro del zip para envios en background
	private String				agentDocument;
	private String				formInfringement;
	private String				typeTicket;
	private String				date;
	private String				between1;
	private String				between2;
	private String				dependence;
	private String				issuedBy;
	private Company				newLine;
	private Company				company;
	private String				observations;
	private String				categories;
	private String				numberTicketContravention;
	private String				hour;
	private String				between1Imputed;
	private String				between2Imputed;
	private String				explanationCategory;
	private String				brandStr;
	private String				referencePlace;
	
	@Ignore
	public static final int		STATUS_PENDING		= 0;//Apenas se crean
	@Ignore
	public static final int		STATUS_CONFIRMED	= 1;//Si no hubo conexión para enviar luego
	@Ignore
	public static final int		STATUS_SENDED		= 2;//Envio con ok del server
	@Ignore
	public static final int		STATUS_REJECTED		= 3;//Envio con error del server
	@Ignore
	public static final String	KEY_TICKETNUMBER	= "ticketNumber";
	
	public String getReferencePlace(){
		return referencePlace;
	}
	
	public void setReferencePlace(final String referencePlace){
		this.referencePlace = referencePlace;
	}
	
	public String getBrandStr(){
		return brandStr;
	}
	
	public void setBrandStr(final String brandStr){
		this.brandStr = brandStr;
	}
	
	public String getNumberTicketContravention(){
		return numberTicketContravention;
	}
	
	public void setNumberTicketContravention(final String numberTicketContravention){
		this.numberTicketContravention = numberTicketContravention;
	}
	
	public String getHour(){
		return hour;
	}
	
	public void setHour(final String hour){
		this.hour = hour;
	}
	
	public String getBetween1Imputed(){
		return between1Imputed;
	}
	
	public void setBetween1Imputed(final String between1Imputed){
		this.between1Imputed = between1Imputed;
	}
	
	public String getBetween2Imputed(){
		return between2Imputed;
	}
	
	public void setBetween2Imputed(final String between2Imputed){
		this.between2Imputed = between2Imputed;
	}
	
	public String getExplanationCategory(){
		return explanationCategory;
	}
	
	public void setExplanationCategory(final String explanationCategory){
		this.explanationCategory = explanationCategory;
	}
	
	public String getCategories(){
		return categories;
	}
	
	public void setCategories(final String categories){
		this.categories = categories;
	}
	
	public String getObservations(){
		return observations;
	}
	
	public void setObservations(final String observations){
		this.observations = observations;
	}
	
	public Company getNewLine(){
		return newLine;
	}
	
	public void setNewLine(final Company newLine){
		this.newLine = newLine;
	}
	
	public Company getCompany(){
		return company;
	}
	
	public void setCompany(final Company company){
		this.company = company;
	}
	
	public String getIssuedBy(){
		return issuedBy;
	}
	
	public void setIssuedBy(final String issuedBy){
		this.issuedBy = issuedBy;
	}
	
	public String getDependence(){
		return dependence;
	}
	
	public void setDependence(final String dependence){
		this.dependence = dependence;
	}
	
	public String getTypeTicket(){
		return typeTicket;
	}
	
	public void setTypeTicket(final String typeTicket){
		this.typeTicket = typeTicket;
	}
	
	public String getDate(){
		return date;
	}
	
	public void setDate(final String date){
		this.date = date;
	}
	
	public String getBetween1(){
		return between1;
	}
	
	public void setBetween1(final String between1){
		this.between1 = between1;
	}
	
	public String getBetween2(){
		return between2;
	}
	
	public void setBetween2(final String between2){
		this.between2 = between2;
	}
	
	public long getTicketNumber(){
		return ticketNumber;
	}
	
	public void setTicketNumber(final long ticketNumber){
		this.ticketNumber = ticketNumber;
	}
	
	public int getBrandId(){
		return brandId;
	}
	
	public void setBrandId(final int brandId){
		this.brandId = brandId;
	}
	
	public long getId(){
		return id;
	}
	
	public void setId(final long id){
		this.id = id;
	}
	
	public String getLatitude(){
		return latitude;
	}
	
	public void setLatitude(final String latitude){
		this.latitude = latitude;
	}
	
	public String getLongitude(){
		return longitude;
	}
	
	public void setLongitude(final String longitude){
		this.longitude = longitude;
	}
	
	public TypeInfringement getTypeInfringement(){
		return typeInfringement;
	}
	
	public void setTypeInfringement(final TypeInfringement typeInfringement){
		this.typeInfringement = typeInfringement;
	}
	
	public int getStatus(){
		return status;
	}
	
	public void setStatus(final int status){
		this.status = status;
	}
	
	public int getStatusBackend(){
		return statusBackend;
	}
	
	public void setStatusBackend(final int statusBackend){
		this.statusBackend = statusBackend;
	}
	
	public String getStreet(){
		return street;
	}
	
	public void setStreet(final String street){
		this.street = street;
	}
	
	public String getHeight(){
		return height;
	}
	
	public void setHeight(final String height){
		this.height = height;
	}
	
	public String getAddress(){
		return address;
	}
	
	public void setAddress(final String address){
		this.address = address;
	}
	
	public User getUser(){
		return user;
	}
	
	public void setUser(final User user){
		this.user = user;
	}
	
	public long getIdBackend(){
		return idBackend;
	}
	
	public void setIdBackend(final long idBackend){
		this.idBackend = idBackend;
	}
	
	public String getCopyDomain(){
		return copyDomain;
	}
	
	public void setCopyDomain(final String copyDomain){
		this.copyDomain = copyDomain;
	}
	
	public String getTypesInfringements(){
		return typesInfringements;
	}
	
	public void setTypesInfringements(final String typesInfringements){
		this.typesInfringements = typesInfringements;
	}
	
	public String getTypeDomain(){
		return typeDomain;
	}
	
	public void setTypeDomain(final String typeDomain){
		this.typeDomain = typeDomain;
	}
	
	public String getDomain(){
		return domain;
	}
	
	public void setDomain(final String domain){
		this.domain = domain;
	}
	
	public String getModel(){
		return model;
	}
	
	public void setModel(final String model){
		this.model = model;
	}
	
	public Color getColor(){
		return color;
	}
	
	public void setColor(final Color color){
		this.color = color;
	}
	
	public TypeVehicle getTypeVehicle(){
		return typeVehicle;
	}
	
	public void setTypeVehicle(final TypeVehicle typeVehicle){
		this.typeVehicle = typeVehicle;
	}
	
	public int getInternal(){
		return internal;
	}
	
	public void setInternal(final int internal){
		this.internal = internal;
	}
	
	public Act getAct(){
		return act;
	}
	
	public void setAct(final Act act){
		this.act = act;
	}
	
	public Actz getActz(){
		return actz;
	}
	
	public void setActz(final Actz actz){
		this.actz = actz;
	}
	
	public long getCaptureTs(){
		return captureTs;
	}
	
	public void setCaptureTs(final long captureTs){
		this.captureTs = captureTs;
	}
	
	public String getDeviceId(){
		return deviceId;
	}
	
	public void setDeviceId(final String deviceId){
		this.deviceId = deviceId;
	}
	
	public Country getCountry(){
		return country;
	}
	
	public void setCountry(final Country country){
		this.country = country;
	}
	
	public Line getLine(){
		return line;
	}
	
	public void setLine(final Line line){
		this.line = line;
	}
	
	public String getNameImputed(){
		return nameImputed;
	}
	
	public void setNameImputed(final String nameImputed){
		this.nameImputed = nameImputed;
	}
	
	public String getLastNameImputed(){
		return lastNameImputed;
	}
	
	public void setLastNameImputed(final String lastNameImputed){
		this.lastNameImputed = lastNameImputed;
	}
	
	public String getGenderImputed(){
		return genderImputed;
	}
	
	public void setGenderImputed(final String genderImputed){
		this.genderImputed = genderImputed;
	}
	
	public String getStreetImputed(){
		return streetImputed;
	}
	
	public void setStreetImputed(final String streetImputed){
		this.streetImputed = streetImputed;
	}
	
	public String getHeightImputed(){
		return heightImputed;
	}
	
	public void setHeightImputed(final String heightImputed){
		this.heightImputed = heightImputed;
	}
	
	public String getFloorImputed(){
		return floorImputed;
	}
	
	public void setFloorImputed(final String floorImputed){
		this.floorImputed = floorImputed;
	}
	
	public String getApartmentImputed(){
		return apartmentImputed;
	}
	
	public void setApartmentImputed(final String apartmentImputed){
		this.apartmentImputed = apartmentImputed;
	}
	
	public Locality getLocalityImputed(){
		return localityImputed;
	}
	
	public void setLocalityImputed(final Locality localityImputed){
		this.localityImputed = localityImputed;
	}
	
	public String getZipCodeImputed(){
		return zipCodeImputed;
	}
	
	public void setZipCodeImputed(final String zipCodeImputed){
		this.zipCodeImputed = zipCodeImputed;
	}
	
	public TypeDocument getTypeDocumentImputed(){
		return typeDocumentImputed;
	}
	
	public void setTypeDocumentImputed(final TypeDocument typeDocumentImputed){
		this.typeDocumentImputed = typeDocumentImputed;
	}
	
	public String getDocumentImputed(){
		return documentImputed;
	}
	
	public void setDocumentImputed(final String documentImputed){
		this.documentImputed = documentImputed;
	}
	
	public String getDocumentIssuedImputed(){
		return documentIssuedImputed;
	}
	
	public void setDocumentIssuedImputed(final String documentIssuedImputed){
		this.documentIssuedImputed = documentIssuedImputed;
	}
	
	public String getLicense(){
		return license;
	}
	
	public void setLicense(final String license){
		this.license = license;
	}
	
	public Municipality getLicenseMunicipality(){
		return licenseMunicipality;
	}
	
	public void setLicenseMunicipality(final Municipality licenseMunicipality){
		this.licenseMunicipality = licenseMunicipality;
	}
	
	public Category getLicenseCategory(){
		return licenseCategory;
	}
	
	public void setLicenseCategory(final Category licenseCategory){
		this.licenseCategory = licenseCategory;
	}
	
	public String getLicenseExpiration(){
		return licenseExpiration;
	}
	
	public void setLicenseExpiration(final String licenseExpiration){
		this.licenseExpiration = licenseExpiration;
	}
	
	public String getSignatureImputed(){
		return signatureImputed;
	}
	
	public void setSignatureImputed(final String signatureImputed){
		this.signatureImputed = signatureImputed;
	}
	
	public String getDocumentationHeld(){
		return documentationHeld;
	}
	
	public void setDocumentationHeld(final String documentationHeld){
		this.documentationHeld = documentationHeld;
	}
	
	public String getDocumentAgent(){
		return documentAgent;
	}
	
	public void setDocumentAgent(final String documentAgent){
		this.documentAgent = documentAgent;
	}
	
	public String getVehicleRemission(){
		return vehicleRemission;
	}
	
	public void setVehicleRemission(final String vehicleRemission){
		this.vehicleRemission = vehicleRemission;
	}
	
	public int getRetentionLicense(){
		return retentionLicense;
	}
	
	public void setRetentionLicense(final int retentionLicense){
		this.retentionLicense = retentionLicense;
	}
	
	public int getRetentionVehicle(){
		return retentionVehicle;
	}
	
	public void setRetentionVehicle(final int retentionVehicle){
		this.retentionVehicle = retentionVehicle;
	}
	
	public Cinemometer getCinemometer(){
		return cinemometer;
	}
	
	public void setCinemometer(final Cinemometer cinemometer){
		this.cinemometer = cinemometer;
	}
	
	public Article getArticle(){
		return article;
	}
	
	public void setArticle(final Article article){
		this.article = article;
	}
	
	public TypeDocument getTypeDocumentOwner(){
		return typeDocumentOwner;
	}
	
	public void setTypeDocumentOwner(final TypeDocument typeDocumentOwner){
		this.typeDocumentOwner = typeDocumentOwner;
	}
	
	public String getDocumentOwner(){
		return documentOwner;
	}
	
	public void setDocumentOwner(final String documentOwner){
		this.documentOwner = documentOwner;
	}
	
	public String getTicketZNumber(){
		return ticketZNumber;
	}
	
	public void setTicketZNumber(final String ticketZNumber){
		this.ticketZNumber = ticketZNumber;
	}
	
	public String getFloor(){
		return floor;
	}
	
	public void setFloor(final String floor){
		this.floor = floor;
	}
	
	public String getOffice(){
		return office;
	}
	
	public void setOffice(final String office){
		this.office = office;
	}
	
	public String getMatch(){
		return match;
	}
	
	public void setMatch(final String match){
		this.match = match;
	}
	
	public String getImagen1(){
		return imagen1;
	}
	
	public void setImagen1(final String imagen1){
		this.imagen1 = imagen1;
	}
	
	public String getImagen2(){
		return imagen2;
	}
	
	public void setImagen2(final String imagen2){
		this.imagen2 = imagen2;
	}
	
	public String getAgentDocument(){
		return agentDocument;
	}
	
	public void setAgentDocument(final String agentDocument){
		this.agentDocument = agentDocument;
	}
	
	public String getFormInfringement(){
		return formInfringement;
	}
	
	public void setFormInfringement(final String formInfringement){
		this.formInfringement = formInfringement;
	}
}