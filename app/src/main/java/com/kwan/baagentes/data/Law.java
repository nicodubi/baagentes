package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para persistir Leyes provistos por Kwan
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 14/04/2017.
 */
public class Law extends RealmObject
{
	@PrimaryKey
	private String	idLey;
	private String	descripcionLey;
	
	@Ignore
	public static final String	KEY_DESCRIPCIONLEY		= "descripcionLey";
	
	public String getIdLey()
	{
		return idLey;
	}
	
	public void setIdLey(final String idLey)
	{
		this.idLey = idLey;
	}
	
	public String getDescripcionLey()
	{
		return descripcionLey;
	}
	
	public void setDescripcionLey(final String descripcionLey)
	{
		this.descripcionLey = descripcionLey;
	}
}