package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;

/**
 * Modelo para persistir Líneas de colectivo provistas por Kwan
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 10/12/2016.
 */
public class Line extends RealmObject
{
	private int		id;
	@Index
	private String	sentido;
	@Index
	private int		linea;
	@Index
	private String	cabecera;
	
	@Ignore
	public static final String KEY_LINE		= "linea";
	@Ignore
	public static final String KEY_SENSE	= "sentido";
	@Ignore
	public static final String KEY_CABECERA	= "cabecera";
	
	public int getId()
	{
		return id;
	}
	
	public void setId(final int id)
	{
		this.id = id;
	}
	
	public String getSentido()
	{
		return sentido;
	}
	
	public void setSentido(final String sentido)
	{
		this.sentido = sentido;
	}
	
	public int getLinea()
	{
		return linea;
	}
	
	public void setLinea(final int linea)
	{
		this.linea = linea;
	}
	
	public String getCabecera()
	{
		return cabecera;
	}
	
	public void setCabecera(final String cabecera)
	{
		this.cabecera = cabecera;
	}
}