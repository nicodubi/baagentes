package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para permitirle al usuario elegir el modelo del vehiculo
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 25/03/2017.
 */
public class Model extends RealmObject
{
	@PrimaryKey
	private int		id;
	private String	modelo;
	private int		idMarca;//TODO ver si hace falta agregar un flag para diferenciar por moto, auto o transporte público
	private int		status;
	
	@Ignore
	public static final String	KEY_MODELO	= "modelo";
	
	public int getId()
	{
		return id;
	}
	
	public void setId(final int id)
	{
		this.id = id;
	}
	
	public String getModelo()
	{
		return modelo;
	}
	
	public void setModelo(final String modelo)
	{
		this.modelo = modelo;
	}
	
	public int getIdMarca()
	{
		return idMarca;
	}
	
	public void setIdMarca(final int idMarca)
	{
		this.idMarca = idMarca;
	}
	
	public int getStatus()
	{
		return status;
	}
	
	public void setStatus(final int status)
	{
		this.status = status;
	}
}