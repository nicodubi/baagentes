package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para persistir Provincias provistos por Kwan
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 14/04/2017.
 */
public class Province extends RealmObject
{
	@PrimaryKey
	private int		idProvincia;
	private String	provincia;
	private String	idPais;
	private Country	country;
	
	@Ignore
	public static final String	KEY_PROVINCIA	= "provincia";
	
	public Province()
	{
	}
	
	public int getIdProvincia()
	{
		return idProvincia;
	}
	
	public void setIdProvincia(final int idProvincia)
	{
		this.idProvincia = idProvincia;
	}
	
	public String getProvincia()
	{
		return provincia;
	}
	
	public void setProvincia(final String provincia)
	{
		this.provincia = provincia;
	}
	
	public String getIdPais()
	{
		return idPais;
	}
	
	public void setIdPais(final String idPais)
	{
		this.idPais = idPais;
	}
	
	public Country getCountry()
	{
		return country;
	}
	
	public void setCountry(final Country country)
	{
		this.country = country;
	}
}