package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;

/**
 * Modelo para persistir Calles de CABA provistas por USIG
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 25/03/2017.
 */
public class Street extends RealmObject
{
	private String	idCalle;
	@Index
	private String	nombreCalle;
	private String	keywords;
	@Index
	private String	alturaDesde;
	@Index
	private String	alturaHasta;
	
	@Ignore
	public static final String KEY_NAME	= "nombreCalle";
	
	public String getIdCalle()
	{
		return idCalle;
	}
	
	public void setIdCalle(final String idCalle)
	{
		this.idCalle = idCalle;
	}
	
	public String getNombreCalle()
	{
		return nombreCalle;
	}
	
	public void setNombreCalle(final String nombreCalle)
	{
		this.nombreCalle = nombreCalle;
	}
	
	public String getKeywords()
	{
		return keywords;
	}
	
	public void setKeywords(final String keywords)
	{
		this.keywords = keywords;
	}
	
	public String getAlturaDesde()
	{
		return alturaDesde;
	}
	
	public void setAlturaDesde(final String alturaDesde)
	{
		this.alturaDesde = alturaDesde;
	}
	
	public String getAlturaHasta()
	{
		return alturaHasta;
	}
	
	public void setAlturaHasta(final String alturaHasta)
	{
		this.alturaHasta = alturaHasta;
	}
}