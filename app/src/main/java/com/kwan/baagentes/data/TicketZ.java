package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para cachear los números de actas Z disponibles
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 29/04/2017.
 */
public class TicketZ extends RealmObject
{
	@PrimaryKey
	private int	id;
	private int	used;
	
	@Ignore
	public static final String	KEY_USED	= "used";
	
	public int getId()
	{
		return id;
	}
	
	public void setId(final int id)
	{
		this.id = id;
	}
	
	public int getUsed()
	{
		return used;
	}
	
	public void setUsed(final int used)
	{
		this.used = used;
	}
}