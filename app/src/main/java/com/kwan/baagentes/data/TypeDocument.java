package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para persistir Tipos de documentos provistos por Kwan
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 14/04/2017.
 */
public class TypeDocument extends RealmObject
{
	@PrimaryKey
	private String	tipoDoc;
	private String	documento;
	
	@Ignore
	public static final String	KEY_DOCUMENTO		= "documento";
	
	public String getTipoDoc()
	{
		return tipoDoc;
	}
	
	public void setTipoDoc(final String tipoDoc)
	{
		this.tipoDoc = tipoDoc;
	}
	
	public String getDocumento()
	{
		return documento;
	}
	
	public void setDocumento(final String documento)
	{
		this.documento = documento;
	}
}