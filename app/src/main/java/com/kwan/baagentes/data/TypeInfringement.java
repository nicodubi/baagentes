package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para permitirle al usuario elegir el tipo de la infracción a reportar
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 25/03/2017.
 */
public class TypeInfringement extends RealmObject
{
	@PrimaryKey
	private String	codigo;
	private String	descripcion;
	private int		other; //Boolean
	
	public String getCodigo()
	{
		return codigo;
	}
	
	public void setCodigo(final String codigo)
	{
		this.codigo = codigo;
	}
	
	public String getDescripcion()
	{
		return descripcion;
	}
	
	public void setDescripcion(final String descripcion)
	{
		this.descripcion = descripcion;
	}
	
	public int getOther()
	{
		return other;
	}
	
	public void setOther(final int other)
	{
		this.other = other;
	}
}