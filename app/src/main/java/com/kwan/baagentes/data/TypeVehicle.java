package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;

/**
 * Modelo para persistir Tipos de vehículos provistos por Kwan
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 14/04/2017.
 */
public class TypeVehicle extends RealmObject
{
	private int		idTipoVehiculo;
	private String	descripcion;
	private int		other; //Boolean
	
	@Ignore
	public static final String	KEY_ID	= "idTipoVehiculo";
	
	public int getIdTipoVehiculo()
	{
		return idTipoVehiculo;
	}
	
	public void setIdTipoVehiculo(final int idTipoVehiculo)
	{
		this.idTipoVehiculo = idTipoVehiculo;
	}
	
	public String getDescripcion()
	{
		return descripcion;
	}
	
	public void setDescripcion(final String descripcion)
	{
		this.descripcion = descripcion;
	}
	
	public int getOther()
	{
		return other;
	}
	
	public void setOther(final int other)
	{
		this.other = other;
	}
}