package com.kwan.baagentes.data;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Modelo para persistir Usuarios provistos por Kwan
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 25/03/2017.
 */
public class User extends RealmObject
{
	@PrimaryKey
	private String	username;
	private String	nombre;
	private String	apellido;
	private String	email;
	private String	firma;
	private String	shapassw;
	private String	dni;
	private String	dependencia;
	
	@Ignore
	public static final String	KEY_USERNAME	= "username";
	@Ignore
	public static final String	KEY_NAME		= "nombre";
	@Ignore
	public static final String	KEY_LASTNAME	= "apellido";
	@Ignore
	public static final String	KEY_SIGNATURE	= "firma";
	@Ignore
	public static final String	KEY_SHAPASSW	= "shapassw";
	@Ignore
	public static final String	KEY_TOTP		= "totp";
	@Ignore
	public static final String	KEY_OTPCONSUMED	= "otpConsumido";
	
	public String getDni()
	{
		return dni;
	}
	
	public void setDni(final String dni)
	{
		this.dni = dni;
	}
	
	public String getDependencia()
	{
		return dependencia;
	}
	
	public void setDependencia(final String dependencia)
	{
		this.dependencia = dependencia;
	}
	
	public String getUsername()
	{
		return username;
	}
	
	public void setUsername(final String username)
	{
		this.username = username;
	}
	
	public String getNombre()
	{
		return nombre;
	}
	
	public void setNombre(final String nombre)
	{
		this.nombre = nombre;
	}
	
	public String getApellido()
	{
		return apellido;
	}
	
	public void setApellido(final String apellido)
	{
		this.apellido = apellido;
	}
	
	public String getEmail()
	{
		return email;
	}
	
	public void setEmail(final String email)
	{
		this.email = email;
	}
	
	public String getFirma()
	{
		return firma;
	}
	
	public void setFirma(final String firma)
	{
		this.firma = firma;
	}
	
	public String getShapassw()
	{
		return shapassw;
	}
	
	public void setShapassw(final String shapassw)
	{
		this.shapassw = shapassw;
	}
}