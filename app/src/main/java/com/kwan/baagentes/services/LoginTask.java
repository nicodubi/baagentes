package com.kwan.baagentes.services;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Base64;
import com.afollestad.materialdialogs.MaterialDialog;
import com.boa.interfaces.CallBackListener;
import com.boa.servicies.NetworkStateReceiver;
import com.boa.utils.Api;
import com.boa.utils.Common;
import com.boa.utils.Utils;
import com.kwan.baagentes.R;
import com.kwan.baagentes.activities.MainActivity;
import com.kwan.baagentes.data.TicketB;
import com.kwan.baagentes.data.User;
import com.vusecurity.androidsdkwithbankcode.VUMobileTokenSDK;
import org.json.JSONArray;
import org.json.JSONObject;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Verificación de sesión mediante api
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 25/03/2017.
 */
public class LoginTask extends AsyncTask<Void, Void, String>{
	private Activity			context;
	private String				user, pass, nombre, apellido, email, firma, username, otpConsumido, ultimaOrdenVU;
	private SharedPreferences	preferences;
	private MaterialDialog		progress;
	private boolean				isPolice;
	
	public LoginTask(final Activity context, final String user, final String pass){
		this.context	= context;
		this.user		= user;
		this.pass		= pass;
	}
	
	protected void onPreExecute(){
		try{
			if(progress != null){
				if(progress.isShowing()){
					progress.cancel();
				}
			}
			
			progress = new MaterialDialog.Builder(context)
						.content("Ingresando...")
						.cancelable(false)
						.progress(true, 0)
						.show();
		}catch(Exception e){
			Utils.logError(context, "LoginTask:onPreExecute - Exception:", e);
		}
	}
	
	public static String callLogin(String user, String pass, Context context, boolean encrypt){
		try{
			JSONArray json			= new JSONArray();
			JSONObject jsonObject	= new JSONObject();
			jsonObject.put(Common.KEY_CODE, user);
			
			if(encrypt){
				jsonObject.put(Common.KEY_DESCRIPTION, Base64.encodeToString((Api.encryptPasswordSHA1(pass, context)).getBytes(), Base64.DEFAULT));
			}else{
				jsonObject.put(Common.KEY_DESCRIPTION, pass);//Por la incorporación de la policía, la clave va insegura desencritada
			}
			
			json.put(jsonObject);
			//Cambiar ruta para verificar versión sin llamar a login cuando este
			return Api.request(	Api.LOGIN, Api.METHOD_POST, json.toString(), Base64.encodeToString((user+":"+pass).getBytes(), Base64.DEFAULT), context, Common.AUTH_BASIC);
		}catch(Exception e){
			Utils.logError(context, "LoginTask:callLogin - Exception:", e);
		}
		
		return null;
	}
	
	@Override
	protected String doInBackground(final Void... voids){
		String	result = "";
		try{
			//Debug pass
			if(Common.DEBUG){
				System.out.println("Normal: "+pass);
				System.out.println("SHA1: "+Api.encryptPasswordSHA1(pass, context));
				System.out.println("MD5: "+Api.encryptPasswordMD5(pass, context));
				System.out.println("Token SHA1: "+ Base64.encodeToString((user+":"+Api.encryptPasswordSHA1(pass, context)).getBytes(), Base64.DEFAULT));
				System.out.println("Token MD5: "+ Base64.encodeToString((user+":"+Api.encryptPasswordMD5(pass, context)).getBytes(), Base64.DEFAULT));
			}
			
			Realm.init(context);
			boolean okLogin			= false;
			Realm realm				= Realm.getDefaultInstance();
			final String password	= Api.encryptPasswordMD5(pass, context);
			preferences				= context.getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			
			if(NetworkStateReceiver.isConnected(context)){
				result = callLogin(user, pass, context, false);
				
				if(result != null){
					if(result.equals("Sin internet")){
						realm.close();
						context.runOnUiThread(new Runnable(){
							@Override
							public void run(){
								Utils.showAlertDialog(context, "", "No tienes conexión con el servidor");
							}
						});
						return "ERROR";
					}else{
						if(!result.startsWith("[") && !result.endsWith("]")){
							result = "[]";
						}
					}
				}else{
					result = "[]";
				}
				
				final JSONArray jsonFinal	= new JSONArray(result);
				boolean okJSON				= true;
				isPolice					= false;
				
				if(jsonFinal.length() > 0){
					for(int i=0; i<jsonFinal.length(); i++){
						if(jsonFinal.getJSONObject(i) != null){
							if(	jsonFinal.getJSONObject(i).has(User.KEY_USERNAME) && jsonFinal.getJSONObject(i).has(User.KEY_NAME) &&
								jsonFinal.getJSONObject(i).has(User.KEY_LASTNAME) && jsonFinal.getJSONObject(i).has(Common.KEY_EMAIL) &&
								jsonFinal.getJSONObject(i).has(User.KEY_SIGNATURE) && jsonFinal.getJSONObject(i).has(User.KEY_SHAPASSW) &&
								jsonFinal.getJSONObject(i).has(User.KEY_TOTP)){
								if(	!Utils.isEmpty(jsonFinal.getJSONObject(i).getString(User.KEY_USERNAME)) &&
									!Utils.isEmpty(jsonFinal.getJSONObject(i).getString(User.KEY_SHAPASSW))){
									//Doble check para contemplar login con username y email, tenemos en cuenta de que la clave pueda venir desencriptada también
									if(	(jsonFinal.getJSONObject(i).getString(User.KEY_USERNAME).toLowerCase().trim().equals(user.toLowerCase().trim()) ||
										jsonFinal.getJSONObject(i).getString(Common.KEY_EMAIL).toLowerCase().trim().equals(user.toLowerCase().trim())) &&
										(jsonFinal.getJSONObject(i).getString(User.KEY_SHAPASSW).toLowerCase().trim().equals(password.toLowerCase().trim()) ||
										jsonFinal.getJSONObject(i).getString(User.KEY_SHAPASSW).toLowerCase().trim().equals(pass.toLowerCase().trim()))){
										//OK login contemplando desfasajes por casesentive y espacios
										okLogin			= true;
										username		= jsonFinal.getJSONObject(i).getString(User.KEY_USERNAME);
										nombre			= jsonFinal.getJSONObject(i).getString(User.KEY_NAME);
										apellido		= jsonFinal.getJSONObject(i).getString(User.KEY_LASTNAME);
										email			= jsonFinal.getJSONObject(i).getString(Common.KEY_EMAIL);
										firma			= jsonFinal.getJSONObject(i).getString(User.KEY_TOTP);//Momentaneamente para absorver lo de VU
										otpConsumido	= jsonFinal.getJSONObject(i).getString(User.KEY_OTPCONSUMED);
										ultimaOrdenVU	= jsonFinal.getJSONObject(i).getString("ultimaordenvu");
										
										if(!Utils.isEmpty(jsonFinal.getJSONObject(i).getString("perfil"))){
											if(jsonFinal.getJSONObject(i).getString("perfil").toUpperCase().equals("POLICIA")){
												isPolice = true;
											}
										}
									}
								}
							}else{
								okJSON = false;
							}
						}else{
							okJSON = false;
						}
					}
					
					if(okJSON){
						realm.executeTransaction(new Realm.Transaction(){
							@Override
							public void execute(Realm realm){
								try{
									RealmResults<User> users = realm.where(User.class).findAll();
									
									if(users.size() > 0){
										users.deleteAllFromRealm();
									}
									
									realm.createAllFromJson(User.class, jsonFinal);
									RealmResults<TicketB> ticketBs = realm.where(TicketB.class).findAll();
									
									if(ticketBs.size() > 0){
										ticketBs.deleteAllFromRealm();
									}
								}catch(Exception e){
									Utils.logError(context, "LoginTask:doInBackground:execute - Exception: " , e);
								}
							}
						});
					}else{
						if(okLogin){
							realm.executeTransaction(new Realm.Transaction(){
								@Override
								public void execute(Realm realm){
									try{
										User usuario = new User();
										usuario.setUsername(username);
										usuario.setDni(username);//No se estába guardando el dni del flaco
										usuario.setNombre(nombre);
										usuario.setApellido(apellido);
										usuario.setEmail(email);
										usuario.setFirma(firma);
										usuario.setShapassw(password);
										otpConsumido = "";
										ultimaOrdenVU = "";
										realm.copyToRealmOrUpdate(usuario);
									}catch(Exception e){
										Utils.logError(context, "LoginTask:doInBackground:execute - Exception: " , e);
									}
								}
							});
						}
					}
				}else{
					realm.close();
					context.runOnUiThread(new Runnable(){
						@Override
						public void run(){
							Utils.showAlertDialog(context, "", context.getString(R.string.bad_login));
						}
					});
					return "ERROR";
				}
			}else{
				RealmResults<User> usuarios = realm.where(User.class).findAll();
				
				if(usuarios.size() > 0){
					for(User usuario : usuarios){
						//Doble check para contemplar login con username y email
						if(!Utils.isEmpty(usuario.getUsername()) && !Utils.isEmpty(usuario.getShapassw())){
							if(	(usuario.getUsername().toLowerCase().trim().equals(user.toLowerCase().trim()) ||
								usuario.getEmail().toLowerCase().trim().equals(user.toLowerCase().trim())) &&
								usuario.getShapassw().toLowerCase().trim().equals(password.toLowerCase().trim())){
								okLogin	= true;
								firma	= usuario.getFirma();
							}
						}
					}
				}else{
					realm.close();
					context.runOnUiThread(new Runnable(){
						@Override
						public void run(){
							Utils.showAlertDialog(context, "", "No tienes conexión con el servidor");
						}
					});
					return "ERROR";
				}
			}
			
			realm.close();
			
			if(okLogin){
				return "OK";
			}
		}catch(Exception e){
			Utils.logError(context, "LoginTask:doInBackground - Exception: ", e);
			return "ERROR";
		}
		
		return "";
	}
	
	@Override
	protected void onPostExecute(final String s){
		super.onPostExecute(s);
		try{
			if(s.equals("OK")){
				//Vaciar tickets antes de seguir si cambio de usuario
				if(!preferences.getString(Common.PREF_TOKEN, "")
					.equals(Base64.encodeToString((user+":"+pass).getBytes(), Base64.DEFAULT))){
					Realm realm = Realm.getDefaultInstance();
					realm.executeTransaction(new Realm.Transaction(){
						@Override
						public void execute(Realm realm){
							RealmResults<TicketB> ticketBs = realm.where(TicketB.class).findAll();
							
							if(ticketBs.size() > 0){
								ticketBs.deleteAllFromRealm();
							}
						}
					});
					realm.close();
				}
				
				new PopulateFormTask(context, new CallBackListener(){
					@Override
					public void invoke(){
						new PopulateTicketsTask(context, new CallBackListener(){
							@Override
							public void invoke(){
								context.runOnUiThread(new Runnable(){
									@Override
									public void run(){
										if(progress != null){
											if(progress.isShowing()){
												progress.cancel();
											}
										}
										
										SharedPreferences.Editor editor = preferences.edit();
										editor.putBoolean(Common.PREF_SESSION_STARTED, true);
										editor.putLong(Common.PREF_SESSION_TS, System.currentTimeMillis());
										editor.putString(Common.PREF_CURRENT_LAT, String.valueOf(Common.DEFAULT_LAT));
										editor.putString(Common.PREF_CURRENT_LON, String.valueOf(Common.DEFAULT_LON));
										editor.putString(Common.PREF_SELECTED_LAT, "");
										editor.putString(Common.PREF_SELECTED_LON, "");
										editor.putString(Common.PREF_CURRENT_USER, user);
										editor.putString(Common.PREF_CURRENT_PASS, pass);
										editor.putBoolean(Common.PREF_IS_POLICE, isPolice);//Ahora diferenciamos el perfil del chavón para saber que key usamos
										
										if(!Utils.isEmpty(firma)){
											editor.putString(Common.PREF_SEED, firma);
										}
										
										if(!Utils.isEmpty(otpConsumido)){
											long val;
											
											try{
												val = Long.valueOf(otpConsumido);
											}catch(Exception e){
												val = 1;
											}
											
											editor.putLong(User.KEY_OTPCONSUMED, val);
										}
										
										String result = VUMobileTokenSDK.getOriginCounter(firma, user);
										long originCounter, lastCounter;
										
										try{
											originCounter = Long.valueOf(result);
										}catch(Exception e){
											originCounter = 1;
										}
										
										try{
											lastCounter = Long.valueOf(ultimaOrdenVU);
										}catch(Exception e){
											lastCounter = 1;
										}
										
										if(originCounter != 1){
											if(lastCounter != 1 && lastCounter > originCounter){
												editor.putLong("originCounter", lastCounter);
											}else{
												editor.putLong("originCounter", originCounter);
											}
										}else{
											if(lastCounter != 1){
												editor.putLong("originCounter", lastCounter);
											}
										}
										
										editor.putString(Common.PREF_TOKEN, Base64.encodeToString((user+":"+pass).getBytes(), Base64.DEFAULT));
										editor.apply();
										
										//No va más la firma
										Intent intent = new Intent(context, MainActivity.class);
										context.startActivity(intent);
										context.finish();
									}
								});
							}
						}, false, Base64.encodeToString((user+":"+pass).getBytes(), Base64.DEFAULT), null).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
					}
				}, false).executeOnExecutor(SERIAL_EXECUTOR);
			}else{
				if(progress != null){
					if(progress.isShowing()){
						progress.cancel();
					}
				}
			}
		}catch(Exception e){
			Utils.logError(context, "LoginTask:onPostExecute - Exception: ", e);
		}
	}
}