package com.kwan.baagentes.services;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.RelativeLayout;
import com.afollestad.materialdialogs.MaterialDialog;
import com.boa.interfaces.CallBackValueListener;
import com.boa.utils.Common;
import com.boa.utils.Pdf;
import com.boa.utils.Utils;

/**
 * Created by dgfig on 13/01/2019.
 */
public class PdfTask extends AsyncTask<Void, Void, String>{
	private Activity context;
	private long ts, id;
	private MaterialDialog progress;
	private String pass;
	private RelativeLayout rlAll;
	private CallBackValueListener listener;
	
	public PdfTask(Activity context, long ts, long id, String pass, RelativeLayout rlAll, CallBackValueListener listener){
		this.context = context;
		this.ts = ts;
		this.id = id;
		this.pass = pass;
		this.rlAll = rlAll;
		this.listener = listener;
	}
	
	protected void onPreExecute(){
		try{
			if(progress != null){
				if(progress.isShowing()){
					progress.cancel();
				}
			}
			
			progress = new MaterialDialog.Builder(context)
					.content("Generando...")
					.cancelable(false)
					.progress(true, 0)
					.show();
		}catch(Exception e){
			Utils.logError(context, "PdfTask:onPreExecute - Exception:", e);
		}
	}
	
	@Override
	protected String doInBackground(Void... voids){
		String pdf = null;
		
		try{
			if(Utils.openKeystore(context, pass) == Common.BOOL_YES){
				try{
					Utils.generateVUOtp(context, null);
				}catch(Exception e){
					Utils.logError(context, "PdfTask:printPdf:generateVUOtp - ", e);
				}
				
				pdf = Pdf.generateSignedPdf(context, ts, id, true, true, pass);//No se envía más el pdf por pedido de Saulo
			}else{
				pdf = "";
			}
			
			if(Common.DEBUG){
				System.out.println("PDF PATH: "+pdf);
			}
		}catch(Exception e){
			Utils.logError(context, "PdfTask:doInBackground - Exception:", e);
		}
		
		return pdf;
	}
	
	@Override
	protected void onPostExecute(String s){
		super.onPostExecute(s);
		
		try{
			if(progress != null){
				if(progress.isShowing()){
					progress.cancel();
				}
			}
			
			if(listener != null){
				listener.invoke(s);
			}
		}catch(Exception e){
			Utils.logError(context, "PdfTask:onPostExecute - Exception: ", e);
		}
	}
}