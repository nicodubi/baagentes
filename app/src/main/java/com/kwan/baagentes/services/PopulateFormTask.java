package com.kwan.baagentes.services;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import com.afollestad.materialdialogs.MaterialDialog;
import com.boa.interfaces.CallBackListener;
import com.boa.utils.Api;
import com.boa.utils.Common;
import com.boa.utils.Utils;
import com.kwan.baagentes.data.Brand;
import com.kwan.baagentes.data.Category;
import com.kwan.baagentes.data.Color;
import com.kwan.baagentes.data.Company;
import com.kwan.baagentes.data.Country;
import com.kwan.baagentes.data.Department;
import com.kwan.baagentes.data.Locality;
import com.kwan.baagentes.data.Model;
import com.kwan.baagentes.data.Municipality;
import com.kwan.baagentes.data.Province;
import com.kwan.baagentes.data.TypeDocument;
import com.kwan.baagentes.data.TypeVehicle;
import org.json.JSONArray;
import java.io.InputStream;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Importador de datos para formulario
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 25/03/2017.
 */
public class PopulateFormTask extends AsyncTask<Void, Void, String>{
	private Activity			context;
	private JSONArray			jsonArray;
	private MaterialDialog		progress		= null;
	private CallBackListener	listener		= null;
	private boolean				displayDialog	= true;
	
	public PopulateFormTask(final Activity context){
		this.context = context;
	}
	
	public PopulateFormTask(final Activity context, final CallBackListener listener, final boolean displayDialog){
		this.context		= context;
		this.listener		= listener;
		this.displayDialog	= displayDialog;
	}
	
	protected void onPreExecute(){
		try{
			if(displayDialog){
				if(progress != null){
					if(progress.isShowing()){
						progress.cancel();
					}
				}
				
				progress = new MaterialDialog.Builder(context)
							.content("Iniciando...")
							.cancelable(false)
							.progress(true, 0)
							.show();
			}
		}catch(Exception e){
			Utils.logError(context, "PopulateFormTask:onPreExecute - Exception:", e);
		}
	}
	
	@Override
	protected String doInBackground(final Void... voids){
		String result = null;
		
		try{
			SharedPreferences preferences = context.getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			Utils.saveImei(context);
			
			if(!preferences.getBoolean(Common.PREF_SPLASHED, false) || listener != null){
				Realm.init(context);
				Realm realm	= Realm.getDefaultInstance();
				//Importando tipos de vehículos
				if(realm.where(TypeVehicle.class).count() == 0){
					result = Api.request(Api.TYPE_VEHICLE, Api.METHOD_GET, "", "", context, "");
					
					if(result.startsWith("[") && result.endsWith("]")){
						jsonArray = new JSONArray(result);
						
						if(Common.DEBUG){
							System.out.println("Tipos de vehículos actuales: "+realm.where(TypeVehicle.class).count());
							System.out.println("Tipos de vehículos a impactar: "+jsonArray.length());
							System.out.println("JSON a impactar: "+jsonArray.toString());
						}
						
						if(jsonArray.length() > 0){
							realm.executeTransaction(new Realm.Transaction(){
								@Override
								public void execute(Realm realm){
									try{
										realm.where(TypeVehicle.class).equalTo(Common.KEY_OTHER, Common.BOOL_NO).findAll().deleteAllFromRealm();
										realm.createAllFromJson(TypeVehicle.class, jsonArray);
										RealmResults<TypeVehicle> results = realm.where(TypeVehicle.class).notEqualTo(Common.KEY_OTHER, Common.BOOL_YES).findAll();
										//Corregir valores estándar recibidos de api para diferenciar de los otros
										
										for(int i = results.size() -1; i >=0; i--){
											results.get(i).setOther(Common.BOOL_NO);
										}
									}catch(Exception e){
										Utils.logError(context, "PopulateFormTask:doInBackground:importTypeVehicle - Exception: ", e);
									}
								}
							});
						}
						
						if(Common.DEBUG){
							System.out.println("Tipos de vehículos finales: "+realm.where(TypeVehicle.class).count());
						}
					}
				}
				
				//Importando lineas
				if(realm.where(Company.class).count() == 0){
					result = Api.request(Api.COMPANIES, Api.METHOD_GET, "", "", context, "");
					
					if(result.startsWith("[") && result.endsWith("]")){
						jsonArray = new JSONArray(result);
						
						if(Common.DEBUG){
							System.out.println("Líneas actuales: "+realm.where(Company.class).count());
							System.out.println("Líneas a impactar: "+jsonArray.length());
							System.out.println("JSON a impactar: "+jsonArray.toString());
						}
						
						if(jsonArray.length() > 0){
							realm.executeTransaction(new Realm.Transaction(){
								@Override
								public void execute(Realm realm){
									try{
										realm.where(Company.class).findAll().deleteAllFromRealm();
										realm.createAllFromJson(Company.class, jsonArray);
									}catch(Exception e){
										Utils.logError(context, "PopulateFormTask:doInBackground:importLines - Exception: ", e);
									}
								}
							});
						}
						
						if(Common.DEBUG){
							System.out.println("Líneas finales: "+realm.where(Company.class).count());
						}
					}
				}
				
				//Importando tipos de documento
				if(realm.where(TypeDocument.class).count() == 0){
					result = Api.request(Api.TYPE_DOCUMENT, Api.METHOD_GET, "", "", context, "");
					
					if(result.startsWith("[") && result.endsWith("]")){
						jsonArray = new JSONArray(result);
						
						if(Common.DEBUG){
							System.out.println("Tipos de documento actuales: "+realm.where(TypeDocument.class).count());
							System.out.println("Tipos de documento a impactar: "+jsonArray.length());
							System.out.println("JSON a impactar: "+jsonArray.toString());
						}
						
						if(jsonArray.length() > 0){
							realm.executeTransaction(new Realm.Transaction(){
								@Override
								public void execute(Realm realm){
									try{
										realm.where(TypeDocument.class).findAll().deleteAllFromRealm();
										realm.createAllFromJson(TypeDocument.class, jsonArray);
									}catch(Exception e){
										Utils.logError(context, "PopulateFormTask:doInBackground:importTypeDocument - Exception: ", e);
									}
								}
							});
						}
						
						if(Common.DEBUG){
							System.out.println("Tipos de documento finales: "+realm.where(TypeDocument.class).count());
						}
					}
				}
				
				//Importando países
				if(realm.where(Country.class).count() == 0){
					result = Api.request(Api.COUNTRIES, Api.METHOD_GET, "", "", context, "");
					
					if(result.startsWith("[") && result.endsWith("]")){
						jsonArray = new JSONArray(result);
						
						if(Common.DEBUG){
							System.out.println("Países actuales: "+realm.where(Country.class).count());
							System.out.println("Países a impactar: "+jsonArray.length());
							System.out.println("JSON a impactar: "+jsonArray.toString());
						}
						
						if(jsonArray.length() > 0){
							realm.executeTransaction(new Realm.Transaction(){
								@Override
								public void execute(Realm realm){
									try{
										realm.where(Country.class).findAll().deleteAllFromRealm();
										realm.createAllFromJson(Country.class, jsonArray);
									}catch(Exception e){
										Utils.logError(context, "PopulateFormTask:doInBackground:importCountry - Exception: ", e);
									}
								}
							});
						}
						
						if(Common.DEBUG){
							System.out.println("Países finales: "+realm.where(Country.class).count());
						}
					}
				}
				
				//Importando marcas
				if(realm.where(Brand.class).count() == 0){
					result = Api.request(Api.BRANDS, Api.METHOD_GET, "", "", context, "");
					
					if(result.startsWith("[") && result.endsWith("]")){
						jsonArray = new JSONArray(result);
						
						if(Common.DEBUG){
							System.out.println("Marcas actuales: "+realm.where(Brand.class).count());
							System.out.println("Marcas a impactar: "+jsonArray.length());
							System.out.println("JSON a impactar: "+jsonArray.toString());
						}
						
						if(jsonArray.length() > 0){
							realm.executeTransaction(new Realm.Transaction(){
								@Override
								public void execute(Realm realm){
									try{
										realm.where(Brand.class).findAll().deleteAllFromRealm();
										realm.createAllFromJson(Brand.class, jsonArray);
									}catch(Exception e){
										Utils.logError(context, "PopulateFormTask:doInBackground:importBrands - Exception: ", e);
									}
								}
							});
						}
						
						if(Common.DEBUG){
							System.out.println("Marcas finales: "+realm.where(Brand.class).count());
						}
					}
				}
				
				//Importando modelos
				if(realm.where(Model.class).count() == 0){
					result = Api.request(Api.MODELS, Api.METHOD_GET, "", "", context, "");
					
					if(result.startsWith("[") && result.endsWith("]")){
						jsonArray = new JSONArray(result);
						
						if(Common.DEBUG){
							System.out.println("Modelos actuales: "+realm.where(Model.class).count());
							System.out.println("Modelos a impactar: "+jsonArray.length());
							System.out.println("JSON a impactar: "+jsonArray.toString());
						}
						
						if(jsonArray.length() > 0){
							realm.executeTransaction(new Realm.Transaction(){
								@Override
								public void execute(Realm realm){
									try{
										realm.where(Model.class).findAll().deleteAllFromRealm();
										realm.createAllFromJson(Model.class, jsonArray);
									}catch(Exception e){
										Utils.logError(context, "PopulateFormTask:doInBackground:importModel - Exception: ", e);
									}
								}
							});
						}
						
						if(Common.DEBUG){
							System.out.println("Modelos finales: "+realm.where(Model.class).count());
						}
					}
				}
				
				//Importando provincias
				if(realm.where(Province.class).count() == 0){
					result = Api.request(Api.PROVINCE, Api.METHOD_GET, "", "", context, "");
					
					if(result.startsWith("[") && result.endsWith("]")){
						jsonArray = new JSONArray(result);
						
						if(Common.DEBUG){
							System.out.println("Provincias actuales: "+realm.where(Province.class).count());
							System.out.println("Provincias a impactar: "+jsonArray.length());
							System.out.println("JSON a impactar: "+jsonArray.toString());
						}
						
						if(jsonArray.length() > 0){
							realm.executeTransaction(new Realm.Transaction(){
								@Override
								public void execute(Realm realm){
									try{
										realm.where(Province.class).findAll().deleteAllFromRealm();
										realm.createAllFromJson(Province.class, jsonArray);
									}catch(Exception e){
										Utils.logError(context, "PopulateFormTask:doInBackground:importProvince - Exception: ", e);
									}
								}
							});
						}
						
						if(Common.DEBUG){
							System.out.println("Provincias finales: "+realm.where(Province.class).count());
						}
					}
				}
				
				//Importando colores desde Acarreos
				if(realm.where(Color.class).count() == 0){
					realm.executeTransaction(new Realm.Transaction(){
						@Override
						public void execute(Realm realm){
							try{
								InputStream is = context.getAssets().open("colores.json");
								realm.createAllFromJson(Color.class, is);
							}catch(Exception e){
								Utils.logError(context, "PopulateFormTask:doInBackground:importColor - Exception: ", e);
							}
						}
					});
				}
				
				//TODO verificar cuando haya más data
				realm.executeTransaction(new Realm.Transaction(){
					@Override
					public void execute(Realm realm){
						try{
							realm.copyToRealmOrUpdate(new Category("A1", "A1"));
							realm.copyToRealmOrUpdate(new Category("A2.1", "A2.1"));
							realm.copyToRealmOrUpdate(new Category("A2.2", "A2.2"));
							realm.copyToRealmOrUpdate(new Category("A3", "A3"));
							realm.copyToRealmOrUpdate(new Category("A4.1", "A4.1"));
							realm.copyToRealmOrUpdate(new Category("A4.2", "A4.2"));
							realm.copyToRealmOrUpdate(new Category("A4.3", "A4.3"));
							realm.copyToRealmOrUpdate(new Category("A4.4", "A4.4"));
							realm.copyToRealmOrUpdate(new Category("PA1", "PA1"));
							realm.copyToRealmOrUpdate(new Category("PA2.1", "PA2.1"));
							realm.copyToRealmOrUpdate(new Category("PA2.2", "PA2.2"));
							realm.copyToRealmOrUpdate(new Category("PA3", "PA3"));
							realm.copyToRealmOrUpdate(new Category("PA4.1", "PA4.1"));
							realm.copyToRealmOrUpdate(new Category("PA4.2", "PA4.2"));
							realm.copyToRealmOrUpdate(new Category("PA4.3", "PA4.3"));
							realm.copyToRealmOrUpdate(new Category("PA4.4", "PA4.4"));
							realm.copyToRealmOrUpdate(new Category("B1", "B1"));
							realm.copyToRealmOrUpdate(new Category("B2", "B2"));
							realm.copyToRealmOrUpdate(new Category("C", "C"));
							realm.copyToRealmOrUpdate(new Category("D1", "D1"));
							realm.copyToRealmOrUpdate(new Category("D2.1", "D2.1"));
							realm.copyToRealmOrUpdate(new Category("D2.2", "D2.2"));
							realm.copyToRealmOrUpdate(new Category("E1", "E1"));
							realm.copyToRealmOrUpdate(new Category("E2", "E2"));
							realm.copyToRealmOrUpdate(new Category("F", "F"));
							realm.copyToRealmOrUpdate(new Category("G1", "G1"));
							realm.copyToRealmOrUpdate(new Category("G2", "G2"));
							realm.copyToRealmOrUpdate(new Category("O", "Otros"));
							realm.copyToRealmOrUpdate(new Department("S", "Av. Sarmiento y Av. Costanera Rafael Obligado"));
							realm.copyToRealmOrUpdate(new Department("H", "Herrera 2116"));
							realm.copyToRealmOrUpdate(new Municipality("O", "Otro"));
							realm.copyToRealmOrUpdate(new Locality("TEST", "Localidad de prueba"));
							realm.copyToRealmOrUpdate(new Locality("O", "Otro"));
						}catch(Exception e){
							Utils.logError(context, "PopulateFormTask:doInBackground:importVarious - Exception: ", e);
						}
					}
				});
				
				realm.close();
				SharedPreferences.Editor editor = preferences.edit();
				editor.putBoolean(Common.PREF_SPLASHED, true);
				editor.apply();
			}
			
			if(progress != null){
				if(progress.isShowing()){
					progress.cancel();
				}
			}
			
			if(listener != null){
				listener.invoke();
			}else{
				Utils.checkSession(context, false);
			}
		}catch(Exception e){
			Utils.logError(context, "PopulateFormTask:doInBackground - Exception: ", e);
		}
		
		return result;
	}
}