package com.kwan.baagentes.services;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;
import com.afollestad.materialdialogs.MaterialDialog;
import com.boa.interfaces.CallBackListener;
import com.boa.interfaces.CallBackValueListener;
import com.boa.servicies.NetworkStateReceiver;
import com.boa.utils.Api;
import com.boa.utils.Common;
import com.boa.utils.Utils;
import com.kwan.baagentes.data.Brand;
import com.kwan.baagentes.data.Company;
import com.kwan.baagentes.data.Country;
import com.kwan.baagentes.data.Department;
import com.kwan.baagentes.data.Municipality;
import com.kwan.baagentes.data.Province;
import com.kwan.baagentes.data.TicketB;
import com.kwan.baagentes.data.TypeDocument;
import com.kwan.baagentes.data.TypeInfringement;
import com.kwan.baagentes.data.TypeVehicle;
import org.json.JSONArray;
import org.json.JSONObject;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Importador de datos refrescables para formulario
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 25/03/2017.
 */
public class PopulateTicketsTask extends AsyncTask<Void, Void, String>{
	private Activity				context;
	private MaterialDialog			progress;
	private CallBackListener		listener;
	private CallBackValueListener	listenerWithValue;
	private JSONArray				jsonArray;
	private boolean					displayDialog	= false;
	private String					token			= "";
	
	public PopulateTicketsTask(final Activity context, final CallBackListener listener, final boolean displayDialog, final String token, final CallBackValueListener listenerWithValue){
		this.context			= context;
		this.listener			= listener;
		this.displayDialog		= displayDialog;
		this.token				= token;
		this.listenerWithValue	= listenerWithValue;
	}
	
	protected void onPreExecute(){
		try{
			if(displayDialog){
				if(progress != null){
					if(progress.isShowing()){
						progress.cancel();
					}
				}
				
				progress = new MaterialDialog.Builder(context)
							.content("Cargando...")
							.cancelable(false)
							.progress(true, 0)
							.show();
			}
		}catch(Exception e){
			Utils.logError(context, "PopulateTicketsTask:onPreExecute - Exception:", e);
		}
	}
	
	@Override
	protected String doInBackground(final Void... voids){
		String result = "";
		
		try{
			//Importando tipos de infracción, según usuario
			SharedPreferences preferences	= context.getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
			Realm.init(context);
			Realm realm						= Realm.getDefaultInstance();
			
			if(Utils.isEmpty(token)){
				token = preferences.getString(Common.PREF_TOKEN, "");
			}
			
			if(!Utils.isEmpty(token)){
				//Importando tipos de infracción
				result = Api.request(Api.TYPE_INFRINGEMENT, Api.METHOD_POST, "", token, context, Common.AUTH_BASIC);
				
				if(result.startsWith("[") && result.endsWith("]")){
					final JSONArray jsonArray = new JSONArray(result);
					
					if(Common.DEBUG){
						System.out.println("Tipos de infracción by User actuales: " +realm.where(TypeInfringement.class).count());
						System.out.println("Tipos de infracción by User a impactar: " +jsonArray.length());
					}
					
					if(jsonArray.length() > 0){
						realm.executeTransaction(new Realm.Transaction(){
							@Override
							public void execute(Realm realm){
								try{
									realm.where(TypeInfringement.class).notEqualTo(Common.KEY_OTHER, Common.BOOL_YES).findAll().deleteAllFromRealm();
									realm.createAllFromJson(TypeInfringement.class, jsonArray);
									RealmResults<TypeInfringement> results = realm.where(TypeInfringement.class).notEqualTo(Common.KEY_OTHER, Common.BOOL_YES).findAll();
									//Corregir valores estándar recibidos de api para diferenciar de los otros
									for(int i = results.size() -1; i >=0; i--){
										results.get(i).setOther(Common.BOOL_NO);
									}
								}catch(Exception e){
									Utils.logError(context, "PopulateTicketsTask:doInBackground:importTypeInfringement - Exception: ", e);
								}
							}
						});
					}
					
					if(Common.DEBUG){
						System.out.println("Tipos de infracción by User finales: " +realm.where(TypeInfringement.class).count());
					}
				}
				
				//Agregado para traer los municipios de las licencias o centros de emisión
				result = Api.request(Api.MUNICIPALITIES, Api.METHOD_GET, "", "", context, "");
				
				if(result.startsWith("[") && result.endsWith("]")){
					final JSONArray jsonArray = new JSONArray(result);
					
					if(Common.DEBUG){
						System.out.println("CentroEmisión actuales: " +realm.where(Municipality.class).count());
						System.out.println("CentroEmisión a impactar: " +jsonArray.length());
					}
					
					if(jsonArray.length() > 0){
						realm.executeTransaction(new Realm.Transaction(){
							@Override
							public void execute(Realm realm){
								try{
									for(int i=0; i<jsonArray.length(); i++){
										final JSONObject jsonObject = jsonArray.getJSONObject(i);
										
										if(jsonObject != null){
											if(!Utils.isEmpty(jsonObject.getString(Common.KEY_ID)) && !Utils.isEmpty(jsonObject.getString("nombre"))){
												Municipality municipality = realm.where(Municipality.class).equalTo(Common.KEY_CODE, jsonObject.getString(Common.KEY_ID)).findFirst();
												
												if(municipality == null){
													municipality = new Municipality();
													municipality.setCodigo(jsonObject.getString(Common.KEY_ID));
													municipality.setDescripcion(jsonObject.getString("nombre"));
													realm.copyToRealmOrUpdate(municipality);
												}
											}
										}
									}
								}catch(Exception e){
									Utils.logError(context, "PopulateTicketsTask:doInBackground:importMunicipality - Exception: ", e);
								}
							}
						});
					}
				}
			}
			
			//Refrescar tipos de vehiculos, líneas de colectivo/empresa, tipos de documentos y marcas
			result = Api.request(Api.TYPE_VEHICLE, Api.METHOD_GET, "", "", context, "");
			
			if(result.startsWith("[") && result.endsWith("]")){
				jsonArray = new JSONArray(result);
				
				if(Common.DEBUG){
					System.out.println("Tipos de vehículos actuales: "+realm.where(TypeVehicle.class).count());
					System.out.println("Tipos de vehículos a impactar: "+jsonArray.length());
				}
				
				if(jsonArray.length() > 0){
					realm.executeTransaction(new Realm.Transaction(){
						@Override
						public void execute(Realm realm){
							try{
								realm.where(TypeVehicle.class).equalTo(Common.KEY_OTHER, Common.BOOL_NO).findAll().deleteAllFromRealm();
								realm.createAllFromJson(TypeVehicle.class, jsonArray);
								RealmResults<TypeVehicle> results = realm.where(TypeVehicle.class).notEqualTo(Common.KEY_OTHER, Common.BOOL_YES).findAll();
								//Corregir valores estándar recibidos de api para diferenciar de los otros
								
								for(int i = results.size() -1; i >=0; i--){
									results.get(i).setOther(Common.BOOL_NO);
								}
							}catch(Exception e){
								Utils.logError(context, "PopulateTicketsTask:doInBackground:importTypeVehicle - Exception: ", e);
							}
						}
					});
				}
				
				if(Common.DEBUG){
					System.out.println("Tipos de vehículos finales: "+realm.where(TypeVehicle.class).count());
				}
			}
			
			result = Api.request(Api.COMPANIES, Api.METHOD_GET, "", "", context, "");
			
			if(result.startsWith("[") && result.endsWith("]")){
				jsonArray = new JSONArray(result);
				
				if(Common.DEBUG){
					System.out.println("Líneas actuales: "+realm.where(Company.class).count());
					System.out.println("Líneas a impactar: "+jsonArray.length());
				}
				
				if(jsonArray.length() > 0){
					realm.executeTransaction(new Realm.Transaction(){
						@Override
						public void execute(Realm realm){
							try{
								realm.where(Company.class).findAll().deleteAllFromRealm();
								realm.createAllFromJson(Company.class, jsonArray);
							}catch(Exception e){
								Utils.logError(context, "PopulateTicketsTask:doInBackground:importLines - Exception: ", e);
							}
						}
					});
				}
				
				if(Common.DEBUG){
					System.out.println("Líneas finales: "+realm.where(Company.class).count());
				}
			}
			
			result = Api.request(Api.TYPE_DOCUMENT, Api.METHOD_GET, "", "", context, "");
			
			if(result.startsWith("[") && result.endsWith("]")){
				jsonArray = new JSONArray(result);
				
				if(Common.DEBUG){
					System.out.println("Tipos de documento actuales: "+realm.where(TypeDocument.class).count());
					System.out.println("Tipos de documento a impactar: "+jsonArray.length());
				}
				
				if(jsonArray.length() > 0){
					realm.executeTransaction(new Realm.Transaction(){
						@Override
						public void execute(Realm realm){
							try{
								realm.where(TypeDocument.class).findAll().deleteAllFromRealm();
								realm.createAllFromJson(TypeDocument.class, jsonArray);
							}catch(Exception e){
								Utils.logError(context, "PopulateTicketsTask:doInBackground:importTypeDocument - Exception: ", e);
							}
						}
					});
				}
				
				if(Common.DEBUG){
					System.out.println("Tipos de documento finales: "+realm.where(TypeDocument.class).count());
				}
			}
			
			result = Api.request(Api.BRANDS, Api.METHOD_GET, "", "", context, "");
			
			if(result.startsWith("[") && result.endsWith("]")){
				jsonArray = new JSONArray(result);
				
				if(Common.DEBUG){
					System.out.println("Marcas actuales: "+realm.where(Brand.class).count());
					System.out.println("Marcas a impactar: "+jsonArray.length());
				}
				
				if(jsonArray.length() > 0){
					realm.executeTransaction(new Realm.Transaction(){
						@Override
						public void execute(Realm realm){
							try{
								realm.where(Brand.class).findAll().deleteAllFromRealm();
								realm.createAllFromJson(Brand.class, jsonArray);
							}catch(Exception e){
								Utils.logError(context, "PopulateTicketsTask:doInBackground:importBrands - Exception: ", e);
							}
						}
					});
				}
				
				if(Common.DEBUG){
					System.out.println("Marcas finales: "+realm.where(Brand.class).count());
				}
			}
			
			result = Api.request(Api.DEPARTMENTS, Api.METHOD_GET, "", "", context, "");
			
			if(result.startsWith("[") && result.endsWith("]")){
				jsonArray = new JSONArray(result.replace("\"id\"", "\"codigo\"").replace("\"playa\"", "\"descripcion\""));
				
				if(Common.DEBUG){
					System.out.println("Playas actuales: "+realm.where(Brand.class).count());
					System.out.println("Playas a impactar: "+jsonArray.length());
				}
				
				if(jsonArray.length() > 0){
					realm.executeTransaction(new Realm.Transaction(){
						@Override
						public void execute(Realm realm){
							try{
								realm.where(Department.class).findAll().deleteAllFromRealm();
								realm.createAllFromJson(Department.class, jsonArray);
							}catch(Exception e){
								Utils.logError(context, "PopulateTicketsTask:doInBackground:importDepartments - Exception: ", e);
							}
						}
					});
				}
				
				if(Common.DEBUG){
					System.out.println("Playas finales: "+realm.where(Brand.class).count());
				}
			}
			
			result = Api.request(Api.TICKETS, Api.METHOD_POST, "", token, context, Common.AUTH_BASIC);
			
			if(result.startsWith("{") && result.endsWith("}")){
				JSONObject jsonObject = new JSONObject(result);
				
				if(jsonObject.has("actasB")){
					if(!jsonObject.isNull("actasB")){
						final JSONArray ticketsB = jsonObject.getJSONArray("actasB");
						
						//Verificación para saber de antemano si hay o no actas disponibles
						if(ticketsB.length() > 0){
							realm.executeTransaction(new Realm.Transaction(){
								@Override
								public void execute(Realm realm){
									try{
										if(Common.DEBUG){
											System.out.println("Tickets actuales: " +realm.where(TicketB.class).count());
											System.out.println("Tickets a impactar: " +ticketsB.length());
										}
										
										if(ticketsB.length() > 0){
											for(int i=0; i<ticketsB.length(); i++){
												TicketB ticketB = realm.where(TicketB.class).equalTo(Common.KEY_ID, Integer.valueOf(ticketsB.get(i).toString())).findFirst();
												
												if(ticketB == null){
													ticketB = new TicketB();
													ticketB.setId(Integer.parseInt(ticketsB.get(i).toString()));
													ticketB.setUsed(Common.BOOL_NO);
													realm.copyToRealmOrUpdate(ticketB);
												}
											}
										}
										
										if(Common.DEBUG){
											System.out.println("Tickets finales: " +realm.where(TicketB.class).count());
										}
									}catch(Exception e){
										Utils.logError(context, "PopulateTicketTask:doInBackground:importTicketsB - Exception:", e);
									}
								}
							});
						}else{
							//Vemos si por más de que no respondió la api tenemos en la db
							if(realm.where(TicketB.class).equalTo(TicketB.KEY_USED, Common.BOOL_NO).count() == 0){
								return "ERROR";
							}
						}
					}else{
						//Vemos si por más de que no respondió la api tenemos en la db
						if(realm.where(TicketB.class).equalTo(TicketB.KEY_USED, Common.BOOL_NO).count() == 0){
							return "ERROR";
						}
					}
				}else{
					//Vemos si por más de que no respondió la api tenemos en la db
					if(realm.where(TicketB.class).equalTo(TicketB.KEY_USED, Common.BOOL_NO).count() == 0){
						return "ERROR";
					}
				}
			}else{
				//Vemos si por más de que no respondió la api tenemos en la db
				if(realm.where(TicketB.class).equalTo(TicketB.KEY_USED, Common.BOOL_NO).count() == 0){
					return "ERROR";
				}
			}
			
			//Si cuando entro no hubo red populamos por si no hay data antes de que se loguee
			if(	realm.where(TypeVehicle.class).count() == 0  || realm.where(Company.class).count() == 0 || realm.where(TypeDocument.class).count() == 0 ||
				realm.where(Country.class).count() == 0 || realm.where(Brand.class).count() == 0 || realm.where(Province.class).count() == 0){
				result = "REFRESH";
			}
			
			realm.close();
			
			if(progress != null){
				if(progress.isShowing()){
					progress.cancel();
				}
			}
		}catch(Exception e){
			Utils.logError(context, "PopulateTicketTask:doInBackground - Exception: ", e);
		}
		
		return result;
	}
	
	@Override
	protected void onPostExecute(String s){
		try{
			if(displayDialog){
				if(progress != null){
					if(progress.isShowing()){
						progress.cancel();
					}
				}
			}
			
			if(s == null){
				s = "";
			}
			
			switch(s){
				case "REFRESH":
					new PopulateFormTask(context, listener, false).executeOnExecutor(SERIAL_EXECUTOR);
				break;
				
				case "ERROR":
					if(NetworkStateReceiver.isConnected(context)){
						Toast.makeText(context, "No hay actas disponibles. Contacte al equipo de tecnología para actualizar sus datos", Toast.LENGTH_LONG).show();
					}
					
					if(listenerWithValue != null){
						listenerWithValue.invoke(s);
					}else{
						if(listener != null){
							listener.invoke();
						}
					}
				break;
				
				default:
					if(listenerWithValue != null){
						listenerWithValue.invoke(s);
					}else{
						if(listener != null){
							listener.invoke();
						}
					}
				break;
			}
		}catch(Exception e){
			Utils.logError(context, "PopulateTicketTask:onPostExecute - Exception: ", e);
		}
	}
}