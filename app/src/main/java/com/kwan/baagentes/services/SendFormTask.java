package com.kwan.baagentes.services;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.boa.interfaces.CallBackListener;
import com.boa.servicies.NetworkStateReceiver;
import com.boa.utils.Api;
import com.boa.utils.Common;
import com.boa.utils.SendFormTaskLocker;
import com.boa.utils.Utils;
import com.kwan.baagentes.R;
import com.kwan.baagentes.data.Infringement;
import com.kwan.baagentes.data.TicketB;
import com.kwan.baagentes.data.TypeDocument;
import com.kwan.baagentes.data.User;

import org.json.JSONObject;

import java.io.File;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Importador de datos para formulario: Infracción
 * Created by Boa (David Figueroa davo.figueroa14@gmail.com) on 10/12/2016.
 */
public class SendFormTask extends AsyncTask<Void, Void, String> {
    private Context context;
    private Activity activity;
    private long reportId;
    private CallBackListener listener;
    private MaterialDialog progress;
    private SharedPreferences preferences;
    private JSONObject json;
    private long ticketNumber = 0, ts = 0;
    private String pdf = "";
    private boolean showPopup = false;

    public SendFormTask(final Context context, final long reportId, final String pdf, final CallBackListener listener) {
        this.context = context;
        this.activity = null;
        this.reportId = reportId;
        this.pdf = pdf;
        this.listener = listener;
    }

    public SendFormTask(final Activity activity, final long reportId, final String pdf, long ts, final CallBackListener listener) {
        this.context = activity;
        this.activity = activity;
        this.reportId = reportId;
        this.pdf = pdf;
        this.listener = listener;
        this.ts = ts;
    }

    protected void onPreExecute() {
        try {
            if (progress != null) {
                if (progress.isShowing()) {
                    progress.cancel();
                }
            }

            if (listener != null) {
                progress = new MaterialDialog.Builder(context)
                        .content("Enviando...")
                        .cancelable(false)
                        .progress(true, 0)
                        .show();
            }
        } catch (Exception e) {
            Utils.logError(context, "SendFormTask:onPreExecute - Exception:", e);
        }
    }

    @Override
    protected String doInBackground(final Void... voids) {
        SendFormTaskLocker.getInstance().lockTask();
        String result = "OK";

        try {
            Realm.init(context);
            preferences = context.getSharedPreferences(Common.KEY_PREF, Context.MODE_PRIVATE);
            final Realm realm = Realm.getDefaultInstance();

            if (reportId != 0) {
                //Envío simple de acta visible en el que se está trabajando
                final Infringement infringement = realm.where(Infringement.class).equalTo(Common.KEY_ID, reportId).findFirst();

                if (infringement != null) {
                    //Ahora siempre se muestra el popup con el número de acta
                    showPopup = true;
                    ticketNumber = infringement.getTicketNumber();

                    //Rescatar problema por pdf en background
                    if (Utils.isEmpty(pdf)) {
                        pdf = Utils.reGenerateZip(context, ts);
                    }

                    //Procesar el pdf
                    final String zipPdfAddress = pdf;
                    if (!Utils.isEmpty(pdf)) {
                        pdf = Utils.getBase64FromFile(pdf, context);
                        if(!Utils.isEmpty(pdf)){
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    infringement.setImagen2(pdf);
                                }
                            });
                        }
                    }

                    if(ts != 0){
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                infringement.setCaptureTs(ts);
                            }
                        });
                    }

                    JSONObject json = prepareInfringement(infringement, realm);

                    if (json != null) {
                        if (NetworkStateReceiver.isConnected(context)) {
                            result = Api.request(Api.INFRINGEMENT_REPORT, Api.METHOD_POST, json.toString(), preferences.getString(Common.PREF_TOKEN, ""), context, Common.AUTH_BASIC).trim();
                        } else {
                            result = "";
                        }

                        if (Common.DEBUG) {
                            System.out.println("Result: " + result);
                            System.out.println("Is json: " + (result.startsWith("{") && result.endsWith("}")));
                        }

                        if (result.startsWith("{") && result.endsWith("}")) {
                            try {
                                //Nuevo tratamiento de errores vía api
                                JSONObject jsonObject = new JSONObject(result);

                                if (Common.DEBUG) {
                                    System.out.println("Has Message: " + (jsonObject.has("message")));
                                }

                                if (jsonObject.has("message")) {
                                    if (Common.DEBUG) {
                                        System.out.println("Message is not null: " + (!jsonObject.isNull("message")));
                                    }

                                    if (!jsonObject.isNull("message")) {
                                        if (Utils.isEmpty(jsonObject.getString("message").trim())) {
                                            result = "OK";
                                            //Ok guardamos y seguimos
                                            realm.executeTransaction(new Realm.Transaction() {
                                                @Override
                                                public void execute(final Realm realm) {
                                                    try {
                                                        long infrigmentIDToFindInRealm = getInfrigmentIDToFindInRealm(infringement,reportId);
                                                        Infringement infringement1 = realm.where(Infringement.class).equalTo(Common.KEY_ID, infrigmentIDToFindInRealm).findFirst();
                                                        infringement1.setStatus(Infringement.STATUS_SENDED);
                                                        deleteZipPdfFromDevice(zipPdfAddress);
                                                        if (infringement1.getTicketNumber() != 0) {
                                                            TicketB ticketB = realm.where(TicketB.class).equalTo(Common.KEY_ID, infringement1.getTicketNumber()).findFirst();

                                                            if (ticketB == null) {
                                                                ticketB = new TicketB();
                                                                ticketB.setId((int) infringement1.getTicketNumber());
                                                            }

                                                            ticketB.setUsed(Common.BOOL_YES);
                                                            realm.copyToRealmOrUpdate(ticketB);
                                                        }

                                                        realm.copyToRealmOrUpdate(infringement1);
                                                        pdf = "";
                                                        ts = 0;
                                                    } catch (Exception e) {
                                                        Utils.logError(context, "SendFormTask:doInBackground:execute - Exception:", e);
                                                    }
                                                }
                                            });
                                        } else {
                                            //Marcamos como rechazada porque hay mensaje del servidor
                                            final String message = jsonObject.getString("message").trim();
                                            String error = message.toLowerCase();
                                            error = error.replace("ó", "o").replace("á", "a");

                                            if (!error.contains("cupon vu invalido")) {
                                                realm.executeTransaction(new Realm.Transaction() {
                                                    @Override
                                                    public void execute(final Realm realm) {
                                                        try {
                                                            long infrigmentIDToFindInRealm = getInfrigmentIDToFindInRealm(infringement,reportId);
                                                            Infringement infringement1 = realm.where(Infringement.class).equalTo(Common.KEY_ID, infrigmentIDToFindInRealm).findFirst();
                                                            infringement1.setStatus(Infringement.STATUS_REJECTED);
                                                            infringement1.setMatch(message);
                                                            infringement1.setImagen2(pdf);
                                                            realm.copyToRealmOrUpdate(infringement1);
                                                        } catch (Exception e) {
                                                            Utils.logError(context, "SendFormTask:doInBackground:execute - Exception:", e);
                                                        }
                                                    }
                                                });
                                            }

                                            //Hay mensaje de error del backend, frenar
                                            return jsonObject.getString("message").trim();
                                        }
                                    } else {
                                        return "ERROR";
                                    }
                                } else {
                                    return "ERROR";
                                }
                            } catch (Exception e) {
                                Utils.logError(context, "SendFormTask:doInBackground - Exception:", e);
                            }
                        } else {
                            //Probablemente hubo problema de red, guardamos
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(final Realm realm) {
                                    try {
                                        long infrigmentIDToFindInRealm = getInfrigmentIDToFindInRealm(infringement,reportId);
                                        Infringement infringement1 = realm.where(Infringement.class).equalTo(Common.KEY_ID, infrigmentIDToFindInRealm).findFirst();
                                        infringement1.setStatus(Infringement.STATUS_CONFIRMED);
                                        //ESTA BIEN GUARDAR ACA EL PDF BASE64
                                        infringement1.setImagen2(pdf);
                                    } catch (Exception e) {
                                        Utils.logError(context, "SendFormTask:doInBackground:execute - Exception:", e);
                                    }
                                }
                            });
                            result = "ERROR";
                        }
                    } else {
                        if (Common.DEBUG) {
                            System.out.println("JSON null");
                        }

                        result = "FAIL";
                    }
                }

                //Intenta enviar actas pendientes (de más vieja a más nueva), solamente luego de enviar el acta visible en el que se está trabajando
                sendPendingTickets(realm, false);
            }

            //Intenta enviar actas confirmadas (de más vieja a más nueva), tanto al levantar background como luego de enviar el acta visible en el que se está trabajando
            RealmResults<Infringement> infringements = realm.where(Infringement.class).equalTo(Common.KEY_STATUS, Infringement.STATUS_CONFIRMED).findAllSorted(Common.KEY_ID, Sort.ASCENDING);

            if (Common.DEBUG) {
                System.out.println("BACKGROUND WITH: " + infringements.size());
            }
            //mockChangeAllInfringementsRejectedToStatusConfirmed();
            if (infringements.size() > 0) {
                for (final Infringement infringement : infringements) {
                    if (infringement.getId() != reportId) {
                        ts = infringement.getCaptureTs();
                        pdf = infringement.getImagen2();
                        try {
                            Utils.generateVUOtp(context, new CallBackListener() {
                                @Override
                                public void invoke() {
                                    json = prepareInfringement(infringement, realm);
                                }
                            });
                        } catch (Exception e) {
                            Utils.logError(context, "SendFormTask:doInBackground:generateVUOtp - ", e);
                        }

                        if (json != null) {
                            if (NetworkStateReceiver.isConnected(context)) {
                                result = Api.request(Api.INFRINGEMENT_REPORT, Api.METHOD_POST, json.toString(), preferences.getString(Common.PREF_TOKEN, ""), context, Common.AUTH_BASIC).trim();
                            } else {
                                result = "";
                            }

                            if (result.startsWith("{") && result.endsWith("}")) {
                                try {
                                    //Nuevo tratamiento de errores vía api
                                    JSONObject jsonObject = new JSONObject(result);

                                    if (jsonObject.has("message")) {
                                        if (!jsonObject.isNull("message")) {
                                            if (Utils.isEmpty(jsonObject.getString("message").trim())) {
                                                //Ok guardamos y seguimos
                                                realm.executeTransaction(new Realm.Transaction() {
                                                    @Override
                                                    public void execute(final Realm realm) {
                                                        try {
                                                            Infringement infringement1 = realm.where(Infringement.class).equalTo(Common.KEY_ID, infringement.getId()).findFirst();
                                                            infringement1.setStatus(Infringement.STATUS_SENDED);
                                                            //TODO BORRAR ZIP FILE CORRESPONDIENTE A LA INFRACCION (HAY QUE VER COMO ENCONTRAR LA RUTA SI CON EL CAPTURETS ALCANZA)
                                                            if (infringement1.getTicketNumber() != 0) {
                                                                TicketB ticketB = realm.where(TicketB.class).equalTo(Common.KEY_ID, infringement1.getTicketNumber()).findFirst();

                                                                if (ticketB == null) {
                                                                    ticketB = new TicketB();
                                                                    ticketB.setId((int) infringement1.getTicketNumber());
                                                                }

                                                                ticketB.setUsed(Common.BOOL_YES);
                                                                realm.copyToRealmOrUpdate(ticketB);
                                                            }

                                                            realm.copyToRealmOrUpdate(infringement1);
                                                            pdf = "";
                                                            ts = 0;
                                                        } catch (Exception e) {
                                                            Utils.logError(context, "SendFormTask:doInBackground:execute - Exception:", e);
                                                        }
                                                    }
                                                });
                                            } else {
                                                //Hay mensaje de error del backend, frenar
                                                final String message = jsonObject.getString("message").trim();
                                                String error = message.toLowerCase();
                                                error = error.replace("ó", "o").replace("á", "a");

                                                if (!error.contains("cupon vu invalido")) {
                                                    realm.executeTransaction(new Realm.Transaction() {
                                                        @Override
                                                        public void execute(final Realm realm) {
                                                            try {
                                                                Infringement infringement1 = realm.where(Infringement.class).equalTo(Common.KEY_ID, infringement.getId()).findFirst();
                                                                infringement1.setStatus(Infringement.STATUS_REJECTED);
                                                                infringement1.setMatch(message);
                                                                //infringement1.setImagen2(pdf);
                                                                realm.copyToRealmOrUpdate(infringement1);
                                                            } catch (Exception e) {
                                                                Utils.logError(context, "SendFormTask:doInBackground:execute - Exception:", e);
                                                            }
                                                        }
                                                    });
                                                }
                                                pdf = "";
                                                ts = 0;
                                            }
                                        }

                                        //Por si el backend sigue respondiendo mal las dejamos colgadas para seguir reintentando
                                    }

                                    //Por si el backend sigue respondiendo mal las dejamos colgadas para seguir reintentando
                                } catch (Exception e) {
                                    Utils.logError(context, "SendFormTask:doInBackground - Exception:", e);
                                }
                            }

                            //Si no tiene internet vuelve a reintentar después
                        }
                    }
                    pdf = "";
                    ts = 0;
                }

                //No mostrar mensajes porque estamos en background
                result = "";
            }

            //Revisamos si hay pendientes rechazadas que no pudieron enviarse bien
            sendPendingTickets(realm, true);
            realm.close();
        } catch (Exception e) {
            Utils.logError(context, "SendFormTask:doInBackground - Exception: ", e);
        }

        return result;
    }

    private long getInfrigmentIDToFindInRealm(Infringement infringement, long reportId) {
        long infrigmentIDToReturn = 0;
        if(infringement.getId() != 0){
            infrigmentIDToReturn = infringement.getId();
        }else{
            infrigmentIDToReturn = reportId;
        }
        return infrigmentIDToReturn;
    }

    private void deleteZipPdfFromDevice(String zipPdfAddress) {
        File zipPdfFile = new File(zipPdfAddress);
        if(zipPdfFile.exists()){
            zipPdfFile.delete();
        }
    }

    private void mockChangeAllInfringementsRejectedToStatusConfirmed() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Infringement> infringements = realm.where(Infringement.class).equalTo(Common.KEY_STATUS, Infringement.STATUS_REJECTED).findAllSorted(Common.KEY_ID, Sort.ASCENDING);
        if (infringements.size() > 0) {
            for (final Infringement infringement : infringements) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(final Realm realm) {
                        try {
                            Infringement infringement1 = realm.where(Infringement.class).equalTo(Common.KEY_ID, infringement.getId()).findFirst();
                            infringement1.setStatus(Infringement.STATUS_CONFIRMED);
                            //infringement1.setImagen2(pdf);
                            realm.copyToRealmOrUpdate(infringement1);
                        } catch (Exception e) {
                            Utils.logError(context, "SendFormTask:doInBackground:execute - Exception:", e);
                        }
                    }
                });
            }
        }
    }

    @Override
    protected void onPostExecute(final String s) {
        try {
            if (progress != null) {
                if (progress.isShowing()) {
                    progress.cancel();
                }
            }

            //Prevenir mostrar carteles en background
            if (reportId == 0) {
                showPopup = false;
            }

            switch (s) {
                case "FAIL":
                    if (reportId != 0) {
                        Toast.makeText(context, "El formulario no se ha podido confirmar", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case "ERROR":
                    if (activity != null && showPopup) {
                        Utils.showTicketDialog(activity, "Nro. de acta: I" + ticketNumber, "El formulario se enviará cuando haya conexión", listener);
                    } else {
                        if (reportId != 0) {
                            Toast.makeText(context, "El formulario se enviará cuando haya conexión", Toast.LENGTH_SHORT).show();
                        }

                        if (listener != null) {
                            listener.invoke();
                        }
                    }
                    break;

                default:
                    if (!Utils.isEmpty(s.trim()) && !s.startsWith("{") && !s.endsWith("}") && !s.equals("OK")) {
                        if (reportId != 0) {
                            Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (activity != null && showPopup && ticketNumber > 0) {
                            Utils.showTicketDialog(activity, "Nro. de acta: I" + ticketNumber, "El formulario se ha enviado con éxito", listener);
                        } else {
                            if (reportId != 0) {
                                Toast.makeText(context, "El formulario se ha enviado con éxito", Toast.LENGTH_SHORT).show();
                            }

                            if (listener != null) {
                                listener.invoke();
                            }
                        }
                    }
                    break;
            }
        } catch (Exception e) {
            Utils.logError(context, "SendFormTask:onPostExecute - Exception: ", e);
        }
        SendFormTaskLocker.getInstance().unlockTask();
    }

    private JSONObject prepareInfringement(Infringement infringement, Realm realm) {
        try {
            JSONObject json = new JSONObject();
            json.put("imei", Utils.getImei(context));

            if (infringement.getUser() != null) {
                if (!Utils.isEmpty(infringement.getUser().getDni())) {
                    json.put("agente_documento", infringement.getUser().getDni());
                } else {
                    if (!Utils.isEmpty(infringement.getUser().getUsername())) {
                        json.put("agente_documento", infringement.getUser().getUsername());
                    } else {
                        if (!Utils.isEmpty(preferences.getString(Common.PREF_CURRENT_USER, ""))) {
                            json.put("agente_documento", preferences.getString(Common.PREF_CURRENT_USER, ""));
                        } else {
                            json.put("agente_documento", "36521541");
                        }
                    }
                }
            } else {
                if (!Utils.isEmpty(preferences.getString(Common.PREF_CURRENT_USER, ""))) {
                    json.put("agente_documento", preferences.getString(Common.PREF_CURRENT_USER, ""));
                } else {
                    json.put("agente_documento", "36521541");
                }
            }

            if (!Utils.isEmpty(infringement.getDocumentAgent())) {
                if (infringement.getDocumentAgent().equals("1")) {
                    json.put("solicitud", 1);
                } else {
                    json.put("solicitud", 0);
                }
            } else {
                json.put("solicitud", 0);
            }

            json.put("altura", infringement.getHeight());
            json.put("apellido_imputado", infringement.getLastNameImputed());
            json.put("calle", infringement.getStreet());
            json.put("referencia_lugar", infringement.getReferencePlace());
            json.put("colectivo_interno", infringement.getInternal());

            if (infringement.getNewLine() != null) {
                int line = 0;
                try {
                    line = Integer.parseInt(infringement.getNewLine().getLinea());
                    json.put("colectivo", line);
                } catch (Exception e) {
                    json.put("colectivo", infringement.getNewLine().getLinea());
                }
            } else {
                json.put("colectivo", 0);
            }

            if (infringement.getCompany() != null && infringement.getTypeVehicle() != null) {
                if (infringement.getCompany() != null && infringement.getTypeVehicle().getDescripcion().toLowerCase().equals("colectivo") ||
                        infringement.getTypeVehicle().getDescripcion().toLowerCase().equals("combis")) {
                    json.put("colectivo_empresa", infringement.getCompany().getEmpresa());
                } else {
                    json.put("colectivo_empresa", "");
                }
            } else {
                if (!Utils.isEmpty(infringement.getAgentDocument()) && !Utils.isEmpty(infringement.getBetween1Imputed())) {
                    if (infringement.getBetween1Imputed().toLowerCase().equals("colectivo") || infringement.getBetween1Imputed().toLowerCase().equals("combis")) {
                        json.put("colectivo_empresa", infringement.getAgentDocument());
                    } else {
                        json.put("colectivo_empresa", "");
                    }
                } else {
                    json.put("colectivo_empresa", "");
                }
            }

            //Selección múltiple de tipos de infracciones
            if (!Utils.isEmpty(infringement.getTypesInfringements())) {
                String[] parts = infringement.getTypesInfringements().split(";");
                String types = "";

                for (String part : parts) {
                    String[] code = part.split(" ");
                    types += code[0] + ",";
                }

                json.put("id_codigo_infraccion", types.substring(0, types.length() - 1));
            }

            json.put("color", infringement.getColor().getValor());
            json.put("documentacion_retenida", "");
            json.put("dominio", infringement.getDomain());

            if (!Utils.isEmpty(infringement.getDomain())) {
                if (!Utils.isEmpty(infringement.getCopyDomain())) {
                    if (!infringement.getCopyDomain().equals("D") && !infringement.getCopyDomain().equals("T") &&
                            !infringement.getCopyDomain().equals("C")) {
                        json.put("copiadominio", "O");
                    } else {
                        json.put("copiadominio", infringement.getCopyDomain());
                    }
                } else {
                    json.put("copiadominio", "O");
                }
            }

            json.put("tipoPatente", infringement.getTypeDomain().replace("á", "a"));
            json.put("formaInfraccion", infringement.getFormInfringement());
            json.put("imputado_altura", infringement.getHeightImputed());
            json.put("imputado_calle", infringement.getStreetImputed());
            json.put("imputado_codpos", "");
            json.put("conducta", infringement.getZipCodeImputed());
            json.put("imputado_documento", infringement.getDocumentImputed());
            json.put("imputadoMail", infringement.getExplanationCategory());
            json.put("fechaDecaptura", Long.valueOf(infringement.getCaptureTs()));
            json.put("fechaCaptura", Long.valueOf(infringement.getCaptureTs()));
            json.put("fecha_captura", Long.valueOf(infringement.getCaptureTs()));

            if (infringement.getDependence() != null) {
                json.put("dependencia", infringement.getDependence());
            } else {
                json.put("dependencia", "");
            }

            if (infringement.getIssuedBy() != null) {
                json.put("imputado_licencia_municipio", infringement.getIssuedBy());
            } else {
                json.put("imputado_licencia_municipio", "");
            }

            json.put("imputado_dptooficina", infringement.getOffice());
            json.put("imputado_firma", "");
            json.put("imputado_licencia", infringement.getLicense());

            if (infringement.getLicenseCategory() != null && Utils.isEmpty(infringement.getCategories())) {
                json.put("imputado_licencia_categoria", infringement.getLicenseCategory().getDescripcion());
            } else {
                if (!Utils.isEmpty(infringement.getCategories())) {
                    json.put("imputado_licencia_categoria", infringement.getCategories());
                } else {
                    json.put("imputado_licencia_categoria", "");
                }
            }

            if (!Utils.isEmpty(infringement.getLicenseExpiration())) {
                json.put("imputadoLicenciaVencimiento", Utils.string2Ts(infringement.getLicenseExpiration(), context, "dd/MM/yyyy"));
            } else {
                json.put("imputadoLicenciaVencimiento", "");
            }

            if (infringement.getLocalityImputed() != null) {
                json.put("imputado_localidad", infringement.getLocalityImputed().getDescripcion());
            } else {
                json.put("imputado_localidad", "");
            }

            if (!infringement.getTypeDomain().trim().toLowerCase().equals("sin patente")) {
                if (infringement.getCountry() != null) {
                    json.put("idPais", infringement.getCountry().getCodPais());
                } else {
                    json.put("idPais", "AR");
                }
            }

            if (infringement.getCountry() != null) {
                json.put("id_imputado_pais", infringement.getCountry().getCodPais());
            } else {
                json.put("id_imputado_pais", "AR");
            }

            json.put("entrecalle1", infringement.getBetween1());
            json.put("entrecalle2", infringement.getBetween2());
            json.put("imputado_entrecalle1", infringement.getBetween1());
            json.put("imputado_entrecalle2", infringement.getBetween2());
            json.put("imputado_partido", "");
            json.put("imputado_piso", infringement.getFloor());
            json.put("imputado_provincia", infringement.getApartmentImputed());

            if (infringement.getTypeDocumentImputed() != null) {
                json.put("id_imputado_tipo_documento", infringement.getTypeDocumentImputed().getTipoDoc());
                json.put("id_titular_tipo_documento", infringement.getTypeDocumentImputed().getTipoDoc());
            } else {
                if (!Utils.isEmpty(infringement.getBetween2Imputed())) {
                    TypeDocument typeDocument = realm.where(TypeDocument.class).equalTo(TypeDocument.KEY_DOCUMENTO, infringement.getBetween2Imputed()).findFirst();

                    if (typeDocument != null) {
                        json.put("id_imputado_tipo_documento", typeDocument.getTipoDoc());
                        json.put("id_titular_tipo_documento", typeDocument.getTipoDoc());
                    }
                }
            }

            json.put("latitud_ubicacion", Double.parseDouble(infringement.getLatitude()));
            json.put("longitud_ubicacion", Double.parseDouble(infringement.getLongitude()));

            if (infringement.getBrandId() != 0 && !Utils.isEmpty(infringement.getBrandStr())) {
                json.put("codMarca", infringement.getBrandId());
                json.put("descripcionMarca", infringement.getBrandStr());
            } else {
                json.put("codMarca", 120);
                json.put("descripcionMarca", "Otros");
            }

            json.put("modelo", infringement.getModel());
            json.put("nombreimputado", infringement.getNameImputed());

            long ticketB = infringement.getTicketNumber();

            //Recargar número de ticket por si no lo tenía
            if (ticketB == 0) {
                RealmResults<TicketB> ticketBs = realm.where(TicketB.class).equalTo(TicketB.KEY_USED, Common.BOOL_NO).findAllSorted(Common.KEY_ID);

                if (ticketBs.size() > 0) {
                    ticketB = ticketBs.get(0).getId();

                    if (ticketB == 0 && ticketBs.size() > 1) {
                        ticketB = ticketBs.get(1).getId();
                    }

                    //Evitar que un acta pueda tener un número ya usado
                    if (realm.where(Infringement.class).equalTo(Infringement.KEY_TICKETNUMBER, ticketB).count() > 0) {
                        for (TicketB tb : ticketBs) {
                            if (tb.getUsed() == Common.BOOL_NO && realm.where(Infringement.class).equalTo(Infringement.KEY_TICKETNUMBER, tb.getId()).count() == 0) {
                                ticketB = tb.getId();
                                break;
                            }
                        }
                    }
                }
            }

            json.put("numero_acta", ticketB);
            json.put("numero_acta_z", infringement.getTicketZNumber());
            json.put("actaContravencional", infringement.getNumberTicketContravention());

            if (infringement.getDocumentationHeld().equals("S")) {
                json.put("retencion_licencia", true);
            } else {
                json.put("retencion_licencia", false);
            }

            if (infringement.getVehicleRemission().equals("S")) {
                json.put("retencion_cehiculo", true);
            } else {
                json.put("retencion_cehiculo", false);
            }

            json.put("sexo", infringement.getGenderImputed());

            if (infringement.getTypeVehicle() != null) {
                json.put("idTipovehiculo", infringement.getTypeVehicle().getIdTipoVehiculo());

                if (infringement.getTypeVehicle().getOther() == Common.BOOL_YES) {
                    json.put("tipo_vehiculo_otro_id", infringement.getTypeVehicle().getDescripcion());
                }
            } else {
                json.put("idTipovehiculo", 10);
            }

            json.put("titulardocumento", infringement.getDocumentOwner());
            json.put("vehiculo_remision", infringement.getVehicleRemission());
            json.put("observaciones", infringement.getObservations());
            json.put("pass", Api.encryptBySaulo(context, preferences.getString(Common.PREF_CURRENT_PASSKEY, Common.DEFAULT_PASS)));
            //Datos nuevos de VU
            json.put("vuorden", preferences.getLong("originCounter", 1) + "");
            json.put("vuordenLocal", preferences.getLong(User.KEY_OTPCONSUMED, 1) + "");
            //Enviar el dato loco de VU
            json.put(User.KEY_TOTP, preferences.getString(User.KEY_TOTP, ""));
            json.put("imagenFirma", infringement.getSignatureImputed());
            String version = "";

            try {
                version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            } catch (Exception e) {
            }

            if (Utils.isEmpty(version)) {
                version = preferences.getString(Common.PREF_APPVERSION, "");
            }

            json.put("version", version);
            json.put("AppVersion", version);

            if (Common.DEBUG) {
                System.out.println("INFRACCION: " + json);
                Utils.writeStringInFile(json.toString(), "", context);
            }

            //Para enviar en background y adjuntar el pdf
            if (!Utils.isEmpty(infringement.getImagen2())) {
                pdf = infringement.getImagen2();
            }

            //Verificar que se envíe zip con pdf
            if (Utils.isEmpty(pdf)) {
                if (ts == 0) {
                    ts = infringement.getCaptureTs();
                }

                pdf = Utils.getBase64FromFile(Utils.reGenerateZip(context, ts), context);
            }

            if (pdf.contains("/storage") || pdf.contains("/emulated") || pdf.contains(context.getString(R.string.app_name)) || pdf.contains(".zip")) {
                pdf = Utils.getBase64FromFile(pdf, context);
            }

            if(Utils.isEmpty(pdf)){
                return  null;
            }else{
                json.put("pdf", pdf);
            }

            if (Common.DEBUG) {
                Utils.writeStringInFile(json.toString(), "", context);
            }

            return json;
        } catch (Exception e) {
            Utils.logError(context, "SendFormTask:prepareInfringement - Exception: ", e);
        }

        return null;
    }

    private void sendPendingTickets(final Realm realm, boolean isBackground) {
        String result = "";
        RealmResults<Infringement> infringements = realm.where(Infringement.class).equalTo(Common.KEY_STATUS, Infringement.STATUS_PENDING).findAllSorted(Common.KEY_ID, Sort.ASCENDING);

        if (Common.DEBUG) {
            System.out.println("Pendientes a enviar: " + infringements.size());
        }

        if (infringements.size() > 0) {
            for (final Infringement pending : infringements) {
                if (pending.getId() != reportId) {
                    if ((isBackground && !Utils.isEmpty(pending.getMatch()) || (!isBackground && Utils.isEmpty(pending.getMatch())))) {
                        pdf = pending.getImagen2();
                        ts = pending.getCaptureTs();
                        try {
                            Utils.generateVUOtp(context, new CallBackListener() {
                                @Override
                                public void invoke() {
                                    json = prepareInfringement(pending, realm);
                                }
                            });

                            if (json != null) {
                                if (NetworkStateReceiver.isConnected(context)) {
                                    result = Api.request(Api.INFRINGEMENT_REPORT, Api.METHOD_POST, json.toString(), preferences.getString(Common.PREF_TOKEN, ""), context, Common.AUTH_BASIC)
                                            .trim();
                                } else {
                                    result = "";
                                }

                                if (result.startsWith("{") && result.endsWith("}")) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(result);

                                        if (jsonObject.has("message")) {
                                            if (!jsonObject.isNull("message")) {
                                                if (Utils.isEmpty(jsonObject.getString("message").trim())) {
                                                    //Ok guardamos y seguimos
                                                    realm.executeTransaction(new Realm.Transaction() {
                                                        @Override
                                                        public void execute(final Realm realm) {
                                                            try {
                                                                Infringement infringement1 = realm.where(Infringement.class).equalTo(Common.KEY_ID, pending.getId()).findFirst();
                                                                infringement1.setStatus(Infringement.STATUS_SENDED);
                                                                //TODO BORRAR ZIP FILE CORRESPONDIENTE A LA INFRACCION (HAY QUE VER COMO ENCONTRAR LA RUTA SI CON EL CAPTURETS ALCANZA)

                                                                if (infringement1.getTicketNumber() != 0) {
                                                                    TicketB ticketB = realm.where(TicketB.class).equalTo(Common.KEY_ID, infringement1.getTicketNumber()).findFirst();

                                                                    if (ticketB == null) {
                                                                        ticketB = new TicketB();
                                                                        ticketB.setId((int) infringement1.getTicketNumber());
                                                                    }

                                                                    ticketB.setUsed(Common.BOOL_YES);
                                                                    realm.copyToRealmOrUpdate(ticketB);
                                                                }

                                                                realm.copyToRealmOrUpdate(infringement1);
                                                                pdf = "";
                                                                ts = 0;
                                                            } catch (Exception e) {
                                                                Utils.logError(context, "SendFormTask:doInBackground:execute - Exception:", e);
                                                            }
                                                        }
                                                    });
                                                } else {
                                                    //Hay mensaje de error del backend, frenar
                                                    final String message = jsonObject.getString("message").trim();
                                                    String error = message.toLowerCase();
                                                    error = error.replace("ó", "o").replace("á", "a");

                                                    if (!error.contains("cupon vu invalido")) {
                                                        realm.executeTransaction(new Realm.Transaction() {
                                                            @Override
                                                            public void execute(final Realm realm) {
                                                                try {
                                                                    Infringement infringement1 = realm.where(Infringement.class).equalTo(Common.KEY_ID, pending.getId()).findFirst();
                                                                    infringement1.setStatus(Infringement.STATUS_REJECTED);
                                                                    infringement1.setMatch(message);
                                                                    //infringement1.setImagen2(pdf);
                                                                    realm.copyToRealmOrUpdate(infringement1);
                                                                } catch (Exception e) {
                                                                    Utils.logError(context, "SendFormTask:doInBackground:execute - Exception:", e);
                                                                }
                                                            }
                                                        });
                                                    } else {
                                                        //Confirmamos para reintento por vu
                                                        realm.executeTransaction(new Realm.Transaction() {
                                                            @Override
                                                            public void execute(final Realm realm) {
                                                                try {
                                                                    Infringement infringement1 = realm.where(Infringement.class).equalTo(Common.KEY_ID, pending.getId()).findFirst();
                                                                    infringement1.setStatus(Infringement.STATUS_CONFIRMED);
                                                                    //infringement1.setImagen2(pdf);
                                                                    realm.copyToRealmOrUpdate(infringement1);
                                                                } catch (Exception e) {
                                                                    Utils.logError(context, "SendFormTask:doInBackground:execute - Exception:", e);
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                            } else {
                                                //Por si el backend sigue respondiendo mal las dejamos colgadas para seguir reintentando
                                                realm.executeTransaction(new Realm.Transaction() {
                                                    @Override
                                                    public void execute(final Realm realm) {
                                                        try {
                                                            Infringement infringement1 = realm.where(Infringement.class).equalTo(Common.KEY_ID, pending.getId()).findFirst();
                                                            infringement1.setStatus(Infringement.STATUS_CONFIRMED);
                                                            //infringement1.setImagen2(pdf);
                                                            realm.copyToRealmOrUpdate(infringement1);
                                                        } catch (Exception e) {
                                                            Utils.logError(context, "SendFormTask:doInBackground:execute - Exception:", e);
                                                        }
                                                    }
                                                });
                                            }
                                        } else {
                                            //Por si el backend sigue respondiendo mal las dejamos colgadas para seguir reintentando
                                            realm.executeTransaction(new Realm.Transaction() {
                                                @Override
                                                public void execute(final Realm realm) {
                                                    try {
                                                        Infringement infringement1 = realm.where(Infringement.class).equalTo(Common.KEY_ID, pending.getId()).findFirst();
                                                        infringement1.setStatus(Infringement.STATUS_CONFIRMED);
                                                        //infringement1.setImagen2(pdf);
                                                        realm.copyToRealmOrUpdate(infringement1);
                                                    } catch (Exception e) {
                                                        Utils.logError(context, "SendFormTask:doInBackground:execute - Exception:", e);
                                                    }
                                                }
                                            });
                                        }
                                    } catch (Exception e) {
                                        Utils.logError(context, "SendFormTask:doInBackground - Exception:", e);
                                    }
                                } else {
                                    //Probablemente hubo problema de red, guardamos
                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(final Realm realm) {
                                            try {
                                                Infringement infringement1 = realm.where(Infringement.class).equalTo(Common.KEY_ID, pending.getId()).findFirst();
                                                infringement1.setStatus(Infringement.STATUS_CONFIRMED);
                                                //infringement1.setImagen2(pdf);
                                                realm.copyToRealmOrUpdate(infringement1);
                                            } catch (Exception e) {
                                                Utils.logError(context, "SendFormTask:doInBackground:execute - Exception:", e);
                                            }
                                        }
                                    });
                                }
                            }
                        } catch (Exception e) {
                            Utils.logError(context, "SendFormTask:doInBackground:generateVUOtp - ", e);
                        }
                    }
                }
                pdf = "";
                ts = 0;
            }
        }
    }
}